% 计算sPmP-PmP
clear all
model='model_iasp91.asc'
ele=0  % 台站高程０km
dep=10   % 震源深度10km

for i=1:141
dist(i)=i-1 ;%震中距　km
[spmp,pmp]=spmp_pmp(model,dist(i),dep,ele);
t_pmp(i)=pmp;
t_spmp(i)=spmp;
end
figure(3)
plot(dist,t_pmp,dist,t_spmp,dist,t_spmp-t_pmp)
legend('PmP','sPmP','sPmP-PmP')
title({'sPmP和PmP走时曲线，速度模型=',model,'震源深度=',dep,'km'})
xlabel('震中距/km')
ylabel('走时/s')
t_pmp
t_spmp