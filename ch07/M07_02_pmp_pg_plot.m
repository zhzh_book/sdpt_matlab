%PmP-Pg列线图
clear all
% 速度模型
H=40
hstep=5
H2=H*2;
vp=6.2
%vpg=6.01
%vpmp=6.5
hstep=5 %深度步长km
dmax=200 % 震中距范围
dmin=0
dstep=1
dnum=floor((dmax-dmin)*dstep)
hnum=floor(H/hstep) 
for i=1:hnum
    dep(i)=hstep*(i-1);
    for j=1:dnum
        d(j)=dmin+dstep*(j-1);
        del2=d(j)^2;
        pmp_pg(i,j)=(sqrt(del2+(H2-dep(i))^2)-sqrt(del2+dep(i)^2))/vp;
        %pmp_pg(i,j)=(sqrt(del2+(H2-dep(i))^2))/vpmp-(sqrt(del2+dep(i)^2))/vpg;
    end
   
    plot(d,pmp_pg(i,:));hold on
    pmp_pg(i,60)
end
 title('PmP-Pu到时差随震源深度变化列线图')
 xlabel('震中距/km')
 ylabel('PmP-Pu到时差/s')
 