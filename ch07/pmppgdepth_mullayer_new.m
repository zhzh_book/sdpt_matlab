function [h,rms]= pmppgdepth_mullayer_new( num,del,pg,pmp,model,num_dep,step)
% 用PmP-Pg定震源深度 2015-03-25
flag=1  % P波震相
res2_sum_min=10000;
%for i=1:num_dep
    for i=1:num_dep
    res2_sum(i)=0;
    dep_test(i)=step*i
    xa=0;
    ya=0;
    yb=0;
    zb0=0;
    for j=1:num
    pmp_pg=pmp(j)-pg(j);
    del2=del(j)^2;
    xbb=del(j);
   [travel_t,etaPb,etaPn,num_c,num_m]=travel_time_ps_new(model,xa,ya,dep_test(i),xbb,yb,zb0,flag); 
   travel_t;
   td(j)=travel_t(1)
   %trpb(j)=travel_t(2);
   %trpn(j)=travel_t(3);
    [spmp,pmp1]=spmp_pmp(model,xbb,dep_test(i),0);
   
   %trpmp(j)=travel_t(4);
    trpmp(j)=pmp1
    pmp_pg_cal=trpmp(j)-td(j);
    res2=(pmp_pg-pmp_pg_cal)^2;
    res2_sum(i)=res2_sum(i)+res2;
    end
    res2_sum(i)=res2_sum(i)/num
    if res2_sum(i)<res2_sum_min
        res2_sum_min=res2_sum(i);
        rms=sqrt(res2_sum_min);
        dep_index=i;
        h=dep_test(i);
    end
end
plot(dep_test,sqrt(res2_sum),'.');

xlabel('震源深度/km')
ylabel('PmP-Pu到时差残差/s')
title('用PmP-Pu到时差搜索震源深度')
end

