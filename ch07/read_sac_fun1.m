function [ sacwave ] = read_sac_fun1( fil,dist,sampling_rate,i_fig,sacwave_len,i_start )
%��SAC�ļ�
%   Detailed explanation goes here
sacdata = importdata(fil, ' ', 30);
m=length(sacdata.data(:,1))
sac(1,:)=sacdata.data(:,1);
sac(2,:)=sacdata.data(:,2);
sac(3,:)=sacdata.data(:,3);
sac(4,:)=sacdata.data(:,4);
sac(5,:)=sacdata.data(:,5);
k=i_start/5;
for i=1:sacwave_len/5
    j=(i-1)*5+1;
    sacwave(j)=sac(1,i+k);
     sacwave(j+1)=sac(2,i+k);
     sacwave(j+2)=sac(3,i+k);
     sacwave(j+3)=sac(4,i+k);
     sacwave(j+4)=sac(5,i+k);
end

%sacwave_len=length(sacwave)
for j=1:sacwave_len
    time(j)=j/sampling_rate;
end
sacwave=sacwave-mean(sacwave);
figure(i_fig)
plot(time,sacwave)
title({fil,'dist=',dist,' deg.  sampling-rate=',sampling_rate,'s/s '})

end

