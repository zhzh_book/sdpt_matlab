%利用PmP-Pg定震源深度试验 2015-03-28
% 多层速度模型
clear all
za0=0;
zb0=0;


za=za0-zb0;%自台站深度算起的震源深度
model='model_iasp91.asc'
m=0;
ascdata = importdata(model, ' ', 3);
m=length(ascdata.data(:,1));
layer_num=ascdata.data(:,1);
layer_top=ascdata.data(:,2);
vp=ascdata.data(:,3);
vs=ascdata.data(:,4);
layer_mark=ascdata.data(:,5);

for i=1:m-1
    h(i)=layer_top(i+1)-layer_top(i);  %层厚度
end
h0=h(1);
h(1)=h0-zb0;%以台站深度（高程的负值）作为第一层的厚度
nlayer=m;
% 给定产生绕射波的层顶
num_j=1;
for i=1:nlayer
    if layer_mark(i)==1
        num_c=i;
    else if layer_mark(i)==2
            num_m=i;
        end
    end
    if za>layer_top(i)
        num_j=i;
    end
end
num_c;
num_m;
num_j;
h;

h_moho=0;
for i=1:num_m-1
    h(i)=layer_top(i+1)-layer_top(i);  %地壳内各厚度
    h_moho=h_moho+h(i);
end
h_moho
dep_step=1
num_dep=floor(h_moho/dep_step)
% 试验数据，震源深度20 km，设有5个台站
num=5
del(1)=50;   % 震中距 km
del(2)=60;
del(3)=70;
del(4)=80;
del(5)=90;
del

% Pg到时 （设发震时刻为０，=走时）

pg(1)=9.0;
pg(2)=10.7;
pg(3)=12.3;
pg(4)=14.0;
pg(5)=15.7;
pg
% PmP到时 （设发震时刻为０，=走时）
pmp(1)=12.0;
pmp(2)=13.2;
pmp(3)=14.4;
pmp(4)=15.7;
pmp(5)=17.0;
pmp
% 定深度
[h,rms]=pmppgdepth_mullayer_new(num,del,pg,pmp,model,num_dep,dep_step)
% 结果h =15km，到时差残差rms =0.0058 s
