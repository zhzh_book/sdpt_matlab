%利用Pn-Pg和Sg-Pg定震源深度试验 2015-03-29
% 多层速度模型
clear all
za0=0;
zb0=0;  %台站深度（高程的负值）


za=za0-zb0;%自台站深度算起的震源深度
%model='model_1.asc'
model='model_iasp91.asc'
m=0;
ascdata = importdata(model, ' ', 3);
m=length(ascdata.data(:,1));
layer_num=ascdata.data(:,1);
layer_top=ascdata.data(:,2);
vp=ascdata.data(:,3);
vs=ascdata.data(:,4);
layer_mark=ascdata.data(:,5);

for i=1:m-1
    h(i)=layer_top(i+1)-layer_top(i);  %层厚度
end
h0=h(1);
h(1)=h0-zb0;%从台站位置起计算第一层的厚度
nlayer=m;
% 给定产生绕射波的层顶
num_j=1;
for i=1:nlayer
    if layer_mark(i)==1
        num_c=i;
    else if layer_mark(i)==2
            num_m=i;
        end
    end
    if za>layer_top(i)
        num_j=i;
    end
end
num_c;
num_m;
num_j;
h;

h_moho=0;
for i=1:num_m-1
    h(i)=layer_top(i+1)-layer_top(i);  %地壳内各厚度
    h_moho=h_moho+h(i);
end
h_moho
dep_step=1
num_dep=floor(h_moho/dep_step)
% 试验数据，震源深度18 km
% 计算理论走时，走时加0.5*rand(1),震中距加5*rand(1)的随机误差。

 model='model_iasp91.asc'  
    xa=0;
    ya=0;
    za0=15
    yb=0;
    num=5
    zb0=0;
step=20 % km 
   for k=1:5  
        dis_test(k)=step*k+200; 
   flag=1;  % P波震相 
   [travel_t,etaPb,etaPn,num_c,num_m]=travel_time_ps_new(model,xa,ya,za0,dis_test(k),yb,zb0,flag); 
   travel_t;
   td(k)=travel_t(1);
   %trpb(j)=travel_t(2);
   trpn(k)=travel_t(3);
   %trpmp(j)=travel_t(4);
   flag=2;   % S震相
   [travel_t_s,etaPb,etaPn,num_c,num_m]=travel_time_ps_new(model,xa,ya,za0,dis_test(k),yb,zb0,flag); 
    tds(k)=travel_t_s(1);  % Sg走时
   end
   pg0=td
    pn0=trpn
    sg0=tds
    del0=dis_test
     for i=1:num 
   pg(i)=pg0(i)+0.1*randn(1);
   pn(i)=pn0(i)+0.1*randn(1);
   sg(i)=sg0(i)+0.1*randn(1);
   del(i)=del0(i)+5.0*randn(1);
   end
   pg
   pn
   sg
   del
[depth,dif_rms]= pnpgsgdepth_mullayer1( num,pn,pg,sg,del,model,num_dep,dep_step)
% 结果：模型model_1.asc
%h =18    18    19    18     深度4个台站的结果
%dis =210   250   270   290  震中距4个台站的结果
%depth =18.2500   4个台平均深度
%dep_std =0.5000  深度结果的标准偏差  （残差平方和除以（台站数-1），然后开方）
