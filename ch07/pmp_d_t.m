function [ d_pmp,t_pmp ] = pmp_d_t( j,v,h,thet,num_m,kexi_d)
%计算PmP走时和震中距
% 输入变量：j震源所在层号，v -P波各层波速，h-地壳各层厚度，
% thet-从震源下行射线到所在层界面的入射角，num_m-莫霍面下层序号（地壳层数=num_m-1）
% kexi_d-震源到所在层底的距离
j;
v;
h;
thet;
num_m;
kexi_d;
%   Detailed explanation goes here
thet_d(j)=thet;
if j==num_m-1
        thet_d(num_m-1)=thet_d(j);
    else 
    for i=j:num_m-2
        thet_d(i+1)=asin(sin(thet_d(i))*v(i+1)/v(i));
    end
    end
    thet_u(num_m-1)=thet_d(num_m-1);  % 莫霍面入射角=反射角
    for i=num_m-2:-1:1
        thet_u(i)=asin(sin(thet_u(i+1))*v(i)/v(i+1));
    end
    % 计算PmP
    %disp('下行')
    t_pmp=kexi_d/(cos(thet_d(j))*v(j));
    if j<num_m-1
        for i=j+1:1:num_m-1
            t_pmp=t_pmp+h(i)/(cos(thet_d(i))*v(i));
        end
    end
    %disp('上行');
    for i=num_m-1:-1:1
        t_pmp=t_pmp+h(i)/(cos(thet_u(i))*v(i));
    end
   %disp('上行完');
    %计算震中距
    %disp('下行')
     d_pmp=kexi_d*tan(thet_d(j));
    if j<num_m-1
        for i=j+1:1:num_m-1
            d_pmp=d_pmp+h(i)*tan(thet_d(i));
        end
    end
   % disp('上行');
    %for i=num_m-1:-1:1
        for i=1:num_m-1
        d_pmp=d_pmp+h(i)*tan(thet_u(i));
    end
%disp('上行完')
end

