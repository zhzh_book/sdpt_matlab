% 滑动时窗互相关系数叠加法-合成波形试验
clear all
%构成试验信号
num_sig=1000
sampling_rate=100
f0=sampling_rate/2
scale=30     %绘图道间距
s1=3
s2=5
s3=7
a1=7
a2=2
a3=5
sig=zeros(1,num_sig);
for i=s1*sampling_rate+1:s1*sampling_rate+100
    sig(i)=sig(i)+sin(2*pi*(i-s1*sampling_rate)/100)*a1;
end
for i=s2*sampling_rate+1:s2*sampling_rate+100
    sig(i)=sig(i)+sin(2*pi*(i-s2*sampling_rate)/100)*a2;
end
for i=s3*sampling_rate+1:s3*sampling_rate+100
    sig(i)=sig(i)+sin(2*pi*(i-s3*sampling_rate)/100)*a3;
end
sig_ori=sig;
num_sta=15  %台站数
num_sta=6
noise=zeros(num_sta,num_sig);

noise_amp=4
for i=1:num_sig
    for i_sta=1:num_sta
        noise(i_sta,i)=noise_amp*randn(1);
        sigi(i_sta,i)= noise(i_sta,i)+sig(i);
    end
    %noise(i)=2.0*randn(1);
    t(i)=i/sampling_rate;
end
size(sig)
mean_sig=mean(sig)
mean_noi=mean(noise)
std_sig=std(sig)
std_noi=std(noise)
figure(1)
subplot(3,1,1)
plot(t,sig_ori)
ylabel('sig-syn')
%axis([0 num_sig -10 10])
subplot(3,1,2)
plot(t,noise(1,1:num_sig))
ylabel('noise_1')
subplot(3,1,3)
plot(t,sigi(1,1:num_sig))
ylabel('sigi_1')
xlabel('时间/s')
figure(2)
sigsum=zeros(num_sig);
for i_sta=1:num_sta
    for j=1:num_sig
    sigsum(j)=sigsum(j)+sigi(i_sta,j);
    end
    plot(t,sigi(i_sta,:)+(i_sta-1)*scale);hold on
    
end
sigsum=sigsum/num_sta;    %按震相对齐的波形叠加，压低不相关噪声
plot(t,sigsum+num_sta*scale)
xlabel('时间/s')
%计算叠加滑动时窗互相关系数时间序列
c_local=zeros(num_sig);
c_ori=zeros(num_sig);
c_global=zeros(num_sig);
c_ori_max=0
num_win=sampling_rate*0.2  % 
for j=1:num_sig-num_win
for i=1:i_sta
    for m=i+1:i_sta
        cim=0;
        cii=0;
        cmm=0;
        
        for k=1:num_win
            cim=cim+sigi(i,j-1+k)*sigi(m,j-1+k);
            cii=cii+sigi(i,j-1+k)^2;
            cmm=cmm+sigi(m,j-1+k)^2;
        end
        c_local(j)=c_local(j)+cim/(sqrt(cii)*sqrt(cmm));
        c_ori(j)=c_ori(j)+cim;
    end
    
end
   if abs(c_ori(j))>c_ori_max
        c_ori_max=abs(c_ori(j));
        j_max=j;
    end
end
j_max0=j_max
c_global=c_ori/c_ori_max;  
c_local=c_local*2/(i_sta*(i_sta-1));
figure(3)
subplot(2,1,1)
plot(t,c_local)
axis([0 num_sig/sampling_rate -0.5 1.1])
ylabel('c-local')
subplot(2,1,2)
plot(t,c_global)
axis([0 num_sig/sampling_rate -0.5 1.1])
ylabel('c-global')
xlabel('时间/s')


