%计算分层地壳中的地震波走时sPmP和PmP
%2017-10-02修正
% 给定速度模型
%测试模型是基于中国平均2层地壳模型
% ( 中国地区地震走时表,国家地震局地球物理研究所编，《震相走时便查表》，地震出版社，1980)
%  设计的。模型文件的上面3行是注释行，有5列数据：
% 层序号，层顶深度，该层内P波速度，该层内S波速度，注释：1：康拉德面，2：莫霍面
% 为了检验走时计算程序，在上地壳和下地壳中都加了层，但层内速度没变。
% 一下是速度模型内容，去掉%号即为模型文件model_test.asc
%velocity model for testing travel_time_new.
%remark: 1:Corad discon.; 2: Moho discon.
%no   top-depth  P(km/s)  S(km/s) remark
%1     0        5.71           3.40    0
%2     8        5.71           3.40    0
%3     16       6.53           3.77    1
%4     30       6.53           3.77    0
%5     40       7.97           4.45    2
%6     100      7.97           4.45    0
function [spmp,pmp]=spmp_pmp(model,dis,dep,ele)
%返回值：
%spmp-sPmP走时
%pmp-PmP走时
%输入：
% model-多层均匀地壳速度模型，模型顶面为海平面
% dis-震中距
% dep-震源深度，从海平面算起
% ele-台站高程(km)
% small=0.1 km  % 搜索停止判据
small=0.1;
model;
% 读入速度模型
m=0;
ascdata = importdata(model, ' ', 3);
m=length(ascdata.data(:,1));
layer_num=ascdata.data(:,1);
layer_top=ascdata.data(:,2);
vp=ascdata.data(:,3);
vs=ascdata.data(:,4);
layer_mark=ascdata.data(:,5);
for i=1:m-1
    h(i)=layer_top(i+1)-layer_top(i);  %层厚度
end
nlayer=m;
h(1)=h(1)+ele;%第一层的厚度从台站位置算起，作为对台站高程的校正
dep=dep+ele;%自台站平面算起的震源深度
%求震源所在层号num_j
num_j=1;
for i=1:nlayer
    if layer_mark(i)==1
        num_c=i;
    else if layer_mark(i)==2
            num_m=i;
        end
    end
    if dep>layer_top(i)
        num_j=i;
    end
end
num_c;
num_m;
num_j;

kexi=dep;
if num_j>1
for i=1:num_j-1
kexi=kexi-h(i);
end
end
k=num_m;
j=num_j;   % 震源所在层号
kexi;  %震源到所在层顶的距离
kexi_d=h(j)-kexi; %震源到所在层底的距离
if num_j>=num_m %震源在莫霍面以下
    %disp('震源在莫霍面以下，无PmP和sPmP');
else
%计算PmP走时
%disp('计算PmP走时');
num_thet=90;
step_thet=pi/180;   % 第一轮thet_j步长1度
for i_thet=1:num_thet
    i_thet;
    thet_d(j)=step_thet*i_thet;
    %disp('粗')
    [ d_pmp,t_pmp ] = pmp_d_t( j,vp,h,thet_d(j),num_m,kexi_d);
    d_pmp_dis(i_thet)=d_pmp;
    % 比较
    if d_pmp>dis
        i_thet_l=i_thet;
        break
    end
end
% 细搜索
 thet_d_s=step_thet*(i_thet_l-1);
 thet_d_l=step_thet*(i_thet_l);

num_thet=100;
step1=(thet_d_l-thet_d_s)/num_thet;
for i_thet=1:num_thet
    thet_d(j)=step1*i_thet+ thet_d_s;
    %disp('细')
    [ d_pmp,t_pmp ] = pmp_d_t( j,vp,h,thet_d(j),num_m,kexi_d);
    d_pmp_dis(i_thet)=d_pmp;
    % 比较
    if d_pmp>dis
        i_thet_l=i_thet;
        break
    end
end
thet_d_m= thet_d_s+(step1*(i_thet_l)+step1*(i_thet_l-1))/2;
%disp('结果')
[ d_pmp,t_pmp ] = pmp_d_t( j,vp,h,thet_d_m,num_m,kexi_d);
dif_dis_pmp=d_pmp-dis;


end
%计算sPmP走时

num_thet=90;
step_thet=pi/180;   % 第一轮thet_j步长1度
for i_thet=1:num_thet


    i_thet;
    thet_d(j)=step_thet*i_thet;
     %thet_d(j)=step_thet*i_thet-1;   %2017-10-02
  %从震源到近源地面反射点的走时T1sPmP
  T1sPmP=0;
    EQ=0;
    if j>1 
    for i_u=1:j-1   % j是震源所在层号
        is(i_u)=asin(sin(thet_d(j))*vs(i_u)/vs(j));
     T1sPmP=T1sPmP+h(i_u)/(vs(i_u)*cos(is(i_u)));
     EQ=EQ+h(i_u)*tan(is(i_u));
    end
    %{
else
     is(1)=thet_d(1);
     T1sPmP=T1sPmP+h(1)/(vs(1)*cos(is(1)));
     EQ=EQ+h(1)*tan(is(1));
    end
   %}
    end
    is(1)=thet_d(1);
    T1sPmP=T1sPmP+kexi/(vs(j)*cos(thet_d(j)));
    EQ=EQ+kexi*tan(thet_d(j));
    QD=dis-EQ;
    %从地面反射点向下的PmP
    thet_0=asin(sin(is(1))*vp(1)/vs(1));
    
    [ d_pmp1,t_pmp1 ] = pmp_d_t( 1,vp,h,thet_0,num_m,h(1));
    d_spmp_dis(i_thet)=EQ+d_pmp1;
    % 比较
    if d_spmp_dis(i_thet)>dis
        i_thet_l=i_thet;
        break
    end
end
% 细搜索
 thet_d_s=step_thet*(i_thet_l-1);
 thet_d_l=step_thet*(i_thet_l);

num_thet=100;
step1=(thet_d_l-thet_d_s)/num_thet;
for i_thet=1:num_thet
    thet_d(j)=step1*i_thet+ thet_d_s;
    i_thet;
  %从震源到近源地面反射点的走时T1sPmP
  T1sPmP=0;
    EQ=0;
     if j>1 
    for i_u=1:j-1   % j是震源所在层号
        is(i_u)=asin(sin(thet_d(j))*vs(i_u)/vs(j));
     T1sPmP=T1sPmP+h(i_u)/(vs(i_u)*cos(is(i_u)));
     EQ=EQ+h(i_u)*tan(is(i_u));
    end
     end
    %{
else
     is(1)=thet_d(1);
     T1sPmP=T1sPmP+h(1)/(vs(1)*cos(is(1)));
     EQ=EQ+h(1)*tan(is(1));
    end
   %}
       is(1)=thet_d(1);
    T1sPmP=T1sPmP+kexi/(vs(j)*cos(thet_d(j)));
    EQ=EQ+kexi*tan(thet_d(j));
    QD=dis-EQ;
    %从地面反射点向下的PmP
    thet_0=asin(sin(is(1))*vp(1)/vs(1));
    
    [ d_pmp1,t_pmp1 ] = pmp_d_t( 1,vp,h,thet_0,num_m,h(1));
    d_spmp_dis(i_thet)=EQ+d_pmp1;
    % 比较
    if d_spmp_dis(i_thet)>dis
        i_thet_l=i_thet;
        break
    end
end
thet_d_m= thet_d_s+(step1*(i_thet_l)+step1*(i_thet_l-1))/2;
%{
 %从震源到近源地面反射点的走时T1sPmP
  T1sPmP=0;
    EQ=0;
    if j>1
        for i_u=1:j-1   % j是震源所在层号
        is(i_u)=asin(sin(thet_d_m)*vs(i_u)/vs(j));
     T1sPmP=T1sPmP+h(i_u)/(vs(i_u)*cos(is(i_u)));
     EQ=EQ+h(i_u)*tan(is(i_u));
        end
    else
     is(1)=thet_d_m;
     T1sPmP=T1sPmP+h(1)/(vs(1)*cos(is(1)));
     EQ=EQ+h(1)*tan(is(1));
    end
    T1sPmP=T1sPmP+kexi/(vs(j)*cos(thet_d_m));
    EQ=EQ+kexi*tan(thet_d_m);
    QD=dis-EQ;
    %从地面反射点向下的PmP
    thet_0=asin(sin(is(1))*vp(1)/vs(1));
    
    [ d_pmp1,t_pmp1 ] = pmp_d_t( 1,vp,h,thet_0,num_m,h(1));
 %}  
dif_dis_spmp=d_pmp1+EQ-dis;
t_spmp1=t_pmp1+T1sPmP;
T1sPmP
t_pmp1
%disp('计算sPmP走时');
%待补
dis;
spmp=t_spmp1;
dif_dis_spmp1=dif_dis_spmp;
pmp=t_pmp;
dif_dis_pmp1=dif_dis_pmp;
end


