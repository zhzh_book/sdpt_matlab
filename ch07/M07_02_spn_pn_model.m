%用sPn-Pn到时差求深度,洪星，2006，多层地壳sPn-Pn计算公式
%水平分层速度模型
clear all
model='model_iasp91.asc'
% 读入速度模型
m=0;
ascdata = importdata(model, ' ', 3);
m=length(ascdata.data(:,1));
layer_num=ascdata.data(:,1);
layer_top=ascdata.data(:,2);
vp=ascdata.data(:,3);
vs=ascdata.data(:,4);
layer_mark=ascdata.data(:,5);
mm=m
for i=1:m-1
  h(i)=layer_top(i+1)-layer_top(i);  %层厚度
   d(i)=layer_top(i);
   v(1,i)=vp(i);
   v(2,i)=vs(i);
end
d(m)=layer_top(m)
h0=d(m-1)
vpm=v(1,m-1)
nlayer=m-1;
ele=0
d(1)=d(1)+ele;%第一层的厚度从台站位置算起，作为对台站高程的校正



% 台站坐标
sta(1)=0
sta(2)=0
sta(3)=0
%震源坐标
epi(1)=200
epi(2)=0
epi(3)=25 %震源深度
% 构建震源深度随sPn-Pn到时差的变化
step=1  %深度步长
num_dep=floor(h0)/step
 for ii=1:nlayer
        k(ii)=sqrt(vpm^2-v(2,ii)^2)/(vpm*v(2,ii))+sqrt(vpm^2-v(1,ii)^2)/(vpm*v(1,ii))
    end
for i=1:num_dep
    dep(i)=i*step;
    for ii=2:nlayer+1
        if dep(i)<d(ii) & dep(i)>=d(ii-1)
        j=ii-1
        end
    end 
   const=0;
   for ii=1:j-1
       const=const+h(ii)*(k(ii)-k(j));
   end
    t_spn_pn(i)=dep(i)*k(j)+const;
end   
figure(1)
plot(t_spn_pn,dep)
title({'由sPn-Pn到时差估计震源深度曲线，速度模型=',model})
ylabel('深度/km')
xlabel('sPn-Pn/s')
figure(2)
plot(dep,t_spn_pn)
title({'sPn-Pn到时差随深度变化曲线，速度模型=',model})
xlabel('深度/km')
ylabel('sPn-Pn/s')