% 在震中距-震源深度坐标系中画给定ScS-P和S-P的震中距和震源深度关系图
scs_file='disScS.txt'

scsdata = importdata(scs_file, ' ', 2);
mscs=length(scsdata.data(:,1));
disScS=scsdata.data(:,1);
s_file='disS.txt'
sdata = importdata(s_file, ' ', 2);
ms=length(sdata.data(:,1));
disS=sdata.data(:,1);
dis_max=disScS(1)+1
dis_min=disScS(ms)-1

for i=1:ms
    dep(i)=i;
  end
figure(1)
plot(dep,disScS,'r.',dep,disS,'b.')
legend('ScS-P=681.3','S-P=220.1')
xlabel('震源深度/千米')
ylabel('震中距/度')
figure(2)
plot(disScS,dep,'r.',disS,dep,'b.')
legend('ScS-P=681.3','S-P=220.1')
ylabel('震源深度/千米')
xlabel('震中距/度')