function [h,rms]= pmppgdepth( num,del,pg,pmp,H,vp)
% 用PmP-Pg定震源深度 2015-03-25
step=1
idep=floor(H/step)
H2=2*H;
res2_sum_min=10000;
for i=1:idep
    res2_sum(i)=0;
    dep_test(i)=step*i;
    for j=1:num
    pmp_pg=pmp(j)-pg(j);
    del2=del(j)^2;
    pmp_pg_cal=(sqrt(del2+(H2-dep_test(i))^2)-sqrt(del2+dep_test(i)^2))/vp;
    res2=(pmp_pg-pmp_pg_cal)^2;
    res2_sum(i)=res2_sum(i)+res2;
    end
    res2_sum(i)=res2_sum(i)/num;
    if res2_sum(i)<res2_sum_min
        res2_sum_min=res2_sum(i);
        rms=sqrt(res2_sum_min);
        dep_index=i;
        h=dep_test(i);
    end
end
plot(dep_test,sqrt(res2_sum))
xlabel('震源深度/km')
ylabel('PmP-Pu到时差残差/s')
title('用PmP-Pu到时差搜索震源深度')
end

