%区域地震波走时计算
%2014-07-26
clear all
model='model_iasp91.asc'
%震源坐标
xa=0
ya=0
za0=6   % 给定震源深度
%台站坐标
xb=30   % 给定震中距
yb=0
zb0=0
flag=1 %计算P波走时
%flag=2  %计算S波走时
[travel_t,etaPb,etaPn,num_c,num_m]=travel_time_ps1(model,xa,ya,za0,xb,yb,zb0,flag); 
%画出震源在上地壳时给定深度的Pn、Pb、Pg走时曲线
xa=0
ya=0
za0=18
yb=0
zb0=0
%max_dis=300
max_dis=300
for n=1:1:max_dis
    xbb(n)=n;
   [travel_t,etaPb,etaPn,num_c,num_m]=travel_time_ps1(model,xa,ya,za0,xbb(n),yb,zb0,flag); 
   travel_t
   td(n)=travel_t(1);
   trpb(n)=travel_t(2);
   trpn(n)=travel_t(3);
   trpmp(n)=travel_t(4);
end
figure(1)
%plot(xbb,td,'r.',xbb,trpb,'b.',xbb,trpn,'black.');
plot(xbb,td,'r.',xbb,trpb,'b.',xbb,trpn,'black.',xbb,trpmp,'green.');
if flag==1
    legend('Pg','Pb','Pn','PmP')
    title({'Pg,Pb,Pn,PmP走时曲线，震源深度=',za0,' 速度模型',model})
else
    legend('Sg','Sb','Sn','SmS')
    title({'Sg,Sb,Sn,SmS走时曲线，震源深度=',za0,' 速度模型',model})
end
ylabel('走时/s')
xlabel('震中距/km') 


ascdata = importdata(model, ' ', 3);
size(ascdata.data)
m=length(ascdata.data(:,1))
layer_num=ascdata.data(:,1);
layer_top=ascdata.data(:,2);
vp=ascdata.data(:,3);
vs=ascdata.data(:,4);
layer_mark=ascdata.data(:,5);
str=sprintf('速度模型 %s',model);
disp(str)
str=sprintf('层序号 层顶深度（km) P（km/s)  S(km/s)  标记');
disp(str)
for i=1:m
str=sprintf('%d      %5.1f       %5.2f     %5.2f     %d',layer_num(i),layer_top(i),vp(i),vs(i),layer_mark(i));
disp(str)
end
str=sprintf('标记=1，康拉德；=2，莫霍');
if flag==1
    disp(str)
str=sprintf('Pg,Pb,Pn,PmP走时曲线，震源深度=%5.1f km',za0);
disp(str)
str = sprintf('上地壳Pb盲区距离：%5.1f km',etaPb);
disp(str)
str = sprintf('地壳内Pn盲区距离：%5.1f km',etaPn);
disp(str)
else
    disp(str)
str=sprintf('Sg,Sb,Sn,SmS走时曲线，震源深度=%5.1f km',za0);
disp(str)
str = sprintf('上地壳Sb盲区距离：%5.1f km',etaPb);
disp(str)
str = sprintf('地壳内Sn盲区距离：%5.1f km',etaPn);
disp(str)    
end
pbi=0
pni=0
pnbi=0
for i=1:1:max_dis
    if trpb(i)>0
        if trpb(i)<td(i)
            pbi=i;
            break;
        end
    end
end
for i=1:max_dis
    if trpn(i)>0
        if trpn(i)<td(i)
            pni=i;
            break;
        end
    end
end
for i=1:max_dis
    if trpn(i)>0
        if trpn(i)<trpb(i)
            pnbi=i;
            break;
        end
    end
end
if flag==1
str=sprintf('Pb对Pg的临界距离=%d km',pbi);
disp(str)
str=sprintf('Pn对Pg的临界距离=%d km',pni);
disp(str)
str=sprintf('Pn对Pb的临界距离=%d km',pnbi);
disp(str)
else
  str=sprintf('Sb对Sg的临界距离=%d km',pbi);
disp(str)
str=sprintf('Sn对Sg的临界距离=%d km',pni);
disp(str)
str=sprintf('Sn对Sb的临界距离=%d km',pnbi);
disp(str)
end


