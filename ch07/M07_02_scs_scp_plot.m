% 画ScS-P和S-P列线图、ScS-P和S-P列线图
clear all
model='iasp91'
tt=taupTime('iasp91',250,'P','deg',10)
 timeP1=tt(1).time
tt=taupTime('iasp91',5,'ScS','deg',20)
tt=taupTime('iasp91',180,'S','deg',10)
tt=taupTime('iasp91',120,'ScS','deg',5)
dep_step=60 % 深度步长60km
dis_step=1  % 震中距步长1度
dis_start=20  % 震中距20度开始
dep_num=11; 
%dep_num=1;
dis_num=61;  %P限于震中距小于80度
%dis_num=1; 
flagScS=0;
flagS=0;
% ScS-P,S-P,1-80 deg.
figure(1)
    for i=1:dep_num
    i
    dep(i)=(i-1)*dep_step;
        for j=1:dis_num
            j;
        dis(i,j)=(j-1)*dis_step+dis_start;
        tt=taupTime(model,dep(i),'P','deg',dis(i,j));
        
        timeP(i,j)=tt(1).time;  
       
        tt=taupTime(model,dep(i),'S','deg',dis(i,j));
        timeS(i,j)=tt(1).time;   
         tt=taupTime(model,dep(i),'ScS','deg',dis(i,j));
         timeScS(i,j)=tt(1).time; 
        % tt=taupTime(model,dep(i),'ScP','deg',dis(i,j));
        % timeScP(i,j)=tt(1).time; 
        difS_P(i,j)=timeS(i,j)-timeP(i,j);
        difScS_P(i,j)=timeScS(i,j)-timeP(i,j);
       % difScP_P(i,j)=timeScP(i,j)-timeP(i,j);
        end
      plot(difS_P(i,:),difScS_P(i,:));hold on
      legend('ScS-P----S-P')  
end

for j=1:5:61
    %for j=1:1:1
    
 plot(difS_P(:,j),difScS_P(:,j),'r-');hold on
end 
    %  legend('ScP-P----S-P')
%difS_P(2,11)
%difScS_P(2,11)
%depth=dep(2)
%distance=dis(2,11)
      % plot(difS_P(:,2),difScS_P(:,2),'r.');
% 结果
%ans =294.4662 s   S-P
%ans =632.9782 s   ScS-P
%depth =60 km
%distance =30 degrees
% % ScP-P,S-P,1-70 deg.
dep_step=60 % 深度步长60km
dis_step=1  % 震中距步长1度
dis_start=0  % 震中距0度开始
dep_num=11; 
dis_num=80;  %P限于震中距小于80度

figure(2)
    for i=1:dep_num
    i;
    dep(i)=(i-1)*dep_step
        for j=1:dis_num
            j;
        dis(i,j)=(j-1)*dis_step+dis_start;
        [tt,flag]=taupTime_z(model,dep(i),'P','deg',dis(i,j));
        if flag==1
          timeP(i,j)=0;
        else
        timeP(i,j)=tt(1).time;  
        end
        [tt,flag]=taupTime_z(model,dep(i),'S','deg',dis(i,j));
        if flag==1
          timeS(i,j)=0;
        else
        timeS(i,j)=tt(1).time; 
        end
        %{
 [tt,flag]=taupTime_z(model,dep(i),'ScS','deg',dis(i,j));
        
         if flag==1
          timeScS(i,j)=0;
         else
             timeScS(i,j)=tt(1).time;
         end
        %}
         [tt,flag]=taupTime_z(model,dep(i),'ScP','deg',dis(i,j));
         if flag==1
          timeScP(i,j)=0;
        else
         timeScP(i,j)=tt(1).time; 
         end
         if timeS(i,j)==0 || timeP(i,j)==0
             difS_P(i,j)=0;
         else
        difS_P(i,j)=timeS(i,j)-timeP(i,j);
         end
         if timeScP(i,j)==0 || timeP(i,j)==0
             difScP_P(i,j)=0;
         else
        difScP_P(i,j)=timeScP(i,j)-timeP(i,j);
         end
        end
      
        k=0;
        l=0;
      for m=1:dis_num
          if difScP_P(i,m)>0 & difS_P(i,m)>0  & k==0
              k=m
          end
          if difScP_P(i,m)>0 & difS_P(i,m)>0  
                    l=m
          end
      end
        %l=dis_num;      
      
     % plot(difS_P(i,k:l),difScS_P(i,k:l),'-');hold on
       plot(difS_P(i,k:l),difScP_P(i,k:l),'-');hold on
       i
       k
       l
       difS_P(i,k:l)
       difScP_P(i,k:l)
      legend('ScP-P----S-P')  
    end


k=1;
for j=1:5:dis_num
    j
   
       for m=1:dep_num
         % if difScS_P(m,j)>0 & difS_P(m,j)>0 
               if difScP_P(m,j)>0 & difS_P(m,j)>0 
     dis_scp=(j-1)*dis_step+dis_start   
      dep_scp=(m-1)*dep_step
 plot(difS_P(m,j),difScP_P(m,j),'r.');hold on
               end 
       end
 %plot(difS_P(:,j),difScP_P(:,j),'r.');hold on
end 
   
    %  legend('ScP-P----S-P')
    %{
difS_P(2,11)
difScP_P(2,11)
depth=dep(2)
distance=dis(2,11)
      % plot(difS_P(:,2),difScS_P(:,2),'r.');
% 结果
%ans = 111.4018 s　　　 S-P
%ans = 573.2153 s      ScP-P
%depth =60  km
%distance =10   degrees    
% 根据ScS-P到时差和S-P到时差估计震中距和震源深度
%dif_s_p=294.4662  %S-P
%dif_scs_p=632.9782 %ScS-P
dif_s_p=264.4662  %S-P
dif_scs_p=602.9782 %ScS-P
difmin=100000
for i=1:dep_num
    for j=1:dis_num
        absdif=abs(difS_P(i,j)-dif_s_p)+ abs(difScS_P(i,j)-dif_scs_p);
        if absdif<difmin
           difmin=absdif; 
           i_min=i;
           j_min=j;
        end
    end
end
depth_final= (i_min-1)*dep_step
distance_final=(j_min-1)*dis_step+dis_start
%depth =60 km
%distance =30 degrees
if i_min==1 & j_min>1
    difS_P_11=difS_P(i_min,j_min-1);
    difS_P_md=(difS_P(i_min,j_min+1)-difS_P(i_min,j_min-1))/20;
    difScS_P_11=difScS_P(i_min,j_min-1);
    difScS_P_md=(difScS_P(i_min+1,j_min)-difScS_P(i_min,j_min))/10;
    difmin=100000;
    for ii=1:20
        for jj=1:20
       absdif=abs(difS_P_11+difS_P_md*ii-dif_s_p)+ abs(difScS_P_11+difScS_P_md*jj-dif_scs_p);
        if absdif<difmin
           difmin=absdif; 
           ii_min=ii;
           jj_min=jj;
        end 
        end
    end
   depth_final_1= depth_final+ii_min*dep_step/10
distance_final_1=distance_final+jj_min*dis_step/10 
end
if i_min>1 & j_min>1
    difS_P_11=difS_P(i_min-1,j_min-1);
    difS_P_md=(difS_P(i_min,j_min+1)-difS_P(i_min,j_min-1))/20;
    difScS_P_11=difScS_P(i_min-1,j_min-1);
    difScS_P_md=(difScS_P(i_min+1,j_min)-difScS_P(i_min-1,j_min))/20;
    difmin=100000;
    for ii=1:20
        for jj=1:20
       absdif=abs(difS_P_11+difS_P_md*ii-dif_s_p)+ abs(difScS_P_11+difScS_P_md*jj-dif_scs_p);
        if absdif<difmin
           difmin=absdif; 
           ii_min=ii;
           jj_min=jj;
        end 
        end
    end
   depth_final_1= depth_final-ii_min*dep_step/10
distance_final_1=distance_final-jj_min*dis_step/10 
end

if i_min>1 & j_min==1
    difS_P_11=difS_P(i_min-1,j_min);
    difS_P_md=(difS_P(i_min,j_min+1)-difS_P(i_min,j_min))/10;
    difScS_P_11=difScS_P(i_min-1,j_min);
    difScS_P_md=(difScS_P(i_min+1,j_min)-difScS_P(i_min-1,j_min))/20;
end
if i_min==1 & j_min==1
    difS_P_11=difS_P(i_min,j_min);
    difS_P_md=(difS_P(i_min,j_min+1)-difS_P(i_min,j_min))/10;
    difScS_P_11=difScS_P(i_min,j_min-1);
    difScS_P_md=(difScS_P(i_min+1,j_min)-difScS_P(i_min,j_min))/10;
end
i_min
j_min
ii_min
jj_min
%}
