% 计算走时
clear all
% model='model_iasp91.asc'  
xa=0;
ya=0;
xb=0;zb0=0;
model='js4.asc' 
dis=57 % 震中距km
    step=1 % km 
    
   for k=1:20  % 震源深度  
      flag=1;
      dep(k)=step*k; 
   [travel_t,etaPb,etaPn,num_c,num_m]=travel_time_ps_new(model,xa,ya,dep(k),xb,dis,zb0,flag)
   %[Pg,sPg,Sg]=travel_Pg_sPg_Sg(model,dep(k),dis); 
   t_Pg(k)=travel_t(1);
   t_sPg(k)=travel_t(6);
   flag=2;
    [travel_t,etaPb,etaPn,num_c,num_m]=travel_time_ps_new(model,xa,ya,dep(k),xb,dis,zb0,flag)
   t_Sg(k)=travel_t(1);
   end
    
     figure(1)
      plot(dep,t_Pg,dep,t_sPg,dep,t_Sg)
      legend('Pg','sPg','Sg')
    xlabel('震源深度/km')
    ylabel('走时/s')
    title({'走时曲线，震中距=',dis,'km  地壳模型=',model})
    figure(2)
    plot(dep,t_sPg-t_Pg,dep,t_Sg-t_Pg)
      legend('sPg-Pg','Sg-Pg')
    xlabel('震源深度/km')
    ylabel('走时差/s')
    title({'走时差曲线，震中距=',dis,'km  地壳模型=',model})
    
    figure(3)
    plot(t_sPg-t_Pg,-dep,t_Sg-t_Pg,-dep)
      legend('sPg-Pg','Sg-Pg')
    ylabel('震源深度/km')
    xlabel('走时差/s')
    title({'走时差曲线，震中距=',dis,'km  地壳模型=',model})