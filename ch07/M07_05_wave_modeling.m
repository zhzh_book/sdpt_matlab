% 显示合成地震图2015-05-11
clear all
dist=57
sampling_rate=100
% 计算走时

% model='model_iasp91.asc'  
xa=0;
ya=0;
xb=0;zb0=0;
model='js4.asc' 
dis=57 % 震中距km
step=1 % km 
    
   for k=1:20  % 震源深度  
      flag=1;
      dep(k)=step*k; 
      [travel_t,etaPb,etaPn,num_c,num_m]=travel_time_ps_new(model,xa,ya,dep(k),xb,dis,zb0,flag)
      %[Pg,sPg,Sg]=travel_Pg_sPg_Sg(model,dep(k),dis); 
      t_Pg(k)=travel_t(1);
      t_sPg(k)=travel_t(6);
      flag=2;
      [travel_t,etaPb,etaPn,num_c,num_m]=travel_time_ps_new(model,xa,ya,dep(k),xb,dis,zb0,flag)
      t_Sg(k)=travel_t(1);
   end
    
   figure(101)
   plot(dep,t_Pg,dep,t_sPg,dep,t_Sg)
   legend('Pg','sPg','Sg')
   xlabel('震源深度/km')
   ylabel('走时/s')
   title({'走时曲线，震中距=',dis,'km  地壳模型=',model})
   figure(102)
   plot(dep,t_sPg-t_Pg,dep,t_Sg-t_Pg)
   legend('sPg-Pg','Sg-Pg')
   xlabel('震源深度/km')
   ylabel('走时差/s')
   title({'走时差曲线，震中距=',dis,'km  地壳模型=',model})
    
   figure(103)
   plot(t_sPg-t_Pg,-dep,t_Sg-t_Pg,-dep)
   legend('sPg-Pg','Sg-Pg')
   ylabel('震源深度/km')
   xlabel('走时差/s')
   title({'走时差曲线，震中距=',dis,'km  地壳模型=',model})

% 带通滤波器：0.5Hz-2Hz,奈奎斯特频率f0=sampling_rate/2
f0=sampling_rate/2
fu1=0.1
fu=0.2
fh=6.0
fh2=6.1

% 带通滤波器：0.1Hz-3Hz,奈奎斯特频率f0=sampling_rate/2
f11=fu1/f0
f12=fu/f0
f21=fh/f0
f22=fh2/f0
f=[0,f11,f12,f21,f22,1];m=[0,0,1,1,0,0];

i_fig=1
fil='ASCBG.YC1.z'
[ sacwavez1,sacwave_len ] = read_sac_fun( fil,dist,sampling_rate,i_fig );
bg(1,:)=sacwavez1;
num_wave=sacwave_len
t=(1:num_wave)/sampling_rate;

load JS_YC_BHZ_2.txt;
bg(13,:)=JS_YC_BHZ_2(271:sacwave_len+270);  % 使初动与合成地震图对齐
%bg(13,:)= JS_YC_BHZ_2(1:sacwave_len);

X=bg(13,:);
[ Y ] =filt( X,f,m );
bg(13,:)=Y;
bg(13,:)=bg(13,:)-mean(bg(13,:));
bg(13,:)=bg(13,:)/max(abs(bg(13,1:700)));
plot(t,bg(13,:),'g');
figure(98)
plot(t,bg(13,:),'b')
%递归积分
%bg13_int(1)=0;
%for i=1:num_wave-1
%bg13_int(i+1)=bg13_int(i)+(bg(13,i))/sampling_rate;
%end
bg13_int=bg(13,:);
%bg13_int(1)=0;
%num_win=50;  %滑动窗平均
%for i=1:num_wave-num_win
    %for i=2:num_wave
    %bg13_int(i)=0;
    %bg13_int(i)=bg13_int(i-1)+bg(13,i)/sampling_rate;
    %for j=i:i+num_win-1
    %   bg13_int(i)=bg13_int(i)+bg(13,j);
    %end
    %bg13_int(i)=bg13_int(i)/num_win;
%end

%plot(t,bg(13,:),'r');hold on
plot(t,bg13_int,'g');
fil='ASCBG.YC2.z'
i_fig=2
[ sacwavez2,sacwave_len ] = read_sac_fun( fil,dist,sampling_rate,i_fig );
bg(2,:)=sacwavez2;
%plot(t,bg(13,:),'r');hold on
plot(t,bg13_int,'g');
fil='ASCBG.YC3.z'
i_fig=3
[ sacwavez3,sacwave_len ] = read_sac_fun( fil,dist,sampling_rate,i_fig );
bg(3,:)=sacwavez3;
%plot(t,bg(13,:),'r');hold on
plot(t,bg13_int,'g');
fil='ASCBG.YC4.z'
i_fig=4
[ sacwavez4,sacwave_len ] = read_sac_fun( fil,dist,sampling_rate,i_fig );
bg(4,:)=sacwavez4;
%plot(t,bg(13,:),'r');hold on
plot(t,bg13_int,'g');
fil='ASCBG.YC6.z'
i_fig=5
[ sacwavez6,sacwave_len ] = read_sac_fun( fil,dist,sampling_rate,i_fig );
bg(5,:)=sacwavez6;
%plot(t,bg(13,:),'r');hold on
plot(t,bg13_int,'g');
fil='ASCBG.YC8.z'
i_fig=6
[ sacwavez8,sacwave_len ] = read_sac_fun( fil,dist,sampling_rate,i_fig );
bg(6,:)=sacwavez8;
%plot(t,bg(13,:),'r');hold on
plot(t,bg13_int,'g');
fil='ASCBG.YC10.z'
i_fig=7
[ sacwavez10,sacwave_len ] = read_sac_fun( fil,dist,sampling_rate,i_fig );
bg(7,:)=sacwavez10;
%plot(t,bg(13,:),'r');hold on
plot(t,bg13_int,'g');
fil='ASCBG.YC12.z'
i_fig=8
[ sacwavez12,sacwave_len ] = read_sac_fun( fil,dist,sampling_rate,i_fig );
bg(8,:)=sacwavez12;
%plot(t,bg(13,:),'r');hold on
plot(t,bg13_int,'g');
fil='ASCBG.YC14.z'
i_fig=9
[ sacwavez14,sacwave_len ] = read_sac_fun( fil,dist,sampling_rate,i_fig );
bg(9,:)=sacwavez14;
%plot(t,bg(13,:),'r');hold on
plot(t,bg13_int,'g');
fil='ASCBG.YC16.z'
i_fig=10
[ sacwavez16,sacwave_len ] = read_sac_fun( fil,dist,sampling_rate,i_fig );
bg(10,:)=sacwavez16;
%plot(t,bg(13,:),'r');hold on
plot(t,bg13_int,'g');
fil='ASCBG.YC18.z'
i_fig=11
[ sacwavez18,sacwave_len ] = read_sac_fun( fil,dist,sampling_rate,i_fig );
bg(11,:)=sacwavez18;
%plot(t,bg(13,:),'r');hold on
plot(t,bg13_int,'g');
fil='ASCBG.YC20.z'
i_fig=12
[ sacwavez20,sacwave_len ] = read_sac_fun( fil,dist,sampling_rate,i_fig );
bg(12,:)=sacwavez20;
%plot(t,bg(13,:),'r');hold on
plot(t,bg13_int,'g');

for i=1:13
X=bg(i,:);
[ Y ] =filt( X,f,m );
bg1(i,:)=Y;
%bg1(i,:)=X;   %no filtering
end

%[ wave_filtered ] =filt( sacwave,f,m );

figure(99)
plot(t,bg1(13,:));
figure(13)
for i=1:4
    plot(t,bg(i,:)/max(abs(bg(i,1:700)))-(i)*1);hold on
    %plot(t,bg(i,:));hold on
end
for i=5:12
   plot(t,bg(i,:)/max(abs(bg(i,1:700)))-(4)*1-(i-4)*2);hold on
   % plot(t,bg(i,:));hold on
end
plot(t,bg(13,:)/max(abs(bg(13,1:700)))-13,'r');
%plot(t,bg(13,:),'r');
xlabel('时间/s')
% figure(14) 
% subplot(4,1,1)
% plot(t,sacwavez1)
% ylabel('z1')
% subplot(4,1,2)
% plot(t,sacwavez2)
% ylabel('z2')
% subplot(4,1,3)
% plot(t,sacwavez3)
% ylabel('z3')
% subplot(4,1,4)
% plot(t,sacwavez4)
% ylabel('z4')

% figure(15)
% for i=1:4
%     plot(t,bg1(i,:)/max(abs(bg1(i,1:700)))-(i)*1);hold on
% end
% for i=5:12
%     plot(t,bg1(i,:)/max(abs(bg1(i,1:700)))-(4)*1-(i-4)*2);hold on
% end
P0=0.5  %波形的P到时0.5s
% plot(t,bg1(13,:)/max(abs(bg1(13,1:700)))-13,'r');hold on
% plot(t_sPg-t_Pg+P0,-dep,t_Sg-t_Pg+P0,-dep)
% xlabel('时间/s')
% ylabel('震源深度/km')
% title({'滤波频带',fu,'-',fh,'Hz'})
% figure(16) 
% for i=1:4
%    subplot(4,1,i)
%    plot(t,bg1(i,:))
% end

% 求实际信号与合成信号的互相关系数
for i=1:12
    if i<5
    x(i)=i;
    else x(i)=4+(i-4)*2;
    end
    no=0;
    deno1=0;
    deno2=0;
   % for j=1:num_wave
         %for j=1:1000  %前10秒互相关
              for j=1:700 %前10秒互相关
    no=no+bg1(i,j)*bg1(13,j);
    deno1=deno1+bg1(i,j)*bg1(i,j);
    deno2=deno2+bg1(13,j)*bg1(13,j);
    end
    corr(i)=no/sqrt(deno1*deno2);
end
corr
% figure(97)
% plot(x,corr,'-.')
% 对合成波形差分成速度
for i=1:12
    for j=1:num_wave-1
        bg_dif(i,j)=bg(i,j+1)-bg(i,j);
    end
    bg_dif(i,num_wave)=0;
    max_dif(i)=max(abs(bg_dif(i,:)));
    bg_dif(i,:)=bg_dif(i,:)/max_dif(i);   %归一化
end
max_bg13=max(abs(bg1(13,:)));
bg1(13,:)=bg1(13,:)/max_bg13;
figure(17)
for i=1:4
    plot(t,bg_dif(i,:)-i);hold on
end
for i=5:12
    plot(t,bg_dif(i,:)-4-(i-4)*2);hold on
end
plot(t,bg1(13,:)-21);
xlabel('时间/s')
% 频谱分析
f=(1:num_wave)/20.48;
figure(18)
for i=1:12
    temp=bg1(i,:);
    bg1_fft=abs(fft(temp));
    plot(f,bg1_fft(1:num_wave));hold on
end
    
    bg13_fft=abs(fft(bg1(13,:)));
    plot(f,bg13_fft(1:num_wave),'r');
% 计算功率谱
dur=2 % 2s
segment=dur*sampling_rate
for i=1:13
    if i<5
        dep(i)=i;
    else 
        dep(i)=(i-4)*2+4;
    end
    psd_noise(i,:)=log10(abs(pwelch(bg1(i,601:600+segment))));
    psd_sg(i,:)=log10(abs(pwelch(bg1(i,801:800+segment))));
    psd_rg(i,:)=log10(abs(pwelch(bg1(i,1001:1000+segment*2))));
    max_sg(i)=max( psd_sg(i,:));
    max_rg(i)=max( psd_rg(i,:));
end

f=(1:segment/2)/dur;
% figure(21)
len_psd_noise=length(psd_noise(1,:))
%plot(f,psd_noise(1,1:segment/2),f,psd_sg(1,1:segment/2),f,psd_rg(1,1:segment/2));
legend('noise','sg','rg')
%figure(22)
med_sg=median(max_sg(1:12))
for i=1:24
    med_line(i)=med_sg;
end

%plot(dep(1:12),max_sg(1:12),dep(1:12),max_rg(1:12),dep(1:12),max_rg(1:12)-med_sg);hold on
%legend('maxsg','maxrg','maxrg/medsg')
%xlabel('震源深度/km')
%ylabel('log10(abs(psd))')
%plot(med_line);hold on

%plot(dep(1:12),max_rg(1:12)-med_sg,'r');hold on

p=polyfit(dep(1:12),max_rg(1:12)-med_sg,1)
p1=p(1)
p2=p(2)
y1(1)=p(1)*dep(1)+p(2)
y1(2)=p(1)*dep(12)+p(2)
x1(1)=dep(1)
x1(2)=dep(12)
plot(x1,y1,'r')
y1(1)=max_rg(13)-med_sg
y1(2)=max_rg(13)-med_sg
x1(1)=dep(1)
x1(2)=dep(12)
plot(x1,y1,'black')

% figure(23)
% plot(dep(1:12),max_sg(1:12),dep(1:12),max_rg(1:12),dep(1:12),max_rg(1:12)-med_sg);hold on
% legend('maxsg','maxrg','maxrg/medsg')
% xlabel('震源深度/km')
% ylabel('log10(abs(psd))')



med_sg=median(max_sg(1:12))
for i=1:24
    med_line(i)=med_sg;
end
plot(med_line);hold on

p=polyfit(dep(1:9),max_rg(1:9)-med_sg,1)
p1=p(1)
p2=p(2)
y1(1)=p(1)*dep(1)+p(2)
y1(2)=p(1)*dep(9)+p(2)
x1(1)=dep(1)
x1(2)=dep(9)
plot(x1,y1,'r')
y1(1)=max_rg(13)-med_sg
y1(2)=max_rg(13)-med_sg
x1(1)=dep(1)
x1(2)=dep(12)
plot(x1,y1,'black')







