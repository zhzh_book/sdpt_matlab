%利用sPmP-PmP定震源深度
% 计算sPmP-PmP理论到时
clear all
model='model_iasp91.asc'
elev=0  % 台站高程０km
dep=15   % 震源深度15km
num=5   %台站数
for i=1:num
dist(i)=40+i*20; %震中距　km
[spmp,pmp]=spmp_pmp(model,dist(i),dep,elev);
t_pmp(i)=pmp;
t_spmp(i)=spmp;
ele(i)=0; %台站高程设为0
end
dist
t_pmp
t_spmp
for i=1:5
dist1(i)=dist(i)+5*randn(1);
t_pmp1(i)=t_pmp(i)+0.1*randn(1);
t_spmp1(i)=t_spmp(i)+0.1*randn(1);
end
dist1
t_pmp1
t_spmp1
h_moho=35 %地壳厚度
dep_step=1
num_dep=floor(h_moho/dep_step)
%用sPmP-PmP定震源深度，搜索法
[h,rms]= spmp_pmp_depth_mullayer( num,dist1,t_spmp1,t_pmp1,model,num_dep,dep_step,ele)