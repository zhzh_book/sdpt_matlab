function [h,rms]= spmp_pmp_depth_mullayer( num,del,spmp1,pmp1,model,num_dep,step,ele)

% 用sPmP-PmP定震源深度 2015-05-12
%num:提供sPmP和PmP到时对的个数
%del:提供sPmP和PmP到时对的各台站震中距
%spmp:提供的sPmP数组，含sPmP到时
%pmp:提供的PmP数组，含与sPmP数组对应的PmP到时
%model:地球速度模型
%num_dep：搜索深度个数
%step:搜索深度步长
%lel：提供sPmP和PmP到时对的各台站的高程（假定为球地球），单位km
%返回值：
%h:相对于球地球模型表面的震源深度
flag=1  % P波震相
res2_sum_min=10000;
    for i=1:num_dep
    res2_sum(i)=0;
    dep_test(i)=step*i;
   
    for j=1:num
    spmp_pmp1=spmp1(j)-pmp1(j);
    del2=del(j)^2;
    [spmp,pmp]=spmp_pmp(model,del(j),dep_test(i),ele(j)); %返回给定震中距（del(j))和给定震源深度(dep_test(i))的走时
    %model是地球模型，ele(j)是台站高程（km）
     spmp_pmp_cal=spmp-pmp; %计算的sPmP-PmP走时差（也是到时差）
    res2=(spmp_pmp1-spmp_pmp_cal)^2;
    res2_sum(i)=res2_sum(i)+res2;
    end
    res2_sum(i)=res2_sum(i)/num;
    if res2_sum(i)<res2_sum_min
        res2_sum_min=res2_sum(i);
        rms=sqrt(res2_sum_min);
        dep_index=i;
        h=dep_test(i);
    end
end
plot(dep_test,sqrt(res2_sum),'.');hold on

xlabel('震源深度/km')
ylabel('sPmP-PmP到时差残差/s')
title('用sPmP-PmP到时差搜索震源深度')
end

