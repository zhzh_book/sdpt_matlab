function [ wave_filtered ] =filt( sacwave,f,m )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
%f=[0,0.04,0.04,1];m=[0,0,1,1];
%高通滤波器:拐角频率为0.04*采样率/2=0.04*50/2=1Hz；m给出相应的幅度
%利用Matlab中的帮助 help fir2; help filter; help filtfilt
%参考书：《Matlab及在电子信息课程中的应用》 第7章    
%2007-12-03   赵仲和
b=fir2(30,f,m);n=0:30;
%利用Matlab中的信号处理工具箱中的FIR滤波器设计工具fir2生成递归滤波器系数b,
% 30  为滤波器的阶
%subplot(3,2,1);stem(n,b,'.')
%xlabel('n');ylabel('h(n)');
%axis([0,30,-0.4,0.5]),line([0,30],[0,0])
[h,w]=freqz(b,1,256);
wave_filtered=filtfilt(b,1,sacwave);
%wave1_filtered=wave1;
%for i=1:1:300
 %   wave1_filtered(i+580)=0;
%end
%filtfilt为零相位滤波器，即正向和反向各进行一次滤波
num=length(sacwave)
for i=1:num
    t(i)=i/40;
end
figure(100)
subplot(2,1,1)
plot(t,sacwave)
subplot(2,1,2)
plot(t,wave_filtered)
title('波形')
xlabel('时间/s')
ylabel('counts')

end

