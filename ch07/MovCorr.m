function y = MovCorr(Data1,Data2)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

 [N,~] = size(Data1)

 correlationTS = nan(N, 1);

 for t = 100+1:N-100
     correlationTS(t, 1) = corr(Data1(t-100+1:t, 1),Data2(t+100-100+1:t+100,1),'rows','complete');
 end
     y = [Data1(:,1),correlationTS];
 end



