%计算分层地壳中的地震波走时
% 给定速度模型
%测试模型是基于中国平均2层地壳模型
% ( 中国地区地震走时表,国家地震局地球物理研究所编，《震相走时便查表》，地震出版社，1980)
%  设计的。模型文件的上面3行是注释行，有5列数据：
% 层序号，层顶深度，该层内P波速度，该层内S波速度，注释：1：康拉德面，2：莫霍面
% 为了检验走时计算程序，在上地壳和下地壳中都加了层，但层内速度没变。
% 一下是速度模型内容，去掉%号即为模型文件model_test.asc
%velocity model for testing travel_time_new.
%remark: 1:Corad discon.; 2: Moho discon.
%no   top-depth  P(km/s)  S(km/s) remark
%1     0        5.71           3.40    0
%2     8        5.71           3.40    0
%3     16       6.53           3.77    1
%4     30       6.53           3.77    0
%5     40       7.97           4.45    2
%6     100      7.97           4.45    0
function [travel_t,etaPb,etaPn,num_c,num_m]=travel_time_ps1(model,xa,ya,za0,xb,yb,zb0,flag)
%返回值：travel_t矢量，元素为
%travel_t(1)-直达P走时
%travel_t(2)-Pb走时
%travel_t(3)-Pn走时
%travel_t(4)-PmP走时
%travel_t(5)-sPn走时
%travel_t(6)-直达S走时
% etaPb-Pb的盲区距离，etaPn-Pn的盲区距离，
% num_c-康拉德面序号，num_m-莫霍面序号，地壳总层数=num_m-1,Vnum_m对应上地幔P波速度 （地面序号为1，）
%xa,ya,za0:震源坐标（北、东、下）
%xb,yb,zb0:台站坐标（北、东、下）
model;
%fid=fopen(x,'r');
%a=fscanf(fid,'%d %f %f %f %d',[5 inf]);
%fclose(fid);
%a=a';
%[m,n]=size(a);
m=0;
ascdata = importdata(model, ' ', 3);
m=length(ascdata.data(:,1));
layer_num=ascdata.data(:,1);
layer_top=ascdata.data(:,2);
vp=ascdata.data(:,3);
vs=ascdata.data(:,4);
layer_mark=ascdata.data(:,5);
for i=1:m-1
    h(i)=layer_top(i+1)-layer_top(i);  %层厚度
end
nlayer=m;
h;
h0=h(1);
h(1)=h0-zb0;%以台站深度（高程的负值）作为第一层的厚度
za=za0-zb0;%自台站深度算起的震源深度
%求震源所在层号j

% 给定产生绕射波的层顶
num_j=1;
for i=1:nlayer
    if layer_mark(i)==1
        num_c=i;
    else if layer_mark(i)==2
            num_m=i;
        end
    end
    if za>layer_top(i)
        num_j=i;
    end
end
num_c;
num_m;
num_j;

%k=nlayer; %Moho界面绕射波
%  为便于计算Moho面以下地震的直达波走时，人为地增加了下面的一层
%nlayer=nlayer+1;
%h(nlayer)=1000;
%vp(nlayer)=vp(nlayer-1);
%vs(nlayer)=vs(nlayer-1);
%计算P波走时(flag=1)或S波走时（flag=2）
if flag==1
    v=vp;
else
    v=vs;
end
% -------
%depth=0.0;
%ntime=0;
%for i=1:nlayer
%   depth=depth+h(i);
%   if za<depth & ntime==0
%       j=i;
%       ntime=1;
%   end
%end
%j
%disp(model);%显示速度模型文件名

%str=sprintf('速度模型 %s',model);
%disp(str)
%str=sprintf('层序号 层顶深度（km) P（km/s)  S(km/s)  标记');
%disp(str)
%for i=1:m
%str=sprintf('%d      %5.1f       %5.2f     %5.2f     %d',layer_num(i),layer_top(i),vp(i),vs(i),layer_mark(i));
%disp(str)
%end
%str=sprintf('标记=1，康拉德；=2，莫霍');
%disp(str)
delta=sqrt((xb-xa)^2+(yb-ya)^2);%震中距
str=sprintf('震中距=%6.1f km，震源深度=%6.1f km',delta,za0);
%disp(str);
kexi=za;
if num_j>1
for i=1:num_j-1
kexi=kexi-h(i);
end
end
%计算Pn走时
k=num_m;
j=num_j;
num_m;
num_j;
if num_j<num_m %震源在莫霍面以上
kexi;  %震源到所在层顶的深度
% 计算P绕射波走时
sum1=0.0;
sum3=0.0;
for i=1:num_j-1
  omega=sqrt(v(k)^2-v(i)^2);
  sum1=sum1+h(i)*omega/(v(i)*v(k));
  sum3=sum3+h(i)*v(i)/omega;
end
sum2=0.0;
sum4=0.0;
for i=num_j:k-1
   omega=sqrt(v(k)^2-v(i)^2);
   sum2=sum2+h(i)*omega/(v(i)*v(k));
   sum4=sum4+h(i)*v(i)/omega;
end
omega=sqrt(v(k)^2-v(j)^2);
Trkj=delta/v(k)-kexi*omega/(v(j)*v(k))+sum1+2*sum2;
etakj=-kexi*v(j)/omega+sum3+2*sum4;
if delta<etakj
    Trkj=0.0;
end
%str = sprintf('地壳内Pn走时：%6.2f s',Trkj);
%disp(str)
trPn=Trkj;
etaPn=etakj;   %地壳内Pn的盲区距离
else
    %disp('没有地壳内Pn');
    trPn=0;
    etaPn=0;
end
%计算上地壳震源的Pb走时
k=num_c;
j=num_j;
if num_j<num_c %震源在康拉德面以上
kexi;  %震源到所在层顶的深度
% 计算上地壳Pb走时
sum1=0.0;
sum3=0.0;
for i=1:num_j-1
  omega=sqrt(v(k)^2-v(i)^2);
  sum1=sum1+h(i)*omega/(v(i)*v(k));
  sum3=sum3+h(i)*v(i)/omega;
end
sum2=0.0;
sum4=0.0;
for i=num_j:k-1
   omega=sqrt(v(k)^2-v(i)^2);
   sum2=sum2+h(i)*omega/(v(i)*v(k));
   sum4=sum4+h(i)*v(i)/omega;
end
omega=sqrt(v(k)^2-v(j)^2);
Trkj=delta/v(k)-kexi*omega/(v(j)*v(k))+sum1+2*sum2;
etakj=-kexi*v(j)/omega+sum3+2*sum4;
if delta<etakj
    Trkj=0.0;
end
trPb=Trkj;
etaPb=etakj;   %上地壳Pb的盲区距离
str = sprintf('上地壳内Pb走时：%6.2f s',Trkj);
%disp(str);
trPb=Trkj;

etaPb=etakj;
else
    %disp('没有上地壳Pb');
    trPb=0;
    etaPb=0;
end

%计算直达波走时
kexi;
%small=2;
small=0.2;
jj=0;
delta_star=0.0;
kexi;
za;
j;
if j>1
yita1=delta*kexi/(za+0.01);
yita2=delta;
phai1=atan(yita1/(kexi+0.01));
phai2=atan(delta/(kexi+0.01));
p1=phai1*180/pi;
p2=phai2*180/pi;
thet1(j)=phai1;
thet2(j)=phai2;
    sum5=0.0;
    sum6=0.0;
    for i=j-1:-1:1
        thet1(i)=asin(sin(thet1(i+1))*v(i)/v(i+1));
        thet2(i)=asin(sin(thet2(i+1))*v(i)/v(i+1));
        sum5=sum5+h(i)*tan(thet1(i));
        sum6=sum6+h(i)*tan(thet2(i));
     end
   delta1=yita1+sum5;
   delta2=yita2+sum6;
yita_star=(delta-delta1)*(yita2-yita1)/((delta2-delta1)+yita1+0.01);
phai_star=atan(yita_star/(kexi+0.01));
while abs(delta-delta_star)>small
thet(j)=phai_star;
sum7=0.0;
   for i=j-1:-1:1
        thet(i)=asin(sin(thet(i+1))*v(i)/v(i+1));
        sum7=sum7+h(i)*tan(thet(i));
   end
   delta_star=yita_star+sum7;
   if delta_star>delta  
         yita_star=(delta-delta1)*(yita_star-yita1)/(delta_star-delta1)+yita1;
   else 
  yita_star=(delta-delta_star)*(yita2-yita_star)/(delta2-delta_star+small)+yita_star;
   end
   phai_star=atan(yita_star/(kexi+0.01));
end
jj;
delta_star;
 td=kexi/(cos(phai_star)*v(j));
 thet(j)=phai_star;
 for i=j-1:-1:1
     thet(i)=asin(sin(thet(i+1))*v(i)/v(i+1));
     td=td+h(i)/(cos(thet(i))*v(i));
 end
else
    td=sqrt((xa-xb)^2+(ya-yb)^2+(za-zb0)^2)/v(1);
end
td;

if za0>layer_top(num_m)
    str=sprintf('上地幔Pn %5.2f s',td);
    %disp(str);
else if za0>layer_top(num_c)
        str=sprintf('下地壳Pb %5.2f s',td);
        %disp(str);
    else
        str=sprintf('上地壳直达波Pg %5.2f s',td);
        %disp(str);
    end
end
travel_t(1)=td;  %-直达P走时
travel_t(2)=trPb; %-Pb走时
travel_t(3)=trPn; %-Pn走时
% -------------------------------------
%计算PmP走时
%model
%velocity model for testing travel_time_new.m
%remark: 1:Corad discon.; 2: Moho discon.
%no   top-depth  P(km/s)  S(km/s) remark
%1     0        5.71           3.40    0
%2     8        5.71           3.40    0
%3     16       6.53           3.77    1
%4     30       6.53           3.77    0
%5     40       7.97           4.45    2
%6     100      7.97           4.45    0
m=0;
ascdata = importdata(model, ' ', 3);
m=length(ascdata.data(:,1));   %模型总层数
layer_num=ascdata.data(:,1);
layer_top=ascdata.data(:,2);
vp=ascdata.data(:,3);
vs=ascdata.data(:,4);
layer_mark=ascdata.data(:,5);
num_m;   % 莫霍面位置
h_moho=0;
for i=1:num_m-1
    h(i)=layer_top(i+1)-layer_top(i);  %地壳内各厚度
    h_moho=h_moho+h(i);
end
h_moho;
nlayer=m;
h;
h0=h(1);
h(1)=h0-zb0;%以台站深度（高程的负值）作为第一层的厚度
za=za0-zb0;%自台站深度算起的震源深度
%求震源所在层号j
% 生成虚拟地壳
for i=1:num_m-1
    vp(num_m-1+i)=vp(num_m-i);
    vs(num_m-1+i)=vs(num_m-i);
    h(num_m-1+i)=h(num_m-i);
    layer_top(num_m+i)=layer_top(num_m-1+i)+ h(num_m-1+i);
end
layer_top;
if flag==1
    v=vp;
else
    v=vs;
end
m=2*num_m-2;  % 虚拟模型层数
nlayer=m;
vp;
vs;
h;
za=2*h_moho-za; % 虚拟震源深度
za_pmp=za;
kexi;
small=0.2;
jj=0;
delta_star=0.0;
kexi;
za;
j;
j=2*(num_m-1)-j+1;
kexi=za;
if j>1
kexi=kexi-layer_top(j);
end
kexi;
pmp_j=j;
%if j>1
%yita1=delta*kexi/(za+0.01); 
yita1=0;
yita2=0;
delta1=yita1;
delta2=yita2;
phai_step=pi/180;  % 初始取1度
phai1=atan(yita1/(kexi+0.01));
phai2=atan(yita2/(kexi+0.01))+phai_step;
p1=phai1*180/pi;
p2=phai2*180/pi;
thet1(j)=phai1;
thet2(j)=phai2;
while delta2<=delta
    sum5=0.0;
    sum6=0.0;
    for i=j-1:-1:1
        thet1(i)=asin(sin(thet1(i+1))*v(i)/v(i+1));      
        thet2(i)=asin(sin(thet2(i+1))*v(i)/v(i+1));
        sum5=sum5+h(i)*tan(thet1(i));
        sum6=sum6+h(i)*tan(thet2(i));
    end
     thet1j=thet1(j);
   yita1=(kexi+0.01)*tan(thet1(j));
   yita2=(kexi+0.01)*tan(thet2(j));  
   delta1=yita1+sum5;
   delta2=yita2+sum6;
   
   thet1(j)=thet2(j);
   thet2(j)=thet2(j)+phai_step;
   thet2j=thet2(j);
end  
delta1;
delta2;
thet1(j)=thet1j;
thet2(j)=thet1j+phai_step/100;
%thet2(j)=thet1j+phai_step/10;%粗
for k=1:100
  
    sum5=0.0;
    sum6=0.0;
    for i=j-1:-1:1
        thet1(i)=asin(sin(thet1(i+1))*v(i)/v(i+1));      
        thet2(i)=asin(sin(thet2(i+1))*v(i)/v(i+1));
        sum5=sum5+h(i)*tan(thet1(i));
        sum6=sum6+h(i)*tan(thet2(i));
    end
     thet1j=thet1(j);
     thet2j=thet2(j);
   
   yita1=(kexi+0.01)*tan(thet1(j));
   yita2=(kexi+0.01)*tan(thet2(j));
   delta1=yita1+sum5;
   delta2=yita2+sum6;
   thet1(j)=thet2(j);
  thet2(j)=thet2(j)+phai_step/100;
    %thet2(j)=thet2(j)+phai_step/10; %粗
   if delta2>delta
       break
   end
   
end  
kkk=k;
delta_star=(delta1+delta2)/2;
delta_dif=delta_star-delta;
phai_star=(thet1j+thet2j)/2;
 td=kexi/(cos(phai_star)*v(j));
 thet(j)=phai_star;
 for i=j-1:-1:1
     thet(i)=asin(sin(thet(i+1))*v(i)/v(i+1));
     td=td+h(i)/(cos(thet(i))*v(i));
 end
%else
 %   td=sqrt((xa-xb)^2+(ya-yb)^2+(za-zb0)^2)/v(1);
%end
travel_t(4)=td;
kexi
j


%disp('disp:travel_t');
travel_t;
size(travel_t);
length(travel_t);
%disp('moho=')
h_moho;
%travel_t(4)-PmP走时
%travel_t(5)-sPn走时
%travel_t(6)-直达S走时
end
% 震中位置xa,ya,za
% 台站位置xb,yb,zb
% 这里zb为台站的深度（向下为正），zb=-高程（km),计算时h(1)=h(1)-zb;za=za-zb,即震源深度也归算
% 到台站所在平面。
% 公式（Leeand Stewart,1981，参考图23，
% 绕射波：Tr(j,k)为震源在第j层内射线沿第k层顶的绕射波射线走时，eta(j,k)是相应的
% 临界距离
% Tr(j,k)=delta/v(k)-kexi*omega(k,j)+SUM1(i=1:j-1)(h(i)*omega(k,i)/(v(i)*v(
% k)))
%        +2*SUM2(i=j:k-1)(h(i)*omega(k,i)/(v(i)*v(k)))
% eta(j,k)=-kexi*v(j)/omega(k,j)+SUM3(i=1:j-1)(h(i)*v(i)/omega(k,i))
%        +2*SUM4(i=j:k-1)(h(i)*v(i)/omega(k,i))
% 式中j=1,2,...N-1,k=2,3,...N,其中T(j,k)是震源在第j层中，沿第k层顶的绕射射线，
% delta是震中距，由式delta=sqrt((xb-xa)^2+(yb-ya)^2)给出，xa和ya是震中坐标，
% xb和yb是台站坐标。kexi是震源到所在层(第ｊ层)顶的距离，
% kexi=za-(h(1)+h(2)+...+h(ｊ-1)),
% omega(k,i)=sqrt(v(k)^2-v(i)^2),omega(k,j)=sqrt(v(k)^2-v(j)^2)
% za是震源深度。
% 计算直达波走时：Td(j,k)为震源在第j层内的直达波射线走时
% delta1=yita1+SUM(i=j-1:1)(h(i)*tan(thet1(i))
% delta2=yita2+SUM(i=j-1:1)(h(i)*tan(thet2(i))
% 式中sin(thet(i))/v(i)=sin(thet(i+1))/v(i+1)    for 1<=i<j
% yita1=delta*kexi/za;  yita2=delta
% (yita_star-yita1)/(yita2-yita1)=(delta-delta1)/(delta2-delta1)
% 于是 yita_star=(delta-delta1)*(yita2-yita1)/(delta2-delta1)+yita1
% phai_star=atan(yita_star/kexi)
% delta_star=yita_star+SUM(i=j-1:1)(h(i)*tan(thet(i))
% 迭代：
% if delta_star>delta  
%         yita_star=(delta-delta1)*(yita_star-yita1)/(delta_star-delta1)+yita1
% else 
%         yita_star=(delta-delta_star)*(yita2-yita_star)/(delta2-delta_star)
%         +yita_star
%  end
% 检查迭代是否终止：
% 给定小值small,    if abs(delta-delta_star)<amall  迭代结束。
% 
