%三分量单台识别深度震相 2015-05-06
%Виллемсон О.В., Овчинников В.М.，1996， 
%Об определении глубины землетрясений 
%по цифровым данным трехкомпонентной станции ， 
% Физика Земли, 1996.,）
% 梁金仓 张文孝译，1999，用三分向台站的数字化资料确定震源深度,世界地震译丛，1999
clear all
%读入地震信号
sampling_rate=40  %采样率是40s/s
%sampling_rate=20  %采样率是20s/s
sacwave_len=sampling_rate*50
i_fig=100
%2012-05-12阿富汗-塔吉克斯坦边界地区，昆明台
filz='2012.133.23.33.55.0106.IC.KMI.10.BHZ.M.SAC_ASC'
%filz='2012.133.23.33.55.0481.IC.KMI.00.BHZ.M.SAC_ASC'
dist=30.84  % IRIS台站目录
i_start=sampling_rate*50
[ sacwavez ] = read_sac_fun1( filz,dist,sampling_rate,i_fig,sacwave_len,i_start );
file='2012.133.23.33.55.0106.IC.KMI.10.BHE.M.SAC_ASC'
%file='2012.133.23.33.55.0481.IC.KMI.00.BHE.M.SAC_ASC'
[ sacwavee ] = read_sac_fun1( file,dist,sampling_rate,i_fig,sacwave_len,i_start );
filn='2012.133.23.33.55.0106.IC.KMI.10.BHN.M.SAC_ASC'
%filn='2012.133.23.33.55.0481.IC.KMI.00.BHN.M.SAC_ASC'
dist=30.84  % IRIS台站目录
[ sacwaven ] = read_sac_fun1( filn,dist,sampling_rate,i_fig,sacwave_len,i_start );
% 带通滤波器：0.11Hz-3Hz,奈奎斯特频率f0=sampling_rate/2
f0=sampling_rate/2
fu1=0.49
fu=0.50
fh=3
fh2=3.1
f11=fu1/f0
f12=fu/f0
f21=fh/f0
f22=fh2/f0
f=[0,f11,f12,f21,f22,1];m=[0,0,1,1,0,0];
[ wavez_filtered ] =filt( sacwavez,f,m );
[ wavee_filtered ] =filt( sacwavee,f,m );
[ waven_filtered ] =filt( sacwaven,f,m );

sacwave(1,:)=wavez_filtered;
sacwave(2,:)=wavee_filtered;
sacwave(3,:)=waven_filtered;
time=(1:sacwave_len)/sampling_rate;
figure(1)
subplot(3,1,1)
plot(time,sacwave(1,:))
title({filz,'dist=',dist,' deg.  sampling-rate=',sampling_rate,'s/s '})
subplot(3,1,2)
plot(time,sacwave(2,:))
title({file,'dist=',dist,' deg.  sampling-rate=',sampling_rate,'s/s '})
subplot(3,1,3)
plot(time,sacwave(3,:))
title({filn,'dist=',dist,' deg.  sampling-rate=',sampling_rate,'s/s '})
%计算协方差矩阵随时间的变化

% 时间窗长num_win=sampling_rate*time_win
time_win=2  %窗长2s
num_win=sampling_rate*time_win

for i_mov=1:sacwave_len-num_win
for i=1:3
    for j=1:num_win
        wave_win(i,j)=sacwave(i,i_mov-1+j);
    end
end
    c=cov(wave_win');
     [T,E] = eig(c);  %计算协方差矩阵的特征向量和特征值
     Td(i_mov,:,:) = double(T);  %矩阵中的列向量为特征向量,已经归一化，因此是该向量的方向余弦
     Ed(i_mov,3) = double(E(3,3));
     Ed(i_mov,1) = double(E(1,1));  %矩阵中的对角线元素为相应的特征值，由小到大排列
      Ed(i_mov,2) = double(E(2,2));
       
end
i_mov=12*sampling_rate
Ed(i_mov,1) 
Ed(i_mov,2) 
Ed(i_mov,3) 
Td(i_mov,:,:)
eda(:,:)=Ed(i_mov,:)
tda(:,:)=Td(i_mov,:,:)
ep(1)= Td(i_mov,1,3) 
ep(2)= Td(i_mov,2,3)
ep(3)= Td(i_mov,3,3)
g1=zeros(sacwave_len); 
g2=zeros(sacwave_len); 
d1=zeros(sacwave_len);
d2=zeros(sacwave_len);
cf1=zeros(sacwave_len);
cf2=zeros(sacwave_len);
n=8
m=4
for i_mov=num_win:sacwave_len-2*num_win
    for i=-num_win/4:1:num_win/4   % 共（N/2+1）个点，结果使g1会大于1。
        et(1)=Td(i_mov,1,3);
       et(2)=Td(i_mov,2,3); 
       et(3)=Td(i_mov,3,3);
        g1(i_mov)=g1(i_mov)+(1-(Ed(i_mov+i,1)+Ed(i_mov+i,2))/(2*Ed(i_mov+i,3)));
        g2(i_mov)=g2(i_mov)+ep*et';
    end
     for i=-num_win/2:1:num_win/2
         p=0;
         for j=1:3
             p=p+ep(j)*sacwave(j,i_mov-1+i);
         end
         d1(i_mov)=d1(i_mov)+p*p;
         d2(i_mov)=p;
     end
    g1(i_mov)=g1(i_mov)*2/num_win;
    g2(i_mov)=g2(i_mov)*2/num_win;
    d1(i_mov)=d1(i_mov)/num_win;
    cf1(i_mov)=g1(i_mov)^n*g2(i_mov)^m*d1(i_mov);
    cf2(i_mov)=g1(i_mov)^n*g2(i_mov)^m*d2(i_mov);
end
cf1_len=length(cf1)
cf2_len=length(cf2)
time=(1:50*sampling_rate)/sampling_rate;
disp('Ok1')
 figure(2)
subplot(3,1,1)
 plot(time,d1)
 ylabel('d1')
 subplot(3,1,2)
 plot(time,g1)
 ylabel('g1')
 subplot(3,1,3)
 plot(time,g2)
 ylabel('g2')
 disp('OK2')
 
 figure(3)
 subplot(2,1,1)
 plot(time,cf1)
 ylabel('cf1')
 subplot(2,1,2)
 plot(time,cf2)
 ylabel('cf2')
