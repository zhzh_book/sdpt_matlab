function [Pg,sPg,Sg]=travel_Pg_sPg_Sg(model,depth,dis)
%计算Pg、pPg、sPg、Sg走时
%  输入：model 速度模型，dep 震源深度，del 震中距
%返回：Pg、pPg、sPg和Sg走时
m=0;
ascdata = importdata(model, ' ', 3);
m=length(ascdata.data(:,1));
layer_num=ascdata.data(:,1);
layer_top=ascdata.data(:,2);
vp=ascdata.data(:,3);
vs=ascdata.data(:,4);
layer_mark=ascdata.data(:,5);
nlayer=m;


num_j=1;
for i=1:nlayer
    if layer_mark(i)==1
        num_c=i;
    else if layer_mark(i)==2
            num_m=i;
        end
    end
    if depth>layer_top(i)
        num_j=i; %震源所在层序号
    end
end
num_c;
num_m;
num_j;
h_moho=0;
for i=1:num_m-1
    h(i)=layer_top(i+1)-layer_top(i);  %地壳内各厚度
    h_moho=h_moho+h(i);
end
if num_j==1
    r=sqrt(dis^2+depth^2)
    Pg=r/vp(1);
    Sg=r/vs(1);
    cri_angle_s=asin(vs(1)/vp(1));
    cri_d_s=depth*tan(cri_angle_s);
    sPg=depth/(cos(cri_angle_s)*vs(1))+(dis-cri_d_s)/vp(1);
else
    
    Pg=0;
    Sg=0;
    sPg=0;
end
end
    
