function [depth,dif_rms]= pnpgsgdepth_mullayer1( num,pn,pg,sg,del,model,num_dep,step)
% 用Pn-Pg和Sg-Pg定震源深度 2015-03-25
weight=1;
num_dep=35
 xa=0;
    ya=0;
    yb=0;
    zb0=0;
num_dis0=floor(200/step);
num_dis=floor(300/step); % 最大震中距取300km
res2_min=10000;
for i=1:num_dep   %深度搜索
            i
    dep_test(i)=step*i;
    
    res2(i)=0;
for j=1:num
    j
    pn_pg(j)=pn(j)-pg(j);
    sg_pg(j)=sg(j)-pg(j);
    dis_test(j)=del(j);
  
    
   flag=1;  % P波震相 
   [travel_t,etaPb,etaPn,num_c,num_m]=travel_time_ps_new(model,xa,ya,dep_test(i),dis_test(j),yb,zb0,flag); 
   travel_t;
   td=travel_t(1);
   %trpb(j)=travel_t(2);
   trpn=travel_t(3);
   %trpmp(j)=travel_t(4);
   flag=2;   % S震相
   [travel_t_s,etaPb,etaPn,num_c,num_m]=travel_time_ps_new(model,xa,ya,dep_test(i),dis_test(j),yb,zb0,flag); 
    tds=travel_t_s(1);  % Sg走时
    
    pn_pg_cal=trpn-td;
    sg_pg_cal=tds-td;
    
    res2(i)=res2(i)+(pn_pg(j)-pn_pg_cal)^2+weight*(sg_pg(j)-sg_pg_cal)^2;
end
    if res2(i)<res2_min
        res2_min=res2(i);
        dep_index=i;
        h=i*step;
    end
 end

depth=h
dep_index
dif_rms=sqrt(res2_min/num)
plot( dep_test,sqrt(res2/num),'.')
xlabel('震源深度/km')
ylabel('残差均方根/s')
title('Pn-Pu和Sg-Pu观测与计算到时差残差均方根随深度变化曲线')

end

