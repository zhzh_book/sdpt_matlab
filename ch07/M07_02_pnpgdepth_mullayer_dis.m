%计算Pn-Pg列线图 2015-05-10
% 多层速度模型，已知各台站的震中距
clear all
za0=0;
zb0=0;  %台站深度（高程的负值）


za=za0-zb0;%自台站深度算起的震源深度
%model='model_1.asc'
model='model_iasp91.asc'
m=0;
ascdata = importdata(model, ' ', 3);
m=length(ascdata.data(:,1));
layer_num=ascdata.data(:,1);
layer_top=ascdata.data(:,2);
vp=ascdata.data(:,3);
vs=ascdata.data(:,4);
layer_mark=ascdata.data(:,5);

for i=1:m-1
    h(i)=layer_top(i+1)-layer_top(i);  %层厚度
end
h0=h(1);
h(1)=h0-zb0;%从台站位置起计算第一层的厚度
nlayer=m;
% 给定产生绕射波的层顶
num_j=1;
for i=1:nlayer
    if layer_mark(i)==1
        num_c=i;
    else if layer_mark(i)==2
            num_m=i;
        end
    end
    if za>layer_top(i)
        num_j=i;
    end
end
num_c;
num_m;
num_j;
h;

h_moho=0;
for i=1:num_m-1
    h(i)=layer_top(i+1)-layer_top(i);  %地壳内各厚度
    h_moho=h_moho+h(i);
end
h_moho
dep_step=5
num_dep=floor(h_moho/dep_step)
dis_step=1
dis_start=100
num_dis=201
% 试验数据，震源深度18 km
% 计算理论走时，走时加0.5*rand(1),震中距加5*rand(1)的随机误差。

 model='model_iasp91.asc'  
    xa=0;
    ya=0;
    yb=0;
    zb0=0;
    for i=1:num_dep
    za0=i*dep_step;
    for j=1:num_dis
        dis(j)=dis_start+(j-1)*dis_step; 
   flag=1;  % P波震相 
   [travel_t,etaPb,etaPn,num_c,num_m]=travel_time_ps_new(model,xa,ya,za0,dis(j),yb,zb0,flag); 
   travel_t;
   td(i,j)=travel_t(1);
   %trpb(j)=travel_t(2);
   trpn(i,j)=travel_t(3);
   
    end
    end
   figure(1)
   
  plot(dis,td(1,:)-trpn(1,:),dis,td(2,:)-trpn(2,:),dis,td(3,:)-trpn(3,:),dis,td(4,:)-trpn(4,:),.....
       dis,td(5,:)-trpn(5,:),dis,td(6,:)-trpn(6,:),dis,td(7,:)-trpn(7,:));
   legend('h=0km','h=5km','h=10km','h=15km','h=20km','h=25km','h=30km')
   xlabel('震中距/km')
   ylabel('Pd-Pn走时差/s')
   title({'上行直达波Pd与下行折射波Pn走时差列线图，地壳模型',model})
   