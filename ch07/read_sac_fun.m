function [ sacwave,sacwave_len ] = read_sac_fun( fil,dist,sampling_rate,i_fig )
%��SAC�ļ�
%   Detailed explanation goes here
sacdata = importdata(fil, ' ', 30);
m=length(sacdata.data(:,1))
sac(1,:)=sacdata.data(:,1);
sac(2,:)=sacdata.data(:,2);
sac(3,:)=sacdata.data(:,3);
sac(4,:)=sacdata.data(:,4);
sac(5,:)=sacdata.data(:,5);
for i=1:m
    j=(i-1)*5+1;
    sacwave(j)=sac(1,i);
     sacwave(j+1)=sac(2,i);
     sacwave(j+2)=sac(3,i);
     sacwave(j+3)=sac(4,i);
     sacwave(j+4)=sac(5,i);
end

sacwave_len=length(sacwave)
for j=1:sacwave_len
    time(j)=j/sampling_rate;
end
sacwave=sacwave-mean(sacwave);
% figure(i_fig)
% plot(time,sacwave)
% title({fil,'dist=',dist,' deg.  sampling-rate=',sampling_rate,'s/s '})

end

