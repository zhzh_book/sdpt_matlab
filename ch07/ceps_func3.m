function [y_flag,cep_wave]=ceps_func3( sacwave,sampling_rate,f_low,f_high,i_start,i_end,i_fig);
%倒谱分析


%cep_wave=cceps(sacwave(i_start+1:sacwave_len)); %复倒谱 1261:信号到达在63秒
%cep_wave=cceps(sacwave(i_start+1:i_end));
% 按定义计算
num_data=i_end-i_start
win_time=num_data/sampling_rate
f_num_low=f_low*win_time
f_num_high=f_high*win_time
for i=1:num_data
    time_csm(i)=i/sampling_rate;
end
for i=1:num_data
    wave_seg(i)=sacwave(i_start+i);
    time(i)=i/sampling_rate;
    f_req(i)=i/win_time;
end
fft_wave=fft(wave_seg);
log_fft=log(abs(fft_wave));
[ Y,m ] = demmean(log_fft,20 ); %对功率谱的对数进行去趋势
figure(i_fig)
plot(f_req,log_fft,'b-');hold on
axis([0 15 -20 20])
plot(f_req,Y,'r:');hold on
plot(f_req,m,'b:');hold on
Y1=Y;
for i=1:f_num_low
    Y1(i)=0;
end
for i=f_num_high:num_data
    Y1(i)=0;
end
plot(f_req,Y1,'g-')
cep_wave=abs(fft(Y1));


%cep_wave=ifft(abs(log(abs(fft_wave))));
%cep_wave=ifft(abs(log(abs(fft_wave))));
%cep_wave=abs(cceps(wave_seg));
%len_cep=length(cep_wave)
%abs_cep_wave=rceps(wave_seg);
%hil_cep_wave=cceps(abs(hilbert(wave_seg)));


y_flag=0;
end

