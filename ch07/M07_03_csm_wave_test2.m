%  倒谱叠加方法试验，读SAC波形文件, 生成书中采用的图件
clear all

%2012-05-12阿富汗-塔吉克斯坦边界地区，昆明台
fil='2012.133.23.33.55.0106.IC.KMI.10.BHZ.M.SAC_ASC'
dist=30.84  % IRIS台站目录
sampling_rate=40  %采样率是40s/s
i_fig=1  % 生成图号1、2、3
i_start=0*sampling_rate  % 计算倒谱的起点=i_start+1
i_end=600*sampling_rate
[ sacwave,sacwave_len ] = read_sac_fun( fil,dist,sampling_rate,i_fig );
% 设计滤波器
% 带通滤波器：0.5Hz-2Hz,奈奎斯特频率f0=sampling_rate/2
f0=sampling_rate/2
fu1=0.4
fu=0.5
fh=2.0
fh2=2.2
% 带通滤波器：0.05Hz-0.5Hz,奈奎斯特频率f0=sampling_rate/2
fu1=0.1
fu=0.11
fh=3
fh2=3.1
f11=fu1/f0
f12=fu/f0
f21=fh/f0
f22=fh2/f0
f=[0,f11,f12,f21,f22,1];m=[0,0,1,1,0,0];
% 低通滤波器：0.05Hz-0.5Hz

fu=1
fh=1

f12=fu/f0
f21=fh/f0

%f=[0,f12,f21,1];m=[0,1,1,0];
[ wave_filtered ] =filt( sacwave,f,m );
fft_ori=fft(sacwave);
fft_fil=fft(wave_filtered);
figure(99)
subplot(2,1,1)
loglog(abs(fft_ori));hold on
subplot(2,1,2)
loglog(abs(fft_fil))
%倒谱叠加
num_csm=8
num_csm=20
i_start=61.8*sampling_rate
i_end=i_start+30*sampling_rate
i_step=12*sampling_rate
num_data=30*sampling_rate
cep_csm_sum=zeros(30*sampling_rate);
cep_csm_sum(10)
cep_csm_mul=ones(30*sampling_rate);
for i=1:40
    cep_csm_mul(i)=0;
end
cep_csm_mul(10)
len_cep_csm_sum=length(cep_csm_sum)
for i=1:num_data
    time_csm(i)=i/sampling_rate;
end

% -------------

f_low=0.5   %对数功率谱频率窗 0.5Hz-2Hz
f_high=2
for i=1:num_csm
    i_fig=100+i
    [y_flag,cep_wavei]=ceps_func3( wave_filtered,sampling_rate,f_low,f_high,i_start,i_end,i_fig);
    for j=1:len_cep_csm_sum
   cep_csm_sum(j)=cep_csm_sum(j)+cep_wavei(j);
    end
   figure(i)
   plot(time_csm,cep_csm_sum);hold on
   axis([ 0 15 -10 40])
    x=time_csm;
   noisy=cep_csm_sum;
   [ Y,m ] = demmean(noisy,40 );
   plot(time_csm,Y,'r');hold on
   plot(time_csm,m,'b:')
   title({'倒谱叠加,段数=',i})
   xlabel('倒频率/s')
   
   %for j=1:40
    %   cep_wavei(j)=0;
   %end
   %scal=mean(abs(cep_wavei));
   %for j=1:len_cep_csm_sum
   %cep_csm_mul(j)=cep_csm_mul(j)*cep_wavei(j)/scal;
   %end
   %figure(num_csm+i)
   %plot(time_csm,cep_csm_mul);
    %x=time_csm;
   %noisy=cep_csm_mul;
   %[ Y,m ] = demmean(noisy,20 )
   %plot(time_csm,Y,'r');hold on
   %plot(time_csm,m,'b:')
   i_start=i_start+i_step;
   i_end=i_end+i_step;
   i_fig=i_fig+3;
end
