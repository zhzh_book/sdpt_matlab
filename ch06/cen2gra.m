function [ gra_d ] = cen2gra(cen_d )
%地心纬度换算成地理纬度
%大地坐标系WGS85，扁率f=1/298.257223563
%输入：地心纬度（单位为度，北纬为正，南纬为负）gra_d
%返回：地理纬度（单位为度，北纬为正，南纬为负）cen_d
f=1/298.257223563;
cen_r=cen_d*pi/180;
gra_r=atan(tan(cen_r)/((1-f)^2));
gra_d=gra_r*180/pi;
end

