function stations=getStns(fn)
    stns = struct('net',{},'stn',{},'lat',{},'lon',{},'hei',{});
    stations = struct('net',{},'stn',{},'lat',{},'lon',{},'hei',{});
    stanInfo = importdata(fn);
    stanNum = length(stanInfo.rowheaders);
    for i=1:stanNum
        nst = char(stanInfo.textdata(i));
        net = nst(1:2);
        stn = nst(1:5);
        lat = stanInfo.data(i,1);
        lon = stanInfo.data(i,2);
        hei = stanInfo.data(i,3) ;
        stns(i) = struct('net',net,   ...
                                 'stn',stn,  ...
                                 'lat',lat,    ...
                                 'lon',lon,  ...
                                 'hei',hei   ...
                                 );
    end
    stations = stns;