% 用单纯形法定位震源位置和发震时刻
% 2010-07-03 赵仲和
% 本程序SimpLoc.m为主程序
% 台站坐标文件名'station25'
% 程序中调用getStns.m读台站坐标文件
% 程序中调用ll2xy1.m将经纬度换算成x,y坐标（使用的是最简单公式）
% 计算中调用函数arrival_rms,计算到时残差均方根作为搜索的目标函数
% 函数arrival_rms中调用函数travel_time，计算分层模型的直达波和绕射波走时
% 震相数据文件SimpLoc.pha
% 使用分层地壳模型model_z(试验时使用单层模型），其中分别给出各层中的P波和S波速度
% 其中的单纯形法取自《精通MATLAB最优化计算》
%（龚纯 王正林编著，电子工业出版社，2009），137-141页
% 读入台站坐标
clear all
model='model_z'   %速度模型文件
stations=getStns('station25');  %台站文件，一个有25个台站的假想地方性小台网
st_num=length(stations)
for i=1:st_num
    st_lat(i)=stations(i).lat;
    st_lon(i)=stations(i).lon;
    st_ele(i)=stations(i).hei;
    st_name(i,:)=stations(i).stn;
end
st_name

% 台站经纬度变换成X、Y坐标
% 取台网中心作为直角坐标原点,sx正向北，sy正向东
ori_lat=mean(st_lat)
ori_lon=mean(st_lon)
for i=1:st_num
    [sta(1,i),sta(2,i)]=ll2xy1(st_lat(i),st_lon(i),ori_lat,ori_lon);
    sta(3,i)=st_ele(i);
end
sta
figure(1);
plot(sta(1,:),sta(2,:),'o');
% 读入震相文件中的第1行(震相数据文件格式为hypoDD中使用的震相数据文件格式）
fd=fopen('SimpLoc.pha','r');  %震相数据文件SimpLoc3150.pha只包含台网东部10个台的P、S到时
s1=fscanf(fd,'%s/')    % #   
oyear=fscanf(fd,'%i/')   % 发针时刻年
omanth=fscanf(fd,'%i/')  % 月
oday=fscanf(fd,'%i/')    % 日
ohour=fscanf(fd,'%i/')   % 时
omin=fscanf(fd,'%i/')    % 分
osec=fscanf(fd,'%f/')    % 秒
olat=fscanf(fd,'%f/')    % 震中纬度
olon=fscanf(fd,'%f/')    % 震中经度
[ox,oy]=ll2xy1(olat,olon,ori_lat,ori_lon)
odepth=fscanf(fd,'%f/')  % 震源深度（km）
nerr=fscanf(fd,'%f/')    % 北南向水平误差（km）
eerr=fscanf(fd,'%f/')    % 东西向水平误差（km）
derr=fscanf(fd,'%f/')    % 震源深度误差（km） 
rms=fscanf(fd,'%f/')     % 残差均方根(sec）
event_id=fscanf(fd,'%i/')  % 事件标识号
% 读入震相数据
    ph_i=fscanf(fd,'%s %f %f %s',[8,inf]);
    ph_i=ph_i'  ; % 矩阵转置
ph_i  ;  %显示
ph_num=length(ph_i)   %震相到时数据个数
  for i=1:ph_num
    s1=char(ph_i(i,1));
    s2=char(ph_i(i,2));
    s3=char(ph_i(i,3));
    s4=char(ph_i(i,4));
    s5=char(ph_i(i,5));
    ph_sta(i,1:5)=[s1 s2 s3 s4 s5];
    ph_ori(1,i)=ph_i(i,6);
    ph(2,i)=ph_i(i,7);
    %当前只由P和S震相
    ph_phase(i)=char(ph_i(i,8));
    C=strncmp(ph_phase(i),'P',1);
    if C==1 
        ph(3,i)=1;  % P波代码为1
    end
    C=strncmp(ph_phase(i),'S',1);
    if C==1 
        ph(3,i)=2 ; % S波代码为2
    end
end
ph_sta;     %震相数据中的台站代码(5个字母)
% 对合成到时数据加入随机误差
rr=0.2  % 随机误差水平（秒）
rr=0.1
rr=0.0
%rr=0
for i=1:ph_num 
    R = normrnd(0,1);
    ph(1,i)=ph_ori(1,i)+rr*R;
end
fclose(fd);
% 找出震相数据对应的台站文件中的台站脚标
 for i=1:ph_num
     for j=1:st_num
         C=strcmp(ph_sta(i,1:5),st_name(j,1:5));
         if C==1
               ph(4,i)=j;
               break
         end
     end
 end
 ph;
 % 下例中去掉了西面的3排台站（已在数据文件中去掉了西边的15个台）
%for i=1:30
   %ph(2,i)=0;
%end
% 初值的选取
% 方法一：P到时最小的台站的X、Y坐标作为初始震中的X、Y坐标；
% 最小P到时-2秒作为发针时刻初值，震源深度初值为10km
[ph_temp,ph_index]=sort(ph(1,:));   %本次试算只用了第31-50号震相数据
ph_index(1)  % 当前版本中P到时和S到时是混排的,待改
i=ph_index(1)
j=ph(4,i);
ini_lat=st_lat(j);
ini_lon=st_lon(j);
ini_x=sta(1,j);
ini_y=sta(2,j);
figure(2);
for i=1:ph_num
if ph(2,i)>0.5
plot(sta(2,ph(4,i)),sta(1,ph(4,i)),'+');hold on;
end
end
ini_t=ph(1,i)-2.0
ini_z=10 % 震源深度初值
% 建立单纯形法的初始单纯形(随机选取)
%X(1,1)=ini_x;
%X(2,1)=ini_y;
X(1,1)=-2.0;  %固定x,y
X(2,1)=-2.0;
X(3,1)=ini_z;
X(4,1)=ini_t;
for i=2:5
    for j=1:4
    %X(j,i)=X(j,1)+randn*5*i;
    X(j,i)=X(j,1)+5*i;   %不用随机数
    end
   % for j=1:2
   % X(j,i)=-2.0; % 固定x,y坐标
   % end
   % X(3,i)=ini_z; % 固定深度  
   
end
% 开始单纯形法搜索
%function [x,minf] = minSimpSearch(f,X,alpha,sita,gama,beta,var,eps)
format long;
eps = 1.0e-4;   % 精度（结束判据）
alpha=1.2;  %反映系数(a>1) 
sita=0.5;   %紧缩系数(0<s<1)
gama=2.0;   %扩展系数(g>1)
beta=0.3;   %收缩系数(0<b<1)
begib=0
N = size(X);
n = N(2);
FX = zeros(1,n);

while 1
    for i=1:n
  %      FX(i) = Funval(f,var,X(:,i));
  FX(i)=arrival_rms(model,X(:,i),sta,ph) ;  
  %计算在初始单纯形各定点的目标函数(到时残差加权均方根)
    end
    [XS,IX] = sort(FX);   % 将单纯形的顶点按目标函数值的大小重新排序(由小到大) 
    Xsorted = X(:,IX);    % 排序后的单纯形顶点值(x,y,z,t)

    px = sum(Xsorted(:,1:(n-1)),2)/(n-1);  % 单纯形中心的（x,y,z,t）
    px;
   % Fpx = Funval(f,var,px);
   Fpx=arrival_rms(model,px,sta,ph); % 单纯形中心的目标函数值
  % 精度判断：如果得到的单纯形个顶点的目标函数与其中心点的目标函数的根均方差足够小，则结束：
  SumF = 0;
    for i=1:n
        SumF = SumF + (FX(IX(i)) - Fpx)^2;
    end
    SumF = sqrt(SumF/n); 
    if SumF <= eps
        x = Xsorted(:,1);
        break;   % 单纯形法搜索结束
    else     % 否则：
        x2 = px + alpha*(px - Xsorted(:,n)); % 将中心点向外反射
        x2;
        %fx2 = Funval(f,var,x2);
        fx2=arrival_rms(model,x2,sta,ph);
        if fx2 < XS(1)
            x3 = px + gama*(x2 - px);   % 中心点位置扩展
            x3;
            %fx3 = Funval(f,var,x3);
            fx3=arrival_rms(model,x3,sta,ph);
            if fx3 < XS(1)
                Xsorted(:,n) = x3;
                X = Xsorted;
                continue;
            else
                Xsorted(:,n) = x2;
                X = Xsorted;
                continue;
            end
        else
            if fx2 < XS(n-1)
                Xsorted(:,n) = x2;
                X = Xsorted;
                continue;
            else
                if fx2 < XS(n)
                    Xsorted(:,n) = x2;
                end
                x4 = px + beta*(Xsorted(:,n) - px);  % 中心点位置压缩
                x4;
                %fx4 = Funval(f,var,x4);
                fx4=arrival_rms(model,x4,sta,ph);
                %FNnew = Funval(f,var,Xsorted(:,n));
                FNnew=arrival_rms(model,Xsorted(:,n),sta,ph);
                if fx4 < FNnew
                    Xsorted(:,n) = x4;
                    X = Xsorted;
                    continue;
                else
                    x0 = Xsorted(:,1);
                    for i=1:n
                        Xsorted(:,j) = x0 + sita*(Xsorted(:,j) - x0);
                    end
                end
            end
        end
    end
    X = Xsorted;
end
%minf = Funval(f,var,x)
minf=arrival_rms(model,x,sta,ph);
x
format short;
% 单纯形搜索结束
plot(ini_y,ini_x,'r+');hold on;
plot(x(2),x(1),'b*');hold on;
plot(oy,ox,'ro');
title('红圆圈—合成震中；红十字—定位初值；蓝星—定位结果')

