%Powell方法地震定位
%同M06_03_PowellLoc2_di.m
%给定搜索深度范围和步长，对每个深度用鲍威尔方法搜索最佳震中位置的x、y及目标函数值，
%对不同深度的鲍威尔搜索结果，取其目标函数最小的结果为最终解
%对各台到时残差取平均值，作为发震时刻
%当把使用台站、合成数据加入的误差水平设定成与M06_03_searchLoc.m中的设定一致时，可将搜索法画出的目标函数等值线
%与鲍威尔方法的搜索路径叠加显示。
%2017-09-20记
%数据准备：本段同M06_03_searchLoc.m
clear all
c=clock;
year=num2str(c(1),4);
manth=num2str(c(2),2);
day=num2str(c(3),2);
hour=num2str(c(4),2);
minute=num2str(c(5),2);
second=num2str(c(6),3);

model='model_z'   %速度模型文件
stations=getStns('station25');  %台站文件，一个有25个台站的假想地方性小台网
st_num=length(stations)
for i=1:st_num
    st_lat(i)=stations(i).lat;
    st_lon(i)=stations(i).lon;
    st_ele(i)=stations(i).hei;
    st_name(i,:)=stations(i).stn;
end
st_name;

% 台站经纬度变换成X、Y坐标
% 取台网中心作为直角坐标原点,sx正向北，sy正向东
ori_lat=mean(st_lat)
ori_lon=mean(st_lon)
for i=1:st_num
    [sta(1,i),sta(2,i)]=ll2xy1(st_lat(i),st_lon(i),ori_lat,ori_lon);
    sta(3,i)=st_ele(i);
end
sta;
figure(1);
%plot(sta(1,:),sta(2,:),'o');
% 读入震相文件中的第1行(震相数据文件格式为hypoDD中使用的震相数据文件格式）
fd=fopen('SimpLoc.pha','r');  %震相数据文件SimpLoc.pha
%fd=fopen('syn2005001.pha','r');  
% 震相数据对应于假想震源x=-2km,y=-2km,z=13km,t0=30sec
s1=fscanf(fd,'%s/')    % #   
oyear=fscanf(fd,'%i/')   % 发针时刻年
omanth=fscanf(fd,'%i/')  % 月
oday=fscanf(fd,'%i/')    % 日
ohour=fscanf(fd,'%i/')   % 时
omin=fscanf(fd,'%i/')    % 分
osec=fscanf(fd,'%f/')    % 秒
olat=fscanf(fd,'%f/')    % 震中纬度
olon=fscanf(fd,'%f/')    % 震中经度
[ox,oy]=ll2xy1(olat,olon,ori_lat,ori_lon)
odepth=fscanf(fd,'%f/')  % 震源深度（km）
nerr=fscanf(fd,'%f/')    % 北南向水平误差（km）
eerr=fscanf(fd,'%f/')    % 东西向水平误差（km）
derr=fscanf(fd,'%f/')    % 震源深度误差（km） 
rms=fscanf(fd,'%f/')     % 残差均方根(sec）
event_id=fscanf(fd,'%i/')  % 事件标识号
% 读入震相数据
    ph_i=fscanf(fd,'%s %f %f %s',[8,inf]);
    ph_i=ph_i';   % 矩阵转置
ph_i;    %显示
ph_num=length(ph_i)   %震相到时数据个数
  for i=1:ph_num
    s1=char(ph_i(i,1));
    s2=char(ph_i(i,2));
    s3=char(ph_i(i,3));
    s4=char(ph_i(i,4));
    s5=char(ph_i(i,5));
    ph_sta(i,1:5)=[s1 s2 s3 s4 s5];
     ph_ori(1,i)=ph_i(i,6);
    ph(2,i)=ph_i(i,7);
    %当前只由P和S震相
    ph_phase(i)=char(ph_i(i,8));
    C=strncmp(ph_phase(i),'P',1);
    if C==1 
        ph(3,i)=1;  % P波代码为1
    end
    C=strncmp(ph_phase(i),'S',1);
    if C==1 
        ph(3,i)=2;  % S波代码为2
    end
end
ph_sta ;    %震相数据中的台站代码(5个字母)
fclose(fd);
% 找出震相数据对应的台站文件中的台站脚标
 for i=1:ph_num
     for j=1:st_num
         C=strcmp(ph_sta(i,1:5),st_name(j,1:5));
         if C==1
               ph(4,i)=j;
               break
         end
     end
 end
 % 对合成到时数据加入随机误差
rr=0.1  % 随机误差水平（秒）
%rr=0.1
%rr=0.0
rr=0
for i=1:ph_num 
    R = normrnd(0,1);
    ph(1,i)=ph_ori(1,i)+rr*R;
end
%抛台为试验不同台站分布对定位结果的影响
% 到时数据分布: 单数序号为P,双数序号为S
% 每对数字所在位置为台站位置(自下而上:北,向右:东
% 9,10   19,20   29,30    39,40   49,50
% 7,8   .....
% 5,6   .....
% 3,4   .....
% 1,2    11,12   21,22   31,32    41,42
% 下例中去掉了下面的3排台站
%for i=1:6
 %   ph(2,i)=0;
  %  ph(2,i+10)=0;
   % ph(2,i+20)=0;
    %ph(2,i+30)=0;
    %ph(2,i+40)=0;
%end
% ------------------
% 下例中去掉了西面的3排台站
for i=1:30
   ph(2,i)=0;
end
%去掉东面的两排
%for i=11:50
  %  ph(2,i)=0;
%end
%for i=1:10  %去掉S到时
% ph(2,i*2)=0;
%end
%for i=1:4
 %   ph(2,i)=0;
%end
%------------------------------------------------------------------------------------
%降维


%z待定变量为3(x,y,t)
m=2
n=m+1
%近台初值
%P到时最小的台站的X、Y坐标作为初始震中的X、Y坐标；
% 最小P到时-2秒作为发震时刻初值
[ph_temp,ph_index]=sort(ph(1,:));  % 该函数实现数组中元素从小到大排序
% ph_temp数组中给出排序后的数据，ph_index数组给出排序后元素在原来数组中的顺序号
ph_index(1);  % 当前版本中P到时和S到时是混排的,待改
% 取权重不为零的最小到时对应的台站作为初始震中
for ii=1:ph_num
i=ph_index(ii);
if ph(2,i)>0
j=ph(4,i);
break
end
end
ini_lat=st_lat(j);
ini_lon=st_lon(j);
ini_x=sta(1,j);
ini_y=sta(2,j);
ini_t=ph(1,i)-2.0;
ini_t=ph(1,i)-5.0; %试发震时刻初值对结果的影响
%给定震源深度
d_delt=2 %深度间隔1km
d_min=9 %起始搜索深度
d_max=17%结束搜索深度
d_num=floor((d_max-d_min)/d_delt)+1%搜索深度点数
eps=0.1
eps_rms=0.1
P=[1 0 ;0 1 ]  %Powell方法初始搜索方向
stepJT=0.05%进退法寻找极小值区间的搜索步长0.5km
mind=100; %大数，作为搜索深度的起始min
for d_i=1:d_num
    dep(d_i)=d_min+(d_i-1)*d_delt;
  % ini_z=dep(d_i); % 震源深度初值
   x0=[ini_x ini_y] ;   %近台初值
   search_P(:,1,d_i)=x0;
   s_num(d_i)=1;
%-----------------------------------------------------------------------------------------------------
%套用龚纯 王正林《精通Matlab最优化计算》，p141-144
%[x,minf] = minPowell(f,x0,P,var,eps)
%目标函数：f
%初始搜索点：x0
%线性无关的初始向量组：P
%在地震定位搜索中，以北、东为x和y，
% P=[1 0;0 1] ([第1向量的x分量 第2向量的x分量；第1向量的x分量 第2向量的x分量
%自变量向量：var
%精度：eps
%目标函数取最小值时的自变量值：x
%目标函数的最小值：minf
i_dir=0;
while 1
    PP=P;
    y = zeros(size(P));
    i_dir=i_dir+1;
    y(:,1) = x0;
    
    for i=1:n-1
       %dirJT=P(:,i)/norm(P(:,i));
       dirJT=P(:,i);
       [a,b] = minJT_P2(model,y(:,i),dep(d_i),sta,ph,dirJT,stepJT);
        [tl, minf1] = minHJ_P2(model,sta,dep(d_i),ph,a,b,eps)
        y(:,i+1) = tl;
        s_num(d_i)=s_num(d_i)+1;
        search_P(:,s_num(d_i),d_i)=tl;
    end
    yn=y(:,n);
    y1=y(:,1);
    P(:,n) = y(:,n) - y(:,1);
    normP=norm(P(:,n));
   % if norm(P(:,n)) <= eps  && minf<=eps_rms %停止搜索判据
         if norm(P(:,n)) <= eps  
        x = y(:,n);
        break;
    else
        for j=1:n
            hypo=[y(1,j) y(2,j) dep(d_i) 0];
            [FY(j)  hypo]=arrival_rms_P2(model,hypo,sta,ph);
        end
        maxDF = -inf;
        m = 0;
        for j=1:n-1
            df = FY(j) - FY(j+1);
            if df > maxDF
                maxDF = df;
                m = j+1;
            end
        end
        yy=2*y(:,n)-y(:,1);
        hypo=[yy(1) yy(2) dep(d_i) 0];
        [tmpF hypo] =arrival_rms_P2(model,hypo,sta,ph);
        fl = FY(1) - 2*FY(n) + tmpF;
        if fl<2*maxDF
     %       dirJT=P(:,n)/norm(P(:,n));
            dirJT=P(:,n);
             [a,b] = minJT_P2(model,yy ,dep(d_i),sta,ph,dirJT,stepJT);
       [ tl, minf1 ]= minHJ_P2(model,sta,dep(d_i),ph,a,b,eps);
            x0 = tl;
            P(:,m:(n-1)) = P(:,(m+1):n);
            s_num(d_i)=s_num(d_i)+1;
        search_P(:,s_num(d_i),d_i)=tl;
        else
            x0 = y(:,n);
        end
    end
end
d_i
hypo=[x(1) x(2) dep(d_i) 0]
[mintemp, hypo]= arrival_rms_P2(model,hypo,sta,ph);
minf (d_i)=mintemp
hypo
if minf(d_i)<mind
    di=d_i
    mind=minf(d_i)
    hypoi=hypo
end
xy(:,d_i)=x
s_num(d_i)=s_num(d_i)+1;
search_P(1,s_num(d_i),d_i)=hypo(1);
search_P(2,s_num(d_i),d_i)=hypo(2);
snum(d_i)=s_num(d_i);
end
di1=di
dep1=dep
minf11=minf
hypoi1=hypoi
%发震时刻
searchP1=search_P(1,:,:)
searchP2=search_P(2,:,:)
figure(1)
for i=1:snum(di)-1
    line([search_P(2,i,di) search_P(2,i+1,di)],[search_P(1,i,di) search_P(1,i+1,di)]);hold on
    plot(search_P(2,i,di), search_P(1,i,di),'r+');hold on
end
plot(search_P(2,snum(di),di),search_P(1,snum(di),di),'r+');
%-------------计时---------------------------------------------------
c1=clock;
year1=num2str(c1(1),4);
manth1=num2str(c1(2),2);
day1=num2str(c1(3),2);
hour1=num2str(c1(4),2);
minute1=num2str(c1(5),2);
second1=num2str(c1(6),3);
stepJT1=stepJT
str=['Powell法定位，起始时间  ',year,'-',manth,'-',day,' ',hour,':',minute,':',second];
disp(str)
str1=['Powell法定位，结束时间  ',year1,'-',manth1,'-',day1,' ',hour1,':',minute1,':',second1];
disp(str1)