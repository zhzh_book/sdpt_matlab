function [rms,hypo]=arrival_rms_P2(model,hypo,sta,ph)
%计算目标函数值
% 目标函数为各到时数据的残差加权均方根
% 给定:震源hypo(4):x,y,z,t,在本函数中没有真正给出发震时刻，
%要由到时、走时和残差计算发震时刻t0，这不同于给定发震时刻计算rms的函数arrival_rms
%给定台站坐标sta(:,3),速度模型文件名（例如model_z，单层模型）
% 到时数据文件ph(1,i)：到时；ph(2,i):权；ph(3,i): 1-P到时,2-S到时
sumr=0;
sumw=0;
hypo;

ph_num=length(ph);
t0=0;
t0_num=0;
for i=1:ph_num
    j=ph(4,i);   % 该到时的台站序号
    if ph(2,i)>0.5
    [td(i),tr]=travel_time(model,hypo(1),hypo(2),hypo(3),sta(1,j),sta(2,j),sta(3,j),ph(3,i));
     if ph(3,i)==1 || ph(3,i)==2    %直达波走时
        t0=t0+(ph(1,i)-td(i))*ph(2,i);
        t0_num=t0_num+1;
     end
    end
end
t0num=t0_num;
  t0=t0/t0_num;
  %t0=hypo(4)
  %t0=30  %临时固定
for i=1:ph_num
    j=ph(4,i);   % 该到时的台站序号
        if ph(3,i)==1 || ph(3,i)==2
              sumr=sumr+((ph(1,i)-t0-td(i))*ph(2,i))^2;
    end
    sumw=sumw+ph(2,i)^2;
end
sumr;
sumw;
hypo(4)=t0;
rms=sqrt(sumr/sumw);
