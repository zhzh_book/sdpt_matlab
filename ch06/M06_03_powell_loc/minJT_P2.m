function  [x1,x3] = minJT_P2(model,x0,depth,sta,ph,dirJT,stepJT);
%model:地壳速度模型
%x0：x、y坐标，一维搜索的起点
%sta：台站坐标
%ph：震相到时、权、震相代码、台站编号
%dirJT：一维搜索的方向向量
%stepJT：寻找极小值所在区间的前进步长
%minx，maxx：返回极小值所在区间边界
x0;
x1 = x0;
hypo1=[x1(1) x1(2) depth 0];
k = 0;
h = stepJT;
while 1
    x4 = x1 + h*dirJT;
    hypo4=[x4(1) x4(2) depth 0];
    k = k+1;
   f4= arrival_rms_P2(model,hypo4,sta,ph);
   f1= arrival_rms_P2(model,hypo1,sta,ph);
    if f4 < f1
        x2 = x1;
        x1 = x4;
        f2 = f1;
        f1 = f4;
        h = 2*h;
    else
        if k==1
            h = -h;
            x2 = x4;
            f2 = f4;
        else
            x3 = x2;
            x2 = x1;
            x1 = x4;
            break;
        end
    end
end
end

