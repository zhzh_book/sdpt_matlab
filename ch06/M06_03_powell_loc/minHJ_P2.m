function  [x minf1] = minHJ_P2(model,sta,depth,ph,a,b,eps)
%format long;
l= a + 0.382*(b-a);
u = a + 0.618*(b-a);
k=1;
tol = b-a;
hypol=[l(1) l(2) depth 0];  %l是英字母，不是数字1！
hypou=[u(1) u(2) depth 0];  %l是英字母，不是数字1！
while norm(tol)>eps && k<100000
   [fl  hypol]= arrival_rms_P2(model,hypol,sta,ph)
   [ fu hypou]= arrival_rms_P2(model,hypou,sta,ph)
    if fl > fu
        a = l;
        l = u;
        u = a + 0.618*(b - a);
    else
        b = u;
        u = l;
        l = a + 0.382*(b-a);
    end
    k = k+1;
    tol = abs(b - a);
end
if k == 100000
    disp('找不到最小值！');
    x = NaN;
    minf = NaN;
    return;
end
x = (a+b)/2
hypo=[x(1) x(2) depth 0];
[minf1  hypo]=  arrival_rms_P2(model,hypo,sta,ph)
format short;


end

