function [ ini_x,ini_y,ini_t,ini_z] = inglada(sta,ph,vp,vpvs)
%Inglada方法求定位初值
% 对直达P波构成两两相差方差组
p_num=0;
s_num=0;
ph_num=length(ph);
for i=1:ph_num
    if  ph(3,i)==1 & ph(2,i)>0   %可用的P到时
          p_num=p_num+1;
          xp(p_num)=sta(1,ph(4,i));
          yp(p_num)=sta(2,ph(4,i));
          tp(p_num)=ph(1,i);
    end
     if  ph(3,i)==2 & ph(2,i)>0   %可用的S到时
          s_num=s_num+1;
          xs(s_num)=sta(1,ph(4,i));
          ys(s_num)=sta(2,ph(4,i));
          ts(s_num)=ph(1,i);
    end
end
pnum=p_num;
snum=s_num;
ing_num=0;
vs=vp/vpvs;
if pnum>1
    for i=2:p_num
      ing_num=ing_num+1;
      xyt(ing_num,1)=xp(i)-xp(i-1);
       xyt(ing_num,2)=yp(i)-yp(i-1);
      xyt(ing_num,3)=-vp^2*(tp(i)-tp(i-1));
      b_ing(ing_num)=0.5*((xp(i)^2+yp(i)^2)-(xp(i-1)^2+yp(i-1)^2)-vp^2*(tp(i)^2-tp(i-1)^2));
    end
end
if snum>1
     for i=2:s_num
      ing_num=ing_num+1;
       xyt(ing_num,1)=xs(i)-xs(i-1);
       xyt(ing_num,2)=ys(i)-ys(i-1);
      xyt(ing_num,3)=-(vs)^2*(ts(i)-ts(i-1));
      b_ing(ing_num)=0.5*((xs(i)^2+ys(i)^2)-(xs(i-1)^2+ys(i-1)^2)-vs^2*(ts(i)^2-ts(i-1)^2));
    end
end
ingnum=ing_num
if ing_num>2
Xing=xyt\b_ing';
ini_x=Xing(1);
ini_y=Xing(2);
ini_t=Xing(3);
for i=1:p_num
    z(i)=vp^2*(tp(i)-ini_t)^2-(xp(i)-ini_x)^2-(yp(i)-ini_y)^2;
end
ini_z=mean(z);
ini_z=sqrt(ini_z);
end
if ing_num<3
    disp('震相数据太少，Inglada方法失败')
end
    
end

