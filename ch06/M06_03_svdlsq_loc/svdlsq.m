% 奇异值分解最小二乘法求解线性方程组A*X=b
% A 为mxm系数矩阵，X为待求n维矢量，b为m维矢量
% 求X使目标函数rms达到极小。
% rms=sqrt(sum(res(i)*res(i))/m).
function [X,V,isigma]=svdlsq(A,b,eps)
[m,n]=size(A)
nb=size(b)
[U,S,V]=svd(A); % 求矩阵A的SVD分解，A=U*S*V'
A1=U*S*V'; % 检查分解结果
UtU=U'*U ;  % 应为mxm单位矩阵
VtV=V'*V;   % 应为nxn单位矩阵
VVt=V*V';
AAt=A*A';
AtA=A'*A;
[Vaat,Daat]=eig(AAt);
[Vata,Data]=eig(AtA);
AAt*Vaat(:,5);    %   计算BX=aX 的左侧
Daat(5,5)*Vaat(:,5);   % 计算BX=aX的右侧，二者相等，确认U的各列为AAT的本证矢量
for i=1:n
    u(i)=sqrt(Data(i,i));
end    % 得到的u(i)是ATA的本征值的非负平方根，这正是SVD得到的S的对角线元素
q=U'*b;
n=size(V);
k=0;
for i=1:n
    sigma(i)=S(i,i);
    if sigma(i)>eps
        isigma(i)=1/sigma(i);
        k=k+1;
    else
        isigma(i)=0;
    end
end
k;
for i=1:k
    p(i)=isigma(i)*q(i);
end
for i=k+1:n
    p(i)=0;
end
    X=V*p';


