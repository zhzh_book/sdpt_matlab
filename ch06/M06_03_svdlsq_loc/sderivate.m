%函数sderivate计算震源A处对台站B的走时偏导数
function Ai=sderivate(model,xa,ya,za0,xb,yb,zb0,flag)
%计算分层地壳中的地震波走时偏导数
% 给定速度模型
% h(i), for i=1,2,...,N为第i层的厚度，其中第N层为地壳下的上地幔；v(i)为每一层中的波速，
% 可给定为P波速度或S波速度。
% 速度模型文件 model.txt：例如中国平均2层地壳模型
% ( 中国地区地震走时表,国家地震局地球物理研究所编，《震相走时便查表》，地震出版社，1980)
%  3    (层数N)  
%  (层号  层厚度(km)  P波速度（km/s) S波速度（km/s))
%  1     16         5.71           3.40
%  2     24         6.53           3.77
%  3     300       7.97           4.45 
%function [td,tr]=travel(x,xa,ya,za0,xb,yb,zb0,flag)
model;
fid=fopen(model,'r');
a=fscanf(fid,'%d %f %f %f',[4 inf]);
fclose(fid);
a=a';
[m,n]=size(a);
nlayer=m;
for i=1:nlayer
 h(i)=a(i,2);
 vp(i)=a(i,3);
 vs(i)=a(i,4);
end
h0(1)=h(1);
h(1)=h(1)-zb0; % 用台站高程修正第1层厚度
vp;
vs;
xa;
ya;
za=za0-zb0; % 在以台站为0平面时的震源深度
xb;
yb;
zb=0.0; % 以台站为0平面
%求震源所在层号j
% 给定产生绕射波的层顶
k=nlayer; %Moho界面绕射波
%  为便于计算Moho面以下地震的直达波走时，人为地增加了下面的一层
nlayer=nlayer+1;
h(nlayer)=1000;
vp(nlayer)=vp(nlayer-1);
vs(nlayer)=vs(nlayer-1);
%计算P波走时(flag=1)或S波走时（flag=2）
if flag==1
    v=vp;
else
    v=vs;
end
% -------
depth=0.0;
ntime=0;
for i=1:nlayer
   depth=depth+h(i);
   if za<depth & ntime==0
       j=i;
       ntime=1;
   end
end
j;  %震源所在层的序号
delta=sqrt((xb-xa)^2+(yb-ya)^2);   % 震中距
kexi=za;
for i=1:j-1
kexi=kexi-h(i);
end
kexi;   %震源所在层中从该层顶到震源的垂直距离，=震源深度-该层层顶的深度（上面各层厚度之和）
%计算直达波走时
small=2;
jj=0;
delta_star=0.0;
za;
j;
if j>1
yita1=delta*kexi/(za+0.01); 
yita2=delta;
phai1=atan(yita1/(kexi+0.01));
phai2=atan(delta/(kexi+0.01));
p1=phai1*180/pi;
p2=phai2*180/pi;
thet1(j)=phai1;
thet2(j)=phai2;
    sum5=0.0;
    sum6=0.0;
    for i=j-1:-1:1
        thet1(i)=asin(sin(thet1(i+1))*v(i)/v(i+1));
        thet2(i)=asin(sin(thet2(i+1))*v(i)/v(i+1));
        sum5=sum5+h(i)*tan(thet1(i));
        sum6=sum6+h(i)*tan(thet2(i));
     end
   delta1=yita1+sum5;
   delta2=yita2+sum6;
yita_star=(delta-delta1)*(yita2-yita1)/((delta2-delta1)+yita1+0.01);
phai_star=atan(yita_star/(kexi+0.01));
while abs(delta-delta_star)>small
thet(j)=phai_star;
sum7=0.0;
   for i=j-1:-1:1
        thet(i)=asin(sin(thet(i+1))*v(i)/v(i+1));
        sum7=sum7+h(i)*tan(thet(i));
   end
   delta_star=yita_star+sum7;
   if delta_star>delta  
         yita_star=(delta-delta1)*(yita_star-yita1)/(delta_star-delta1)+yita1;
   else 
  yita_star=(delta-delta_star)*(yita2-yita_star)/(delta2-delta_star+small)+yita_star;
   end
   phai_star=atan(yita_star/(kexi+0.01));
end
jj;
delta_star;
 td=kexi/(cos(phai_star)*v(j));
 thet(j)=phai_star;
 for i=j-1:-1:1
     thet(i)=asin(sin(thet(i+1))*v(i)/v(i+1));
     td=td+h(i)/(cos(thet(i))*v(i));
 end
 sinphai=sin(phai_star);
 cosphai=cos(phai_star);
else
    td=sqrt((xa-xb)^2+(ya-yb)^2+(za-zb)^2)/v(1);
    %phai_star=atan(delta/(kexi+0.01));
    r=sqrt(delta^2+kexi^2);
    sinphai=delta/r;
    cosphai=kexi/r;
end
% 计算直达波走时偏导数( 公式（Lee and Stewart,1981，式4.113,p104)
temp=sinphai/((delta+0.01)*v(j));
Ai(1)=(xa-xb)*temp;
Ai(2)=(ya-yb)*temp;
Ai(3)=cosphai/v(j);
Ai(4)=1;

% 震中位置xa,ya,za
% 台站位置xb,yb,zb
% 这里zb为台站的深度（向下为正），zb=-高程（km),计算时h(1)=h(1)-zb;za=za-zb,即震源深度也归算
% 到台站所在平面。
% 公式（Lee and Stewart,1981，参考图23)
% 绕射波：Tr(j,k)为震源在第j层内射线沿第k层顶的绕射波射线走时，eta(j,k)是相应的
% 临界距离
% Tr(j,k)=delta/v(k)-kexi*omega(k,j)+SUM1(i=1:j-1)(h(i)*omega(k,i)/(v(i)*v(
% k)))
%        +2*SUM2(i=j:k-1)(h(i)*omega(k,i)/(v(i)*v(k)))
% eta(j,k)=-kexi*v(j)/omega(k,j)+SUM3(i=1:j-1)(h(i)*v(i)/omega(k,i))
%        +2*SUM4(i=j:k-1)(h(i)*v(i)/omega(k,i))
% 式中j=1,2,...N-1,k=2,3,...N,其中T(j,k)是震源在第j层中，沿第k层顶的绕射射线，
% delta是震中距，由式delta=sqrt((xb-xa)^2+(yb-ya)^2)给出，xa和ya是震中坐标，
% xb和yb是台站坐标。kexi是震源到所在层(第ｊ层)顶的距离，
% kexi=za-(h(1)+h(2)+...+h(ｊ-1)),
% omega(k,i)=sqrt(v(k)^2-v(i)^2),omega(k,j)=sqrt(v(k)^2-v(j)^2)
% za是震源深度。
% 计算直达波走时：Td(j,k)为震源在第j层内的直达波射线走时
% 公式（Lee and Stewart,1981，参考图24)
% delta1=yita1+SUM(i=j-1:1)(h(i)*tan(thet1(i))
% delta2=yita2+SUM(i=j-1:1)(h(i)*tan(thet2(i))
% 式中sin(thet(i))/v(i)=sin(thet(i+1))/v(i+1)    for 1<=i<j
% yita1=delta*kexi/za;  yita2=delta
% (yita_star-yita1)/(yita2-yita1)=(delta-delta1)/(delta2-delta1)
% 于是 yita_star=(delta-delta1)*(yita2-yita1)/(delta2-delta1)+yita1
% phai_star=atan(yita_star/kexi)
% delta_star=yita_star+SUM(i=j-1:1)(h(i)*tan(thet(i))
% 迭代：
% if delta_star>delta  
%         yita_star=(delta-delta1)*(yita_star-yita1)/(delta_star-delta1)+yita1
% else 
%         yita_star=(delta-delta_star)*(yita2-yita_star)/(delta2-delta_star)
%         +yita_star
%  end
% 检查迭代是否终止：
% 给定小值small,    if abs(delta-delta_star)<amall  迭代结束。
% 计算直达波走时偏导数
% 公式（Lee and Stewart,1981，参考图24)
% 
