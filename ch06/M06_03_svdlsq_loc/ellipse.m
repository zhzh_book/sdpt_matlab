% 画椭圆
function c=ellipse(a,b,centerx,centery,alpha,color_flag)
% a: 半长轴长度，b：半短轴长度, alpha:半长轴与x轴（北）的夹角(弧度），由北向东量
npoint=100 % 半短轴方向画出的点数
x1step=a/npoint; %x1步长
a
b
al=alpha*180/pi;
al
for i=1:2*npoint+1
    x1(i)=x1step*(i-1-npoint);  % 基本椭圆
  
        
    y1(i)=b*sqrt(1-x1(i)^2/a^2);  % 右半边 
    y11(i)=-y1(i);     % 左半边
    x(i)=-y1(i)*sin(alpha)+x1(i)*cos(alpha);  % 右半边转动alpha角后的椭圆
    y(i)=y1(i)*cos(alpha)+x1(i)*sin(alpha);
    x2(i)=-y11(i)*sin(alpha)+x1(i)*cos(alpha);   % 左半边转动alpha角后的椭圆
    y2(i)=y11(i)*cos(alpha)+x1(i)*sin(alpha);
end
if color_flag==1
   plot(y+centery,x+centerx,'r'); hold on  
   plot(y2+centery,x2+centerx,'r'); hold on
else
   plot(y+centery,x+centerx,'b'); hold on  
   plot(y2+centery,x2+centerx,'b'); hold on 
end
%plot(y1+centery,x1+centerx); hold on  % 基本椭圆上半
% plot(y11+centery,x1+centerx);         % 基本椭圆下半
%xlabel({'a=',a,'b=',b,'alpha=',alpha*180/pi})
c=1;