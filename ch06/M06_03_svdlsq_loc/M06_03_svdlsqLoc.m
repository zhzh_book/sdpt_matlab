% 奇异值分解最小二乘法地震定位主程序svdlsqLoc.m
% 2010-10-06 赵仲和
% 台站坐标文件名'station25'
% 程序中调用getStns.m读台站坐标文件
% 程序中调用ll2xy1.m将经纬度换算成x,y坐标（使用的是最简单公式）
% 计算中调用函数arrival_rms,计算到时残差均方根作为搜索的目标函数
% 函数arrival_rms中调用函数travel_time，计算分层模型的直达波和绕射波走时
% 震相数据文件svdlsqLoc.pha
% 使用分层地壳模型model_z(试验时使用单层模型），其中分别给出各层中的P波和S波速度
% 读入台站坐标
clear all
model='model_z';   %速度模型文件
stations=getStns('station25');  %台站文件，一个有25个台站的假想地方性小台网
%stations=getStns('network25.ll');  %台站文件，一个有25个台站的假想地方性小台网
st_num=length(stations)
for i=1:st_num    
    st_lat(i)=stations(i).lat;
    st_lon(i)=stations(i).lon;
    st_ele(i)=stations(i).hei;
    st_name(i,:)=stations(i).stn;
end
st_name;

% 台站经纬度变换成X、Y坐标
% 取台网中心作为直角坐标原点,sx正向北，sy正向东
ori_lat=mean(st_lat);
ori_lon=mean(st_lon);
for i=1:st_num
    [sta(1,i),sta(2,i)]=ll2xy1(st_lat(i),st_lon(i),ori_lat,ori_lon);
    sta(3,i)=st_ele(i);
end
sta;

% 读入震相文件中的第1行(震相数据文件格式为hypoDD中使用的震相数据文件格式）
%fd=fopen('svdlsqLoc.pha','r');  %震相数据文件svdlsqLoc.pha
fd=fopen('syn2015001.pha','r');  
s1=fscanf(fd,'%c/');    % #   
oyear=fscanf(fd,'%i/');   % 发震时刻年
omanth=fscanf(fd,'%i/');  % 月
oday=fscanf(fd,'%i/');    % 日
ohour=fscanf(fd,'%i/');   % 时
omin=fscanf(fd,'%i/');    % 分
osec=fscanf(fd,'%f/');    % 秒
olat=fscanf(fd,'%f/');    % 震中纬度
olon=fscanf(fd,'%f/');    % 震中经度
[ox,oy]=ll2xy1(olat,olon,ori_lat,ori_lon)
odepth=fscanf(fd,'%f/');  % 震源深度（km）
nsrr=fscanf(fd,'%f/');    % 北南向水平误差（km）
ewrr=fscanf(fd,'%f/');    % 东西向水平误差（km）
derr=fscanf(fd,'%f/');    % 震源深度误差（km） 
rms0=fscanf(fd,'%f/');     % 残差均方根(sec）
event_id=fscanf(fd,'%i/');  % 事件标识号
% 读入震相数据
    ph_i=fscanf(fd,'%s %f %f %s',[8,inf]);
    ph_i=ph_i';   % 矩阵转置
ph_i;   %显示
sizeph=size(ph_i)
ph_num=sizeph(1)   %震相到时数据个数
if ph_num<3
    disp('震相到时数据个数小于3')
    break
else
    ph_sta_num=0
  for i=1:ph_num
    s1=char(ph_i(i,1));
    s2=char(ph_i(i,2));
    s3=char(ph_i(i,3));
    s4=char(ph_i(i,4));
    s5=char(ph_i(i,5));
    ph_sta(i,1:5)=[s1 s2 s3 s4 s5];
    flag=0;
    if i==1
        ph_sta_num=ph_sta_num+1;
    else
        for j=1:i-1
            c=strcmp(ph_sta(i,1:5),ph_sta(j,1:5));
            if c==1    %有相同台站
                flag=1;
                break
            end
        end
        if flag==0
            ph_sta_num=ph_sta_num+1;
            flag=0;
    end
     phstanum=ph_sta_num;     
    end
    ph_ori(1,i)=ph_i(i,6);
    ph(2,i)=ph_i(i,7);
    %当前只有P和S震相
    ph_phase(i)=char(ph_i(i,8));
    C=strncmp(ph_phase(i),'P',1);
    if C==1 
        ph(3,i)=1;  % P波代码为1
    end
    C=strncmp(ph_phase(i),'S',1);
    if C==1 
        ph(3,i)=2;  % S波代码为2
    end
  end
  p_num=0;
  s_num=0;
  for i=1:ph_num
    if  ph(3,i)==1 & ph(2,i)>0   %可用的P到时
          p_num=p_num+1;
    end
     if  ph(3,i)==2 & ph(2,i)>0   %可用的S到时
          s_num=s_num+1;
    end
end
pnum=p_num
snum=s_num
% 对合成到时数据加入随机误差
%rr=0.2  % 随机误差水平（秒）
rr=0.1
%rr=0
for i=1:ph_num 
    R = normrnd(0,1);
    ph(1,i)=ph_ori(1,i)+rr*R;
end
%抛台为试验不同台站分布对定位结果的影响
% 到时数据分布: 单数序号为P,双数序号为S
% 每对数字所在位置为台站位置(自下而上:北,向右:东
% 9,10   19,20   29,30    39,40   49,50
% 7,8   .....
% 5,6   .....
% 3,4   .....
% 1,2    11,12   21,22   31,32    41,42
% 下例中去掉了下面的3排台站
%for i=1:6
 %   ph(2,i)=0;
  %  ph(2,i+10)=0;
   % ph(2,i+20)=0;
    %ph(2,i+30)=0;
    %ph(2,i+40)=0;
%end
% ------------------
% 下例中去掉了西面的3排台站
%for i=1:30
   % ph(2,i)=0;
%end
%去掉东面的两排
%for i=31:50
    %ph(2,i)=0;
%end
% ------------------
ph;% 输出震相到时、权重（0或1）、震相代码：1-Pg，2-Sg
ph_sta;     %震相数据中的台站代码(5个字母)
fclose(fd);
% 找出震相数据对应的台站文件中的台站脚标
 for i=1:ph_num
     for j=1:st_num
         C=strcmp(ph_sta(i,1:5),st_name(j,1:5));
         if C==1
               ph(4,i)=j;
               break
         end
     end
 end
 ph;
% 初值的选取
% 方法一：P到时最小的台站的X、Y坐标作为初始震中的X、Y坐标；
% 最小P到时-2秒作为发震时刻初值，震源深度初值为10km
[ph_temp,ph_index]=sort(ph(1,:));  % 该函数实现数组中元素从小到大排序
% ph_temp数组中给出排序后的数据，ph_index数组给出排序后元素在原来数组中的顺序号
ph_index(1);  % 当前版本中P到时和S到时是混排的,待改
% 取权重不为零的最小到时对应的台站作为初始震中
for ii=1:ph_num
    i=ph_index(ii);
    if ph(2,i)>0
        j=ph(4,i);
        break
    end
end
ini_lat=st_lat(j);
ini_lon=st_lon(j);
ini_x=sta(1,j);
ini_y=sta(2,j);
ini_t=ph(1,i)-2.0;
ini_z=10; % 震源深度初值
% 试验不好的初值2010-10-07
%ini_lat=39.5
%ini_lon=115.5
%ini_z=0.10
%ini_t=20.0
%替换参数法求初值（《近震分析》第五章）
% 观测到时数据文件ph(1,:)：到时；ph(2,:):权；
 %ph(3,:):1-直达P到时,2-直达S到时，3-折射P（Pn）到时，4-折射S（Sn）到时
%ph(4,:)：台站索引（对应于台站列表中同一台站的序号）
%[ ini_x,ini_y,ini_t,ini_z] = sub_para(sta,ph)
%---------------------------------------------------------------
%Inglada方法求初值
vp=6.5    %地壳内平均P波速度
vpvs=1.73  %地壳内平均P波S波速度比
pp_num=p_num-1
ss_num=s_num-1
if pp_num>0 & ss_num>0
    ppss_num=pp_num+ss_num
else if pp_num>0 &ss_num<1
    ppss_num=pp_num
    else if pp_num<1 & ss_num>0
        ppss_num=ss_num
    end
 end
 end
 if ppss_num>2
   disp('Inglada初值')
   [ ini_xi,ini_yi,ini_ti,ini_zi] = inglada(sta,ph,vp,vpvs)         %Inglada方法     
 end
% [ ini_x,ini_y,ini_t,ini_z] = inglada_xy(sta,ph,vp,vpvs)   %先初定发震时刻的Inglada方法
%判断计算出的初值位置是否优于近台
flag=1
  [t_ing,tr]=travel_time(model,ini_xi,ini_yi,ini_zi,ini_x,ini_y,ini_z,flag);
  if t_ing+ini_ti<ini_t+2   %表明Inglada震中距最近台的距离小于待定震中到最近台的震中距
      disp('采用Inglada初值')
      ini_x=ini_xi
      ini_y=ini_yi
      ini_z=ini_zi
      ini_t=ini_ti
  end
%-------------------------------------------------------------------------------
%只有3个台的3个P到时

%建立svdlsq地震定位的迭代循环
% 设置参数
num_itr=50; % 最大迭代次数
min_dx=0.01 % 每次迭代震源x坐标改变量最小值
min_dy=min_dx % 每次迭代震源ｙ坐标改变量最小值（与ｘ坐标的改变量最小值相同）
min_dz=0.01%每次迭代震源z坐标改变量最小值
min_dt=0.01
% 如果x,y,z,t的改变量都小于上列最小值，则迭代停止
epi=zeros(4,num_itr+1);
%迭代循环准备
[epi(1,1),epi(2,1)]=ll2xy1(ini_lat,ini_lon,ori_lat,ori_lon); % 震中初值
%试验
epi(1,1)=ini_x;
epi(2,1)=ini_y;
epi(3,1)=ini_z; % 震源深度初值
epi(4,1)=ini_t; % 发震时刻初值
%----------------
for j=1:4
    del(j)=0.10;    % 试验迭代终止
end
%迭代循环开始
for ii=1:1:num_itr
  xa=epi(1,ii);
  ya=epi(2,ii);
  za=epi(3,ii);
  jj_pha=0;
  for i=1:1:ph_num
      ph(2,i);
  if ph(2,i)>0
      jj_pha=jj_pha+1;
  xb=sta(1,ph(4,i));
  yb=sta(2,ph(4,i));
  zb=sta(3,ph(4,i));
  flag=ph(3,i);  %震相代码  1:P,2:S
  [td,tr]=travel_time(model,xa,ya,za,xb,yb,zb,flag); % 直达波走时td和绕射波走时tr
  Ai=sderivate(model,xa,ya,za,xb,yb,zb,flag);% 直达波走时偏导数
% 在震源epi(j,ii)处建立系数矩阵A(jj_pha,j),j=1,2,3,4;jj_pha=1,2,...,ph_num1
% ph_num1为实际使用的震相数据个数) 
  for j=1:4
      A(jj_pha,j)=Ai(j);
  end
 
  residual(jj_pha)=(ph(1,i)-epi(4,ii)-td)*ph(2,i);  %加权残差=（观测到时-发震时刻-计算走时）*震相权值
  end
  end 
  ph_num1=jj_pha   % 实际使用的震相数据个数
%计算残差均方根rms
rms_iter(ii)=0.0;
for i=1:ph_num1
    rms_iter(ii)=rms_iter(ii)+residual(i)^2;
end
rms_iter(ii)=sqrt(rms_iter(ii)/ph_num1);
% svdlsq
b=residual';
eps=0.001   % 奇异值的截断限
[del,V,isigma]=svdlsq(A,b,eps) ;
% del为校正量
%返回的isigma是对角矩阵S^-1的对角元素,为S的大于截断限的元素的倒数
% 限制震源深度校正量不能过大
   if abs(del(3))>10
     del(3)=sign(del(3))*10;
 end
%-------------------------
for j=1:4
    epi(j,ii+1)=epi(j,ii)+del(j);
end
%限定震源深度不能小于0.1
if epi(3,ii+1)<0.1
    epi(3,ii+1)=0.1;
end
 %if del(1)<min_dx
   if del(1)<min_dx & del(2)<min_dy & del(3)<min_dz & del(4)<min_dt
    ii
  break
 end
end   %迭代循环结束
xa=epi(1,ii+1)
ya=epi(2,ii+1)
za=epi(3,ii+1)
origin_s=epi(4,ii+1)
for i=1:1:ph_num
    xb=sta(1,ph(4,i));
    yb=sta(2,ph(4,i));
    zb=sta(3,ph(4,i));
    flag=ph(3,i);
    [td,tr]=travel_time(model,xa,ya,za,xb,yb,zb,flag); 
    residual(i)=(ph(1,i)-epi(4,ii+1)-td)*ph(2,i);  %加权残差=（观测到时-发震时刻-计算走时）*震相权值
end
xb;
yb;
%计算残差均方根rms
rms_final=0.0;
for i=1:ph_num
    rms_final=rms_final+residual(i)^2;
end
rms_final=sqrt(rms_final/ph_num);
rms_iter(ii+1)=rms_final;
epi ; %迭代过程中的震源位置
rms_iter;  % 相应的rms
figure(1)
% 画出有震相数据的台站
for i=1:ph_num
    if ph(2,i)>0.5
        plot(sta(2,ph(4,i)),sta(1,ph(4,i)),'o');hold on;
    end
end
plot(oy,ox,'ro');hold on;  % 画出理论震中
plot(epi(2,ii+1),epi(1,ii+1),'b*');hold on; % 画出最终定位结果的震中位置
plot(epi(2,1),epi(1,1),'r+'); hold on % 画出震中初值位置
title('红圆圈—合成震中；红十字—定位初值；蓝星—定位结果')
for i=1:4
    epi_final(i)=epi(i,ii+1)% 最终定位结果
end

%# 2010  5  8 10 30 30.00  40.0719  116.0939   13.00 2.50  1.50  3.00  0.10   1
xxa=xa;
xxb=xb;
o_lat=ori_lat
o_lon=ori_lon
[lat_final,lon_final]=xy2ll1(xa,ya,ori_lat,ori_lon)
rms_final  % 残差均方根(sec）
id_final=event_id   % 事件标识号
%计算震中距和方位角
for i=1:ph_num
    xd(i)=(xa-sta(1,ph(4,i)));
    yd(i)=(ya-sta(2,ph(4,i)));
    distanse(i)=sqrt(xd(i)^2+yd(i)^2); %震中距
    azimuth(i)=atan2(yd(i),xd(i)) *180/pi  ;%方位角
    if azimuth(i)<0
        azimuth(i)=360+azimuth(i);
    end
end
ph(4,:);
xd;
yd;
distanse;
azimuth;
output_file='svdlsqLoc_final.pha'
fd=fopen(output_file,'w');  %写震相数据文件svdlsqLoc.pha
%输出定位结果和震相数据
fprintf(fd,'#%i %i %i %i %i %5.2f  %8.4f  %9.4f  %6.2f  %4.2f  %4.2f  %5.2f  %6.4f  %i\n',oyear,omanth,oday,ohour,omin,origin_s,....
    lat_final,lon_final,za,nsrr,ewrr,derr,rms_final,event_id);
%发震时刻年，月，日，时，分，秒；震中纬度，经度；震源深度；北南向误差；东西向误差；
%震源深度误差；残差均方根；事件ID号
for i=1:ph_num
    if ph(3,i)==1
        pha='P';
    end
    if ph(3,i)==2
            pha='S';
    end
    fprintf(fd,'%s %7.3f  %d %s %7.3f  %7.3f  %5.1f\n',ph_sta(i,:),ph(1,i),ph(2,i),pha,residual(i),distanse(i),azimuth(i));
%台站名，震相到时，权，震相代码，到时残差，震中距（km），方位角（°）
end
fclose(fd);
st_lat;
st_lon;
oyear;
omanth;
oday;
ohour;
omin;
origin_s;
%合成数据对应的地震参数：o：0.00   lat：40.1000   lon：116.2000   h：11.00
%合成数据对应的地震参数：ox = 1.1186；oy = 7.0258；h=11.0；o=0.0
%替换参数法得到的初值：ini_x = 1.1189；ini_y =7.0307；ini_z =10.9860；ini_t =0.0037；
%地壳平均速度V =6.5063
%数据无误差时的定位结果：epi_final = 1.1187(x)    7.0258(y)   10.9996(h)   0.0001(o)
end
%误差估计
ercof=1
rderr=0
colf=0.95
c= loc_err( ercof,rderr,isigma,epi_final,rms_final,V,colf)