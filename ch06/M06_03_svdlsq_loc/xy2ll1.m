function [slat,slon]=xy2ll1(sx,sy,olat,olon)
%直角坐标换算经纬度,方法一
% output
%       slat：台站纬度，单位度
%       slon：台站经度，单位度
% input
%       sx：台站直角坐标x值，单位公里
%       sy：台站直角坐标y值，单位公里
%       olat：原点纬度，单位度
%       olon：原点经度，单位度
% example
%       
    slat=sx/111.12+olat;
    slon=sy/(111.12*cos((olat+slat)*pi/360))+olon;