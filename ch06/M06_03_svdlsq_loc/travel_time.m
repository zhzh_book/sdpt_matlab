%计算分层地壳中的地震波走时
% 给定速度模型
% h(i), for i=1,2,...,N为第i层的厚度，其中第N层为地壳下的上地幔；v(i)为每一层中的波速，
% 可给定为P速度或S波速度。
% 速度模型文件 model.txt：例如中国平均2层地壳模型
% ( 中国地区地震走时表,国家地震局地球物理研究所编，《震相走时便查表》，地震出版社，1980)
%  3    (层数N)  
%  (层号  层厚度(km)  P波速度（km/s) S波速度（km/s))
%  1     16         5.71           3.40
%  2     24         6.53           3.77
%  3     300       7.97           4.45 
function [td,tr]=travel_time(x,xa,ya,za0,xb,yb,zb0,flag)
%函数输入：
% x: 速度模型文件名，例如x='model.txt'
%xa,ya,za：震中位置，xa北为正，ya东为正,za下为正（km）
% xb,yb,zb：台站位置，这里zb为台站的深度（向下为正），zb=-高程（km),计算时h(1)=h(1)-zb;za=za-zb,
%即震源深度也归算到台站所在平面。
%flag:1:P震相，2：S震相
x;
fid=fopen(x,'r');
a=fscanf(fid,'%d %f %f %f',[4 inf]);
fclose(fid);
a=a';
[m,n]=size(a);
nlayer=m;
for i=1:nlayer
 h(i)=a(i,2);
 vp(i)=a(i,3);
 vs(i)=a(i,4);
end
h0(1)=h(1);
h(1)=h0(1)-zb0;
vp;
vs;
xa;
ya;
za=za0-zb0;
xb;
yb;
zb=0.0;
%求震源所在层号j
% 给定产生绕射波的层顶
k=nlayer; %Moho界面绕射波
%  为便于计算Moho面以下地震的直达波走时，人为地增加了下面的一层
nlayer=nlayer+1;
h(nlayer)=1000;
vp(nlayer)=vp(nlayer-1);
vs(nlayer)=vs(nlayer-1);
%计算P波走时(flag=1)或S波走时（flag=2）
if flag==1
    v=vp;
else
    v=vs;
end
% -------
depth=0.0;
ntime=0;
for i=1:nlayer
   depth=depth+h(i);
   if za<depth & ntime==0
       j=i;
       ntime=1;
   end
end
j;
delta=sqrt((xb-xa)^2+(yb-ya)^2);
if j<k
kexi=za;
for i=1:j-1
kexi=kexi-h(i);
end
kexi;
% 计算P绕射波走时
sum1=0.0;
sum3=0.0;
for i=1:j-1
  omega=sqrt(v(k)^2-v(i)^2);
  sum1=sum1+h(i)*omega/(v(i)*v(k));
  sum3=sum3+h(i)*v(i)/omega;
end
sum2=0.0;
sum4=0.0;
for i=j:k-1
   omega=sqrt(v(k)^2-v(i)^2);
   sum2=sum2+h(i)*omega/(v(i)*v(k));
   sum4=sum4+h(i)*v(i)/omega;
end
omega=sqrt(v(k)^2-v(j)^2);
Trkj=delta/v(k)-kexi*omega/(v(j)*v(k))+sum1+2*sum2;
etakj=-kexi*v(j)/omega+sum3+2*sum4;
if delta<etakj
    Trkj=0.0;
end
tr=Trkj;
eta=etakj;   %绕射波的临界距离
else
    disp('没有绕射波')
    tr=0;
end
%计算直达波走时
kexi=za;
for i=1:j-1
kexi=kexi-h(i);
end
kexi;
small=2;
jj=0;
delta_star=0.0;
kexi;
za;
j;
if j>1
yita1=delta*kexi/(za+0.01); 
yita2=delta;
phai1=atan(yita1/(kexi+0.01));
phai2=atan(delta/(kexi+0.01));
p1=phai1*180/pi;
p2=phai2*180/pi;
thet1(j)=phai1;
thet2(j)=phai2;
    sum5=0.0;
    sum6=0.0;
    for i=j-1:-1:1
        thet1(i)=asin(sin(thet1(i+1))*v(i)/v(i+1));
        thet2(i)=asin(sin(thet2(i+1))*v(i)/v(i+1));
        sum5=sum5+h(i)*tan(thet1(i));
        sum6=sum6+h(i)*tan(thet2(i));
     end
   delta1=yita1+sum5;
   delta2=yita2+sum6;
yita_star=(delta-delta1)*(yita2-yita1)/((delta2-delta1)+yita1+0.01);
phai_star=atan(yita_star/(kexi+0.01));
while abs(delta-delta_star)>small
thet(j)=phai_star;
sum7=0.0;
   for i=j-1:-1:1
        thet(i)=asin(sin(thet(i+1))*v(i)/v(i+1));
        sum7=sum7+h(i)*tan(thet(i));
   end
   delta_star=yita_star+sum7;
   if delta_star>delta  
         yita_star=(delta-delta1)*(yita_star-yita1)/(delta_star-delta1)+yita1;
   else 
  yita_star=(delta-delta_star)*(yita2-yita_star)/(delta2-delta_star+small)+yita_star;
   end
   phai_star=atan(yita_star/(kexi+0.01));
end
jj;
delta_star;
 td=kexi/(cos(phai_star)*v(j));
 thet(j)=phai_star;
 for i=j-1:-1:1
     thet(i)=asin(sin(thet(i+1))*v(i)/v(i+1));
     td=td+h(i)/(cos(thet(i))*v(i));
 end
else
    td=sqrt((xa-xb)^2+(ya-yb)^2+(za-zb)^2)/v(1);
end

% 震中位置xa,ya,za
% 台站位置xb,yb,zb
% 这里zb为台站的深度（向下为正），zb=-高程（km),计算时h(1)=h(1)-zb;za=za-zb,即震源深度也归算
% 到台站所在平面。
% 公式（Leeand Stewart,1981，参考图23，
% 绕射波：Tr(j,k)为震源在第j层内射线沿第k层顶的绕射波射线走时，eta(j,k)是相应的
% 临界距离
% Tr(j,k)=delta/v(k)-kexi*omega(k,j)+SUM1(i=1:j-1)(h(i)*omega(k,i)/(v(i)*v(
% k)))
%        +2*SUM2(i=j:k-1)(h(i)*omega(k,i)/(v(i)*v(k)))
% eta(j,k)=-kexi*v(j)/omega(k,j)+SUM3(i=1:j-1)(h(i)*v(i)/omega(k,i))
%        +2*SUM4(i=j:k-1)(h(i)*v(i)/omega(k,i))
% 式中j=1,2,...N-1,k=2,3,...N,其中T(j,k)是震源在第j层中，沿第k层顶的绕射射线，
% delta是震中距，由式delta=sqrt((xb-xa)^2+(yb-ya)^2)给出，xa和ya是震中坐标，
% xb和yb是台站坐标。kexi是震源到所在层(第ｊ层)顶的距离，
% kexi=za-(h(1)+h(2)+...+h(ｊ-1)),
% omega(k,i)=sqrt(v(k)^2-v(i)^2),omega(k,j)=sqrt(v(k)^2-v(j)^2)
% za是震源深度。
% 计算直达波走时：Td(j,k)为震源在第j层内的直达波射线走时
% delta1=yita1+SUM(i=j-1:1)(h(i)*tan(thet1(i))
% delta2=yita2+SUM(i=j-1:1)(h(i)*tan(thet2(i))
% 式中sin(thet(i))/v(i)=sin(thet(i+1))/v(i+1)    for 1<=i<j
% yita1=delta*kexi/za;  yita2=delta
% (yita_star-yita1)/(yita2-yita1)=(delta-delta1)/(delta2-delta1)
% 于是 yita_star=(delta-delta1)*(yita2-yita1)/(delta2-delta1)+yita1
% phai_star=atan(yita_star/kexi)
% delta_star=yita_star+SUM(i=j-1:1)(h(i)*tan(thet(i))
% 迭代：
% if delta_star>delta  
%         yita_star=(delta-delta1)*(yita_star-yita1)/(delta_star-delta1)+yita1
% else 
%         yita_star=(delta-delta_star)*(yita2-yita_star)/(delta2-delta_star)
%         +yita_star
%  end
% 检查迭代是否终止：
% 给定小值small,    if abs(delta-delta_star)<amall  迭代结束。
% 
