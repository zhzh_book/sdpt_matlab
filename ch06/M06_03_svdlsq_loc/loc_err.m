function c= loc_err( ercof,rderr,isigma,epi_final,rms_final,V,colf)
%地震定位误差估计
% 误差估计：
% 取rms(ii)为结果的均方根,w^2=rms(ii)^2*ercof+rderr^2
% 令ercof=1:本次残差均方根的权重系数,rderr=0.05sec:先验到时读数误差
% 平方误差矩阵C=w^2*V*S^-2*V'

w2=rms_final^2*ercof+rderr^2
w=sqrt(w2)
S1=zeros(4,4);
for i=1:4
    S1(i,i)=isigma(i);
end
S1
% S1的对角元素为S对角元素的倒数
vvt=V*V';
s2=S1*S1';
s2vt=s2*V';
vs2vt=V*s2vt;
c=w2*vs2vt;
% 单分量误差估计(x,y,z,t)
for i=1:4
    err(i)=sqrt(c(i,i));
end
c3=c(1:3,1:3)   % 关于x,y,z协方差矩阵
nsrr=sqrt(c3(1,1))    %北南向误差（km）
ewrr=sqrt(c3(2,2))   %东西向误差（km）
derr=sqrt(c3(3,3))   %深度误差（km）
[eig_vector3,eig_value3]=eig(c3) ; 
%得到误差椭球的3个主轴的半长度及相应的方向矢量
c2=c(1:2,1:2);   % 关于x,y的协方差矩阵
[eig_vector2,eig_value2]=eig(c2);
%得到误差椭圆的2个主轴的半长度平方及相应的方向矢量

%标度因子f=sqrt(m*F(cofl,m,∞),其中m为2（椭圆），3（椭球）。
% Cofl为置信水平，例如0.95。 
% Matlab：F=finv(cofl,m,∞)=finv(cofl,m,100000) 
% 二变量95%置信水平的标度因子clcoff:

m=2
F=finv(colf,m,100000)
clcoff=sqrt(m*F)
if eig_value2(1,1)>=eig_value2(2,2)
    a=sqrt(eig_value2(1,1))*clcoff;
    b=sqrt(eig_value2(2,2))*clcoff;
    cosalpha=eig_vector2(1,1);
    sinalpha=eig_vector2(2,1);
    alpha=atan2(sinalpha,cosalpha);
else
    b=sqrt(eig_value2(1,1))*clcoff;
    a=sqrt(eig_value2(2,2))*clcoff;
    cosalpha=eig_vector2(1,2);
    sinalpha=eig_vector2(2,2);
    %cosalpha=cos(30*pi/180)
    %sinalpha=sin(30*pi/180)
    alpha=atan2(sinalpha,cosalpha);
end
alphad=alpha*180/pi
a
b
%figure(2)
  c=ellipse(a,b,epi_final(1),epi_final(2),alpha,1); %颜色标记为1：红色椭圆
    %  作为单变量的震源深度的误差估计
    % 把震源深度作为单变量来估计误差范围，即在给定值新水平，不限定震中位置，
    % 其震源深度真值以此置信水平落入所估计的误差范围。
  
m=1
F=finv(colf,m,100000)
clcoffd=sqrt(m*F) 
err_depth=sqrt(c3(3,3))*clcoffd
% 误差椭球的3个半轴长度为3个本征值eig_value3;
% 3个半轴的方向余弦为3个本征矢量的各分量。
% 按HYP2000中的定义，垂直误差ERZ和水平误差ERH是从误差椭球主轴的长度和
%方向导出的简化的误差表示。将3个主轴（它们的长度是标准误差）投影到穿
%过震源的垂直线上，其中的最大值为ERZ。ERH是从上方看去投影到水平面上的
%主轴中最长主轴的长度。

m=3
F=finv(colf,m,100000)
clcoff=sqrt(m*F)
erh=0;
erz=0;
for i=1:3
    ah(i)=sqrt(eig_value3(i,i))*sqrt(eig_vector3(1,i)^2+eig_vector3(2,i)^2);
    az(i)=sqrt(eig_value3(i,i))*abs(eig_vector3(3,i));
end
[ah_temp,ah_index]=sort(ah);
[az_temp,az_index]=sort(az);
ah1=ah_temp(3)*clcoff;
ah2=ah_temp(2)*clcoff;
az1=az_temp(3)*clcoff;
   alpha=atan2(eig_vector3(2,ah_index(3)),eig_vector3(1,ah_index(3)));
   c=ellipse(ah1,ah2,epi_final(1),epi_final(2),alpha,2); 
  %xlabel('误差范围：红色—两变量误差椭圆；蓝色—三变量误差椭球在地面投影')
  xlabel('km')
  ylabel('km')
   % 画椭圆颜色标记为2：蓝色椭圆
% 按HYPO81的算法
% 采用Hyp2000的定义w2
erh81m=sqrt(c3(1,1)+c3(2,2))
erz81m=sqrt(c3(3,3))
% 按原HYPO81BJ和BLOC86的的定义，系数用本次定位时的残差均方根rms
erh81=sqrt(c3(1,1)+c3(2,2))*rms_final/w
erz81=sqrt(c3(3,3))*rms_final/w
% 实际上标度因子为1.0，对于变量误差估计，相当于致信水平为68%
c=1
end

