function [sx,sy]=ll2xy3(slat,slon,olat,olon)
%经纬度换算直角坐标,方法三
%（适用于北纬70度-南纬70度）
%朱介寿等《地震学中的计算方法》
% output
%       sx：台站直角坐标x值，单位公里
%       sy：台站直角坐标y值，单位公里
% input
%       slat：台站纬度，单位度
%       slon：台站经度，单位度
%       olat：原点纬度，单位度
%       olon：原点经度，单位度
% example
%

laix = 6378.137;   %地球半长轴（km）
ellipse = 1/298.25722;  %使用1984年地球椭圆参数
saix=laix-ellipse*laix

ell = (laix^2-saix^2)/(laix^2)
%rn = laix/((1-(ell*(sin(olat*(pi/180))^2)))^(1/2));
rn = sin(olat*(pi/180));
rn = rn^2;
rn = ell*rn;
rn = 1 -rn;
rn = rn^(1/2);
rn = laix/rn

%rm = (laix*(1-ell))/((1-(ell*(sin(olat*(pi/180))^2)))^(3/2));
rm = sin(olat*(pi/180));
rm = rm^2;
rm = ell*rm;
rm = 1-rm;
rm = rm^(3/2);
rm = (laix*(1-ell))/rm

%sy=rn*(slon-olon)*cos(olat*(pi/180));
sy = rn*(slon-olon)*pi/180;
sy = sy*cos(olat*(pi/180));
%sx=rm*(slat-olat)+((sy^2)*tan(olat*(pi/180)))/(2*rn);
sx=rm*(slat-olat)*pi/180;
sx = sx +(sy^2)*(tan(olat*(pi/180)))/(2*rn);
end
