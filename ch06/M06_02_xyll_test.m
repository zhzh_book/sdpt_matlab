%测试经纬度与xy坐标的转换函数
olat_d=40.0     %台网中心纬度（°）
olon_d=116.0   %台网中心经度（°）
slat_d=41.5  %台站纬度（°）
slon_d=118.2%台站经度（°）
[s_x1,s_y1]=ll2xy1(slat_d,slon_d,olat_d,olon_d)
[s_x2,s_y2]=ll2xy2(slat_d,slon_d,olat_d,olon_d)
[s_x3,s_y3]=ll2xy3(slat_d,slon_d,olat_d,olon_d)
%s_x1 =166.6800       s_y1 =185.1974
%s_x2 = 166.5727      s_y2 = 185.8020
%s_x3 = 168.8703       s_y3 =187.8665
slat1_d=39.5
slon1_d=114.5
[s_x11,s_y11]=ll2xy1(slat1_d,slon1_d,olat_d,olon_d)
[s_x12,s_y12]=ll2xy2(slat1_d,slon1_d,olat_d,olon_d)
[s_x13,s_y13]=ll2xy3(slat1_d,slon1_d,olat_d,olon_d)
dif_x1=s_x11-s_x1
dif_y1=s_y11-s_y1
dif_x2=s_x12-s_x2
dif_y2=s_y12-s_y2
dif_x3=s_x13-s_x3
dif_y3=s_y13-s_y3
%dif_x1 =-222.2400     dif_y1 = -313.3479
%dif_x2 =-222.0872     dif_y2 =-314.3635
%dif_x3 = -223.3099    dif_y3 = -315.9573
dis1=sqrt(dif_x1^2+dif_y1^2)
dis2=sqrt(dif_x2^2+dif_y2^2)
dis3=sqrt(dif_x3^2+dif_y3^2)
%dis1 =384.1582
%dis2 =384.8989
%dis3 = 386.9061
[slat1,slon1]=xy2ll1(s_x1,s_y1,olat_d,olon_d)
[slat2,slon2]=xy2ll2(s_x1,s_y1,olat_d,olon_d)
[slat3,slon3]=xy2ll3(s_x1,s_y1,olat_d,olon_d)
%用简化转换得到的x、y，用3种算法转换经纬度：
%slat1 = 41.5000   slon1 =118.2000
%slat2 =41.5010    slon2 =118.1929
%slat3 =41.4809    slon3 =118.1687