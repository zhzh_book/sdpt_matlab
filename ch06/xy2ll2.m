function [slat,slon]=xy2ll2(sx,sy,olat,olon)
%直角坐标换算经纬度,方法二（适用于北纬70度-南纬70度）
%（李汯鑑和斯图尔特《微震台网的原理与方法》）
% output
%       slat：台站纬度，单位度
%       slon：台站经度，单位度
% input
%       sx：台站直角坐标x值，单位公里
%       sy：台站直角坐标y值，单位公里
%       olat：原点纬度，单位度
%       olon：原点经度，单位度
% example
%       
    lave=(olat)/2*(pi/180);
    pb=1.8428071+0.0187098*(sin(lave)^2)+0.0001583*(sin(lave)^4);
    slat=sx/(60*pb)+olat;
    
    lave=(olat+slat)/2*(pi/180);
    pa=(1.8553654+0.0062792*(sin(lave)^2)+0.0000319*(sin(lave)^4))*cos(lave);
    pb=1.8428071+0.0187098*(sin(lave)^2)+0.0001583*(sin(lave)^4);
    
    slat=sx/(60*pb)+olat;
    slon=sy/(60*pa)+olon;
end