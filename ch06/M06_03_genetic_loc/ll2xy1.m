function [sx,sy]=ll2xy1(slat,slon,olat,olon)
%经纬度换算直角坐标,方法一
% output
%       sx：台站直角坐标x值，单位公里
%       sy：台站直角坐标y值，单位公里
% input
%       slat：台站纬度，单位度
%       slon：台站经度，单位度
%       olat：原点纬度，单位度
%       olon：原点经度，单位度
% example
%       
    sx=(slat-olat)*111.12;
    sy=(slon-olon)*111.12*cos((slat+olat)*pi/360);