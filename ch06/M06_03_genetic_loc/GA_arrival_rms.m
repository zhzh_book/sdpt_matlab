function ObjV=GA_arrival_rms(model,X,sta,ph)
%计算多个个体的目标函数
% X为nx4矩阵，n为个体个数，4列，依次为个体（震源点）的x,y,z,t。
Num=length(X);
X=X';
hypo=zeros(4,1);
ObjV=zeros(Num,1);
for i=1:Num
    hypo=X(:,i);
    ObjV(i)=arrival_rms(model,X(:,i),sta,ph);
end
 ObjV;   
