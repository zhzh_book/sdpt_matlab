function rms=arrival_rms(model,hypo,sta,ph)
%计算目标函数值
% 目标函数为各到时数据的残差加权均方根
% 给定:震源hypo(4):x,y,z,t,给定台站坐标sta(:,3),速度模型文件名（例如model_z，单层模型）
% 到时数据文件ph(1,i)：到时；ph(2,i):权；ph(3,i): 1-P到时,2-S到时
sumr=0;
sumw=0;
hypo;
% if hypo(3)<0　　
%           hypo(3)=0.0;
%           end
ph_num=length(ph);
for i=1:ph_num
    j=ph(4,i);   % 该到时的台站序号
        
    [td,tr]=travel_time(model,hypo(1),hypo(2),hypo(3),sta(1,j),sta(2,j),sta(3,j),ph(3,i));
    if i==25
        td;
    end
    if ph(3,i)==1 || ph(3,i)==2
        sumr=sumr+((ph(1,i)-hypo(4)-td)*ph(2,i))^2;
    end
    sumw=sumw+ph(2,i)^2;
end
sumr;
sumw;
rms=sqrt(sumr/sumw);
% 如果该震源深度小于0，则强令该深度为0 