%遗传算法地震定位试验例1
%以"多元单峰函数的优化实例"为蓝本，源码：yj3.m
% 雷英杰等，《MATLAB遗传算法工具箱及应用》，p111，西安电子科技大学出版社，2004
% 2010-07-06 赵仲和
% 本程序GALoc2.m为主程序
% 台站坐标文件名'station25'
% 程序中调用getStns.m读台站坐标文件
% 程序中调用ll2xy1.m将经纬度换算成x,y坐标（使用的是最简单公式）
% 计算中调用函数arrival_rms,计算到时残差均方根作为搜索的目标函数
% 函数arrival_rms中调用函数travel_time，计算分层模型的直达波和绕射波走时
% 使用分层地壳模型model_z(试验时使用单层模型），其中分别给出各层中的P波和S波速度
% 读入台站坐标
clear all
%计时开始
c=clock;

model='model_z'   %速度模型文件
stations=getStns('station25');  %台站文件，一个有25个台站的假想地方性小台网
st_num=length(stations)
for i=1:st_num
    st_lat(i)=stations(i).lat;
    st_lon(i)=stations(i).lon;
    st_ele(i)=stations(i).hei;
    st_name(i,:)=stations(i).stn;
end
st_name

% 台站经纬度变换成X、Y坐标
% 取台网中心作为直角坐标原点,sx正向北，sy正向东
ori_lat=mean(st_lat)
ori_lon=mean(st_lon)
for i=1:st_num
    [sta(1,i),sta(2,i)]=ll2xy1(st_lat(i),st_lon(i),ori_lat,ori_lon);
    sta(3,i)=st_ele(i);
end
sta
figure(2);
plot(sta(1,:),sta(2,:),'o');
% 读入震相文件中的第1行(震相数据文件格式为hypoDD中使用的震相数据文件格式）
fd=fopen('SimpLoc.pha','r');  %震相数据文件SimpLoc.pha
s1=fscanf(fd,'%s/')    % #   
oyear=fscanf(fd,'%i/')   % 发针时刻年
omanth=fscanf(fd,'%i/')  % 月
oday=fscanf(fd,'%i/')    % 日
ohour=fscanf(fd,'%i/')   % 时
omin=fscanf(fd,'%i/')    % 分
osec=fscanf(fd,'%f/')    % 秒
olat=fscanf(fd,'%f/')    % 震中纬度
olon=fscanf(fd,'%f/')    % 震中经度
[ox,oy]=ll2xy1(olat,olon,ori_lat,ori_lon)
odepth=fscanf(fd,'%f/')  % 震源深度（km）
nerr=fscanf(fd,'%f/')    % 北南向水平误差（km）
eerr=fscanf(fd,'%f/')    % 东西向水平误差（km）
derr=fscanf(fd,'%f/')    % 震源深度误差（km） 
rms=fscanf(fd,'%f/')     % 残差均方根(sec）
event_id=fscanf(fd,'%i/')  % 事件标识号
% 读入震相数据
ph_i=fscanf(fd,'%s %f %f %s',[8,inf]);
ph_i=ph_i';   % 矩阵转置
ph_i;             % 显示
ph_num=length(ph_i)   %震相到时数据个数
  for i=1:ph_num
    s1=char(ph_i(i,1));
    s2=char(ph_i(i,2));
    s3=char(ph_i(i,3));
    s4=char(ph_i(i,4));
    s5=char(ph_i(i,5));
    ph_sta(i,1:5)=[s1 s2 s3 s4 s5];
    ph_ori(1,i)=ph_i(i,6);
    ph(2,i)=ph_i(i,7);
    %当前只由P和S震相
    ph_phase(i)=char(ph_i(i,8));
    C=strncmp(ph_phase(i),'P',1);
    if C==1 
        ph(3,i)=1;  % P波代码为1
    end
    C=strncmp(ph_phase(i),'S',1);
    if C==1 
        ph(3,i)=2;  % S波代码为2
    end
end
ph_sta     %震相数据中的台站代码(5个字母)
fclose(fd);
% 对合成到时数据加入随机误差
%rr=0.2  % 随机误差水平（秒）
rr=0.1
%rr=0
for i=1:ph_num 
    R = normrnd(0,1);
    ph(1,i)=ph_ori(1,i)+rr*R;
end
% 找出震相数据对应的台站文件中的台站脚标
 for i=1:ph_num
     for j=1:st_num
         C=strcmp(ph_sta(i,1:5),st_name(j,1:5));
         if C==1
               ph(4,i)=j;
               break
         end
     end
 end
 ph
%定义遗传算法参数
NIND=40;               %个体数目(Numbe of individuals)
MAXGEN=500;            %最大遗传代数(Maximum number of generations)
NVAR=4;               %变量的维数(震源位置x,y,z和发针时刻t)
PRECI=20;              %变量的二进制位数(Precision of variables)
GGAP=0.9;              %代沟(Generation gap)
trace=zeros(MAXGEN, 2);
%建立区域描述器(Build field descriptor)
%FieldD=[rep([PRECI],[1,NVAR]);rep([-50;50],[1, NVAR]);rep([1;0;1;1],[1,NVAR])];
%rep.m：重复一个矩阵，同repmat.m
FieldD=[rep(PRECI,[1,NVAR]);-25,-25,0,0;25,25,30,50;rep([1;0;1;1],[1,NVAR])]
%参见雷英杰等，2004，第65页
Chrom=crtbp(NIND, NVAR*PRECI);                       %创建初始种群
gen=0;                                               %代计数器
%ObjV=objfun1(bs2rv(Chrom, FieldD));%计算初始种群个体的目标函数值
Xini=bs2rv(Chrom, FieldD); % X 为NINDx4的矩阵，函数bs2rv见雷英杰等，2004，第64页
ObjV=GA_arrival_rms(model,Xini,sta,ph)% 计算到时残差均方根,为NINDx1的列矢量
while gen<MAXGEN                                     %迭代
    FitnV=ranking(ObjV)                         %分配适应度值(Assign fitness values)
    SelCh=select('sus', Chrom, FitnV, GGAP);         %选择
    SelCh=recombin('xovsp', SelCh, 0.7);             %重组
    SelCh=mut(SelCh);                                %变异
    %ObjVSel=objfun1(bs2rv(SelCh, FieldD)); %计算子代目标函数值
    X=bs2rv(SelCh, FieldD)
    ObjVSel=GA_arrival_rms(model,X,sta,ph); % 计算到时残差均方根
    [Chrom ObjV]=reins(Chrom, SelCh, 1, 1, ObjV, ObjVSel);     %重插入
    gen=gen+1;                                                 %代计数器增加
    trace(gen, 1)=min(ObjV);                                   %遗传算法性能跟踪
    trace(gen, 2)=sum(ObjV)/length(ObjV);
end
figure(1);
plot(trace(:,1));hold on;
plot(trace(:,2),'-.');grid;
legend(' 种群均值的变化','解的变化')
%输出最优解及其对应的20个自变量的十进制值,Y为最优解,I为种群的序号
[Y, I]=min(ObjV);
X=bs2rv(Chrom, FieldD);
X(I,:)
figure(2);
plot(sta(1,:),sta(2,:),'o');hold on;
plot(Xini(:,2),Xini(:,1),'r+');hold on;
plot(oy,ox,'ro');
plot(X(I,2),X(I,1),'b*');hold on;
rms_final=arrival_rms(model,X(I,:),sta,ph)   % 计算到时残差均方根
title('red o :synthetic  red + :initial  blue *:final')
%计时
year=num2str(c(1),4);
manth=num2str(c(2),2);
day=num2str(c(3),2);
hour=num2str(c(4),2);
minute=num2str(c(5),2);
second=num2str(c(6),3);
c1=clock;
year1=num2str(c1(1),4);
manth1=num2str(c1(2),2);
day1=num2str(c1(3),2);
hour1=num2str(c1(4),2);
minute1=num2str(c1(5),2);
second1=num2str(c1(6),3);
str=['遗传算法定位，起始时间  ',year,'-',manth,'-',day,' ',hour,':',minute,':',second];
disp(str)
str1=['遗传算法定位，结束时间  ',year1,'-',manth1,'-',day1,' ',hour1,':',minute1,':',second1];
disp(str1)
%下面的两次计算中由于引入的到时随机误差不同，所以结果不同
%ans =-1.8936   -2.2289   12.6827   30.0432
%rms_final = 0.0774
%遗传算法定位，起始时间  2016-10-20 15:28:18.4；结束时间  2016-10-20 15:32:42.1
%ans = -2.2605   -2.0457   13.0380   29.9978
%rms_final =0.0969
%遗传算法定位，起始时间  2016-10-20 16:47:30.7；结束时间  2016-10-20 16:52:52.9