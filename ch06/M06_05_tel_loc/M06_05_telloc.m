%小台网定位远震
clear all
%读入台站坐标
stations=getStns('station25');  %台站文件，一个有25个台站的假想地方性小台网
%stations=getStns('network25.ll');  %台站文件，一个有25个台站的假想地方性小台网
st_num=length(stations)
for i=1:st_num    
    st_lat(i)=stations(i).lat;
    st_lon(i)=stations(i).lon;
    st_ele(i)=stations(i).hei;
    st_name(i,:)=stations(i).stn;
end
st_name;

% 台站经纬度变换成X、Y坐标
% 取台网中心作为直角坐标原点,sx正向北，sy正向东
ori_lat=mean(st_lat);
ori_lon=mean(st_lon);
for i=1:st_num
    [sta(1,i),sta(2,i)]=ll2xy1(st_lat(i),st_lon(i),ori_lat,ori_lon);
    sta(3,i)=st_ele(i);
end
%读入视速度表
file='apa_vel.txt'
fid=fopen(file,'r');
vel=fscanf(fid,'%i  %f  %f',[3 inf]);
%vel=vel';
sizevel=size(vel);
vel_num=length(vel(1,:))
%读入远震初至震相到时数据
% 读入震相文件中的第1行(震相数据文件格式为hypoDD中使用的震相数据文件格式）
%fd=fopen('svdlsqLoc.pha','r');  %震相数据文件syn_tel01.pha
%#2015 6 7 8 9 30.00   60.0000   130.0000   33.00  0.00  0.00   0.00  0.0000  2015001
fd=fopen('syn_tel01.pha','r');  
s1=fscanf(fd,'%c/');    % #   
oyear=fscanf(fd,'%i/');   % 远震在台网中心的到时：年
omanth=fscanf(fd,'%i/');  % 月
oday=fscanf(fd,'%i/');    % 日
ohour=fscanf(fd,'%i/');   % 时
omin=fscanf(fd,'%i/');    % 分
osec=fscanf(fd,'%f/');    % 秒
olat=fscanf(fd,'%f/');    % 震中纬度
olon=fscanf(fd,'%f/');    % 震中经度
odepth=fscanf(fd,'%f/');  % 震源深度（km）
nsrr=fscanf(fd,'%f/');    % 北南向水平误差（km）
ewrr=fscanf(fd,'%f/');    % 东西向水平误差（km）
derr=fscanf(fd,'%f/');    % 震源深度误差（km） 
rms0=fscanf(fd,'%f/');     % 残差均方根(sec）
event_id=fscanf(fd,'%i/');  % 事件标识号
% 读入震相数据
ph_i=fscanf(fd,'%s %f %f %s',[8,inf]);
ph_i=ph_i';   % 矩阵转置
ph_i ;%显示
ph_num=length(ph_i)   %震相到时数据个数
  for i=1:ph_num
    s1=char(ph_i(i,1));
    s2=char(ph_i(i,2));
    s3=char(ph_i(i,3));
    s4=char(ph_i(i,4));
    s5=char(ph_i(i,5));
    ph_sta(i,1:5)=[s1 s2 s3 s4 s5];
    ph_ori(1,i)=ph_i(i,6);
    ph(2,i)=ph_i(i,7);
    %只有P震相
    ph_phase(i)=char(ph_i(i,8));
    C=strncmp(ph_phase(i),'P',1);
    if C==1 
        ph(3,i)=1;  % P波代码为1
    end
    C=strncmp(ph_phase(i),'S',1);
    if C==1 
        ph(3,i)=2;  % S波代码为2
    end
  end
  % 找出震相数据对应的台站文件中的台站脚标
 for i=1:ph_num
     for j=1:st_num
         C=strcmp(ph_sta(i,1:5),st_name(j,1:5));
         if C==1
               ph(4,i)=j;
               break
         end
     end
 end
 % 试验到时误差对定位结果的影响，对合成到时数据加入随机误差
rr=0.2  % 随机误差水平（秒）
%rr=0.1
%rr=0
for i=1:ph_num 
    R = normrnd(0,1);
    ph(1,i)=ph_ori(1,i)+rr*R;
end
%由远震到时，计算视慢度tao
%构造方程组AX=b
a=[1 2 3;4 7 2;7 4 3;5 4 6];
b=[2;4 ;5;3];
x=a\b;
sizea=size(a);
sizb=size(b);
jj_pha=0;
for i=1:1:ph_num
  ph(2,i);
  if ph(2,i)>0
      jj_pha=jj_pha+1;
      A(1,jj_pha)=-sta(1,ph(4,i));
      A(2,jj_pha)=-sta(2,ph(4,i));
      A(3,jj_pha)=1;
      b(jj_pha)=ph(1,i);  % 25（行）x 1（列）
  end
end
A=A' ;  %转置成25（行）x  3（列）
sizA=size(A);
sizeb=size(b);
X=A\b;
xlength=length(X);
tao_x=X(1)
tao_y=X(2)
t_o=X(3)    %原点到时
tao=sqrt(tao_x^2+tao_y^2)   %视慢度（s/km)
vel_km=1/tao
vel_d=1/(tao*111.12)
%由视速度求震中距
i_max=1
for i=1:vel_num-1
    if vel_d<vel(3,i) 
        i_max=i;
        break
    end
end
i_min=1
for j=i_max:-1:1
    if vel_d>vel(3,j)
        i_min=j;
        break
    end
end
dis_min=vel(2,i_min)
dis_max=vel(2,i_max)
dis_d=(dis_max+dis_min)/2   %震中距（°）
dis_round=(dis_max-dis_min)/2   %由于查视速度表造成的震中距不确定范围（°）
%计算方位角
azi_r=atan2(tao_y,tao_x);   %坐标系：x-北，y-东
if azi_r<0
    azi_r=azi_r+2*pi;
end
azi_d=azi_r*180/pi
%由方位角和震中距转换成经纬度
 [ lat,lon] = ad2ll( azi_d,dis_d,ori_lat,ori_lon )
 figure(1)
% plot(lon,lat,'r*');hold on

% 画出有震相数据的台站
for i=1:ph_num
    if ph(2,i)>0.5
        plot(sta(2,ph(4,i)),sta(1,ph(4,i)),'o');hold on;
    end
end
xlimits=[100 100];
ylimits=[100 100];
xlabel('km')
ylabel('km')
plot(0,0,'r+');hold on
X=[-100 100];
Y=[0 0];
line(X,Y);hold on
X=[0 0];
Y=[-100 100];
line(X,Y);hold on
X=[0 tan(azi_r)*100];
Y=[0 100];
line(X,Y)

dis_d
azi_d
%计算各台站的到时残差
rms=0;
for i=1:ph_num
    t_cal(i)=t_o-(tao_x*sta(1,ph(4,i))+tao_y*sta(2,ph(4,i)));
    t_res(i)=ph(1,i)-t_cal(i);
    rms=rms+t_res(i)^2;
end
rms=sqrt(rms/ph_num)
t_res
length(t_res)
figure(2)
j=[1:ph_num];
plot(j,t_res,'*');hold on
ylabel('残差/s')
xlabel('台站序号')
X=[0 ph_num];
Y=[rms rms];
line(X,Y);hold on
Y=[-rms -rms];
line(X,Y);hold on
Y=[0 0];
line(X,Y)
        