function [ cen_d ] = gra2cen( gra_d )
%地理纬度换算成地心纬度
%大地坐标系WGS85，扁率f=1/298.257223563
%输入：地理纬度（单位为度，北纬为正，南纬为负）gra_d
%返回：地心纬度（单位为度，北纬为正，南纬为负）cen_d
f=1/298.257223563;
gra_r=gra_d*pi/180;
cen_r=atan(tan(gra_r)*((1-f)^2));
cen_d=cen_r*180/pi;
end

