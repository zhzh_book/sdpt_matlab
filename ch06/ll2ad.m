function [ azi,dist] =ll2ad(lat,long,lat0,long0)
%ll2ad_func计算震中对台站的方位角和震中距
%   lat0,lon0 台站纬度和经度（度）
%   lat1,lon1 震中纬度和经度（度）
%   azi:震中对台站的方位角（度） 
%   dist:震中距（度）
 f = 1/298.25722;  %使用1984年地球椭圆参数
 %f=0
 fai0_r=pi/2-atan((1-f)^2*tan(pi*lat0/180))  %地心余纬度
long0_r=long0*pi/180;
%fai_r=(90-lat)*pi/180;
fai_r=pi/2-atan((1-f)^2*tan(pi*lat/180))  %地心余纬度
%fai0_r=(90-lat0)*pi/180;
long_r=long*pi/180;
cos_delta=cos(fai0_r)*cos(fai_r)+sin(fai0_r)*sin(fai_r)*cos(long_r-long0_r);
delta_r=acos(cos_delta);
cos_alpha=(cos(fai_r)-cos(delta_r)*cos(fai0_r))/(sin(delta_r)*sin(fai0_r));
sin_alpha=sin(long_r-long0_r)*sin(fai_r)/sin(delta_r);
alpha_r=asin(sin_alpha);
if cos_alpha>0
    if alpha_r<0 
        alpha_r=2*pi+alpha_r;
    end
end
if cos_alpha<0
    alpha_r=pi-alpha_r;
end
azi=alpha_r*180/pi;
dist=delta_r*180/pi;
%



end

