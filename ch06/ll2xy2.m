function [sx,sy,sz]=ll2xy2(slat,slon,olat,olon)
%经纬度换算直角坐标,方法二（试用于北纬70度-南纬70度）

% output
%       sx：台站直角坐标x值，单位公里
%       sy：台站直角坐标y值，单位公里
%       sz：台站直角坐标z值，单位公里
% input
%       slat：台站纬度，单位度
%       slon：台站经度，单位度
%       olat：原点纬度，单位度
%       olon：原点经度，单位度
% example
%       
    lave=(olat+slat)/2*(pi/180);
    pa=(1.8553654+0.0062792*(sin(lave)^2)+0.0000319*(sin(lave)^4))*cos(lave);
    pb=1.8428071+0.0187098*(sin(lave)^2)+0.0001583*(sin(lave)^4);
    
    sx=60*pb*(slat-olat);
    sy=60*pa*(slon-olon);
   