function [ lat,lon] = ad2ll( azi,dist,lat0,long0 )
%震中方位角和震中距转换乘地理经纬度
%输入：azi：震中方位角（°）
 %            dist：震中距（°）
 %            lat0：台站地理纬度（°）
 %           lon0：台站经度（°）
 %返回：lat：震中地理纬度（°）
 %            lon：震中经度（°）
 f = 1/298.25722;  %使用1984年地球椭圆参数
%f=0
fai0a_d=atan((1-f)^2*tan(pi*lat0/180))*180/pi
fai0_r=pi/2-atan((1-f)^2*tan(pi*lat0/180))  %地心余纬度
long0_r=long0*pi/180  %经度（弧度）
delta_r=dist*pi/180
alfa_r=azi*pi/180
cosfai=cos(delta_r)*cos(fai0_r)+sin(delta_r)*sin(fai0_r)*cos(alfa_r);
fai_r=acos(cosfai) %地心余纬度 (0<fai_r<pi)
%fai=fai_r*180/pi
sin_long_dif=sin(alfa_r)*sin(delta_r)/sin(fai_r)
cos_long_dif=(cos(delta_r)-cos(fai0_r)*cos(fai_r))/(sin(fai0_r)*sin(fai_r))
dif_r=asin(sin_long_dif)
if cos_long_dif>0 
    if dif_r<0
    dif_r=2*pi-dif_r
    end
end
if cos_long_dif>0
        if dif_r>0
    dif_r=dif_r
        end
end
if dif_r+pi<2*pi 
    long_r=long0_r+dif_r
else long_r=long0_r+dif_r-2*pi
end
if long_r>pi
    long_r=long_r-2*pi
end
lon=long_r*180/pi %longitude of epicenter  95.19 (95.66 EDSP-ISA)
lat_r=pi/2-fai_r
lat=lat_r*180/pi
lat_r=atan(tan(lat_r)/((1-f)^2))
lat=lat_r*180/pi  %latitude of epicenter 3.00 (3.41 EDSP)
end
