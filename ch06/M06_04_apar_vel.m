%根据P波走时差求视速度
clear all
model='iasp91'

taupTime('iasp91',5,'ScS','deg',20)
dep=15
vel_num=900
for i=1:vel_num
    tt=taupTime(model,15,'P','deg',i*0.1+1);    %走时从1.1°起
 timeP(i)=tt.time;      
dis(i)=tt.distance;
dep(i)=tt.srcDepth;
ray(i)=tt.rayParam;
end
figure(1)
plot(dis,timeP,'r.');
xlabel('震中距/度')
ylabel('走时/s')
figure(2)
tt=taupTime(model,15,'P','deg',0.9);   %0.9°走时
tt10=taupTime(model,15,'P','deg',1);   %1.0°走时
timeP9=tt.time
timeP10=tt10.time
%apar_vp(1)=(0.2*111.19)/(timeP(1)-timeP9);
apar_vp(1)=0.2/(timeP(1)-timeP9);    %1.1-0.9:vp(1)=1.0°的vp
dis_v(1)=1.0;
apar_vp(2)=0.2/(timeP(2)-timeP10);    %1.2-1.1:vp(1)=1.1°的vp
dis_v(2)=1.1;
for i=3:vel_num
   % apar_vp(i)=(0.2*111.19)/(timeP(i+1)-timeP(i-1));
   apar_vp(i)=0.2/(timeP(i)-timeP(i-2));     %第i点的apar_vp(i)对应的距离dis_v(i)=(0.9+i*0.1)°
   dis_v(i)=0.9+i*0.1;
end
plot(dis_v(1:vel_num),apar_vp(1:vel_num))
xlabel('震中距/度')
%ylabel('视速度/(km/s)')
ylabel('视速度/(°/s)')
fil='apa_vel'
fid=fopen(fil,'w')
fprintf(fid,'%d\n',vel_num)
for i=1:vel_num
    fprintf(fid,'%i   %5.1f   %9.4f   %9.4f\n',i,dis_v(i),apar_vp(i),1/apar_vp(i));
end
fclose(fid)
fil='apa_vel1'
fid=fopen(fil,'w')

for i=1:90
    j=(i-1)*10+1;
    fprintf(fid,'%5.1f %9.4f   %9.4f\n',dis_v(j),apar_vp(j),1/apar_vp(j));
end
fclose(fid)
for i=1:798
    dis(i)
    if dis(i)==16.1
        av=apar_vp(i);
        break
    end
    
end
ava=av

