function [rms,o]=search_rms(model,hypo,sta,ph)
%计算rms和发震时刻hypo(4)
% 目标函数为各到时数据的残差加权均方根
% 给定:震源hypo(4):x,y,z,t,给定台站坐标sta(:,3),速度模型文件名（例如model_z，单层模型）
% 到时数据文件ph(1,i)：到时；ph(2,i):权；ph(3,i): 1-P到时,2-S到时
sumr=0;
sumw=0;
hypo;
%sta(1,13)
%sta(2,13)
%sta(3,13)
ph_num=length(ph);
%计算走时tdi(i)
for i=1:ph_num
    j=ph(4,i);   % 该到时的台站序号
    [td,tr]=travel_time(model,hypo(1),hypo(2),hypo(3),sta(1,j),sta(2,j),sta(3,j),ph(3,i));
%    if i==25
%        td
%    end
tdi(i)=td;
end

mean_residual=0;
ph_used=0;
for i=1:ph_num
    mean_residual=(mean_residual+ph(1,i)-tdi(i))*ph(2,i);
    if (ph(2,i))>0.5
        ph_used=ph_used+1;
    end
end
ph_used;
mean_residual=mean_residual/ph_used;
o=mean_residual;   %发震时刻=各台残差（到时-走时）的均值，实现目标函数降维
for i=1:ph_num
    j=ph(4,i);   % 该到时的台站序号
      if (ph(2,i))>0.5 
    [td,tr]=travel_time(model,hypo(1),hypo(2),hypo(3),sta(1,j),sta(2,j),sta(3,j),ph(3,i));
      end
%    if i==25
%        td
%    end
    if ph(3,i)==1 || ph(3,i)==2
        sumr=sumr+((ph(1,i)-o-td)*ph(2,i))^2;
    end
    sumw=sumw+ph(2,i)^2;
end
sumr;
sumw;
rms=sqrt(sumr/sumw);

