% 用网格搜索法定位震源位置和发震时刻
% 2010-11-03 赵仲和
% 本程序SearchLoc.m为主程序
% 台站坐标文件名'station25'
% 程序中调用getStns.m读台站坐标文件
% 程序中调用ll2xy1.m将经纬度换算成x,y坐标（使用的是最简单公式）
% 计算中调用函数search_rms,计算到时残差均方根作为搜索的目标函数
% 函数search_rms中调用函数travel_time，计算分层模型的直达波和绕射波走时
% 震相数据文件SimpLoc.pha
% 使用分层地壳模型model_z(试验时使用单层模型），其中分别给出各层中的P波和S波速度
% 读入台站坐标
clear all
c=clock;
year=num2str(c(1),4);
manth=num2str(c(2),2);
day=num2str(c(3),2);
hour=num2str(c(4),2);
minute=num2str(c(5),2);
second=num2str(c(6),3);

model='model_z'   %速度模型文件
stations=getStns('station25');  %台站文件，一个有25个台站的假想地方性小台网
st_num=length(stations)
for i=1:st_num
    st_lat(i)=stations(i).lat;
    st_lon(i)=stations(i).lon;
    st_ele(i)=stations(i).hei;
    st_name(i,:)=stations(i).stn;
end
st_name

% 台站经纬度变换成X、Y坐标
% 取台网中心作为直角坐标原点,sx正向北，sy正向东
ori_lat=mean(st_lat)
ori_lon=mean(st_lon)
for i=1:st_num
    [sta(1,i),sta(2,i)]=ll2xy1(st_lat(i),st_lon(i),ori_lat,ori_lon);
    sta(3,i)=st_ele(i);
end
sta

figure(1);
plot(sta(1,:),sta(2,:),'o');
% 读入震相文件中的第1行(震相数据文件格式为hypoDD中使用的震相数据文件格式）
fd=fopen('SimpLoc.pha','r');  %震相数据文件SimpLoc.pha
%fd=fopen('syn2005001.pha','r');  
% 震相数据对应于假想震源x=-2km,y=-2km,z=13km,t0=30sec
s1=fscanf(fd,'%s/')    % #   
oyear=fscanf(fd,'%i/')   % 发针时刻年
omanth=fscanf(fd,'%i/')  % 月
oday=fscanf(fd,'%i/')    % 日
ohour=fscanf(fd,'%i/')   % 时
omin=fscanf(fd,'%i/')    % 分
osec=fscanf(fd,'%f/')    % 秒
olat=fscanf(fd,'%f/')    % 震中纬度
olon=fscanf(fd,'%f/')    % 震中经度
[ox,oy]=ll2xy1(olat,olon,ori_lat,ori_lon)
odepth=fscanf(fd,'%f/')  % 震源深度（km）
nerr=fscanf(fd,'%f/')    % 北南向水平误差（km）
eerr=fscanf(fd,'%f/')    % 东西向水平误差（km）
derr=fscanf(fd,'%f/')    % 震源深度误差（km） 
rms=fscanf(fd,'%f/')     % 残差均方根(sec）
event_id=fscanf(fd,'%i/')  % 事件标识号
% 读入震相数据
ph_i=fscanf(fd,'%s %f %f %s',[8,inf]);
ph_i=ph_i';   % 矩阵转置
ph_i;    %显示
ph_num=length(ph_i)   %震相到时数据个数
  for i=1:ph_num
    s1=char(ph_i(i,1));
    s2=char(ph_i(i,2));
    s3=char(ph_i(i,3));
    s4=char(ph_i(i,4));
    s5=char(ph_i(i,5));
    ph_sta(i,1:5)=[s1 s2 s3 s4 s5];
    ph_ori(1,i)=ph_i(i,6);
    ph(2,i)=ph_i(i,7);
    %当前只由P和S震相
    ph_phase(i)=char(ph_i(i,8));
    C=strncmp(ph_phase(i),'P',1);
    if C==1 
        ph(3,i)=1;  % P波代码为1
    end
    C=strncmp(ph_phase(i),'S',1);
    if C==1 
        ph(3,i)=2;  % S波代码为2
    end
end
ph_sta ;    %震相数据中的台站代码(5个字母)
fclose(fd);
% 找出震相数据对应的台站文件中的台站脚标
 for i=1:ph_num
     for j=1:st_num
         C=strcmp(ph_sta(i,1:5),st_name(j,1:5));
         if C==1
               ph(4,i)=j;
               break
         end
     end
 end
 % 对合成到时数据加入随机误差
%rr=0.2  % 随机误差水平（秒）
rr=0.1
%rr=0
for i=1:ph_num 
    R = normrnd(0,1);
    ph(1,i)=ph_ori(1,i)+rr*R;
end
%抛台为试验不同台站分布对定位结果的影响
% 到时数据分布: 单数序号为P,双数序号为S
% 每对数字所在位置为台站位置(自下而上:北,向右:东
% 9,10   19,20   29,30    39,40   49,50
% 7,8   .....
% 5,6   .....
% 3,4   .....
% 1,2    11,12   21,22   31,32    41,42
% 下例中去掉了下面的3排台站
%for i=1:6
 %   ph(2,i)=0;
  %  ph(2,i+10)=0;
   % ph(2,i+20)=0;
    %ph(2,i+30)=0;
    %ph(2,i+40)=0;
%end
% ------------------
% 下例中去掉了西面的3排台站
for i=1:30
   ph(2,i)=0;
end
%去掉东面的两排
%for i=11:50
%    ph(2,i)=0;
%end
%for i=1:10  %去掉S到时
%    ph(2,i*2)=0;
%end
%for i=1:4
%    ph(2,i)=0;
%end
% 网格搜索
%找出台网边缘
sta_xmin=1000;
sta_xmax=-1000;
sta_ymin=1000;
sta_ymax=-1000;
for i=1:1:st_num
    if sta(1,i)<sta_xmin
        sta_xmin=sta(1,i);
    end
    if sta(1,i)>sta_xmax
        sta_xmax=sta(1,i);
    end
    if sta(2,i)<sta_ymin
        sta_ymin=sta(2,i);
    end
    if sta(2,i)>sta_ymax
        sta_ymax=sta(2,i);
    end
end
sta_xmin
sta_xmax
sta_ymin
sta_ymax
%设定搜索范围和网格间距
%grid_d=5 %网格间距=5km
%grid_d=2 %网格间距=2km
%grid_d=1 %网格间距=1km   0.1km!
grid_d=0.5 %网格间距=0.5km
grid_x0=sta_xmin-20 %网格范围：左边缘=最左台站x坐标-50km
grid_xm=sta_xmax+20 %网格范围：右边缘=最右台站x坐标+50km
grid_y0=sta_ymin-10 %网格范围：下边缘=最南台站y坐标-50km
grid_ym=sta_ymax+10 %网格范围：上边缘=最北台站y坐标+50km
%grid_x0=150 %网格范围：左边缘
%grid_xm=200 %网格范围：右边缘
%grid_y0=50 %网格范围：下边缘
%grid_ym=100 %网格范围：上边缘 
% 网格搜索
% 计算搜索点数
num_x=round((grid_xm-grid_x0)/grid_d)+1
num_y=round((grid_ym-grid_y0)/grid_d)+1
z0_grid=13; % 对应震相数据SimpLoc.pha,试验时假定只搜索x0,y0
%z0_grid=11; % 对应震相数据syn2005001.pha,试验时假定只搜索x0,y0
for kx=1:1:num_x
    x0_grid=grid_x0+grid_d*(kx-1);
       for ky=1:1:num_y 
        y0_grid=grid_y0+grid_d*(ky-1);
        hypo(1)=x0_grid;
        hypo(2)=y0_grid;
        hypo(3)=z0_grid;
        hypo(4)=0;
        [r,o]=search_rms(model,hypo,sta,ph);
        res(kx,ky)=r;
        ori(kx,ky)=o;
       epizk(kx,ky)=z0_grid;
    end
end
kx_min=0;
ky_min=0;
%res_min=1000;
res_min=0.11;

for kx=1:1:num_x
    for ky=1:1:num_y
        if res(kx,ky)<res_min
            res_min=res(kx,ky);
            kx_min=kx;
            ky_min=ky;
        end
        
    end
end
kx_min
ky_min
epi_x=grid_x0+(kx_min-1)*grid_d
epi_y=grid_y0+(ky_min-1)*grid_d
epi_z=epizk(kx_min,ky_min)
epi_o=ori(kx_min,ky_min)
rms_final=res_min
for kx=1:1:num_x
    grid_x(kx)=grid_x0+(kx-1)*grid_d;
end
for ky=1:1:num_y
    grid_y(ky)=grid_y0+(ky-1)*grid_d;
end
sizeres=size(res)
%res(kx,ky)在话等值线时ky是横坐标，kx是纵坐标，不需换位
contour(grid_y,grid_x,res,500) %500条线！
%contour(grid_y,grid_x,res)
xlabel('km')
ylabel('km')
hold on
for i=1:ph_num
    if ph(2,i)>0.5
    %if ph(2,i)>grid_d
        plot(sta(2,ph(4,i)),sta(1,ph(4,i)),'+');hold on;
    end
end
%plot(sta(1,ph(4,i),sta(2,:),'+');hold on;
plot(epi_y,epi_x,'blacko');hold on;
plot(oy,ox,'r*');
%plot(sta(1,:),sta(2,:),'o');
title('红星：预期位置，黑圈：搜索结果')

c1=clock;
year1=num2str(c1(1),4);
manth1=num2str(c1(2),2);
day1=num2str(c1(3),2);
hour1=num2str(c1(4),2);
minute1=num2str(c1(5),2);
second1=num2str(c1(6),3);
str=['搜索法定位，起始时间  ',year,'-',manth,'-',day,' ',hour,':',minute,':',second];
disp(str)
str1=['搜索法定位，结束时间  ',year1,'-',manth1,'-',day1,' ',hour1,':',minute1,':',second1];
disp(str1)
str2=['搜索间隔',num2str(grid_d),'km'];
disp(str2)
