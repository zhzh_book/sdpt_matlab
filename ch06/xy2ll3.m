function [slat,slon]=xy2ll3(sx,sy,olat,olon)
%直角坐标换算经纬度,方法三（适用于北纬70度-南纬70度）
%朱介寿等《地震学中的计算方法》
% output
%       slat：台站纬度，单位度
%       slon：台站经度，单位度
% input
%       sx：台站直角坐标x值，单位公里
%       sy：台站直角坐标y值，单位公里
%       olat：原点纬度，单位度
%       olon：原点经度，单位度
% example
%     

    laix = 6378.137;       %km
    ellipse = 1/298.25722;  %使用1984年地球椭圆参数
    saix=laix-ellipse*laix;

    ell = (laix^2-saix^2)/(laix^2);
    rn = laix/((1-(ell*(sin(olat*(pi/180)))^2))^(1/2));
    rm = (laix*(1-ell))/((1-(ell*(sin(olat*(pi/180)))^2))^(3/2));
    
    slon = (sy/(rn*cos(olat*(pi/180)))+olon*pi/180)*180/pi;
    slat = (sx/rm-((sy^2)*tan(olat*(pi/180)))/(2*rm*rn)+olat*pi/180)*180/pi;
end
    