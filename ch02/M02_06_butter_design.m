%M02_06_butter_design
clear all
wp=6*2*pi
ws=8*2*pi
Ap=0.9
Rp=-20*log10(Ap)
Ap1=10^(-Rp/20)
As=0.1
As=0.2
Rs=-20*log10(As)
As1=10^(-Rs/20)
[N,Wc]=buttord(wp,ws,Rp,Rs,'s')
fc=Wc/(2*pi)
[z,p,k]=butter(N,Wc,'s')
[b,a]=zp2tf(z,p,k)
[h,w]=freqs(b,a);
plot(w/(2*pi),abs(h),'b');hold on
axis([0 15 0 1.1])
ylabel('��һ�����')
grid on
xlabel('Ƶ��/Hz')

% ��һ��
[z,p,k]=butter(N-1,Wc,'s')
fc1=Wc/(2*pi)
[b,a]=zp2tf(z,p,k)
[h,w]=freqs(b,a);
plot(w/(2*pi),abs(h),'r');hold on
axis([0 15 0 1.1])
ylabel('��һ�����')
grid on
xlabel('Ƶ��/Hz')
