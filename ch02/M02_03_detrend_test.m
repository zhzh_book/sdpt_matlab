%  去掉分段线性趋势
sig = [0 1 -2 1 0 1 -2 1 0 0 1 -2 1 0 1 -2 1 0];      % 无线性趋势的信号
trend = [0 1 2 3 4 3 2 1 0 0 1 2 3 4 3 2 1 0];      % 两段线性趋势
x = sig+trend;                    % 信号+趋势
y = detrend(x,'linear',[5 9 10 14])         % 断点在第5个元素
z=[1 : 18]
length(x)
length(y)
length(z)
figure(1)
plot(z,x,'-b.','LineWidth',2, 'MarkerSize',20);hold on
plot(z,y,'-.ro','LineWidth',2);hold on
plot(z,trend,':g+','LineWidth',2)
axis([0 19 -3 5] )
title('去线性趋势示例')
xlabel('数据点序号')
ylabel('无量纲数值')