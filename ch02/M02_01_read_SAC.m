%  读SAC波形文件
clear all
fil='2002146IUDAV00BHEM.SAC'
sacdata = importdata(fil, ' ', 30);
m=length(sacdata.data(:,1))
sac(1,:)=sacdata.data(:,1);
sac(2,:)=sacdata.data(:,2);
sac(3,:)=sacdata.data(:,3);
sac(4,:)=sacdata.data(:,4);
sac(5,:)=sacdata.data(:,5);
for i=1:m
    j=(i-1)*5+1;
    sacwave(j)=sac(1,i);
     sacwave(j+1)=sac(2,i);
     sacwave(j+2)=sac(3,i);
     sacwave(j+3)=sac(4,i);
     sacwave(j+4)=sac(5,i);
end
sacwave_len=length(sacwave)
sample_rate=20  % 据SEED的RESP文件
for j=1:sacwave_len
    time(j)=j/sample_rate;
end
figure(1)
plot(time,sacwave)
title({fil,'dist=5.54 deg.sample-rate=20s/s'})
