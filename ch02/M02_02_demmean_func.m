function [ Y,m ] =M02_02_demmean_func(X,M )
%demmean: 去掉滑动均值
%采用公式见第1章 地震波形预处理
% X为数组，数组长度为n，求滑动平均值的长度为2M+1,要求n>=2*M+1
n=length(X)
M
if 2*M+1<n+1 
for i=1:M
    m(i)=0;
    for k=1:2*M+1
    m(i)=m(i)+X(k);
    end
    m(i)=m(i)/(2*M+1);
    Y(i)=X(i)-m(i);
end
for i=M+1:n-M
     m(i)=0;
    for k=1:2*M+1
    m(i)=m(i)+X(i-M+k-1);
    end
    m(i)=m(i)/(2*M+1);
    Y(i)=X(i)-m(i);
end
for i=n-M+1:n
     m(i)=0;
    for k=1:2*M+1
    m(i)=m(i)+X(n-2*M+k-1);
    end
    m(i)=m(i)/(2*M+1);
    Y(i)=X(i)-m(i);
end
end


end

