%向下重采样  2020.10.30
clear all
t0=0.01;%原采样间隔
Fs0=1/t0; %原采样率 100sps
f0=Fs0/2; %原截止频率=1/2采样率=50Hz
t1=0.02;%新采样间隔
%t1=0.02
%t1=0.01
Fs1=1/t1;%新采样率50sps
ratio=t1/t0   %新采样间隔/原采样间隔=0.5
N0=40000;%原数据点数
N1=floor(N0*t0/t1) %新数据点数=20000
n=0:N0-1;
t=n/Fs0 ;%数据时间长度
zeta=Fs1/(2*f0)  % 重采样的低通滤波器参数
%zeta=1
n1=0:N1-1 ;%新采样点数
t1=n1/Fs1 ; %新数据时间长度
xn0=sin(2*pi*0.01*t)+sin(2*pi*1*t)+0.5*sin(2*pi*30*t);   %原始波形数据，含0.01、1、30Hz正弦波
%xn0=sin(2*pi*0.1*t);
figure(1)

% 重采样+低通滤波，生成x(m), 只包含0.01Hz和1Hz的信号
m1=floor(Fs0/ratio)+Fs0+1   % 
m2=N1-floor(Fs0/ratio)-1
for m=m1:m2
    x(m)=0;
    for n=floor(m*ratio)-Fs0:1:floor(m*ratio)+Fs0
        x(m)=x(m)+zeta*xn0(n)*sinc(zeta*(m*ratio-n));
    end
end
xl=length(x)

 %以高采样率（1000sps)画图
 Fs2=Fs0*10
 N2=N0*10
 n2=0:N2-1;
 t2=n2/Fs2;
 xn2=sin(2*pi*0.01*t2)+sin(2*pi*1*t2)+0.5*sin(2*pi*30*t2);   %原始波形数据，含0.01、1、30Hz正弦波
 plot(t2(1:N2),xn2(1:N2),'g','LineWidth',2);hold on   
 plot(t(1:N0),xn0(1:N0),'-r.')  ;hold on  %采样率100sps
axis([5 10 -1.5 2.5])
xlabel('t/s')
ylabel('无量纲数值')
 plot(t1(m1:m2),x(m1:m2),'--bo','LineWidth',2)  %采样率50sps
%{
figure(11);
Nf=1/t0   % =100
plot(t(1+1:N0-Nf),xn0(1+Nf:N0-1),'-.ro');hold on
%plot(t(1+1:N0-Nf),xn0(1+Nf:N0-1),'-.ro','LineWidth',4);hold on
% 重采样+低通滤波，生成x(m)
xlabel('时间/s')
m1=floor(Nf/ratio)+Nf+1   % 
m2=N1-floor(Nf/ratio)-1
for m=m1:m2
    x(m)=0;
    for n=floor(m*ratio)-Nf:1:floor(m*ratio)+Nf
        x(m)=x(m)+zeta*xn0(n)*sinc(zeta*(m*ratio-n));
           end
end

xl=length(x)

n1=0:N1-1;tt1=n1/Fs1;
 nn1=length(n1)
 ntt1=length(tt1)
 nx=length(x)
%figure(2);
% plot(tt1(1:xl),x(1:xl));
 %蓝线是向下重采样的结果.在重采样的同时已实现了低通滤波
     plot(tt1(floor(Nf/ratio)+Nf+1-Fs1+1:N1-floor(Nf/ratio)-1-Fs1+1),x(floor(Nf/ratio)+Nf+1:N1-floor(Nf/ratio)-1)*zeta,'b');hold on
 %plot(tt1(floor(Nf/ratio)+Nf+1-Fs1+1:N1-floor(Nf/ratio)-1-Fs1+1),x(floor(Nf/ratio)+Nf+1:N1-floor(Nf/ratio)-1),'-b','LineWidth',4,'Markersize',20);hold on
 ty=0:0.001:4096*t0-Nf/Fs0;
 z=sin(2*pi*1*(ty+Nf/Fs0-0.01))+0.5*sin(2*pi*30*(ty+Nf/Fs0-0.01));
 %z=sin(2*pi*0.1*(ty+Nf/Fs0))
% plot(ty,z,'--y')
 %plot(ty,z,'--y','LineWidth',2)
% axis([5 10 -1.5 1.5])
%----------------------------------------------------------------------------------------------------------------------
 figure(12)
 plot(t(1+1:N0-Nf),xn0(1+Nf:N0-1),'-.ro','LineWidth',4);hold on
% 重采样+低通滤波
xlabel('时间/s')
 plot(tt1(floor(Nf/ratio)+Nf+1-Fs1+1:N1-floor(Nf/ratio)-1-Fs1+1),x(floor(Nf/ratio)+Nf+1:N1-floor(Nf/ratio)-1),'-b','LineWidth',4,'Markersize',20);hold on
 ty=0:0.001:4096*t0-Nf/Fs0;
 z=sin(2*pi*1*(ty+Nf/Fs0-0.01))+0.5*sin(2*pi*30*(ty+Nf/Fs0-0.01));
 %z=sin(2*pi*0.1*(ty+Nf/Fs0))
 plot(ty,z,'--y','LineWidth',2)
 axis([5 5.2 -1 1.5])
 %------------------------------------------------------------------------------------------------------------
%}