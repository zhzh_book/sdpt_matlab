%M03_xx_antifilter_freq.m
% 读入示例地震波形：巴楚地震台CTS-1垂直向记录M7.72013年4月16日伊朗-巴基斯坦交界地震
%XX_BCH_BHZ_2.txt
%按+/-10V量程输入数据采集器，转换灵敏度为1.192e-6V/count
%CTS-1地震计灵敏度1000V/m/s (2000V/m/s?)   标称值
%得到地动速度到数字记录的转换灵敏度G=1000/1.192e-6=8.3893e+8count/m/s
clear all
C=(1000/1.192e-6)/1.0e+9  % 0.8389 count/nm/s     count/纳米/s    按标称值给出
C=1048.77/1000     %XX_BCH_BHZ_2.par中的calib factor=1048.77   count/um/s
load XX_BCH_BHZ_2.txt
Fs=100
wave=XX_BCH_BHZ_2;
wavelen=length(wave)
n1=6000
n2=4000
n2=0
n1=60000
wave1([1:n1])=wave([n2+1:n2+n1]);
wavelen=length(wave1)
t1=([1:n1])/Fs;
figure(1)
plot(t1,wave1)
xlabel('时间/s')
ylabel('count')
title('CTS-1原始记录')
fft_wave1=fft(wave1);
%以CTS-1地震计为例，在频率域进行地震仪器校正
%Num_zero=2
Num_zero=3  %位移响应
Num_pole=10
ze(1)=0+0i;
ze(2)=0+0i;
ze(3)=0;

pole=[-0.037025+0.037024i  -0.037025-0.037024i   -266.57+266.57i   -266.57-266.57i    -333.81+89.403i    -333.81-89.403i  .....
    -244.35-244.36i   -244.35+244.36i  -89.440+333.80i    -89.440-333.80i]
m=2.4206e+20     %10极点的归一化因子（公式（3.33）中的m，归一化参考频率=0.1Hz）
[b,a]=zp2tf(ze',pole',m)   %零极点必须是列矢量
df=Fs/wavelen
n=floor(Fs/df)
w=[1:n]*df*2*pi;
[h]=freqs(b,a,w);

w;
wl=length(w)
figure(2)
subplot(2,1,1)
%semilogx(w/(2*pi),abs(h),'b');hold on   %幅频响应
loglog(w/(2*pi),abs(h),'b');hold on
title('CTS-1地震计3零点10极点位移响应')
%plot(w,abs(h),co);hold on
%axis([0 15 0 1.1])
ylabel('归一化振幅')
grid on
xlabel('频率/Hz')
%axis([0 1 0 1.1])
%set(gca,'YTick',0:0.1:1.1)
subplot(2,1,2)
%semilogx(w/(2*pi),unwrap(angle(h))*180/pi,'b');hold on
semilogx(w/(2*pi),unwrap(angle(h))*180/pi,'b');hold on
%axis([0 15 -1000 1000])
grid on
ylabel('相位/°')
xlabel('频率/Hz')
%-------------------------------------------------------

w_wave1=([1:n1])*2*pi*Fs/n1;
f_wave1=w_wave1/(2*pi);
h_wave1=freqs(a,b,w_wave1);   %构成反滤波器
h1_wave1=zeros(1,n1);
%构建与傅里叶变换得到的前段与后端共轭形式数据对应的滤波器
%万永革，2012，第429页）
%简单地高通：截断0-0.0083Hz，带来一点边界效应
%ff=0
%ff=0.0083
%ff=0.02
ff=0.04
%ff=0.05
nn=floor(ff/df)
for i=1:nn
h_wave1(i)=0;
end
for ii=1:n1/2
    h1_wave1(ii)=h_wave1(ii);
    h1_wave1(n1-ii)=conj(h1_wave1(ii));
end

figure(3)                    %位移反滤波器
subplot(211)
loglog(f_wave1,abs(h1_wave1)/C)
title('CTS-1地震计位移仪器校正滤波器')
xlabel('频率/Hz')
ylabel('校正灵敏度/nm/count')
grid on
subplot(212)
semilogx(f_wave1,unwrap(phase(h1_wave1))*180/pi,'b');hold on
grid on
ylabel('相位/°')
xlabel('频率/Hz')
%axis([0 100 0 1.1])
%----频率域滤波
for i=1:n1
fft_ywave1(i)=fft_wave1(i)*h1_wave1(i);
end

ywave1=real(ifft(fft_ywave1))/C;                        %变成地动位移nm
figure(4)
plot(t1,ywave1)
title('CTS-1校正后的地动位移')
xlabel('时间/s')
ylabel('位移/nm')
figure(5)
loglog(f_wave1(1:n1/2),abs(fft_wave1(1:n1/2)))
title('滤波前')
xlabel('频率/Hz')
ylabel('谱振幅')
figure(6)
loglog(f_wave1(1:n1/2),abs(fft_ywave1(1:n1/2)))
title('滤波后')
xlabel('频率/Hz')
ylabel('谱振幅')
%——————位移反滤波器，是速度响应加一个零值零点
%用DD-1记录该地震的波形
%由固有周期、阻尼及电子电路传递函数参数计算零极点
%DD-1

% DD1的仪器常数：
T1=1.0
D1=0.45
T3=0.05
D3=0.707
%n1=2*pi/T1
%a1=-n1*D1
%a2=n1*sqrt(D1^2-1)
%p1=a1+a2
%p2=a1-a2
%p1 p2
n01=2*pi/T1
a1=-n01*D1
a2=n01*sqrt(D1^2-1)
pole(1)=a1+a2    %极点1
pole(2)=a1-a2    %极点2
%p6 p7
n3=2*pi/T3
a1=-n3*D3
a2=n3*sqrt(D3^2-1)
pole(3)=a1+a2    %极点3
pole(4)=a1-a2    %极点4
zero(1)=0
zero(2)=0
zero(3)=0
%（据王广福参数）
%电子电路参数（分子）：
b0=-0.0291
b1=0.3279
b2=11.8101
b3=0.0243
p=[b3 b2 b1 b0]
zero1=roots(p)   %电子电路造成的3个非零的零点 
zero(4)=zero1(1)
zero(5)=zero1(2)
zero(6)=zero1(3)
%电子电路参数（分母）：
a1=1.7287
a2=0.8314
a3=0.1106
a0=1
p=[a3 a2 a1 a0]
poles1=roots(p)  %电子电路造成的3个极点 p3,p4,p5
pole(5)=poles1(1)
pole(6)=poles1(2)
pole(7)=poles1(3)
zero
pole
%DD-1地震仪的6零点和7极点
k=1
[b_dd1,a_dd1]=zp2tf(zero',pole',k)
w_dd1=([1:n1])*2*pi*Fs/n1;
h_dd1=freqs(b_dd1,a_dd1,w_dd1);
f0=4
f0_num=f0/df
abs_f0=abs(h_dd1(f0_num))
k=1/abs_f0
for i=1:n1
h_dd1(i)=h_dd1(i)*k;
end
abs_f01=abs(h_dd1(0.1/df))*1000
abs_f4=abs(h_dd1(4/df))
f_wave1=w_dd1/(2*pi);
 h1_dd1=zeros(1,n1);
%构建与傅里叶变换得到的前段与后端共轭形式数据对应的滤波器
%万永革，2012，第429页）

for ii=1:n1/2
    h1_dd1(ii)=h_dd1(ii);
    h1_dd1(n1-ii)=conj(h_dd1(ii));
end
disp('OKOKOKOK')
figure(7)                    %位移DD-1
semilogx(f_wave1,abs(h1_dd1))
title('DD-1位移响应')
%axis([0 100 0 1.1])
%----频率域滤波
fft_ywave1=fft(ywave1);   %地动位移
for i=1:n1
fft_zwave1(i)=fft_ywave1(i)*h1_dd1(i);
end

zwave1=real(ifft(fft_zwave1));
figure(8)
plot(t1,zwave1)
xlabel('时间/s')
ylabel('位移/nm')   %仿真DD-1，放大倍数=1
title('DD-1位移记录，放大倍数=1')
figure(9)
loglog(f_wave1(1:n1/2),abs(fft_wave1(1:n1/2)))
title('滤波前')
xlabel('频率/Hz')
ylabel('谱振幅')
figure(10)
loglog(f_wave1(1:n1/2),abs(fft_zwave1(1:n1/2)))
title('滤波后')
xlabel('频率/Hz')
ylabel('谱振幅')
% 去DD-1响应
h_dd1=freqs(a_dd1,b_dd1,w_dd1);
f0=4
f0_num=f0/df
abs_f0=abs(h_dd1(f0_num))
k=1/abs_f0
h_dd1=h_dd1*k;
abs_f01=abs(h_dd1(0.1/df))*1000
f_wave1=w_dd1/(2*pi);
 h1_dd1=zeros(1,n1);
%构建与傅里叶变换得到的前段与后端共轭形式数据对应的滤波器
%万永革，2012，第429页）
%nn=floor(0.01/df)+1
%for i=1:nn
   % h_dd1(i)=0;
%end
for ii=1:n1/2
    h1_dd1(ii)=h_dd1(ii);
    h1_dd1(n1-ii)=conj(h_dd1(ii));
end

figure(11)                    %位移DD-1
plot(f_wave1,abs(h1_dd1))
%axis([0 100 0 1.1])
%----频率域滤波,去DD-1
for i=1:n1
fft_xwave1(i)=fft_zwave1(i)*h1_dd1(i);
end

xwave1=real(ifft(fft_xwave1));
figure(12)
plot(t1,xwave1)
xlabel('时间/s')
ylabel('位移/nm')
title('DD-1仪器校正后的地动位移')
figure(9)
loglog(f_wave1(1:n1/2),abs(fft_zwave1(1:n1/2)))
title('滤波前')
xlabel('频率/Hz')
ylabel('谱振幅')
figure(13)
loglog(f_wave1(1:n1/2),abs(fft_xwave1(1:n1/2)))
title('滤波后')
xlabel('频率/Hz')
ylabel('谱振幅')


