%td2paz
clear all
%由固有周期、阻尼及电子电路传递函数参数计算零极点
%DD-1
%电子电路参数（分子）：王广福，1986，p
b0=-0.0291
b1=0.3279
b2=11.8101
b3=0.0243
b=[b3 b2 b1 b0]

z=roots(b)   %电子电路造成的3个非零的零点

%电子电路参数（分母）：
a1=1.7287
a2=0.8314
a3=0.1106
a0=1
a=[a3 a2 a1 a0]
p=roots(a)  %电子电路造成的3个极点 p3,p4,p5
%从有理分式到零极点表示，要乘以因子b3/a3
b3a3=b3/a3
va0=106.733
omega0=47.8
df=0.01
n=10000
w=[1:n]*df*2*pi;
ha=freqs(b,a,w);
figure(101)
loglog(w/(2*pi),abs(ha));hold on
[b,a]=zp2tf(z,p,1)
hb=freqs(b,a,w);
loglog(w/(2*pi),abs(hb)*b3a3,'r*');
% DD1的仪器常数：
T1=1.0
D1=0.45
T3=0.05
D3=0.707
%n1=2*pi/T1
%a1=-n1*D1
%a2=n1*sqrt(D1^2-1)
%p1=a1+a2
%p2=a1-a2
%p1 p2
n1=2*pi/T1
a11=-n1*D1
a12=n1*sqrt(D1^2-1)
p1=a11+a12    %极点1
p2=a11-a12    %极点2
%p6 p7
n3=2*pi/T3
a31=-n3*D3
a32=n3*sqrt(D3^2-1)
p6=a31+a32    %极点6
p7=a31-a32    %极点7
pole7=[p1 p2 p(1) p(2) p(3) p6 p7]
zero7=[0 0 0 z(1) z(2) z(3)]
k3=1
[b7,a7]=zp2tf(zero7',pole7',k3)
[h0]=freqs(b7,a7,w);
%----------------------------------------
%计算归一化角频率omega0
nn=100000
f=[1:nn]*0.01;
s=2*pi*f*1i;
j=1

for i=1:nn
    absh(i)=abs(s(i)^2*n3^2/((s(i)-pole7(1))*(s(i)-pole7(2))*(s(i)-pole7(6))*(s(i)-pole7(7))));
    if absh(i)>0.999 &absh(i)<1.001
        f0(j)=f(i);
        absh0(j)=absh(i);
        ii(j)=i;
        j=j+1;
    end
end
    f00=f0
    absh00=absh0
    ii0=ii
%得到归一化频率=7.6Hz

om0=2*pi*7.6000   %omega0 =47.7522
s=om0*1i
C3=abs((1+a1*s+a2*(s^2)+a3*(s^3))/(b0+b1*s+b2*(s^2)+b3*(s^3)))   %0.4475
C3f0=C3/om0   %0.009372
C3f0_100=C3f0*100
C3f01=1/C3f0   % 106.7002             (王广福，1986，p81, V0a=106.733  
%----------薛兵
C3_1=abs(s+4.545)   %47.9680
C3f0_1=C3_1/om0   %1.0045
C3f0_1_100=C3f0_1*100
%---------------------------------

%简单积分放大器 时间常数=0.22s
tao=0.22
p3=-1/tao
pole5=[p1 p2 p3 p6 p7]
zero5=[0 0 0]
b11=1
b12=-2*a11
b13=n1^2
b21=-p3
b31=1
b32=-2*a31
b33=n3^2
ze=3
po=5
k3=n3^2
[b5,a5]=zp2tf(zero5',pole5',k3)
[h]=freqs(b5,a5,w);
figure(2)
loglog(w/(2*pi),abs(h)*C3f0_1,'r*'); hold on %5极点全响应
zero1=[0 0 0]
pole1=[p1 p2]
% 归一化频率7.60Hz，得到地震计的速度传递函数归一化因子c1=0.9898:
c1=abs((s-p1)*(s-p2)/s^2)
k1=c1/om0    %引入位移传递函数归一化因子c1/（2*pi*f0), f0=7.6Hz
[b1,a1]=zp2tf(zero1',pole1',k1)
[h1]=freqs(b1,a1,w);

%figure(2)
loglog(w/(2*pi),abs(h1),'b');  hold on%地震计
zero3=[ ]
pole3=[p6 p7]
% 归一化频率7.60Hz，得到记录笔的传递函数归一化因子c3=1.0101:
c3=abs((s-p6)*(s-p7)/k3)
[b3,a3]=zp2tf(zero3',pole3',c3*k3)
[h3]=freqs(b3,a3,w);

%figure(3)
loglog(w/(2*pi),abs(h3),'g');  hold on %记录器笔
zero2=[ ]
pole2=[p3]
k2=C3_1
[b2,a2]=zp2tf(zero2',pole2',k2)
[h2]=freqs(b2,a2,w);
%figure(4)
loglog(w/(2*pi),abs(h2),'y'); %积分放大器
legend('5极点全响应,地震计响应,记录笔响应,放大器响应')
xlabel('频率/Hz')
ylabel('归一化振幅')
title('DD-1地震仪的归一化振幅响应，归一化频率=7.6Hz')
figure(3)
loglog(w/(2*pi),abs(h2),'r'); hold on%积分放大器
zero4=[z(1) z(2) z(3) ]
pole4=[p(1) p(2) p(3)]
k4=1   %由有理分式到零极点表示时引入的常数因子，是分子s3系数b3与分母s3系数a3之比
[b4,a4]=zp2tf(zero4',pole4',k4)
[h4]=freqs(b4,a4,w);
%[h4]=freqs(b,a,w);  %两个算法是一致的，相差系数b3a3
loglog(w/(2*pi),abs(h4)*b3a3*C3,'b'); %3个极点3个非零零点的电子电路,C3是归一化因子
%loglog(w/(2*pi),abs(h4)*C3,'b');      %用原来给定的有理分式就不需要乘以b3a3
legend('1极点无零点','3极点3非零零点')
xlabel('频率/Hz')
ylabel('归一化振幅')
figure(4)
for i=1:n
h0(i)=h1(i)*h3(i)*h4(i);
end
loglog(w/(2*pi),abs(h0)*C3*b3a3,'b');  hold on%全响应7极点
loglog(w/(2*pi),abs(h)*C3f0_1,'r'); %全响应5极点
legend('DD-1 7极点','DD-1 5极点')
xlabel('频率/Hz')
ylabel('归一化振幅')
figure(5)
c=180/pi
semilogx(w/(2*pi),unwrap(phase(h0))*c+360,'b');  hold on%全响应7极点
semilogx(w/(2*pi),unwrap(phase(h))*c+360,'r'); %全响应5极点
legend('DD-1 7极点','DD-1 5极点')
xlabel('频率/Hz')
ylabel('相位/°')
%
s=2*pi*7.6*1i
ss=s^2
%归纳
om3=2*pi/T3
om32=om3^2

z7=[0 0 0  -485.9846   -0.0654    0.0377]
p7=[ -2.8274+5.6111i  -2.8274-5.6111i  -88.8442+88.8711i   -88.8442-88.8711i  -4.4771   -2.0594   -0.9806]
[b7,a7]=zp2tf(z7',p7',1)
[h7]=freqs(b7,a7,w);
z5=[0 0 0]
p5=[ -2.8274+5.6111i  -2.8274-5.6111i  -88.8442+88.8711i   -88.8442-88.8711i  -4.545]
c5=C3f0_1*om32
[b5,a5]=zp2tf(z5',p5',1)
[h5]=freqs(b5,a5,w);
figure(6)
h5f0=abs(h5(760))*c5
c7=C3f0*b3a3*om32
h7f0=abs(h7(760))*c7
loglog(w/(2*pi),abs(h7)*c7,'b');  hold on%全响应7极点
loglog(w/(2*pi),abs(h5)*c5,'r');  hold on%全响应5极点
legend('7极点','5极点')
%  按薛兵p105更正后的响应
b111=[1 0 0 0]
a111=[0 1 5.655 39.48]
b211=[1]
a211=[1 4.545]
b311=15791
a311=[1 177.7 15791]
h111=freqs(b111,a111,w);
h211=freqs(b211,a211,w);
h311=freqs(b311,a311,w);
df=0.01
n=10000
w=[1:n]*df*2*pi;
for i=1:n
    h123(i)=h111(i)*h211(i)*h311(i);
end
figure(7)
c123=C3f0_1   %1.0045   f0=7.6Hz
semilogx(w/(2*pi),abs(h123),'r*');hold on
semilogx(w/(2*pi),abs(h111),'b');  hold on
semilogx(w/(2*pi),abs(h211),'g');  hold on
semilogx(w/(2*pi),abs(h311),'y');  hold on
semilogx(w/(2*pi),abs(h123)*c123,'r')