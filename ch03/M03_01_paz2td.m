%paz2td
%由零极点求地震计的固有周期和阻尼
%ns=sqrt(p1*p2);Ts=2*pi/ns
%Ds=-(p1+p2)/(2*ns)
%ng=sqrt(p3*p4);Tg=2*pi/ng
%Dg=-(p3+p4)/(2*ng)
%WWSSN-SP
p1=-3.3678-3.7315i
p2=-3.3678+3.7315i
p3=-7.0372-4.5456i
p4=-7.0372+4.5456i
ns=sqrt(p1*p2);Ts=2*pi/ns
Ds=-(p1+p2)/(2*ns)
ng=sqrt(p3*p4);Tg=2*pi/ng
Dg=-(p3+p4)/(2*ng)
%WWSSN-LP
p1=-0.4189
p2=-0.4189
p3=-0.062832
p4=-0.062832
ns=sqrt(p1*p2);Ts=2*pi/ns
Ds=-(p1+p2)/(2*ns)
ng=sqrt(p3*p4);Tg=2*pi/ng
Dg=-(p3+p4)/(2*ng)
%Kirnos SKD NMSOP

p1=-0.1257-0.2177i
p2=-0.1257+0.2177i
p3=-80.1093
p4=-0.3154
ns=sqrt(p1*p2);Ts=2*pi/ns
Ds=-(p1+p2)/(2*ns)
ng=sqrt(p3*p4);Tg=2*pi/ng
Dg=-(p3+p4)/(2*ng)
%EDSP-IAS SK
disp('EDSP-IAS SK')
p1=-0.127+0.25i
p2=-0.127-0.25i
p3=-0.263
p4=-83.6
ns=sqrt(p1*p2);Ts=2*pi/ns
Ds=-(p1+p2)/(2*ns)
ng=sqrt(p3*p4);Tg=2*pi/ng
Dg=-(p3+p4)/(2*ng)