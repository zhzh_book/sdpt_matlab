%计算电流计记录电动式地震仪的等效参数
%参考文献：王广福，1986，地震观测系统的标定与检查，地震出版社
%以基式垂直向地震仪为例
%先试验选择ini=4*epsilon_s00*epsilon_g00*sigma200   的步长：任给一个步长，循环4次，可判断步长的取值
clear all

%WWSSN-LP  (763)   OK 结果同王广福（1986）第75页
Ts=15.0
Ds=1.0
Tg=100
Dg=1.0
sigma2=0.1
%
%SKH   OK 结果同王广福（1986）第75页
Ts=12.5
Ds=0.45
Tg=1.2
Dg=5.0
sigma2=0.1
%WWSSN-LP  (763)   OK 结果同王广福（1986）第75页
Ts=15.0
Ds=1.0
Tg=100
Dg=1.0
sigma2=0.1
omega_s=2*pi/Ts
epsilon_s=omega_s*Ds
omega_g=2*pi/Tg
epsilon_g=omega_g*Dg
 omega_s2=omega_s^2
 omega_g2=omega_g^2
 m=2*(epsilon_s+epsilon_g)
 p=omega_s2+omega_g2+4*epsilon_s*epsilon_g*(1-sigma2)
 q=2*(epsilon_s*omega_g2+epsilon_g*omega_s2)
 n=omega_s2*omega_g2
% u=omega_s2+omega_g2
 b=[1 -p m*q-4*n -q^2-(m^2-4*p)*n]
 u_root=roots(b)
 k=0
 for i=1:3
     if imag(u_root(i))==0
         u=u_root(i)   %得到u的实根
         i_index(k+1)=i
         k=k+1
     end
     if (k>1)
         u=u_root(i_index(1))
     end
 end
 uu=u^3 -p*u^2+ (m*q-4*n)*u -q^2-(m^2-4*p)*n

 sq1=sqrt((u/2)^2-n)
  if Ts>Tg
 omega_se2=u/2-sq1;  %正向
 else
  omega_se2=u/2+sq1;  %反向
 end
  omega_ge2=n/omega_se2
 omega_se=sqrt(omega_se2)
 omega_ge=sqrt(omega_ge2)
 
 sq2=sqrt((m/4)^2-(p-u)/4)
 if Ts>Tg
 Dse=(m/4-sq2)/omega_se  %正向
 else
 Dse=(m/4+sq2)/omega_se  %反向
 end
 Dge=(p-u)/(4*omega_se*omega_ge*Dse)
 Tse=2*pi/omega_se
 Tge=2*pi/omega_ge
 u=omega_s2+omega_g2
 ue=omega_se2+omega_ge2
 disp('OKOKOKOK')
 %用等效常数计算地震计和电流计以及整个地震仪的传递函数
 ne=2*pi/Tse
 ng=2*pi/Tge
 b1=[1 0 0]
 a1=[1 2*ne*Dse ne^2]
 z1=roots(b1)
 p1=roots(a1)
 n=1000
 [h1,w1]=freqs(b1,a1,n)
 
 %逆运算
 %由等效常数求原始常数 SKZ
 clear all
 %SKZ    试算OK
% Tse=11.2232
 %Dse=0.3722
 %Tge=1.3365
 %Dge=5.5726
 %Ts00=12.5    %已知
% Tse=12.5
 %Dse=0.45
 %Tge=1.2
 %Dge=5.0
 %ini=4*epsilon_s*epsilon_g*0.3
 disp('WWSSN-LP')
 Tse=15.29
 Dse=0.978
 Tge=96.18
 Dge=1.045
 Tg00=96.0
 Ts00=15.0
 Dg00=1.0
 %64型     正向  试算OK  结果同王广福（1986）第70页
 Tse=1.3178
 Dse=0.4571
 Tge=0.1138
 Dge=5.6898
 Ts00=1.5
 %检验程序用-----------------
 sigma200=0.3
 Ds00=0.5
 Dg00=5.0
 Ts00=1.5
 Tg00=0.1
 %SKZ
  Tse=11.2232
 Dse=0.3722
 Tge=1.3365
 Dge=5.5726
 Ts00=12.5    %已知
 omega_s00=2*pi/Ts00
omega_g00=2*pi/Tg00
epsilon_s00=omega_s00*Ds00
 epsilon_g00=omega_g00*Dg00
%----------------------------
ini=0.0
 ini_step=10   %针对64型
 i_max=10000%针对64型
 %WWSSN-LP    Tse<Tge  反向
 
 ini=0
 ini_step=0.0001
 
 for i=1:i_max
    ini=ini+ini_step;
 % ini=4*epsilon_s00*epsilon_g00*sigma200   %为检验程序
  omega_se=2*pi/Tse;
epsilon_se=omega_se*Dse;
omega_ge=2*pi/Tge;
epsilon_ge=omega_ge*Dge;
 omega_se2=omega_se^2;
 omega_ge2=omega_ge^2;
  m_bar=2*(epsilon_se+epsilon_ge);
 p_bar=omega_se2+omega_ge2+4*epsilon_se*epsilon_ge+ini;
 q_bar=2*(epsilon_se*omega_ge2+epsilon_ge*omega_se2);
 n_bar=omega_se2*omega_ge2;
 b=[1  -p_bar  m_bar*q_bar-4*n_bar  -q_bar^2-(m_bar^2-4*p_bar)*n_bar];
 u_root=roots(b);
 for j=1:3
     if imag(u_root(j))==0;
         u_e=u_root(j) ;  %得到u的实根
         i_index=j;
     end
 end
 uu_e=u_e^3 -p_bar*u_e^2+ (m_bar*q_bar-4*n_bar)*u_e -q_bar^2-(m_bar^2-4*p_bar)*n_bar;
 sq1=sqrt((u_e/2)^2-n_bar);
 if Tse>Tge
 omega_s2=u_e/2-sq1;
 else
  omega_s2=u_e/2+sq1;  %反向
 end
 omega_g2=n_bar/omega_s2;
 omega_s=sqrt(omega_s2);
 omega_g=sqrt(omega_g2);
 sq2=sqrt((m_bar/4)^2-(p_bar-u_e)/4);
 if Tse>Tge
 Ds(i)=(m_bar/4-sq2)/omega_s;
 else
 Ds(i)=(m_bar/4+sq2)/omega_s; %反向
 end
 Dg(i)=(p_bar-u_e)/(4*omega_s*omega_g*Ds(i));
 Ts(i)=2*pi/omega_s;
 Tg(i)=2*pi/omega_g;
 epsilon_s=omega_s*Ds(i);
 epsilon_g=omega_g*Dg(i);
 
 ini=0
 ini_step=0.0001
 
 for i=1:i_max
    ini=ini+ini_step;
 % ini=4*epsilon_s00*epsilon_g00*sigma200   %为检验程序
  omega_se=2*pi/Tse;
epsilon_se=omega_se*Dse;
omega_ge=2*pi/Tge;
epsilon_ge=omega_ge*Dge;
 omega_se2=omega_se^2;
 omega_ge2=omega_ge^2;
  m_bar=2*(epsilon_se+epsilon_ge);
 p_bar=omega_se2+omega_ge2+4*epsilon_se*epsilon_ge+ini;
 q_bar=2*(epsilon_se*omega_ge2+epsilon_ge*omega_se2);
 n_bar=omega_se2*omega_ge2;
 b=[1  -p_bar  m_bar*q_bar-4*n_bar  -q_bar^2-(m_bar^2-4*p_bar)*n_bar];
 u_root=roots(b);
 for j=1:3
     if imag(u_root(j))==0;
         u_e=u_root(j) ;  %得到u的实根
         i_index=j;
     end
 end
 uu_e=u_e^3 -p_bar*u_e^2+ (m_bar*q_bar-4*n_bar)*u_e -q_bar^2-(m_bar^2-4*p_bar)*n_bar;
 sq1=sqrt((u_e/2)^2-n_bar);
 if Tse>Tge
 omega_s2=u_e/2-sq1;
 else
  omega_s2=u_e/2+sq1;  %反向
 end
 omega_g2=n_bar/omega_s2;
 omega_s=sqrt(omega_s2);
 omega_g=sqrt(omega_g2);
 sq2=sqrt((m_bar/4)^2-(p_bar-u_e)/4);
 if Tse>Tge
 Ds(i)=(m_bar/4-sq2)/omega_s;
 else
 Ds(i)=(m_bar/4+sq2)/omega_s;   %反向
 end
 Dg(i)=(p_bar-u_e)/(4*omega_s*omega_g*Ds(i));
 Ts(i)=2*pi/omega_s;
 Tg(i)=2*pi/omega_g;
 epsilon_s=omega_s*Ds(i);
 epsilon_g=omega_g*Dg(i);
 %if abs(Tg(i)-Tg00)<0.1
%if abs(Ts(i)-Ts00)<0.1  
if abs(Ts(i)-Ts00)<0.01    
     Ds0=Ds(i)
     Ts0=Ts(i)
     Tg0=Tg(i)
     Dg0=Dg(i)
     %sigma20=i*ini_step/(4*epsilon_s*epsilon_g)
     sigma20=ini/(4*epsilon_s*epsilon_g)
     i0_index=i
  
 end
 end
     Ds0=Ds(i)
     Ts0=Ts(i)
     Tg0=Tg(i)
     Dg0=Dg(i)
     %sigma20=i*ini_step/(4*epsilon_s*epsilon_g)
     sigma20=ini/(4*epsilon_s*epsilon_g)
     i0_index=i
  
 end
 
 %Ts = 1.00 s and Tg = 0.75 s for WWSSN-SP and Ts = 15.0 s and Tg = 100.0 s for WWSSN-LP
 