%最小相位系统
%以DD-1为例（3零点5极点）
clear all
pole5=[ -2.8274+5.6111i  -2.8274-5.6111i  -4.5455  -88.8442+88.8711i  -88.8442-88.8711i]
%pole5=[ -2.8274+5.6111i  -2.8274-5.6111i  -4.5455  ]
zero3=[0 0 0]
k=1000
[b,a]=zp2tf(zero3',pole5',k)
%n=10000
n=32768
df=0.01
 f=[1:n]*0.01;   % 从0.01Hz到327.68Hz
 w=f*2*pi;
[h]=freqs(b,a,w);
h1=h;
 amp=abs(h1);   
 logamp=log(amp);   %以2为底的对数
 logamp(1)
 logamp(n)
 pha=phase(h1);
 ap=hilbert(logamp);   %求振幅谱自然对数的希尔伯特变换
 ap_r=real(ap);    %实部为logamp
 ap_i=imag(ap);   %虚部为相位谱
 figure(1)
 subplot(3,1,1)
semilogx(f,logamp,'r');hold on
 semilogx(f,ap_r,'y.')
 title('DD-1地震仪振幅响应')'
 ylabel('振幅的自然对数')
 subplot(3,1,2)
semilogx(f,pha*180/pi,'r');hold on
 semilogx(f,(-ap_i)*180/pi,'b')
 legend('传递函数','希尔伯特')
ylabel('相位/°')
 title('相位响应')
 subplot(3,1,3)
 semilogx(f,(pha+ap_i)*180/pi)
 title('相位响应差值（传递函数-希尔伯特）')
  xlabel('频率/Hz')
  ylabel('相位/°')
  figure(2)
 pa=hilbert(pha);
 pa_r=real(pa);
 pa_i=imag(pa);
 subplot(2,1,1)
 semilogx(f,pa_i,'r');hold on
semilogx(f,log(amp),'b')
 subplot(2,1,2)
 semilogx(f,pa_r*180/pi,'r');hold on
 semilogx(f,pha*180/pi,'b')
 h1=zeros(1,n);
%构建与傅里叶变换得到的前段与后端共轭形式数据对应的滤波器
%万永革，2012，第429页）
for ii=1:n/2
   h1(ii)=h(ii);
    h1(n-ii+1)=conj(h(ii));
end


amp=abs(h1);   
 logamp=log(amp);
 logamp(1)
 logamp(n)
 pha=phase(h1);
 ap=hilbert(log(amp));   %求振幅谱自然对数的希尔伯特变换
 ap_r=real(ap);    %实部为logamp
 ap_i=imag(ap);   %虚部为相位谱
 figure(3)
 subplot(3,1,1)
semilogx(f,log(amp),'r');hold on
 semilogx(f,ap_r,'b')
 title('DD-1地震仪振幅响应')'
 ylabel('振幅的自然对数')
 subplot(3,1,2)
semilogx(f,pha*180/pi,'r');hold on
 semilogx(f,(-ap_i)*180/pi,'b')
 legend('传递函数','希尔伯特')
ylabel('相位/°')
 title('相位响应')
 subplot(3,1,3)
 semilogx(f,(pha+ap_i)*180/pi)
 title('相位响应差值（传递函数-希尔伯特）')
  xlabel('频率/Hz')
  ylabel('相位/°')
  figure(4)
 pa=hilbert(pha);
 pa_r=real(pa);
 pa_i=imag(pa);
 subplot(2,1,1)
 semilogx(f,pa_i,'r');hold on
semilogx(f,log(amp),'b')
 subplot(2,1,2)
 semilogx(f,pa_r*180/pi,'r');hold on
 semilogx(f,pha*180/pi,'b')
 %SKZ(M02_14_td2paz_SK.m)
 p =[  -0.2233+0.4729i  -0.2233-0.4729i  -51.8758  -0.4882 ]         
z =[0     0     0]
 b_all =[0   52.3641         0         0         0]
a_all =[1.0000   52.8107   48.9863   25.6316    6.9264]
[h]=freqs(b_all,a_all,w);

 h1=h;
 amp=abs(h1);   
 logamp=log(amp);
 logamp(1)
 logamp(n)
 pha=phase(h1);
 ap=hilbert(log(amp));   %求振幅谱自然对数的希尔伯特变换
 ap_r=real(ap);    %实部为logamp
 ap_i=imag(ap);   %虚部为相位谱
 figure(5)
 subplot(3,1,1)
semilogx(f,log(amp),'r');hold on
 semilogx(f,ap_r,'b')
 title('SKZ地震仪振幅响应')'
 ylabel('振幅的自然对数')
 subplot(3,1,2)
semilogx(f,pha*180/pi,'r');hold on
 semilogx(f,(-ap_i)*180/pi,'b')
 legend('传递函数','希尔伯特')
ylabel('相位/°')
 title('相位响应')
 subplot(3,1,3)
 semilogx(f,(pha+ap_i)*180/pi)
 title('相位响应差值（传递函数-希尔伯特）')
  xlabel('频率/Hz')
  ylabel('相位/°')
  figure(6)
 pa=hilbert(pha);
 pa_r=real(pa);
 pa_i=imag(pa);
 subplot(2,1,1)
 semilogx(f,pa_i,'r');hold on
semilogx(f,log(amp),'b')
 subplot(2,1,2)
 semilogx(f,pa_r*180/pi,'r');hold on
 semilogx(f,pha*180/pi,'b')
 h1=zeros(1,n);
%构建与傅里叶变换得到的前段与后端共轭形式数据对应的滤波器
%万永革，2012，第429页）
for ii=1:n/2
   h1(ii)=h(ii);
    h1(n-ii+1)=conj(h(ii));
end


amp=abs(h1);   
 logamp=log(amp);
 logamp(1)
 logamp(n)
 pha=phase(h1);
 ap=hilbert(log(amp));   %求振幅谱自然对数的希尔伯特变换
 ap_r=real(ap);    %实部为logamp
 ap_i=imag(ap);   %虚部为相位谱
 figure(7)
 subplot(3,1,1)
semilogx(f,log(amp),'r');hold on
 semilogx(f,ap_r,'b')
 title('SKZ地震仪振幅响应')'
 ylabel('振幅的自然对数')
 subplot(3,1,2)
semilogx(f,pha*180/pi,'r');hold on
 semilogx(f,(-ap_i)*180/pi,'b')
 legend('传递函数','希尔伯特')
ylabel('相位/°')
 title('相位响应')
 subplot(3,1,3)
 semilogx(f,(pha+ap_i)*180/pi)
 title('相位响应差值（传递函数-希尔伯特）')
  xlabel('频率/Hz')
  ylabel('相位/°')
   figure(8)
 pa=hilbert(pha);
 pa_r=real(pa);
 pa_i=imag(pa);
 subplot(2,1,1)
 semilogx(f,pa_i,'r');hold on
semilogx(f,log(amp),'b')
 subplot(2,1,2)
 semilogx(f,pa_r*180/pi,'r');hold on
 semilogx(f,pha*180/pi,'b')