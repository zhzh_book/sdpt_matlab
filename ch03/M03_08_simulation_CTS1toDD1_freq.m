%CTS-1toDD-1频率域仿真
clear all
r2d=180/pi
load XX_BCH_BHZ_2.txt
Fs=100 %采样率
wave=XX_BCH_BHZ_2;
wavelen=length(wave)
n2=0   %截取信号起始点为n2+1
n1=60000  %截取信号点数
wave1([1:n1])=wave([n2+1:n2+n1]);
wavelen=length(wave1)
t1=([1:n1])/Fs;
figure(1)
plot(t1,wave1)
xlabel('时间/s')
ylabel('count')
title('CTS-1原始记录')

%读取记录波形
% 读入示例地震波形：巴楚地震台CTS-1垂直向记录M7.72013年4月16日伊朗-巴基斯坦交界地震
%XX_BCH_BHZ_2.txt
%按+/-10V量程输入数据采集器，转换灵敏度为1.192e-6V/count
%CTS-1地震计灵敏度1000V/m/s (2000V/m/s?)
%得到地动速度转换灵敏度G=1000/1.192e-6=8.3893e+8count/m/s
Gcts1v=(1000/1.192e-6)/1.0e+9  % 0.8389 count/nm/s     count/纳米/s
%根据数字波形实际信息，1.04877  count/nm/s
%仿真滤波器常数因子
T2=0.05
n2=2*pi/T2
n22=n2^2
Gcts1_dd1=1/1.04877 %仿真滤波器, .取DD-1灵敏度为1 count/nm，=0.9535
%频率域仿真
%
Zcts1_dd1=[-0.037025+0.037025i -0.037025-0.037025i]

Pcts1_dd1=[-2.8274+5.6111i -2.8274-5.6111i -88.8442+88.8711i -88.8442-88.8711i -4.545]
[b,a]=zp2tf(Zcts1_dd1',Pcts1_dd1',1)
b(1)
b(2)
b(3)
b(4)
b(5)
b(6)
a(1)
a(2)
a(3)
a(4)
a(5)
a(6)
p1=-2.8274+5.6111i
p2=-2.8274-5.6111i 
p3=-88.8442+88.8711i 
p4=-88.8442-88.8711i 
p5=-4.545
a11=-(p1+p2)
a12=p1*p2
a21=-(p3+p4)
a22=p3*p4
a5=-p5
a33=a11+a21
a32=a12+a11*a21+a22
a31=a12*a21+a11*a22
a30=a12*a22
a44=a33+a5
a43=a32+a33*a5
a42=a31+a32*a5
a41=a30+a31*a5
a40=a30*a5
df=Fs/n1
f=[1:n1]*df;
ww=f*2*pi;
h=freqs(b,a,ww);
figure(2)
subplot(211)
%semilogx(f,20*log10(abs(h)/abs(h(600))))    % 按1 Hz归一化
semilogx(f,log10(abs(h)*Gcts1_dd1*n22))  
%absh600=abs(h(600))
%f600=f(600)
%title({'CTS-1到DD-1仿真滤波器，振幅响应，0dB对应',absh600,'f0=',f600,'Hz'})
title({'CTS-1到DD-1仿真滤波器，振幅响应'})
grid on
%ylabel('幅度/dB')
ylabel('log(幅度)')
subplot(212)
semilogx(f,unwrap(phase(h))*r2d)
title('相位响应')
ylabel('相位/°')
xlabel('频率/Hz')
grid on
Wcts1_dd1=filt_freq(b,a,wave1,Fs,n1);

figure(3)
plot(t1,Wcts1_dd1)
xlabel('时间/s')
ylabel('count')
title('CTS-1到DD-1仿真波形（频率域仿真）')
figure(4)
plot(f,abs(fft(Wcts1_dd1)))
title('CTS-1到DD-1仿真波形（频率域仿真）的频谱')
xlabel('频率/Hz')
ylabel('振幅')
Wcst1_dd1=filt_freq(b,a,wave1,Fs,n1)*Gcts1_dd1*n22;

figure(5)
plot(t1,Wcst1_dd1)
xlabel('时间/s')
ylabel('count')
title('CTS-1到DD-1仿真波形（频率域仿真）')
%---------------------------------
figure(6)
plot(f,abs(fft(Wcts1_dd1)))
title('CTS-1到DD-1仿真波形（频率域仿真）的频谱')
xlabel('频率/Hz')
ylabel('振幅')
n=1000
figure(7)
[bz1,az1]=bilinear(b,a,Fs) 
[hb, wb]=freqz(bz1,az1,n);
[zb,pb,kb]=tf2zpk(bz1,az1)
semilogx(wb/(2*pi),log10(abs(hb)*Gcts1_dd1*n22))  ;hold on
[bz2,az2]=impinvar(b,a,Fs) 
[hi,wi]=freqz(bz2,az2,n);
[zi,ppi,ki]=tf2zpk(bz2,az2)
subplot(211)
semilogx(f,log10(abs(h)*Gcts1_dd1*n22),'black')  ;hold on
semilogx(wb*Fs/(2*pi),log10(abs(hb)*Gcts1_dd1*n22))  ;hold on
semilogx(wi*Fs/(2*pi),log10(abs(hi)*Gcts1_dd1*n22),'r')  ;

%absh600=abs(h(600))
%f600=f(600)
%title({'CTS-1到DD-1仿真滤波器，振幅响应
grid on
%ylabel('幅度/dB')
ylabel('log(幅度)')
subplot(212)
semilogx(f,unwrap(phase(h))*r2d,'black');hold on
semilogx(wb*Fs/(2*pi),unwrap(phase(hb))*180/pi);hold on
semilogx(wi*Fs/(2*pi),unwrap(phase(hi))*180/pi,'r');
title('相位响应')
ylabel('相位/°')
xlabel('频率/Hz')
grid on
legend('模拟仿真滤波器','双线性变换','脉冲响应不变')
%-------------------------------------------------------------------------------
Zcts1_dd1=[0 0]     %简化的CTS-1
Pcts1_dd1=[-2.8274+5.6111i -2.8274-5.6111i -88.8442+88.8711i -88.8442-88.8711i -4.545]
[b,a]=zp2tf(Zcts1_dd1',Pcts1_dd1',1)

hh=freqs(b,a,ww);
figure(12)
subplot(211)
%semilogx(f,20*log10(abs(h)/abs(h(600))))    % 按1 Hz归一化
loglog(f,abs(h)*Gcts1_dd1*n22,'black')  ;hold on
loglog(f,abs(hh)*Gcts1_dd1*n22,'r')  
%absh600=abs(h(600))
%f600=f(600)
%title({'CTS-1到DD-1仿真滤波器，振幅响应，0dB对应',absh600,'f0=',f600,'Hz'})
title({'CTS-1到DD-1仿真滤波器，振幅响应'})
grid on
%ylabel('幅度/dB')
ylabel('数字数/count')
subplot(212)
semilogx(f,unwrap(phase(h))*r2d,'black');hold on
semilogx(f,unwrap(phase(hh))*r2d,'r')
legend('2极点CTS-1','简化CTS-1')
title('相位响应')
ylabel('相位/°')
xlabel('频率/Hz')
grid on

Wcst1_dd1=filt_freq(b,a,wave1,Fs,n1)*Gcts1_dd1*n22;

figure(13)
plot(t1,Wcst1_dd1)
xlabel('时间/s')
ylabel('count')
title('CTS-1到DD-1仿真波形（频率域仿真）')

%---------------------------------
n=1000
figure(14)
[bz1,az1]=bilinear(b,a,Fs) 
[zb,pb,kb]=tf2zpk(bz1,az1)
[hb, wb]=freqz(bz1,az1,n);
semilogx(wb/(2*pi),log10(abs(hb)*Gcts1_dd1*n22))  ;hold on
[bz2,az2]=impinvar(b,a,Fs) 
[zi,ppi,ki]=tf2zpk(bz2,az2)
[hi,wi]=freqz(bz2,az2,n);
subplot(211)
semilogx(f,log10(abs(h)*Gcts1_dd1*n22),'y*')  ;hold on
semilogx(wb*Fs/(2*pi),log10(abs(hb)*Gcts1_dd1*n22))  ;hold on
semilogx(wi*Fs/(2*pi),log10(abs(hi)*Gcts1_dd1*n22),'r')  ;

%absh600=abs(h(600))
%f600=f(600)
%title({'CTS-1到DD-1仿真滤波器，振幅响应
grid on
%ylabel('幅度/dB')
ylabel('log(幅度)')
subplot(212)
semilogx(f,unwrap(phase(h))*r2d,'y*');hold on
semilogx(wb*Fs/(2*pi),unwrap(phase(hb))*180/pi);hold on
semilogx(wi*Fs/(2*pi),unwrap(phase(hi))*180/pi,'r');
title('相位响应')
ylabel('相位/°')
xlabel('频率/Hz')
grid on
legend('模拟仿真滤波器','双线性变换','脉冲响应不变')
