clear all
load M03_firln02_h.txt %加载线性相位FIR滤波器系数
x=M03_firln02_h;
%load M03_firmn02_h.txt %加载最小相位FIR滤波器系数
%x=M03_firmn02_h;

rate=x(1) %采样率
num=x(2)
for k=1:1:num
    y(k)=x(k+2);
end
% plot(y);%画滤波器系数(待修改)
fmax=rate/2;
T=1/rate % 采样间隔
r=1/T;
% 用Z变换表示FIR滤波器的响应
% realF(f)=sum(y(k)*cos(-2*pi*T*k*f)
% imagFf()=sum(y(k)*sin(-2*pi*T*k*f)
% amp(f)=sqrt(real(F(f)).^2 + imag(F(f)).^2)
% F(f)=complex(realF(f),imagF(f))
% A(f)=abs(F(f))
% theta(f)=angle(F(f))   in radians
% Z = A.*exp(i*theta)
for f=1:1:fmax*200
    f1(f)=f/200;
    realF(f)=0;
    imagF(f)=0;
    F(f)=0+0i;
    for k=1:1:num
       realF(f)=realF(f)+y(k)*cos(-2*pi*T*(k-1)*f1(f));
       imagF(f)=imagF(f)+y(k)*sin(-2*pi*T*(k-1)*f1(f));
       end
    
   F(f)=complex(realF(f),imagF(f));
     theta(f)=angle(F(f))*180/pi;
    %theta(f)=atan2(imagF(f),realF(f))*180/pi;
    
    A(f)=abs(F(f));
    B(f)=20*log10(A(f));
    
 % for k=1:1:40
  %    angle0=-(k-1)*360;
   %   if theta(f)>angle0
  % if theta(f)>0
  %  theta(f)=theta(f)-360;
  % end
  %end
end
disp('OK1')
figure(1)
subplot(3,1,1);
% plot(f1,A);
plot(f1,B);
xlabel('频率/Hz');
ylabel('振幅响应/dB');
%线性相位
title(['FIR线性相位滤波器 firln02 频率响应, 采样率=',num2str(r),'s/s,滤波器系数个数=',num2str(num)]);
%最小相位
subplot(3,1,2);
[h,w]=freqz(y,1);
ff=w*rate/(2*pi);
plot(ff,unwrap(phase(h))*180/pi);
%plot(f1,unwrap(theta));
xlabel('频率/Hz');
ylabel('相位响应/°');
subplot(3,1,3);
plot(y,'.');
xlabel('滤波器系数序号');
ylabel('滤波器系数值');
disp('OK2')
figure(2)

subplot(2,1,1)

plot(ff,20*log10(abs(h)),'r');hold on
plot(f1,B)
disp('OK3')
subplot(2,1,2)
plot(ff,unwrap(phase(h))*180/pi,'r');hold on
plot(f1,unwrap(theta))
disp('OK4')