%M03_05_analfilter_time.m
clear all
%有限脉冲响应法在时间域进行地震仪仿真
%源地震计：CTS-1，速度传递函数2零点、2极点
%目标地震计：DD-1，位移传递函数3零点、5极点
r2d=180/pi
load XX_BCH_BHZ_2.txt
Fs=100 %采样率
wave=XX_BCH_BHZ_2;
wavelen=length(wave)
n2=0   %截取信号起始点为n2+1
n1=60000  %截取信号点数
wave1([1:n1])=wave([n2+1:n2+n1]);
wavelen=length(wave1)
t1=([1:n1])/Fs;
figure(1)
plot(t1,wave1)
xlabel('时间/s')
ylabel('count')
title('CTS-1原始记录')

%读取记录波形
% 读入示例地震波形：巴楚地震台CTS-1垂直向记录M7.72013年4月16日伊朗-巴基斯坦交界地震
%XX_BCH_BHZ_2.txt
%按+/-10V量程输入数据采集器，转换灵敏度为1.192e-6V/count
%CTS-1地震计灵敏度1000V/m/s (2000V/m/s?)
%得到地动速度转换灵敏度G=1000/1.192e-6=8.3893e+8count/m/s
Gcts1v=(1000/1.192e-6)/1.0e+9  % 0.8389 count/nm/s     count/纳米/s
%根据数字波形实际信息，1.04877  count/nm/s
%仿真滤波器常数因子
T2=0.05
n2=2*pi/T2
n22=n2^2
Gcts1_dd1=1/1.04877 %仿真滤波器, .取DD-1灵敏度为1 count/nm，=0.9535
%仿真滤波器零极点（式（3.54）
Zcts1_dd1=[-0.037025+0.037025i -0.037025-0.037025i]
Pcts1_dd1=[-2.8274+5.6111i -2.8274-5.6111i -88.8442+88.8711i -88.8442-88.8711i -4.545]
[b,a]=zp2tf(Zcts1_dd1',Pcts1_dd1',n22)   %

%df=Fs/wavelen
df=0.5/Fs   %频率间隔0.005Hz
n0=floor(Fs/(2*df))
n=2*n0
w=[1:n]*df*2*pi;
[h_cts1_dd1]=freqs(b,a,w);
wl=length(w)
figure(2)
subplot(2,1,1)
loglog(w/(2*pi),abs(h_cts1_dd1),'b');hold on   %幅频响应
ylabel('归一化振幅')
grid on
xlabel('频率/Hz')
subplot(2,1,2)
semilogx(w/(2*pi),unwrap(angle(h_cts1_dd1))*r2d,'b');hold on
grid on
ylabel('相位/°')
xlabel('频率/Hz')
%-------------------------------------------------------
%构建与傅里叶变换得到的前段与后端共轭形式数据对应的滤波器
%万永革，2012，第429页）
%构造脉冲响应

h1_cts1_dd1=zeros(1,n);
for ii=1:n/2
    h1_cts1_dd1(ii)=h_cts1_dd1(ii);
    h1_cts1_d1(n-ii)=conj(h_cts1_dd1(ii));
end
impule=real(ifft(h1_cts1_dd1));
figure(3)
plot(t1(1:n),impule(1:n))   %脉冲响应
title('CTS -1到DD-1仿真滤波器脉冲响应，采样率=100sps')
xlabel('时间/s')
%-------------------------------------------------------
%时间域滤波
%式（2.71）
t_im=2
n_impule=t_im*Fs  %脉冲响应长度
for i=1:n1
    ywave1(i)=0;
   if i<n_impule+1
       for j=1:i
        ywave1(i)=ywave1(i)+wave1(j)*impule(i-j+1);
       end
   end
        if i>n_impule
            for j=i-n_impule:i
           ywave1(i)=ywave1(i)+wave1(j)*impule(i-j+1); 
            end
        end
end
ywave1=ywave1*Gcts1_dd1;   %Gcts1_dd1为仿真滤波器的转换灵敏度，这里假定DD-1的位移灵敏度是1count/nm
figure(4)
plot(t1(1:n1),ywave1)
title({'CTS-1到DD-1时间域仿真波形,脉冲响应长度(s)=',t_im})
xlabel('时间/s')
ylabel('振幅/nm')
%-------------------------------------
%时间域滤波
%式（2.71）
t_im=20
n_impule=t_im*Fs  %脉冲响应长度
for i=1:n1
    ywave1(i)=0;
   if i<n_impule+1
       for j=1:i
        ywave1(i)=ywave1(i)+wave1(j)*impule(i-j+1);
       end
   end
        if i>n_impule
            for j=i-n_impule:i
           ywave1(i)=ywave1(i)+wave1(j)*impule(i-j+1); 
            end
        end
end
ywave1=ywave1*Gcts1_dd1;   %Gcts1_dd1为仿真滤波器的转换灵敏度，这里假定DD-1的位移灵敏度是1count/nm
figure(5)
plot(t1(1:n1),ywave1)
title({'CTS-1到DD-1时间域仿真波形,脉冲响应长度(s)=',t_im})
xlabel('时间/s')
ylabel('振幅/nm')
%-----------------------------------------
%时间域滤波
%式（2.71）
t_im=50
n_impule=t_im*Fs  %脉冲响应长度
for i=1:n1
    ywave1(i)=0;
   if i<n_impule+1
       for j=1:i
        ywave1(i)=ywave1(i)+wave1(j)*impule(i-j+1);
       end
   end
        if i>n_impule
            for j=i-n_impule:i
           ywave1(i)=ywave1(i)+wave1(j)*impule(i-j+1); 
            end
        end
end
ywave1=ywave1*Gcts1_dd1;   %Gcts1_dd1为仿真滤波器的转换灵敏度，这里假定DD-1的位移灵敏度是1count/nm
figure(6)
plot(t1(1:n1),ywave1)
title({'CTS-1到DD-1时间域仿真波形,脉冲响应长度(s)=',t_im})
xlabel('时间/s')
ylabel('振幅/nm')
%时间域滤波
%式（2.71）
t_im=100
n_impule=t_im*Fs  %脉冲响应长度100秒
for i=1:n1
    ywave1(i)=0;
   if i<n_impule+1
       for j=1:i
        ywave1(i)=ywave1(i)+wave1(j)*impule(i-j+1);
       end
   end
        if i>n_impule
            for j=i-n_impule:i
           ywave1(i)=ywave1(i)+wave1(j)*impule(i-j+1); 
            end
        end
end
ywave1=ywave1*Gcts1_dd1;   %Gcts1_dd1为仿真滤波器的转换灵敏度，这里假定DD-1的位移灵敏度是1count/nm
figure(7)
plot(t1(1:n1),ywave1)
title({'CTS-1到DD-1时间域仿真波形,脉冲响应长度(s)=',t_im})
xlabel('时间/s')
ylabel('振幅/nm')

endtime=clock
