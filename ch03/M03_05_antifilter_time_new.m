%M03_xx_antifilter_time.m
%在时间域进行仪器校正，为此，要由模拟滤波器构成相应的数字递归滤波器
% 读入示例地震波形：巴楚地震台CTS-1垂直向记录M7.7  2013年4月16日伊朗-巴基斯坦交界地震
%XX_BCH_BHZ_2.txt
%按+/-10V量程输入数据采集器，转换灵敏度为1.192e-6V/count
%CTS-1地震计灵敏度1000V/m/s (2000V/m/s?)
%得到地动速度转换灵敏度G=1000/1.192e-6=8.389e+8count/m/s
clear all
%G=(1000/1.192e-6)/1.0e+9  % 0.8389 count/nm/s     count/纳米/s  (标称值）
G=1048.77/1000     %XX_BCH_BHZ_2.par中的calib factor=1048.77   count/um/s
%地动位移转换灵敏度Gd，
Gd=G   %count/nm
load XX_BCH_BHZ_2.txt
calib_factor=1048.77001953125  %XX_BCH_BHZ_2.par中给出的标度因子，单位：count/(um/s)
calib=1.049  %count/(nm/s)
sens=1/calib_factor
Fs=100
wave=XX_BCH_BHZ_2;
wavelen=length(wave)
n1=6000
n2=4000
n2=0
n1=60000
wave1([1:n1])=wave([n2+1:n2+n1]);
wavelen=length(wave1)
t1=([1:n1])/Fs;
figure(1)
plot(t1,wave1)
xlabel('时间/s')
ylabel('count')
title('CTS-1原始记录')
fft_wave1=fft(wave1);
%以CTS-1地震计为例，在时间域进行地震仪器校正
%Num_zero=2
%Num_zero=3  %位移响应
%Num_pole=10
% （1）针对地动速度的仪器响应校正
z_cts1(1)=0+0i;
z_cts1(2)=0+0i;
%ze(3)=0;

p_cts1=[-0.037025+0.037024i  -0.037025-0.037024i   -266.57+266.57i   -266.57-266.57i    -333.81+89.403i    -333.81-89.403i  .....
    -244.35-244.36i   -244.35+244.36i  -89.440+333.80i    -89.440-333.80i]  %据《测震学原理与方法》，pdf版，2016
k_cts1=2.4206e+20
[b_cts1,a_cts1]=zp2tf(z_cts1',p_cts1',k_cts1)   %零极点必须是列矢量
df=Fs/wavelen
n=floor(Fs/df)
w=[1:n]*df*2*pi;
[h]=freqs(b_cts1,a_cts1,w);
%以0.04Hz作为归一化频率
n_norm=0.04/df
norm=abs(h(n_norm))
w;
wl=length(w)
figure(2)
subplot(2,1,1)
%semilogx(w/(2*pi),abs(h),'b');hold on   %幅频响应
semilogx(w/(2*pi),abs(h),'b');hold on
title('CTS-1速度响应，2零点，10极点')
%plot(w,abs(h),co);hold on
%axis([0 15 0 1.1])
ylabel('归一化振幅')
grid on
xlabel('频率/Hz')
%axis([0 1 0 1.1])
%set(gca,'YTick',0:0.1:1.1)
subplot(2,1,2)
%semilogx(w/(2*pi),unwrap(angle(h))*180/pi,'b');hold on
semilogx(w/(2*pi),unwrap(angle(h))*180/pi,'b');hold on
%axis([0 15 -1000 1000])
grid on
ylabel('相位/°')
xlabel('频率/Hz')


%-------设计巴特沃斯带通滤波器
%阶数
N=8% 带通滤波器的阶数（极点个数=2N）
Fs=100   %采样率
fn1=0.002;   %拐角频率
fn2=40
%wn=fn*2  /Fs % 按奈奎斯特频率归一化的角频率
wn=[fn1  fn2]*2*pi;
figure(15)
[z_bt,p_bt,k]=butter(N,wn,'s')
[b_bt,a_bt]=zp2tf(z_bt,p_bt,k)
w_bt=[1:n]*df*2*pi;
h_bt=freqs(b_bt,a_bt,w_bt);
subplot(2,1,1)
semilogx(w_bt/(2*pi),abs(h_bt));
ylabel('归一化振幅')
grid on
title('16阶巴特沃斯0.002-40Hz带通滤波器')
%plot(w,abs(h),co);hold on
%axis([0 25 0 1.1])
subplot(2,1,2)
semilogx(w_bt/(2*pi),unwrap(angle(h_bt))*180/pi);
%axis([0 25 -2000 200])
grid on
ylabel('相位/°')
xlabel('频率/Hz')
%----------------------------------------------------------------------------
%10极点CTS-1响应的反滤波器+16阶巴特沃斯滤波器=cts1b
n_pol=10
for i=1:n_pol
z_cts1b10(i)=p_cts1(i)
end


lenz=length(z_cts1b10)
n_ze=2   %CTS-1零点个数，速度：=2；位移：=3
for i=1:N-n_ze   %抵消掉cts-1的两个零值零点
    %z_cts1b(i+10)=z_bt(i);
     z_cts1b10(i+n_pol)=z_bt(i);  %只保留N-2个巴特沃斯滤波器的零值零点加上CTS-1极点作为cts1b的零点
end

for i=1:2*N
        p_cts1b10(i)=p_bt(i);   %巴特沃斯滤波器的极点作为cts1b的极点
end
z10=z_cts1b10
p10=p_cts1b10
figure(16)
k_cts1b10=1
[b_cts1b10,a_cts1b10]=zp2tf(z_cts1b10',p_cts1b10',k_cts1b10)
w_bt=[1:n]*df*2*pi;
h_cts1b10=freqs(b_cts1b10,a_cts1b10,w_bt);
f0=0.1
n_f0=f0/df
norm_cts1=1/abs(h_cts1b10(n_f0))
subplot(2,1,1)
semilogx(w_bt/(2*pi),abs(h_cts1b10)*norm_cts1);
ylabel('归一化振幅')
grid on

title('针对10极点CST-1的仪器校正模拟滤波器（含16阶巴特沃斯0.002-40Hz带通滤波器）')
%plot(w,abs(h),co);hold on
%axis([0 25 0 1.1])
subplot(2,1,2)
semilogx(w_bt/(2*pi),unwrap(angle(h_cts1b10))*180/pi);
%axis([0 25 -2000 200])
grid on
ylabel('相位/°')
xlabel('频率/Hz')
figure(17)

% 构造数字滤波器
subplot(2,1,1)
semilogx(w_bt/(2*pi),abs(h_cts1b10)*norm_cts1);hold on
title('针对10极点CST-1的仪器校正数字滤波器（含16阶巴特沃斯0.002-40Hz带通滤波器）')
ylabel('归一化振幅')
grid on
%[bz_cts1b,az_cts1b]=bilinear(b_cts1b10,a_cts1b10,Fs)     %双线性变换的分子阶数不能大于分母阶数
[bz_cts1b,az_cts1b]=impinvar(b_cts1b10,a_cts1b10,Fs)
%[bz_cts1b,az_cts1b]=bilinear(b_bt,a_bt,Fs)
%figure(7)
wz=[1:n1]*df*2*pi/Fs;
h=freqz(bz_cts1b,az_cts1b,wz);
subplot(2,1,1)
semilogx(wz*Fs/(2*pi),abs(h)*norm_cts1,'r')
subplot(2,1,2)
semilogx(w_bt/(2*pi),unwrap(angle(h_cts1b10))*180/pi);hold on
semilogx(wz*Fs/(2*pi),unwrap(angle(h))*180/pi,'r')

legend('模拟','数字')
figure(18)
y=filter(bz_cts1b,az_cts1b,wave1);    %去CTS-1+2N阶滤波器
%转换因子G
veloci=y/G;
plot(t1,veloci)
title('CTS-1仪器校正（含4阶带通滤波器）后的地动速度')
ylabel('速度/nm/s')
xlabel('时间/s')
%---------------------------------------------------------------------
%-------设计巴特沃斯带通滤波器
%阶数
N=2% 带通滤波器的阶数（极点个数=2N） 
% 取N=2、4、8对比3个巴特沃斯滤波器与2极点CTS-1反滤波器结合后变成的数字滤波器
%结果表明，N=8时不能产生准确的数字滤波器
Fs=100   %采样率
fn1=0.002;   %拐角频率
fn2=40
%wn=fn*2  /Fs % 按奈奎斯特频率归一化的角频率
wn=[fn1  fn2]*2*pi;
figure(25)
[z_bt,p_bt,k]=butter(N,wn,'s')
[b_bt,a_bt]=zp2tf(z_bt,p_bt,k)
w_bt=[1:n]*df*2*pi;
h_bt=freqs(b_bt,a_bt,w_bt);
subplot(2,1,1)
semilogx(w_bt/(2*pi),abs(h_bt));
ylabel('归一化振幅')
grid on
title('8阶巴特沃斯0.002-40Hz带通滤波器')
%plot(w,abs(h),co);hold on
%axis([0 25 0 1.1])
subplot(2,1,2)
semilogx(w_bt/(2*pi),unwrap(angle(h_bt))*180/pi);
%axis([0 25 -2000 200])
grid on
ylabel('相位/°')
xlabel('频率/Hz')
%----------------------------------------------------------------------------
%2极点CTS-1响应的反滤波器+2N阶巴特沃斯滤波器=cts1b
n_pol=2
for i=1:n_pol
z_cts1b2(i)=p_cts1(i)
end


lenz=length(z_cts1b2)
n_ze=2  %CTS-1零点个数，速度：=2；位移：=3
if N>n_ze
for i=1:N-n_ze   %抵消掉cts-1的两个零值零点（速度）或3个（位移）
    %z_cts1b(i+10)=z_bt(i);
     z_cts1b2(i+n_pol)=z_bt(i);  %只保留N-n_ne个巴特沃斯滤波器的零值零点加上CTS-1极点作为cts1b的零点
end
end
for i=1:2*N
        p_cts1b2(i)=p_bt(i);   %巴特沃斯滤波器的极点作为cts1b的极点
end
z2=z_cts1b2
p2=p_cts1b2
figure(26)
k_cts1b2=k   %原巴特沃斯滤波器的归一化常数
[b_cts1b2,a_cts1b2]=zp2tf(z_cts1b2',p_cts1b2',k_cts1b2)
w_bt=[1:n]*df*2*pi;
h_cts1b2=freqs(b_cts1b2,a_cts1b2,w_bt);
f0=0.1
n_f0=f0/df
norm_cts1=1/abs(h_cts1b2(n_f0))

subplot(2,1,1)


semilogx(w_bt/(2*pi),abs(h_cts1b2));hold on
ylabel('归一化振幅')
grid on


%plot(w,abs(h),co);hold on
%axis([0 25 0 1.1])

subplot(2,1,2)
semilogx(w_bt/(2*pi),unwrap(angle(h_cts1b2))*180/pi); hold on
%axis([0 25 -2000 200])
grid on
ylabel('相位/°')
xlabel('频率/Hz')
% 构造数字滤波器
[bz_cts1b,az_cts1b]=bilinear(b_cts1b10,a_cts1b10,Fs)     %双线性变换的分子阶数不能大于分母阶数
%[bz_cts1b,az_cts1b]=impinvar(b_cts1b2,a_cts1b2,Fs)
[bz_cts1b,az_cts1b]=bilinear(b_cts1b2,a_cts1b2,Fs)
%[bz_cts1b,az_cts1b]=bilinear(b_bt,a_bt,Fs)
%figure(7)
%figure(27)
wz=[1:n1]*df*2*pi/Fs;
h=freqz(bz_cts1b,az_cts1b,wz);
subplot(2,1,1)
semilogx(wz*Fs/(2*pi),abs(h),'r')
title('针对2极点CST-1的仪器校正滤波器（含4阶巴特沃斯0.002-40Hz带通滤波器）')
%title('针对2极点CST-1的位移仪器校正滤波器（含8阶巴特沃斯0.002-40Hz带通滤波器）')  %n_ne=3,位移响应
subplot(2,1,2)
semilogx(wz*Fs/(2*pi),unwrap(angle(h))*180/pi,'r')

legend('模拟','数字')

y=filter(bz_cts1b,az_cts1b,wave1);    %去CTS-1+4阶滤波器
figure(28)
%速度转换因子G
veloci=y/G;
plot(t1,veloci)
%位移转换因子Gd
%dis=y/Gd;
%plot(t1,dis)
title('2极点CTS-1仪器校正（含4阶带通滤波器）后的地动速度')
%title('2极点CTS-1位移仪器校正（含8阶带通滤波器）后的地动位移')  %n_ne=3，位移响应
ylabel('速度/nm/s')
%ylabel('位移/nm')
xlabel('时间/s')
%
%位移是速度的积分
dt=1/Fs
dis1(1)=0;
for i=2:n1
    dis1(i)=dis1(i-1)+veloci(i);
end
 dis1=dis1*dt;
figure(29)
plot(t1,dis1)
title('含4阶带通滤波的2极点CTS-1校正后的地动速度积分成地动位移')
xlabel('时间/s')
ylabel('位移/nm')


% 构造数字滤波器，不用加带通滤波器；CTS-1只取两极点的地震计部分
%-------------------------------------
z_cts1a(1)=p_cts1(1)
z_cts1a(2)=p_cts1(2)  
p_cts1a(1)=0
p_cts1a(2)=0
figure(9)
k_cts1a=1
[b_cts1a,a_cts1a]=zp2tf(z_cts1a',p_cts1a',k_cts1a)
w_bt=[1:n]*df*2*pi;
h_cts1a=freqs(b_cts1a,a_cts1a,w_bt);
f0=0.1
n_f0=f0/df
norm_cts1=1/abs(h_cts1a(n_f0))
subplot(2,1,1)
semilogx(w_bt/(2*pi),abs(h_cts1a),'b*');hold on
ylabel('归一化振幅')
grid on

title('CST-1仪器校正滤波器，不含附加带通滤波器')
%plot(w,abs(h),co);hold on
%axis([0 25 0 1.1])
subplot(2,1,2)
semilogx(w_bt/(2*pi),unwrap(angle(h_cts1a))*180/pi,'b*');hold on
%axis([0 25 -2000 200])
grid on
ylabel('相位/°')
xlabel('频率/Hz')
% 构造数字滤波器
[bz_cts1b,az_cts1b]=bilinear(a_cts1a,b_cts1a,Fs)     %双线性变换的分子阶数不能大于分母阶数
[bz_cts1i,az_cts1i]=impinvar(a_cts1a,b_cts1a,Fs)
[bz_cts1b,az_cts1b]=bilinear(b_cts1a,a_cts1a,Fs)     %双线性变换的分子阶数不能大于分母阶数
[bz_cts1i,az_cts1i]=impinvar(b_cts1a,a_cts1a,Fs)
%[bz_cts1b,az_cts1b]=bilinear(b_bt,a_bt,Fs)
%figure(7)
wz=[1:n1]*df*2*pi/Fs;
hb=freqz(bz_cts1b,az_cts1b,wz);
hi=freqz(bz_cts1i,az_cts1i,wz);
subplot(2,1,1)
semilogx(wz*Fs/(2*pi),abs(hb),'r');hold on
semilogx(wz*Fs/(2*pi),abs(hi),'black');
subplot(2,1,2)
semilogx(wz*Fs/(2*pi),unwrap(angle(hb))*180/pi,'r');hold on
semilogx(wz*Fs/(2*pi),unwrap(angle(hi))*180/pi,'black')
legend('模拟','双线性变换','脉冲响应不变')
yb=filter(bz_cts1b,az_cts1b,wave1); %去CTS-1（不含滤波器）
yi=filter(bz_cts1i,az_cts1i,wave1); 
figure(10)
%转换因子G
velob=yb/G;
veloab=detrend(velob);
plot(t1,veloab);hold on
%转换因子G
veloi=yi/G;
veloai=detrend(veloi);
plot(t1,veloai,'r')
ylabel('速度/nm/s')
xlabel('时间/s')
title('CTS-1仪器校正(不含带通滤波器)后的地动速度')
legend('双线性变换校正','脉冲响应不变校正')
%位移是速度的积分
dt=1/Fs
disb(1)=0;
disi(1)=0;
for i=2:n1
    disb(i)=disb(i-1)+veloab(i);
    disi(i)=disi(i-1)+veloai(i);
end
 disb=disb*dt;
 disi=disi*dt;
figure(11)
plot(t1,disb);hold on
plot(t1,disi,'r')
title('不含带通滤波的CTS-1校正后的地动速度积分成地动位移')
xlabel('时间/s')
ylabel('位移/nm')
legend('双线性变换校正','脉冲响应不变校正')

