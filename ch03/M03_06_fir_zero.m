%计算FIR滤波器的零点并绘图     
clear
load M03_firmn02_h.txt %加载最小相位FIR滤波器系数
x=M03_firmn02_h;
%load M03_firln02_h.txt %加载线性相位FIR滤波器系数
%x=M03_firln02_h;
rate=x(1) %采样率
num=x(2)
for k=1:1:num
    y(k)=x(k+2);
end

%subplot(1,2,1);
%plot(y,'.');
%线性相位
%title({'FIR 滤波器 firln02系数 ,滤波器点数=',num});
%最小相位
%title({'FIR 滤波器 firm02系数,滤波器系数个数=',num});
%xlabel('滤波器系数序号');
%ylabel('滤波器系数值');
q=roots(y);
%FIR滤波器的系数y(数组)是以z为自变量的多项式系数，函数roots(y)计算多项式的根，即
%滤波器的零点
%subplot(1,2,2);
figure(1)
plot(real(q),imag(q),'*')
hold on
for i=1:360
    x(i)=sin(i*pi/180);
    y(i)=cos(i*pi/180);
end
plot(x,y);
%线性相位
%title('FIR线性相位滤波器 firl4零点分布(圆圈为z-平面上的单位圆)');
%最小相位
title('FIR最小相位滤波器 firm4零点分布(圆圈为z-平面上的单位圆)');
xlabel('滤波器零点的实部');
ylabel('滤波器零点的虚部');