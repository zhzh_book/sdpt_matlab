function [ w_out ] = filt_freq(b,a,w_in,Fs,n )
%
%   
fft_w_in=fft(w_in);
f=([1:n])*Fs/n;
w=f*2*pi;
h=freqs(b,a,w);
h1=zeros(1,n);
%构建与傅里叶变换得到的前段与后端共轭形式数据对应的滤波器
%万永革，2012，第429页）
for ii=1:n/2
    h1(ii)=h(ii);
    h1(n-ii+1)=conj(h1(ii));
end
%----频率域滤波
for i=1:n
fft_w_out(i)=fft_w_in(i)*h1(i);
end
w_out=real(ifft(fft_w_out));
end

