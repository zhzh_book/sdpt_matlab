%M03_xx_antifilter_time.m
%在时间域进行仪器校正，为此，要由模拟滤波器构成相应的数字递归滤波器
% 读入示例地震波形：巴楚地震台CTS-1垂直向记录M7.72013年4月16日伊朗-巴基斯坦交界地震
%XX_BCH_BHZ_2.txt
%按+/-10V量程输入数据采集器，转换灵敏度为1.192e-6V/count
%CTS-1地震计灵敏度1000V/m/s (2000V/m/s?)
%得到地动速度转换灵敏度G=1000/1.192e-6=8.3893e+8count/m/s
clear all
G=(1000/1.192e-6)/1.0e+9  % 0.8389 count/nm/s     count/纳米/s
K=1000
G=1048.77/1000     %XX_BCH_BHZ_2.par中的calib factor=1048.77   count/um/s
%地动位移转换灵敏度Gd，
Ga=G   %count/nm/s
load XX_BCH_BHZ_2.txt
Fs=100
wave=XX_BCH_BHZ_2;
wavelen=length(wave)
n1=6000
n2=4000
n2=0
n1=60000
wave1([1:n1])=wave([n2+1:n2+n1]);
wavelen=length(wave1)
t1=([1:n1])/Fs;
figure(1)
plot(t1,wave1)
xlabel('时间/s')
ylabel('count')
title('CTS-1原始记录')
fft_wave1=fft(wave1);
df=Fs/wavelen
n=floor(Fs/df)
%-------设计巴特沃斯带通滤波器
N=8   %阶数
N=4
N=2
Fs=100   %采样率
fn1=0.002;   %拐角频率
fn2=40
%wn=fn*2  /Fs % 按奈奎斯特频率归一化的角频率
wn=[fn1  fn2]*2*pi ;
figure(2)
[z_bt,p_bt,k]=butter(N,wn,'s')
[b_bt,a_bt]=zp2tf(z_bt,p_bt,k)
w_bt=[1:n]*df*2*pi;
h_bt=freqs(b_bt,a_bt,w_bt);
subplot(2,1,1)
semilogx(w_bt/(2*pi),abs(h_bt));
ylabel('归一化振幅')
grid on
title('4阶巴特沃斯0.002-40Hz带通滤波器')
%plot(w,abs(h),co);hold on
%axis([0 25 0 1.1])
subplot(2,1,2)
semilogx(w_bt/(2*pi),unwrap(angle(h_bt))*180/pi);
%axis([0 25 -2000 200])
grid on
ylabel('相位/°')
xlabel('频率/Hz')

%构建递归滤波器
 %由pole(1)和pole(2)计算地震计常数
 p_cts1(1)=-0.037024 + 0.037024i
p_cts1(2)=-0.037024 - 0.037024i
 omega0=sqrt(p_cts1(1)*p_cts1(2))
 omega0D1=-(p_cts1(1)+p_cts1(2))/2
 D1=omega0D1/omega0
 T=2*pi/omega0
 %按kanamori的符号
  dt=1/Fs   
 Gdt=Ga*dt
 c0=1/Gdt
 c1=-2*(1+omega0D1*dt)/Gdt
 c2=(1+2*omega0D1*dt+(dt*omega0)^2)/Gdt
 figure(3)
 Kaa=[1 1 ]
 Kbb=[c2 c1 c0]
 Kw=[1:n]*df*pi/Fs ; %最高为50Hz
 Kh=freqz(Kbb,Kaa,Kw);
 f0=0.1
 norm=1/abs(Kh(120))   %0.1Hz位于第120频点
 subplot(2,1,1)
semilogx(Kw*Fs/(2*pi),abs(Kh))

ylabel('归一化振幅')
title({'CTS-1加速度校正递归滤波器频率响应'})
subplot(2,1,2)

semilogx(Kw*Fs/(2*pi),unwrap(phase(Kh))*180/pi)
xlabel('频率/Hz')
ylabel('相位/°')
 acc(1)=0
 acc(2)=0
 for i=3:n1
     acc(i)=acc(i-1)+c2*wave1(i)+c1*wave1(i-1)+c0*wave1(i-2);
 end
 figure(5)
plot(t1,acc)
title('地动加速度')
xlabel('时间/s')
ylabel('加速度/(nm/s^2)')
%滤波
N=2


Wc=[0.002 40]*2/Fs;   %数字滤波器
[z,p,k]=butter(N,Wc)
[b,a]=zp2tf(z,p,k)
w=[1:n1]*df*pi/Fs;
h=freqz(b,a,w);

figure(6)
subplot(2,1,1)
semilogx(w*Fs/(2*pi),abs(h))

ylabel('归一化振幅')
title({'4阶带通滤波器振幅响应，0.002-40Hz'})
subplot(2,1,2)
semilogx(w*Fs/(2*pi),unwrap(phase(h))*180/pi)
xlabel('频率/Hz')
ylabel('相位/°')
acce=filter(b,a,acc);   %带通滤波
figure(7)
plot(t1,acce)
velo(1)=0;
for i=2:n1
    velo(i)=velo(i-1)+acce(i);
end
 velot=velo*dt;
figure(8)
plot(t1,velot)
title('地动加速度积分成地动速度')
xlabel('时间/s')
ylabel('速度/(nm/s)')
dis(1)=0;
veloc=filter(b,a,velot);   %带通滤波
figure(9)
plot(t1,veloc)
for i=2:n1
    dis(i)=dis(i-1)+veloc(i);
end
 dis=dis*dt;
figure(10)
plot(t1,dis)
title('地动速度积分成地动位移')
xlabel('时间/s')
ylabel('位移/nm')
 