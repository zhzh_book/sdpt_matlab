%数据采集器EDAS-C24的反滤波试验
clear all
%线性相位滤波器
%第4级
load M03_firln02_h.txt    %取自童汪练
fir_temp=M03_firln02_h;
fs4=fir_temp(1)
T4=1/fs4
Nfir_n02=fir_temp(2)     %滤波器系数长度
for i=1:Nfir_n02
    firl_n02(i)=fir_temp(i+2);
end
n=1000
figure(1)
[h4,f]=freqz(firl_n02,1,n,fs4)
subplot(2,1,1)
semilogy(f,abs(h4))
ylabel('振幅')
title('firl4')
subplot(2,1,2)
plot(f,unwrap(phase(h4))*180/pi)
xlabel('频率/Hz')
ylabel('相位/°')
figure(2)
freqz(firl_n02,1)
%第3级
load M03_firld10_h.txt    %取自孙丽
fir_temp=M03_firld10_h;
fs3=fir_temp(1)
T3=1/fs3
Nfir_d10=fir_temp(2)     %滤波器系数长度
for i=1:Nfir_d10
    firl_d10(i)=fir_temp(i+2);
end
figure(3)

[h3,f]=freqz(firl_d10,1,n,fs3);

subplot(2,1,1)
semilogy(f,abs(h3))
ylabel('振幅')
title('firl3')
subplot(2,1,2)
plot(f,unwrap(phase(h3))*180/pi)
xlabel('频率/Hz')
ylabel('相位/°')
figure(4)
freqz(firl_d10,1)
%第2级
load M03_firlb16_h.txt    %取自孙丽
fir_temp=M03_firlb16_h;
fs2=fir_temp(1)
T2=1/fs2
Nfir_b16=fir_temp(2)     %滤波器系数长度
for i=1:Nfir_b16
    firl_b16(i)=fir_temp(i+2);
end
figure(5)
[h2,f]=freqz(firl_b16,1,n,fs2);

subplot(2,1,1)
semilogy(f,abs(h2))
title('firl2')
ylabel('振幅')
subplot(2,1,2)
plot(f,unwrap(phase(h2))*180/pi)
xlabel('频率/Hz')
ylabel('相位/°')
figure(6)
freqz(firl_b16,1)
%第1级
load M03_firl108_h.txt    %取自孙丽
fir_temp=M03_firl108_h;
fs1=fir_temp(1)
T=1/fs1
Nfir_108=fir_temp(2)     %滤波器系数长度
for i=1:Nfir_108
    firl_108(i)=fir_temp(i+2);
end
figure(7)
[h1,f]=freqz(firl_108,1,n,fs1);
subplot(2,1,1)
semilogy(f,abs(h1))
title('firl1')
ylabel('振幅')
subplot(2,1,2)
plot(f,unwrap(phase(h1))*180/pi)
xlabel('频率/Hz')
ylabel('相位/°')
figure(8)
freqz(firl_108,1)
%数据采集器输出为100sps信号
%假想信号
%生成假想输入信号
%过采样信号（假定为模拟到数字的第1级）
%1238版本的信号信噪比高10:0.1
fs0=100
fs1=256000
Nzero=0.05*fs1
None=0.25*fs1
Nzero1=0.2*fs1
None2=0.5
t_all=1.0
Nall=t_all*fs1
for i=1:Nzero
    xstep(i)=0;
end
%None=3000
for i=1:Nall
   % xstep(Nzero+i)=1;
    xstep(i)=10*sin(2*pi*10*i/fs1)+5*sin(2*pi*20*i/fs1+pi/2); %两个正弦信号叠加
end
%for i=1:Nall
   % xstep(Nzero+i)=1;
    %xstep(i)=xstep(i)+0.3*sin(2*pi*30*i/fs1+pi/4); %两个正弦信号叠加
%end
for i=1:Nzero
    xstep(i)=0.1*sin(2*pi*25*i/fs1+pi/4);   %40Hz小信号
end
for i=None+1:None+Nzero1
    xstep(i)=0.1*sin(2*pi*25*i/fs1+pi/4);
end
Nxs=length(xstep)
xs=xstep;
for i=1:Nxs
    xstep(i)=xstep(i)+randn(1);
end
    
figure(9)
t=[1:Nxs]/fs1;
plot(t,xstep);hold on
plot(t,xs,'r')
title('采样率=256000sps')
xlabel('时间/s')
ylabel('振幅')
%第1级滤波
xn256000=filter(firl_108,1,xstep);
figure(10)
plot(t,xstep,'y');hold on
plot(t,xn256000,'r');hold on

%抽取因子=8
desi_fact1=8
len32000=length(xn256000)/desi_fact1

%xn32000=zeros(1:len32000)
for i=1:len32000
    xn32000(i)=xn256000((i-1)*desi_fact1+1);
end
t1=[1:len32000]*t_all/len32000;
plot(t1,xn32000,'g')
title('firl1滤波和抽取')
xlabel('时间/s')
ylabel('振幅')
legend('firl1滤波前','firl1滤波后','32000sps')
%第2级滤波
xf32000=filter(firl_b16,1,xn32000);
figure(11)
plot(t1,xn32000,'y');hold on
plot(t1,xf32000,'r');hold on
legend('滤波前','滤波后')
xlabel('时间/s')
ylabel('振幅')
title('firl2滤波，采样率=32000sps')
%抽取因子=16
desi_fact2=16
len2000=length(xf32000)/desi_fact2
for i=1:len2000
    xn2000(i)=xf32000((i-1)*desi_fact2+1);
end
t2=[1:len2000]*t_all/len2000;
figure(12)
plot(t1,xf32000,'y');hold on
plot(t2,xn2000,'r')
legend('firl2滤波后','2000sps')
title('firl2滤波和抽取')
xlabel('时间/s')
ylabel('振幅')
%plot(t2,xn2000,'g')
%%第3级滤波
xf2000=filter(firl_d10,1,xn2000);
figure(13)
plot(t2,xn2000,'b');hold on
plot(t2,xf2000,'r');
legend('firl3滤波前','firl3滤波后')
title('firl3滤波')
xlabel('时间/s')
ylabel('振幅')
%抽取因子=10
desi_fact3=10
len200=length(xf2000)/desi_fact3
for i=1:len200
    xn200(i)=xf2000((i-1)*desi_fact3+1);
end
t3=[1:len200]*t_all/len200;
figure(14)
plot(t2+0.9/fs4,xf2000);hold on
plot(t3,xn200,'r')
legend('firl3滤波后','200sps')
title('firl3滤波和抽取')
xlabel('时间/s')
ylabel('振幅')
%%第4级滤波
xf200=filter(firl_n02,1,xn200);
figure(15)
plot(t3,xn200,'b');hold on
plot(t3,xf200,'r');
legend('滤波前','滤波后')
title('firl4滤波')
xlabel('时间/s')
ylabel('振幅')
figure(16)
plot(t3(1:len200-(Nfir_n02-1)/2),xn200(1:len200-(Nfir_n02-1)/2),'b');hold on
plot(t3(1:len200-(Nfir_n02-1)/2),xf200((Nfir_n02-1)/2+1:len200),'r');
legend('滤波前','滤波后')
title('firl4滤波,经延时校正')
xlabel('时间/s')
ylabel('振幅')
%抽取因子=2
desi_fact4=2
len100=length(xf200)/desi_fact4
for i=1:len100
    xn100(i)=xf200((i-1)*desi_fact4+1);
end
t4=[1:len100]*t_all/len100;
figure(17)

plot(t3+0.5/fs0,xf200,'b');hold on
plot(t4,xn100,'r')
legend('滤波后200sps','抽取后100sps')
title('firl4滤波和抽取')
xlabel('时间/s')
ylabel('振幅')

figure(18)
plot(t4,xn100,'b')

title('firl4滤波并抽取100sps')
xlabel('时间/s')
ylabel('振幅')
figure(19)
len_t=length(t(1:Nxs-(Nfir_n02-1)*fs1/(2*fs3)+358))
len_xs=length(xs(1:Nxs-(Nfir_n02-1)*fs1/(2*fs3)-358))
plot(t(1+358:Nxs-(Nfir_n02-1)*fs1/(2*fs3)),xs(1:Nxs-(Nfir_n02-1)*fs1/(2*fs3)-358),'blue');hold on
plot(t3(1:len200-(Nfir_n02-1)/2-6),xf200((Nfir_n02-1)/2+1+6:len200),'r');
plot(t4(1:len100-(Nfir_n02-1)/4-3),xn100((Nfir_n02-1)/4+1+3:len100),'black');
legend('firl1滤波前无噪声','firl4滤波后200sps','100sps')
title('4级滤波并4次抽取前后比较，经延时校正')
xlabel('时间/s')
ylabel('振幅')
figure(20)
subplot(2,1,1)
plot(t3+0.5/fs0,xf200,'b');
legend('滤波后200sps')
title('firl4滤波和抽取')
xlabel('时间/s')
ylabel('振幅')
subplot(2,1,2)
plot(t4,xn100,'r')
legend('抽取后100sps')
xlabel('时间/s')
ylabel('振幅')
figure(21)
len_t=length(t(1:Nxs-(Nfir_n02-1)*fs1/(2*fs3)+358))
len_xs=length(xs(1:Nxs-(Nfir_n02-1)*fs1/(2*fs3)-358))
subplot(3,1,1)
plot(t(1+358:Nxs-(Nfir_n02-1)*fs1/(2*fs3)),xs(1:Nxs-(Nfir_n02-1)*fs1/(2*fs3)-358),'b');hold on
legend('firl1滤波前无噪声')
%figure(22)
title('4级滤波并3次抽取前后比较，经延时校正')
xlabel('时间/s')
ylabel('振幅')
subplot(3,1,2)
plot(t3(1:len200-(Nfir_n02-1)/2-6),xf200((Nfir_n02-1)/2+1+6:len200),'r');
legend('firl4滤波后200sps')

xlabel('时间/s')
ylabel('振幅')
subplot(3,1,3)
%plot(t4(1:len100-(Nfir_n02-1)/2-6),xn100((Nfir_n02-1)/2+1+6:len100),'r');
plot(t4,xn100)
legend('抽取后100sps')
xlabel('时间/s')
ylabel('振幅')

 