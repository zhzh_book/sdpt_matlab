%M03_xx_antifilter_time.m
%在时间域进行仪器校正，为此，要由模拟滤波器构成相应的数字递归滤波器
% 读入示例地震波形：巴楚地震台CTS-1垂直向记录M7.72013年4月16日伊朗-巴基斯坦交界地震
%XX_BCH_BHZ_2.txt
%按+/-10V量程输入数据采集器，转换灵敏度为1.192e-6V/count
%CTS-1地震计灵敏度1000V/m/s (2000V/m/s?)
%得到地动速度转换灵敏度G=1000/1.192e-6=8.3893e+8count/m/s
clear all
G=(1000/1.192e-6)/1.0e+9  % 0.8389 count/nm/s     count/纳米/s
G=1048.77/1000     %XX_BCH_BHZ_2.par中的calib factor=1048.77   count/um/s
%地动位移转换灵敏度Gd，
Gd=G   %count/nm
load XX_BCH_BHZ_2.txt
Fs=100
wave=XX_BCH_BHZ_2;
wavelen=length(wave)
n1=6000
n2=4000
n2=0
n1=60000
wave1([1:n1])=wave([n2+1:n2+n1]);
wavelen=length(wave1)
t1=([1:n1])/Fs;
figure(1)
plot(t1,wave1)
xlabel('时间/s')
ylabel('count')
title('CTS-1原始记录')
fft_wave1=fft(wave1);
%以CTS-1地震计为例，在频率域进行地震仪器校正
Num_zero=2
Num_zero=3  %位移响应
Num_pole=10
z_cts1(1)=0+0i;
z_cts1(2)=0+0i;
z_cts1(3)=0;

p_cts1=[-0.037025+0.037024i  -0.037025-0.037024i   -266.57+266.57i   -266.57-266.57i    -333.81+89.403i    -333.81-89.403i  .....
    -244.35-244.36i   -244.35+244.36i  -89.440+333.80i    -89.440-333.80i]  %据《测震学原理与方法》，pdf版，2016
k_cts1=2.4206e+20
[b_cts1,a_cts1]=zp2tf(z_cts1',p_cts1',k_cts1)   %零极点必须是列矢量
df=Fs/wavelen
n=floor(Fs/df)
w=[1:n]*df*2*pi;
[h]=freqs(b_cts1,a_cts1,w);
w;
wl=length(w)
figure(2)
subplot(2,1,1)
%semilogx(w/(2*pi),abs(h),'b');hold on   %幅频响应
semilogx(w/(2*pi),abs(h),'b');hold on
title('CTS-1位移响应（3零值零点，2极点')
%plot(w,abs(h),co);hold on
%axis([0 15 0 1.1])
ylabel('归一化振幅')
grid on
xlabel('频率/Hz')
%axis([0 1 0 1.1])
%set(gca,'YTick',0:0.1:1.1)
subplot(2,1,2)
%semilogx(w/(2*pi),unwrap(angle(h))*180/pi,'b');hold on
semilogx(w/(2*pi),unwrap(angle(h))*180/pi,'b');hold on
%axis([0 15 -1000 1000])
grid on
ylabel('相位/°')
xlabel('频率/Hz')


%-------设计巴特沃斯带通滤波器
N=8   %阶数=N*2
N=4
N=2 
Fs=100   %采样率
fn1=0.002;   %拐角频率
fn2=40
%wn=fn*2  /Fs % 按奈奎斯特频率归一化的角频率
wn=[fn1  fn2]*2*pi  
figure(5)
[z_bt,p_bt,k]=butter(N,wn,'s')
[b_bt,a_bt]=zp2tf(z_bt,p_bt,k)
w_bt=[1:n]*df*2*pi;
h_bt=freqs(b_bt,a_bt,w_bt);
subplot(2,1,1)
semilogx(w_bt/(2*pi),abs(h_bt));
ylabel('归一化振幅')
grid on
title('4阶巴特沃斯0.002-40Hz带通滤波器')
%plot(w,abs(h),co);hold on
%axis([0 25 0 1.1])
subplot(2,1,2)
semilogx(w_bt/(2*pi),unwrap(angle(h_bt))*180/pi);
%axis([0 25 -2000 200])
grid on
ylabel('相位/°')
xlabel('频率/Hz')
%CTS-1反滤波器+4阶巴特沃斯滤波器=cts1b
z_cts1b(1)=p_cts1(1)
z_cts1b(2)=p_cts1(2)     %2个
lenz=length(z_cts1b)
%for i=1:N-2   %抵消掉cts-1的两个零值零点
if N>3
    for i=1:N-3   %抵消掉cts-1的3个零值零点,位移响应
    %z_cts1b(i+10)=z_bt(i);
     z_cts1b(i+2)=z_bt(i);  %只保留2个CTS-1极点作为cts1b的零点
end
end

for i=1:2*N
    %p_cts1b(i+num_zcts1)=p_bt(i);
     p_cts1b(i)=p_bt(i);   %巴特沃斯滤波器的极点作为cts1b的极点
end
if N<3
    for i=1:3-N
        p_cts1b(i+2*N)=0;
    end
end
z=z_cts1b
p=p_cts1b
figure(6)
k_cts1b=k
[b_cts1b,a_cts1b]=zp2tf(z_cts1b',p_cts1b',k_cts1b)
w_bt=[1:n]*df*2*pi;
h_cts1b=freqs(b_cts1b,a_cts1b,w_bt);
f0=0.1
n_f0=f0/df
norm_cts1=1/abs(h_cts1b(n_f0))
subplot(2,1,1)
semilogx(w_bt/(2*pi),abs(h_cts1b)*norm_cts1);hold on
ylabel('归一化振幅')
grid on

title('CST-1位移响应校正滤波器（含4阶巴特沃斯0.002-40Hz带通滤波器）')
%plot(w,abs(h),co);hold on
%axis([0 25 0 1.1])
subplot(2,1,2)
semilogx(w_bt/(2*pi),unwrap(angle(h_cts1b))*180/pi);hold on
%axis([0 25 -2000 200])
grid on
ylabel('相位/°')
xlabel('频率/Hz')
% 构造数字滤波器
[bz_cts1b,az_cts1b]=bilinear(b_cts1b,a_cts1b,Fs)     %双线性变换的分子阶数不能大于分母阶数
%[bz_cts1b,az_cts1b]=impinvar(b_cts1b,a_cts1b,Fs)
%[bz_cts1b,az_cts1b]=bilinear(b_bt,a_bt,Fs)
%figure(7)
wz=[1:n1]*df*2*pi/Fs;
h=freqz(bz_cts1b,az_cts1b,wz);
subplot(2,1,1)
semilogx(wz*Fs/(2*pi),abs(h)*norm_cts1,'r')
subplot(2,1,2)
semilogx(wz*Fs/(2*pi),unwrap(angle(h))*180/pi,'r')
y=filter(bz_cts1b,az_cts1b,wave1);
legend('模拟','数字')
figure(8)
%转换因子G
plot(t1,y/G)     %除以G化成地动位移nm
title('CTS-1响应校正后的地动位移（含滤波器）')
ylabel('位移/nm')
xlabel('时间/s')
% 构造数字滤波器，不用加带通滤波器
%-------------------------------------
z_cts1a(1)=p_cts1(1)
z_cts1a(2)=p_cts1(2)  
p_cts1a(1)=0
p_cts1a(2)=0
p_cts1a(3)=0   %位移
figure(9)
k_cts1a=1
[b_cts1a,a_cts1a]=zp2tf(z_cts1a',p_cts1a',k_cts1a)
w_bt=[1:n]*df*2*pi;
h_cts1b=freqs(b_cts1a,a_cts1a,w_bt);
f0=0.1
n_f0=f0/df
norm_cts1=1/abs(h_cts1b(n_f0))
subplot(2,1,1)
semilogx(w_bt/(2*pi),abs(h_cts1b));hold on
ylabel('归一化振幅')
grid on

title('CST-1位移响应校正滤波器，不含附加带通滤波器')
%plot(w,abs(h),co);hold on
%axis([0 25 0 1.1])
subplot(2,1,2)
semilogx(w_bt/(2*pi),unwrap(angle(h_cts1b))*180/pi);hold on
%axis([0 25 -2000 200])
grid on
ylabel('相位/°')
xlabel('频率/Hz')
% 构造数字滤波器
[bz_cts1a,az_cts1a]=bilinear(b_cts1a,a_cts1a,Fs)     %双线性变换的分子阶数不能大于分母阶数
%[bz_cts1b,az_cts1b]=impinvar(b_cts1a,a_cts1a,Fs)
%[bz_cts1b,az_cts1b]=bilinear(b_bt,a_bt,Fs)
%figure(7)
wz=[1:n1]*df*2*pi/Fs;
h=freqz(bz_cts1a,az_cts1a,wz);
subplot(2,1,1)
semilogx(wz*Fs/(2*pi),abs(h),'r')
subplot(2,1,2)
semilogx(wz*Fs/(2*pi),unwrap(angle(h))*180/pi,'r')
legend('模拟','数字')
y=filter(bz_cts1a,az_cts1a,wave1);

figure(10)
%转换因子Gd
plot(t1,y/Gd)
title('CTS-1响应校正后的地动位移（不含滤波器')
ylabel('位移/nm')
xlabel('时间/s')