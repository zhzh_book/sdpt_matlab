clear
%calcutaion of normalization constant of a seismometer
%CTS-1 
%from LiQuan Company Website: http://whlqtech.com/download/CTS-1参考零极点.txt 
%蔡亚先提供
%2
%0. 0.
%0. 0.
%10
%-0.037024 + i(0.037024)
% -0.037024 - i(0.037024)
%-266.58 -266.57
%-266.58 266.57
%-333.8 -89.44
%-333.8 89.44
%-244.36 -244.36
%-244.36 244.36
%-89.44 -333.8
%-89.44 333.8

%Num_pole=10
Num_pole=2

% 取2零点，2极点，计算地震计本身的响应
Num_zero=2
Num_pole=2

%据薛兵《测震学原理与方法》
K=1000    %Vs/m
m=2.4206e20   %（归一化参考频率为0.1Hz）(10极点的归一化因子)
%常数因子G=K*m
G=K*m
K11=0.074049
K12=0.0027416
K1_1000=K11*1000
K12_1000=K12*1000
K21=533.15
K22=142123
K31=667.62
K41=488.71
K51=178.88
K32=119422
K42=119422
K52=119422
%对应的极点是
p1=0.5*(-K11+sqrt(K11^2-4*K12))
p2=0.5*(-K11-sqrt(K11^2-4*K12))
p1_100=p1*100
p2_100=p2*100
p3=0.5*(-K21+sqrt(K21^2-4*K22))
p4=0.5*(-K21-sqrt(K21^2-4*K22))
p5=0.5*(-K31+sqrt(K31^2-4*K32))
p6=0.5*(-K31-sqrt(K31^2-4*K32))
p7=0.5*(-K41+sqrt(K41^2-4*K42))
p8=0.5*(-K41-sqrt(K41^2-4*K42))
p9=0.5*(-K51+sqrt(K51^2-4*K52))
p10=0.5*(-K51-sqrt(K51^2-4*K52))

pole=[-0.037025+0.037024i  -0.037025-0.037024i   -266.57+266.57i   -266.57-266.57i    -333.81+89.403i    -333.81-89.403i  .....
    -244.35-244.36i   -244.35+244.36i  -89.440+333.80i    -89.440-333.80i]
%这与蔡亚先提供的仅在末位有小差别。
zero=[0  0  0]


%计算归一化因子
f0=0.1
om=2*pi*f0
om*1i;
nomi=1;
for i=1:1:Num_zero
    nomi=(om*1i-zero(i))*nomi;
end
denomi=1;
for i=1:1:Num_pole
    denomi=(om*1i-pole(i))*denomi;
end
nomi
denomi
trans=nomi/denomi
trans_real=real(trans);
trans_imag=imag(trans);
trans_amp=sqrt(trans_real*trans_real+trans_imag*trans_imag)
normal=1/trans_amp
%---------------------------
for k=1:1:100000
f01=0.001*k;
om=2*pi*f01;
om*1i;
nomi=1;
for i=1:1:Num_zero
    nomi=(om*1i-zero(i))*nomi;
end
denomi=1;
for i=1:1:Num_pole
    denomi=(om*1i-pole(i))*denomi;
end
nomi;
denomi;
trans=nomi/denomi;
trans_real=real(trans);
trans_imag=imag(trans);
transf_amp(k)=sqrt(trans_real*trans_real+trans_imag*trans_imag);
%transf_phase(k)=atan(trans_imag/trans_real); %弧度为单位
transf_phase(k)=angle(trans); 
end

for k=1:1:100000
    transf_amp(k)=transf_amp(k)*normal;
end
for k=1:1:100000
    x(k)=0.001*k;
end
transf_amp_db=20*log10(transf_amp/transf_amp(1000));  %第1000频点对应1Hz
   %for k=1:1:50000
       %xlog(k)=log10(x(k));
   %end
figure(1)
subplot(2,1,1)
semilogx(x,transf_amp_db,'b')
xlabel('频率/Hz')
ylabel('振幅/dB')
title(['CTS-1地震计:零点个数',num2str(Num_zero),' ,极点个数',num2str(Num_pole),' A0=',num2str(normal),' f0=',num2str(f0),'Hz'])
subplot(2,1,2)
q=transf_phase;
semilogx (x,unwrap(q)*180/pi,'b')
xlabel('频率/Hz')
ylabel('相位/°')
%利用Matlab函数
%2零点，2极点
pole2(1)=pole(1);
pole2(2)=pole(2);
zero2(1)=zero(1);
zero2(2)=zero(2);
df=0.001;
n=100000;
w=[1:n]*df*2*pi;
[b22,a22]=zp2tf(zero2',pole2',1);
[h_cts1_22]=freqs(b22,a22,w);
ff10=w(100)/(2*pi);
ff11=w(101)/(2*pi);
nn=100;
nom22=1/abs(h_cts1_22(nn));
figure(2);
subplot(2,1,1)
semilogx(w/(2*pi),abs(h_cts1_22)*nom22,'b')
xlabel('频率/Hz')
ylabel('归一化振幅')
title(['CTS-1地震计:零点个数',num2str(Num_zero),' ,极点个数', num2str(Num_pole),' A0=',num2str(nom22),' f0=',num2str(f0),'Hz'])
subplot(2,1,2)
q=transf_phase;
semilogx (w/(2*pi),unwrap(phase(h_cts1_22))*180/pi,'b')
xlabel('频率/Hz')
ylabel('相位/°')
%由pole(1)和pole(2)计算地震计常数
n1=sqrt(pole(1)*pole(2));
n1D1=-(pole(1)+pole(2))/2;
D1=n1D1/n1;
T=2*pi/n1;
%利用Matlab函数
%0零点，8极点 （CTS-1系统-地震计）
Num_zero=0;
Num_pole=8;
zero0=[ ];
for i=1:8
    pole8(i)=pole(i+2);
end
pole_8=pole8;
[b08,a08]=zp2tf(zero0',pole8',1);
[h_cts1_08]=freqs(b08,a08,w);
nom08=1/abs(h_cts1_08(nn));
figure(3)
subplot(2,1,1)
semilogx(w/(2*pi),abs(h_cts1_08)*nom08,'b')
xlabel('频率/Hz')
ylabel('归一化振幅')
title(['CTS-1地震计:零点个数',num2str(Num_zero),' ,极点个数',num2str(Num_pole),' A0=',num2str(nom08),' f0=',num2str(f0),'Hz'])
subplot(2,1,2)
q=transf_phase;
semilogx (w/(2*pi),unwrap(phase(h_cts1_08))*180/pi,'b')
xlabel('频率/Hz')
ylabel('相位/°')
%2零点，10极点 （CTS-1系统）
Num_zero=2;
Num_pole=10;
zero2=[0 0 ];
[b210,a210]=zp2tf(zero2',pole',1);
[h_cts1_210]=freqs(b210,a210,w);
nom210=1/abs(h_cts1_210(nn));
figure(4)
subplot(2,1,1)
semilogx(w/(2*pi),abs(h_cts1_210)*nom210,'b')
xlabel('频率/Hz')
ylabel('归一化振幅')
title(['CTS-1地震计:零点个数',num2str(Num_zero),' ,极点个数',num2str(Num_pole),' A0=',num2str(nom210),' f0=',num2str(f0),'Hz'])
subplot(2,1,2)
q=transf_phase;
semilogx (w/(2*pi),unwrap(phase(h_cts1_210))*180/pi,'b')
xlabel('频率/Hz')
ylabel('相位/°')
 
%当变为位移响应时，增加一个零值零点。
%3零点，2极点
pole2=[-0.037025+0.037025i  -0.037025-0.037025i ];
zero3=[0 0 0];
Num_zero=3;
Num_pole=2;
[b32,a32]=zp2tf(zero3',pole2',1);
df=0.001;
f=[1:100000]*df;
w=f*2*pi;
[h_cts1_32]=freqs(b32,a32,w);
nom32=1/abs(h_cts1_32(nn)); %仍以0.1Hz为归一化频率
figure(5)
subplot(2,1,1)
loglog(w/(2*pi),abs(h_cts1_32)*nom32)
xlabel('频率/Hz')
ylabel('归一化振幅')
title(['CTS-1地震计:零点个数', num2str(Num_zero), ' ,极点个数', num2str(Num_pole), ' A0=', num2str(nom32), ' f0=', num2str(f0), 'Hz'])
subplot(2,1,2)
semilogx(w/(2*pi),unwrap(phase(h_cts1_32))*180/pi)
xlabel('频率/Hz')
ylabel('相位/°')
%3零点，10极点
pole=[-0.037025+0.037025i  -0.037025-0.037025i   -266.57+266.57i   -266.57-266.57i    -333.81+89.409i   
    -333.81-89.409i   -244.36-244.36i   -244.36+244.36i  -89.440+333.80i    -89.440-333.80i];
zero3=[0 0 0];
Num_zero=3;
Num_pole=10;
[b310,a310]=zp2tf(zero3',pole',1);
df=0.001;
f=[1:100000]*df;
w=f*2*pi;
[h_cts1_310]=freqs(b310,a310,w);
nom310=1/abs(h_cts1_310(nn)); %仍以0.1Hz为归一化频率
figure(6)
subplot(2,1,1)
loglog(w/(2*pi),abs(h_cts1_310)*nom310)
xlabel('频率/Hz')
ylabel('归一化振幅')
title(['CTS-1地震计:零点个数',num2str(Num_zero),' ,极点个数',num2str(Num_pole),' A0=',num2str(nom310),' f0=',num2str(f0),'Hz'])
subplot(2,1,2)
semilogx(w/(2*pi),unwrap(phase(h_cts1_310))*180/pi)
xlabel('频率/Hz')
ylabel('相位/°')
% bode(tf(b32,a32),{2*pi*0.001,2*pi*1000})
 
%个旧地震台CTS-1 2011参数
Calib_Factor= 985.1500244140625;     % counts/um/s
Calper=1.0;
Scale_Factor=1.488008998141349E19;
zero=[  0.0+0.0i  
        0.0+0.0i
        149.06689453125+219.10960388183594i
        149.06689453125-219.10960388183594i
        33.44247817993164+264.98199462890625i
        33.44247817993164-264.98199462890625i];
pole=[
        -0.03701839968562126-0.03702960163354874i
        -0.03701839968562126+0.03702960163354874i
        -7.11489200592041+266.0903015136719i
        -7.11489200592041-266.0903015136719i
        -12.306750297546387+248.88150024414062i
        -12.306750297546387-248.88150024414062i
        -23.029949188232422+223.48260498046875i
        -23.029949188232422-223.48260498046875i
        -34.88908004760742+183.58909606933594i
        -34.88908004760742-183.58909606933594i
        -47.26866912841797+130.25270080566406i
        -47.26866912841797-130.25270080566406i
        -58.25796890258789+66.42357635498047i
        -58.25796890258789-66.42357635498047i
        -64.11145782470703+0.0i];
Num_zero=6;
Num_pole=15;