## On the Processing of Seismic Records

### If you find the book and procedures are useful in your research you can acknowledge our work by providing a reference to the following work:
### Zhao Z H., Mu L Y., Lin X D.2022. Seismic record processing techniques,Seismological Press(in Chinese), Beijing, China.