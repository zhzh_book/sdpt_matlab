% Matlab生成该信号：
clear

f=5; %正弦波频率为5Hz
tao=0.2 %衰减因子
a=20  %信号被噪声掩盖
tshort=1 %短信号长度(s)
Fs=100 %采样率
t=0:1/Fs:tshort-1/Fs;
Nshort=tshort*Fs; %短信号点数
x1=a*sin(2*pi*f*t);
x2=exp(-t/tao);
x=x1.*x2;
tdelay=5 %信号延时时间sec
figure(1)
plot(t+tdelay,x,'r');
hold on
tlong=10; %长信号长度（s)
N=tlong*Fs; %长信号点数
y1=8*randn(N,1);
y1mean=mean(y1)
y1std=std(y1)  %无偏估计标准偏差
t1=0:1/Fs:tlong-1/Fs;
y1l=length(y1)
t1l=length(t1)
plot(t1,y1)
title('已知信号')
figure(2)
y=y1;
for i=1:1:Nshort
    y(tdelay*Fs+i)=y(tdelay*Fs+i)+x(i);
end

z=randn(N,1);
for i=1:1:Nshort
    z(tdelay*Fs+i)=z(tdelay*Fs+i)+x(i);
    
end
subplot(3,1,1)
plot(t+tdelay,x,'r')
title('(a)      已知信号')
axis([0 tlong -a a])
subplot(3,1,2)
plot(t1,y)
title('(b)      信号加噪声')
%计算x和y的互相关
[rxy,Nlag]=xcov(x,y);
[rxymax,maxindex]=max(rxy);
tstart=maxindex/Fs   %信号起始时间
rxy(500) % 对应互相关极大值，延迟=500，信号自501点起
rxy(501)
Nlag;

t1l=length(t1)
subplot(313)
plot(t1,rxy(1:t1l));hold on
title('(c)     互相关函数（蓝）及希尔伯特变换得到的包络（红）')
Hrxy=hilbert(rxy);
plot(t1,abs(Hrxy(1:t1l)),'r');
xlabel('时间/s')
[Hrxymax,Hmaxindex]=max(abs(Hrxy))
%
[ryz,Nlag]=xcov(y,z);
[ryzmax,maxzindex]=max(ryz)
tzstart=maxzindex/Fs   %信号起始时间
ryzl=length(ryz)
figure(4)
plot(ryz);hold on
Hryz=hilbert(ryz);
hryzl=length(abs(Hryz))
ryz999=ryz(999)
ryz1000=ryz(1000)
ryz1001=ryz(1001)
plot(abs(Hryz),'r');
[Hryzmax,Hmaxzindex]=max(abs(Hryz))
figure(5)
subplot(2,1,1)
plot(y)
subplot(2,1,2)
plot(z)
%直接计算短序列与长序列的互相关
for i=1:N-Nshort
    rxy0(i)=0;
    for j=1:Nshort
    rxy0(i)=rxy0(i)+x(j)*y(i+j-1);
    end
end
rxy0mean=mean(rxy0)
rxy0=rxy0-rxy0mean;
[rxy0max,rxy0maxindex]=max(abs(rxy0)) %找出绝对值最大的位置
figure(6)
subplot(2,1,1)
plot(y)
axis([0 900 -a +a])
subplot(2,1,2)
plot(rxy0);hold on
Hrxy0=hilbert(rxy0);
plot(abs(Hrxy0),'r')
[Hrxy0max,Hmaxxy0index]=max(abs(Hrxy0))
for i=1:N-Nshort
    rxy00=xcov(x,y([i:i+Nshort-1]),0); %序列长度=Nshort
    rxy0(i)=rxy00(1);
end
[rxy0max,rxy0maxindex]=max(abs(rxy0)) %找出绝对值最大的位置
figure(7)
subplot(2,1,1)
plot(y)
axis([0 900 -a +a])
subplot(2,1,2)
plot(rxy0);hold on
Hrxy0=hilbert(rxy0);
plot(abs(Hrxy0),'r')
[Hrxy0max,Hmaxxy0index]=max(abs(Hrxy0))