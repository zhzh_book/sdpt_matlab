%M04_06_wavelet_test.m
%小波试验
Fs=80;%采样率
delta =1/Fs; 
wname = 'db5'; %小波举例:db5
amax = 7; 
a = 2.^[1:amax];% 设定尺度因子

f = scal2frq(a,wname,delta); % 计算相应的伪频率
per = 1./f;  % 计算相应的伪周期
iter = 8;
cfreq = centfrq(wname,8,'plot')  
[Lo_D,Hi_D,Lo_R,Hi_R] = wfilters(wname)     %生成分解滤波器
figure(2)
subplot(221)
stem(Lo_D,'-.')
title('(a)  小波db5分解低通滤波器')
subplot(222)
stem(Hi_D,'-.')
title('(b)  小波db5分解高通滤波器')
subplot(223)
stem(Lo_R,'-.')
title('(c)  小波db5重构低通滤波器')
subplot(224)
stem(Hi_R,'-.')
title('(d)  小波db5重构高通滤波器')
figure(3)
n=1000
[h,w]=freqz(Lo_D,1,n);
f=w*Fs/(2*pi)
subplot(211)
plot(f,abs(h))
title('db5离散小波分解低通滤波器：振幅响应')
grid on
subplot(212)
plot(f,unwrap(angle(h))*180/pi)
ylabel('相位/°')
title('相位响应')
xlabel('频率/Hz')
grid on
figure(4)
[h,w]=freqz(Hi_D,1,n);
subplot(211)
plot(f,abs(h))
title('db5离散小波分解高通滤波器：振幅响应')
grid on
subplot(212)
plot(f,unwrap(angle(h))*180/pi)
title('相位响应')
xlabel('频率/Hz')
grid on
ylabel('相位/°')