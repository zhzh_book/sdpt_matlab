function [y_u,y_n,y_e]=M04_06_pre_filter(x_u,x_n,x_e,m,Fs,Zero,Pole,k,C_une,fn12)
%对3分量记录波形进行仪器校正和带通滤波
%x_u、x_n、x_e：记录波形垂直向、北南向、东西向
%m
%Fs
%Zero
%Pole
%k'
%n
%f1
%f2
C_u=C_une(1)
C_n=C_une(2)
C_e=C_une(3)
n=fn12(1)
fn1=fn12(2)
fn2=fn12(3)
[b,a]=zp2tf(Zero',Pole',k)

t=[1:m]/Fs;
nf=m   %频点数=数据点数
df=Fs/m
f=[1:nf]*df;
w=f*2*pi;
[h_km_60]=freqs(a,b,w);    %构成仪器校正滤波器
l_h_km_60=length(h_km_60)
f=w/(2*pi);
f10=f(10)
f100=f(100)  % 0.1Hz
abs01=abs(h_km_60(100))  %0.9996  因为已经用了归一化因子98570.0
figure(11)
subplot(211)
semilogx(f,abs(h_km_60))
ylabel('归一化振幅')
grid on
xlabel('频率/Hz')
subplot(2,1,2)
semilogx(w/(2*pi),unwrap(angle(h_km_60))*180/pi);
ylabel('相位/°')
grid on
xlabel('频率/Hz')
%-------------------------------------------------------------------------
%构造带通滤波器，使用巴特沃斯滤波器，频带=[0.5 2]Hz，
%--------------------------------------
n=2   %构造4阶带通巴特沃斯滤波器
fn1=1  %拐角频率
fn2=3
%wn=fn*2  /Fs % 按奈奎斯特频率归一化的圆频率
wn=[fn1  fn2]*2*pi  
figure(12)
[z,p,k1]=butter(n,wn,'s')
[b,a]=zp2tf(z,p,k1)
[h_butt]=freqs(b,a,w);
l_h_butt=length(h_butt)
subplot(2,1,1)
semilogx(w/(2*pi),abs(h_butt));
ylabel('归一化振幅')
grid on
xlabel('频率/Hz')
%plot(w,abs(h),co);hold on
%axis([0 25 0 1.1])
%axis([0 1 0 1.1])
%set(gca,'YTick',0:0.1:1.1)
subplot(2,1,2)
semilogx(w/(2*pi),unwrap(angle(h_butt))*180/pi);
%axis([0 25 -2000 200])
ylabel('相位/°')
grid on
xlabel('频率/Hz')
%---------------------------------------------------------
%滤波器和仪器校正滤波器叠加
for i=1:nf
    h_add(i)=h_km_60(i)*h_butt(i);
end
df=Fs/m
f=[1:nf]*df;
figure(13)
subplot(2,1,1)
semilogx(w/(2*pi),abs(h_add));
ylabel('归一化振幅')
grid on
xlabel('频率/Hz')
subplot(2,1,2)
semilogx(w/(2*pi),unwrap(angle(h_add))*180/pi);
ylabel('相位/°')
grid on
xlabel('频率/Hz')
%--------------------------------------------
%构建与傅里叶变换得到的前段与后端共轭形式数据对应的滤波器
%万永革，2012，第429页）
for ii=1:nf/2
    h1_add(ii)=h_add(ii);
    h1_add(nf-ii+1)=conj(h1_add(ii));
end
figure(14)
semilogx(f(1:nf/2),abs(h1_add(1:nf/2)))
%----频率域滤波
fft_x_u=fft(x_u);
fft_x_n=fft(x_n);
fft_x_e=fft(x_e);
for i=1:nf
fft_y_u(i)=fft_x_u(i)*h1_add(i);
fft_y_n(i)=fft_x_n(i)*h1_add(i);
fft_y_e(i)=fft_x_e(i)*h1_add(i);
end
y_u=real(ifft(fft_y_u))/C_u;     %除以转换灵敏度
y_n=real(ifft(fft_y_n))/C_n;
y_e=real(ifft(fft_y_e))/C_e;
figure(15)
subplot(311)
plot(t,y_u)
title('地动速度波形')
ylabel('垂直向/um/s')
subplot(312)
plot(t,y_n)
ylabel('北南向/um/s')
subplot(313)
plot(t,y_e)
xlabel('时间/s')
ylabel('东西向/um/s')
figure(16)
subplot(311)
semilogx(f(1:nf/2),abs(fft_x_u(1:nf/2)))
title('滤波前垂直向记录振幅谱')
ylabel('谱振幅')
subplot(312)
semilogx(f(1:nf/2),abs(fft_x_n(1:nf/2)))
title('滤波前北南向记录振幅谱')
xlabel('频率/Hz')
ylabel('谱振幅')
subplot(313)
%semilogx(f(1:nf/2),abs(fft_x_u(1:nf/2)))
semilogx(f(1:nf/2),abs(fft_x_e(1:nf/2)))    %韩雪君发现（2018-02-10），fft_x_u改成fft_x_e
title('滤波前东西向记录振幅谱')
xlabel('频率/Hz')
ylabel('谱振幅')
figure(17)
subplot(311)
semilogx(f(1:nf/2),abs(fft_y_u(1:nf/2)))
title('滤波后垂直向地动速度谱')
ylabel('谱振幅')
subplot(312)
semilogx(f(1:nf/2),abs(fft_y_n(1:nf/2)))
title('滤波后北南向地动速度谱')
ylabel('谱振幅')
subplot(313)
%semilogx(f(1:nf/2),abs(fft_y_u(1:nf/2)))
semilogx(f(1:nf/2),abs(fft_y_e(1:nf/2)))   %韩雪君发现（2018-02-10），fft_x_u改成fft_x_e
title('滤波后东西向地动速度谱')
xlabel('频率/Hz')
ylabel('谱振幅')
%----------------------------------------------
end

 