function[z_l,z_q]=M04_06_rota_t(y_u,y_r,m,eme_d)
%垂直分量和径向分量沿横向分量轴（T） 旋转
theta_test=eme_d*pi/180
for i=1:m
z_l(i)=y_u(i)*cos(theta_test)-y_r(i)*sin(theta_test);
z_q(i)=y_u(i)*sin(theta_test)+y_r(i)*cos(theta_test);
end
end

