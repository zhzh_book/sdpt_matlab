%3分量记录按射线方向旋转，2018-03-19修
clear all
%p_start=900;
%p_length=50;
%方位角phi(0-360度），入射角theta(0-90度），视出射角ebar(0-90度)
%真出射角etrue(0-90度），cos(etrue*pi/180)=(vp/vs)cos(((90+ebar)/2)*pi/180)
%入射角theta=90-etrue(度）
%视出射角ebar=atan2(Aud/sqrt(Aew^2+Ans^2))*180/pi
%《地震学与地震观测》，地震出版社，2007，第15-16页
%方位角phi=atan2(Aew/Ans)*180/pi
%数据：乌加河台(WJH)记录蒙古国地震200912040057（BMT）
stalat=41.3;stalon=108.1;   %台站坐标（粗）
%震源参数（据中国地震台网(CSN)地震目录）：
 epilat=42.24;epilon=106.79;    %depth=4km，Ms=4.3，O：12-03 16：57（UTC）
% 得到近似方位角：修改：2018-03-19
phi_epi_WJH=atan2((epilon-stalon)*cos(((epilat+stalat)/2)*pi/180),(epilat-stalat))*180/pi   %反方位角： -46°
%乌加河台， EDSP-IAS量得Aud=5.46+0.09=5.55;Aew=2.77+0.1=2.87；Ans=-1.97-1.22=-3.19
%Aud=5.55
%Aew=2.87
%Ans=-3.19
%广东XIG台um/s
Aud=23.9
Aew=7.0
Ans=18.7
ebar=atan2(Aud,sqrt(Aew^2+Ans^2))*180/pi
%ebar=52.2902
vps=1.73
etrue=acos(vps*cos(((90+ebar)/2)*pi/180))*180/pi
theta=90-etrue    %由P波三分量初动振幅（在msdp中量出的初动振幅，地动速度）得到入射角
%入射角=
phi_amp=atan2(Aew,Ans)*180/pi
%phi_amp=138.0227
if Aud>0
    phi_amp=phi_amp+180
end
%phi_amp=318.0227
%乌加河台记录
%epilat=42.24
%epilon=106.79
%depth=4
%stalat=41.3
%stalon=108.1
%乌加河台记录事件%phi_epi_WJH=334°   （2018-03-19修）
%广东XIG台记录Lat23.527，Lon114.374，h=6.0km，Station Lat : 23.7833，Station Lon : 114.635
epilat=23.527
epilon=114.374
depth=6
stalat=23.7833
stalon=114.635
%----------------------------------------------------------2018-03-19修改：
phi_epi=atan2((epilon-stalon)*cos(((epilat+stalat)/2)*pi/180),(epilat-stalat))*180/pi 
%广东XIG台phi_epi =223（震中相对台站的方位角（反方位角）），台站相对与源的方位角（射线方位角）=223-180=43°。
%下面用水平向旋转法得到方位角=40°，两者相差3°。（使用仪器校正并0.5-2Hz带通滤波后的地动速度信号）
%已知震中距为0.351°=39km，方位角差3°造成的位置差约2km。这或可由于定位误差所致。
err=39*3*pi/180   %  2.0km 
if phi_epi<0
    phi_epi=phi_epi+360
end
phi_epi_XIG=phi_epi   % 223.0°
%计算坐标轴旋转后的两个水平分量
%计算坐标轴旋转后的两个水平分量
%ascdata = importdata('wjh200912040057u.asc', ' ', 1);
ascdata = importdata('GD_XIG_BHZ_2.txt', ' ', 1);
md=length(ascdata.data);
md=floor(md/2)*2  %取信号长度（采样点数）为偶数
Fs=100 %采样率

d_start=2000
d_num=2000
m=d_num
t=[1:m]/Fs;
x_u(1:m)=ascdata.data(1+d_start:d_start+d_num);
%ascdata = importdata('wjh200912040057n.asc', ' ', 1);
ascdata = importdata('GD_XIG_BHN_0.txt', ' ', 1);       %读入广东台网记录的一个地方震，震中距39km
x_n(1:m)=ascdata.data(1+d_start:d_start+d_num);
%ascdata = importdata('wjh200912040057e.asc', ' ', 1);
ascdata = importdata('GD_XIG_BHE_1.txt', ' ', 1);
x_e(1:m)=ascdata.data(1+d_start:d_start+d_num);
x_umean=mean(x_u(1:500)) 
x_nmean=mean(x_n(1:500))
x_emean=mean(x_e(1:500))
for i=1:m
    x_u(i)=x_u(i)-x_umean;
    x_n(i)=x_n(i)-x_nmean;
    x_e(i)=x_e(i)-x_emean;
end

%化成地动位移
%乌加河利用JCZ-1的标称零极点值（位移响应），在时间域化成地动位移波形
%------------------------------------------------------------
%广东XIG利用KS2000M-60的标称零极点值（位移响应），在时间域化成地动位移波形
%垂直向Calib Factor: 1248.58count/um/s（转换灵敏度），Calper: 0.1，Scale Factor: 98570.0(归一化因子）
%北南向Calib Factor: 1268.09count/um/s，Calper: 0.1，Scale Factor: 98570.0
%东西向Calib Factor: 1270.61count/um/s，Calper: 0.1，Scale Factor: 98570.0
Zero=[0 0]
Pole=[ -0.074+0.074i  -0.074-0.074i   -222.0+222.0i   -222.0-222.0i]
k=98570
C_u=1248.58
C_n=1248.58
C_e=1270.61
C_une=[C_u C_n C_e]

n=2   %构造4阶带通巴特沃斯滤波器
fn1=1  %拐角频率
fn2=3
fn12=[n fn1 fn2]
%调用函数，取仪器响应和进行带通滤波
[y_u,y_n,y_e]=M04_06_pre_filter(x_u,x_n,x_e,m,Fs,Zero,Pole,k,C_une,fn12);
%--------------------------------------------------------------
%调用函数，自动搜索方位角
%p_start=2600;
p_start=601  %
p_length=50;
back_azi=M04_06_auto_azi(p_start,p_length,y_n,y_e,y_u)
azi_degree=back_azi    
%back_azi =221.3824    自动搜索结果  2018-03-21重算，由此得到射线方位角=221.4-180=41.4°
%Z、N、E旋转乘Z、R、T
azi_d=phi_epi   %223.0079，由震中和台站坐标计算出的（震中）方位角。震中在台站的西南方。
% 射线方位角=223.0-180=43.0°    
[y_r,y_t]=M04_06_rota_2(y_n,y_e,m,azi_d);
% 

%y_r与y_u合成：Z、R、T旋转成L、Q、T
theta=84   %按震中距39km和震源深度4km计算
 %使用原始波形初动振幅（msdp中化成地动速度）得到的入射角
[z_l,z_q]=M04_06_rota_t(y_u,y_r,m,theta);

%在Z-R垂直面上寻找视出射角
%p_start=2600;
p_start=601
p_length=50;
eme_d=M04_06_auto_eme(p_start,p_length,y_u,y_r,m)

%用视入射角eme_d转动
[z_l1,z_q1]=M04_06_rota_t(y_u,y_r,m,eme_d);

%计算能量参数并绘图
n_moving=50;%滑动窗长度0.5秒
energy=M04_06_ene_para(y_u,y_n,y_e,y_r,y_t,z_q1,z_l1,m,Fs,n_moving);
%偏振分析
%计算滑动协方差矩阵
n_cova=51 %0.5s   固定计算协方差矩阵的长度
%c_start=2550
%c_start=2000
c_start=1
c_num=2000-n_cova
 %给定起点c_start，计算协方差矩阵
% a=M04_06_cova(y_u,y_n,y_e,m,c_start,c_num,n_cova);
 a=M04_06_cova(y_n,y_e,y_u,m,c_start,c_num,n_cova);   %2018-03-20修
a11(1:c_num)=a(1,1,1:c_num);
a12(1:c_num)=a(1,2,1:c_num);
a13(1:c_num)=a(1,3,1:c_num);
a22(1:c_num)=a(2,2,1:c_num);
a23(1:c_num)=a(2,3,1:c_num);
a33(1:c_num)=a(3,3,1:c_num);
for i=1:c_num
    A=[a11(i) a12(i) a13(i);
    a12(i) a22(i) a23(i);
    a13(i) a23(i) a33(i)];
    %计算给定起点的协方差矩阵的本征值和本征矢量
    [V,D]=eig(A);
    
    % ====================================================================
    if i==601             %求协方差矩阵的波形段起点位置（初动到时）     （2018-03-20修）
        V1=V
        D1=D
    end
    % ====================================================================
     
    %{
    V1 =    0.5987    0.6061    0.5237   2018-03-21重算
    -0.7863    0.3199    0.5286
    0.1528   -0.7282    0.6681
    D1 = 0.1324         0         0
    0    0.2335         0
    0         0    2.8378
    %}
    V1_len=sqrt(V(1,3)^2+V(2,3)^2+V(3,3)^2);
    V1(i,1)=V(1,3)/V1_len;    %偏振方向 
    V1(i,2)=V(2,3)/V1_len;
    V1(i,3)=V(3,3)/V1_len; 
    lam1(i)=D(3,3);   %最大本征值
    lam2(i)=D(2,2);
    lam3(i)=D(1,1);
    V23=V(2,3);
    phai(i)=atan2(V(2,3),V(1,3))*180/pi;
    if i==601                %求协方差矩阵的波形段起点位置（P初动到时）     （2018-03-20修）
         phai1=phai(i)
    end
    % phai1 = 45.2660     %2018-03-21重算，射线方位角，P初动到时起，波形段长度50点（0.5秒）
    %入射角theta：
    theta(i)=atan2(sqrt(V(1,3)^2+V(2,3)^2),V(3,3))*180/pi;
end

%-------------------------------------------------------------
%计算k1需要已知P波初动方向矢量Vp。
Vp=[0.6711 0.5156 0.5327]
Vp_len=sqrt(Vp(1)^2+Vp(2)^2+Vp(3)^2)
Vp=Vp/Vp_len
nk=50   %计算k3 (式4.63）需要的时间窗长度
KK=1   %式（4.65）中的K
%这里取P波初动处协方差矩阵最大本征值对应的本征矢量作为Vp
q=1   %对比因子，式（4.54），（4.55）
a=0.1   %调节因子，式（4.60）
for i=1:c_num
   RL(i)=1-(lam2(i)/lam1(i))^q ;     %式（4.54）
end

%--------------------------------------------------------------------------
% 偏振滤波器
%用RL作为偏振滤波器（式（4.64）），J=1
Rti=RL;  %  (4.64)
%平滑滤波器，n_smoo=50
n_smoo=50
Rti_smoo(1: c_num)=zeros;
Djti_smoo(1:c_num,1)=zeros;
Djti_smoo(1:c_num,2)=zeros;
Djti_smoo(1:c_num,3)=zeros;
for i=1+n_smoo:c_num-n_smoo
    Rti_smoo(i)=0;
    Djti_smoo(i,1)=0;
    Djti_smoo(i,2)=0;
    Djti_smoo(i,3)=0;
    for j=1:n_smoo
        Rti_smoo(i)=Rti_smoo(i)+Rti(i-n_smoo+j-1);
        for jj=1:3
         Djti_smoo(i,jj)= Djti_smoo(i,jj)+abs(V1(i-n_smoo+j-1,jj));
        end
    end
    Rti_smoo(i)=Rti_smoo(i)/(n_smoo*2+1);
    for jj=1:3
         Djti_smoo(i,jj)= Djti_smoo(i,jj)/(n_smoo*2+1);
        end
end
%偏振滤波
for i=1:c_num
    yf_u(i)=y_u(c_start+i)*Rti_smoo(i)*Djti_smoo(i,1);
    yf_n(i)=y_n(c_start+i)*Rti_smoo(i)*Djti_smoo(i,2);
    yf_e(i)=y_e(c_start+i)*Rti_smoo(i)*Djti_smoo(i,3);
end
t1=[1:c_num]/Fs;

figure(30)
subplot(211)
plot(t1,Rti)
title('Rti')
subplot(212)
plot(t1,Rti_smoo)
title('Rjti_smoo')
figure(31)
subplot(411)
plot(t1,Rti_smoo)
title('偏振滤波器的滤波算子：(a)   Rti')
subplot(412)
plot(t1,Djti_smoo(1:c_num,1))
title('(b)  Djti-u')
subplot(413)
plot(t1,Djti_smoo(1:c_num,2))
title('(c)  Djti-n')
subplot(414)
plot(t1,Djti_smoo(1:c_num,3))
title('(d)  Djti-e')
xlabel('时间/s')
figure(32)
subplot(311)
plot(t1,yf_u)
title('偏振滤波结果：(a)  垂直分量')
subplot(312)
plot(t1,yf_n)
title('(b)  北南分量')
subplot(313)
plot(t1,yf_e)
title('(c)  东西分量')
xlabel('时间/s')



