%M04_06_wavelet_wpt.m
%小波包变换
clear
% 加载信号
load noisdopp; x = noisdopp;
figure(1); subplot(211); 
plot(x); title('Original signal');
% 使用香农熵(Shannon entropy),用db1小波包在第3层分解x 
wpt = wpdec(x,3,'db1');
% 画出小波包树wpt

% 读小波包(2,1)系数 
cfs = wpcoef(wpt,[2 1]);
subplot(212); 
plot(cfs); title('小波包(2,1)系数');
figure(2)
plot(wpt)
%构造原始信号
clear
N=200 %5段信号，每段长度为N
Nall=5*N;
n=0:N-1;
Fs=80
% 5段信号，频率为1,2,5,10,20Hz
x([1:N])=cos(2*pi*1.0*n/Fs);
x([N+1:2*N])=cos(2*pi*2.0*n/Fs);
x([2*N+1:3*N])=cos(2*pi*5.0*n/Fs);
x([3*N+1:4*N])=cos(2*pi*10.0*n/Fs);
x([4*N+1:5*N])=cos(2*pi*20.0*n/Fs);
x=x+0.5;
figure(3)
xfft=fft(x);
xfft_abs=abs(xfft)*2/(5*N);
xfft_abs(1)=xfft_abs(1)/2;
subplot(2,1,1);
t=0:1/Fs:(5*N-1)/Fs
plot(t,x);
title(['原始时间序列，5段，频率依次为1，2，5，10，20Hz,直流分量=0.5，采样率=',num2str(Fs)])
xlabel('时间/s')
ylabel('振幅')
axis([0 5*N/Fs -1 2])
subplot(2,1,2)
m=1:5*N;
f([1:5*N])=Fs/(5*N)*m;
plot(f,xfft_abs);
title('傅立叶变换振幅谱')
xlabel('频率/Hz')
ylabel('振幅')
axis([-1 40 0 0.8])
wname='db5'
figure(4)
wpt = wpdec(x,3,wname);
plot(wpt)
figure(5)
cfs = wpcoef(wpt,[2 0]);
subplot(411); 
plot(cfs); title('小波包(2,0)系数');
cfs = wpcoef(wpt,[2 1]);
subplot(412); 
plot(cfs); title('小波包(2,1)系数');
cfs = wpcoef(wpt,[2 2]);
subplot(413); 
plot(cfs); title('小波包(2,2)系数');
cfs = wpcoef(wpt,[2 3]);
subplot(414); 
plot(cfs); title('小波包(2,3)系数');

figure(6)
cfs = wpcoef(wpt,[1 0]);
subplot(411); 
plot(cfs); title('小波包(1,0)系数');
cfs = wpcoef(wpt,[1 1]);
subplot(412); 
plot(cfs); title('小波包(1,1)系数');
cfs = wpcoef(wpt,[0 0]);
subplot(413); 
plot(cfs); title('小波包(0,0)系数');
subplot(414); 
plot(x); title('原始信号');
figure(7)
cfs = wpcoef(wpt,[1 1]);
subplot(311); 
plot(cfs); title('(a)     小波包(1,1)系数');
cfs = wpcoef(wpt,[2 1]);
subplot(312); 
plot(cfs); title('(b)     小波包(2,1)系数');
subplot(313); 
cfs = wpcoef(wpt,[3 1]);
plot(cfs); title('(c)     小波包(3,1)系数');