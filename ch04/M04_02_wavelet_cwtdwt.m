%M04_06_wavelet_cwtdwt.m
%连续小波变换和离散小波变换
%构造原始信号
clear
N=200 %5段信号，每段长度为N
Nall=5*N;
n=0:N-1;
Fs=80
% 5段信号，频率为1,2,5,10,20Hz
x([1:N])=cos(2*pi*1.0*n/Fs);
x([N+1:2*N])=cos(2*pi*2.0*n/Fs);
x([2*N+1:3*N])=cos(2*pi*5.0*n/Fs);
x([3*N+1:4*N])=cos(2*pi*10.0*n/Fs);
x([4*N+1:5*N])=cos(2*pi*20.0*n/Fs);
x=x+0.5;
figure(1)
xfft=fft(x);
xfft_abs=abs(xfft)*2/(5*N);
xfft_abs(1)=xfft_abs(1)/2;
subplot(2,1,1);
t=0:1/Fs:(5*N-1)/Fs
plot(t,x);
title(['原始时间序列，5段，频率依次为1，2，5，10，20Hz,直流分量=0.5，采样率=',num2str(Fs)])
xlabel('时间/s')
ylabel('振幅')
axis([0 5*N/Fs -1 2])
subplot(2,1,2)
m=1:5*N;
f([1:5*N])=Fs/(5*N)*m;
plot(f,xfft_abs);
title('傅里叶变换振幅谱')
xlabel('频率/Hz')
ylabel('振幅')
axis([-1 40 0 0.8])
%短时傅里叶变换
% specgram(a,nfft,fs,window,numoverlap)使用滑动窗计算加窗的离散傅里叶变换，生成的谱图（spectrogram）是变换
% 后的振幅。a是待变换的信号（矢量），nfft指定计算FFT的长度，fs是采样率，window给出使用的时间窗，numoverlap是每次滑动重叠的
% 点数。时间窗的宽度不能大于计算FFT的长度。
figure(2)
%nw=N
%nw=N/2
nw=300
over=280
specgram(x,5*N,Fs,gausswin(nw),over)
title(['短时傅里叶变换谱图，窗宽=',num2str(nw),', 重叠点数=',num2str(over)])
xlabel('时间/s')
ylabel('频率/Hz')

s=x;
%用小波基函数"db5"进行单级（singel-level）离散小波变换
figure(3)
t=0:1/Fs:(Nall-1)/Fs;
tl=length(t)
t1=0:2/Fs:(Nall-1)/Fs;
t1l=length(t1)

[ca1,cd1] = dwt(s,'db5'); 
ca1l=length(ca1)
%[ca1,cd1] = dwt(s,'db10');
subplot(311); plot(t,s); title('(a)                       原始信号');
xlabel('时间/s')
subplot(323); plot(t1([1:t1l]),ca1([1:t1l])); title('(b)         db5小波分解的近似分量');
xlabel('时间/s')
subplot(324); plot(t1([1:t1l]),cd1([1:t1l])); title('(c)         db5小波分解的细节分量');
%subplot(323); plot(ca1); title('db10小波分解的近似分量'); 
%subplot(324); plot(cd1); title('db10小波分解的细节分量');
xlabel('时间/s')
% 对给定的小波db5或db10，计算两个相应的分解滤波器，然后直接利用这两个滤波器进行小波分解
[Lo_D,Hi_D] = wfilters('db5','d'); 
%[Lo_D,Hi_D] = wfilters('db10','d'); 
[ca1,cd1] = dwt(s,Lo_D,Hi_D);

subplot(325); plot(t1([1:t1l]),ca1([1:t1l])); title('(d)   直接用db5小波滤波器计算的小波分解近似分量'); 
xlabel('时间/s')
subplot(326); plot(t1([1:t1l]),cd1([1:t1l])); title('(e)   直接用db5小波滤波器计算的小波分解细节分量');
%subplot(325); plot(ca1); title('直接用db10小波滤波器计算的小波分解近似分量'); 
%subplot(326); plot(cd1); title('直接用db10小波滤波器计算的小波分解细节分量');
%认识小波滤波器
xlabel('时间/s')
figure(4)
% 给定小波名 
wname = 'db5';
%wname = 'db10';
% 计算给定小波的4个滤波器
[Lo_D,Hi_D,Lo_R,Hi_R] = wfilters(wname); 
subplot(221); stem(Lo_D); 
title('(a)   db5分解低通滤波器'); 
%title('db10分解低通滤波器'); 
subplot(222); stem(Hi_D); 
title('(b)   db5分解高通滤波器');
%title('db10分解高通滤波器');
subplot(223); stem(Lo_R); 
title('(c)   db5重构低通滤波器'); 
%title('db10重构低通滤波器');
subplot(224); stem(Hi_R);
title('(d)   db5重构高通滤波器');
%title('db10重构高通滤波器'); 
%将细节部分cd1再分解
figure(5)
t2=0:4/Fs:(Nall-1)/Fs;
t2l=length(t2)
[ca2,cd2] = dwt(ca1,wname); 
subplot(311); plot(t1([1:t1l]),ca1([1:t1l])); title('(a)     原始信号分解后的近似分量ca1'); 
subplot(312); plot(t2,ca2([1:t2l])); title('(b)     再分解的近似分量ca2'); 
subplot(313); plot(t2,cd2([1:t2l])); title('(c)     再分解的细节分量cd2');
xlabel('时间/s')
%将细节部分cd1再分解
figure(6)
t3=0:8/Fs:(Nall-1)/Fs;
t4=0:16/Fs:(Nall-1)/Fs;
t5=0:32/Fs:(Nall-1)/Fs;
t3l=length(t3)
t4l=length(t4)
t5l=length(t5)
[ca3,cd3] = dwt(ca2,'db5'); 
%[ca3,cd3] = dwt(ca2,'db10');
subplot(311); plot(t2,ca2([1:t2l])); title('(a)     cd1分解后的近似分量ca2'); 
subplot(312); plot(t3,ca3([1:t3l])); title('(b)     再分解的近似分量ca3'); 
subplot(313); plot(t3,cd3([1:t3l])); title('(c)     再分解的细节分量cd3');
xlabel('时间/s')
figure(7)
[ca4,cd4] = dwt(ca3,'db5');
%[ca4,cd4] = dwt(ca3,'db10');
subplot(311); plot(t3,ca3([1:t3l])); title('(a)     cd2分解后的近似分量ca3'); 
subplot(312); plot(t4,ca4([1:t4l])); title('(b)     再分解的近似分量ca4'); 
subplot(313); plot(t4,cd4([1:t4l])); title('(c)     再分解的细节分量cd4');
xlabel('时间/s')
figure(8)
[ca5,cd5] = dwt(ca4,'db5');
%[ca4,cd4] = dwt(ca3,'db10');
subplot(311); plot(t4,ca4([1:t4l])); title('(a)     cd3分解后的近似分量ca4'); 
subplot(312); plot(t5,ca5([1:t5l])); title('(b)     再分解的近似分量ca5'); 
subplot(313); plot(t5,cd5([1:t5l])); title('(c)     再分解的细节分量cd5');
xlabel('时间/s')
figure(9)
freqz(Lo_D,1);
subplot(2,1,1)
title([wname,'离散小波分解低通滤波器'])
xlabel('归一化频率')
ylabel('幅度/dB')
subplot(2,1,2)
xlabel('归一化频率')
ylabel('相位/度')
figure(10)
freqz(Hi_D,1);
subplot(2,1,1)
title([wname,'离散小波分解高通滤波器'])
xlabel('归一化频率')
ylabel('幅度/dB')
subplot(2,1,2)
xlabel('归一化频率')
ylabel('相位/度')
figure(10)
impz(Lo_D,1,10)
%impz(Lo_D,1,20)
%连续小波变换
figure(11)
subplot(6,1,1)
cwt1=cwt(s,32,wname)

plot(t,cwt1'/sqrt(32))
axis([0 N*5/Fs -1 1])

title('(a)     连续小波变换，尺度因子=32')
subplot(6,1,2)
cwt1=cwt(s,16,wname);
plot(t,cwt1'/4)
axis([0 N*5/Fs -1 1.5])
title('(b)        尺度因子=16')
subplot(6,1,3)
cwt1=cwt(s,8,wname);
plot(t,cwt1'/sqrt(8))
axis([0 N*5/Fs -1 1.5])
title('(c)        尺度因子=8')
subplot(6,1,4)
cwt1=cwt(s,4,wname);
plot(t,cwt1'/2)
axis([0 N*5/Fs -1 1.5])
title('(d)        尺度因子=4')
subplot(6,1,5)
cwt1=cwt(s,2,wname);
plot(t,cwt1'/sqrt(2))
axis([0 N*5/Fs -1 1.5])
title('(e)        尺度因子=2')
subplot(6,1,6)
cwt1=cwt(s,1,wname);
plot(t,cwt1')
axis([0 N*5/Fs -1 1.5])
title('(f)        尺度因子=1')
xlabel('时间/s')
figure(12)
cwt1=cwt(s,[0.1,1,2,4,8,16,32,64,128,256],wname,'plot');
title('标度因子a=0.1,1,2,...,256的小波变换系数绝对值')
xlabel('采样点序号')
ylabel('标度因子a')
colorbar
figure(13)
subplot(6,1,1)
plot(t5,cd5([1:t5l])/sqrt(32))
axis([0 (Nall-1)/Fs -1 1.5])
title('(a)     离散小波分解第5级细节部分')
subplot(6,1,2)
plot(t4,cd4([1:t4l])/4)
axis([0 (Nall-1)/Fs -1 1.5])
title('(b)     离散小波分解第4级细节部分')
subplot(6,1,3)
plot(t3,cd3([1:t3l])/sqrt(8))
axis([0 (Nall-1)/Fs -1 1.5])
title('(c)     离散小波分解第3级细节部分')
subplot(6,1,4)
plot(t2,cd2([1:t2l])/2)
axis([0 (Nall-1)/Fs -1 1.5])
title('(d)     离散小波分解第2级细节部分')
subplot(6,1,5)
plot(t1,cd1([1:t1l]))
axis([0 (Nall-1)/Fs -1 1.5])
title('(e)     离散小波分解第1级细节部分')
subplot(6,1,6)
plot(t,s)
axis([0 (Nall-1)/Fs -1 1.5])
title('(f)          原始信号')
xlabel('时间/s')
figure(14)
subplot(6,1,1)  
cwt1=cwt(s,32,wname)
    xx1=cwt1'/sqrt(32);
    for i=1:t5l-1
        yy1(i)=xx1(i*32);
    end
    plot(t5([1:t5l-1]),yy1)
axis([0 (Nall-1)/Fs -1 1.5])
title(['(a) 连续小波变换：尺度因子=32，采样率=',num2str(Fs/32)]);
subplot(6,1,2)
cwt1=cwt(s,16,wname);
xx1=cwt1'/sqrt(16);
    for i=1:t4l-1
        yy1(i)=xx1(i*16);
    end
    plot(t4([1:t4l-1]),yy1)
axis([0  (Nall-1)/Fs -1 1.5])
title(['(b)     尺度因子=16, 采样率=',num2str(Fs/16)])
subplot(6,1,3)
cwt1=cwt(s,8,wname);
xx1=cwt1'/sqrt(8);
    for i=1:t3l-1
        yy1(i)=xx1(i*8);
    end
    plot(t3([1:t3l-1]),yy1)
axis([0 (Nall-1)/Fs -1 1.5])
title(['(c)     尺度因子=8, 采样率=',num2str(Fs/8)])
subplot(6,1,4)
cwt1=cwt(s,4,wname);
xx1=cwt1'/sqrt(4);
    for i=1:t2l-1
        yy1(i)=xx1(i*4);
    end
    plot(t2([1:t2l-1]),yy1)
axis([0 (Nall-1)/Fs -1 1.5])
title(['(d)     尺度因子=4, 采样率=',num2str(Fs/4)])
subplot(6,1,5)
cwt1=cwt(s,2,wname);
xx1=cwt1'/sqrt(2);
    for i=1:t1l-1
        yy1(i)=xx1(i*2);
    end
    plot(t1([1:t1l-1]),yy1)
axis([0 (Nall-1)/Fs -1 1.5])
title(['(e)     尺度因子=2, 采样率=',num2str(Fs/2)])
subplot(6,1,6)
cwt1=cwt(s,1,wname);
xx1=cwt1';
    for i=1:tl-1  
        yy1(i)=xx1(i);
    end
    plot(t([1:tl-1]),yy1)
axis([0 (Nall-1)/Fs -1 1.5])
title(['(f)     尺度因子=1, 采样率=',num2str(Fs)])
xlabel('时间/s')

%小波试验
Fs=80;%采样率
delta =1/Fs; 
wname = 'db5'; %小波举例:db5
amax = 7; 
a = 2.^[1:amax];% 设定尺度因子

f = scal2frq(a,wname,delta); % 计算相应的伪频率
per = 1./f;  % 计算相应的伪周期
iter = 8;
figure(21)
cfreq = centfrq(wname,8,'plot')     % 计算中心频率并显示小波函数和基于中心频率的近似波形
title('小波db5，近似中心频率=0.6667Hz')
xlabel('时间/s')
[Lo_D,Hi_D,Lo_R,Hi_R] = wfilters(wname)     %生成分解滤波器
figure(22)
subplot(221)
stem(Lo_D,'-.')
title('(a)  小波db5分解低通滤波器')
subplot(222)
stem(Hi_D,'-.')
title('(b)  小波db5分解高通滤波器')
subplot(223)
stem(Lo_R,'-.')
title('(c)  小波db5重构低通滤波器')
subplot(224)
stem(Hi_R,'-.')
title('(d)  小波db5重构高通滤波器')
figure(23)
n=1000
[h,w]=freqz(Lo_D,1,n);
f=w*Fs/(2*pi)
subplot(211)
plot(f,abs(h))
title('db5离散小波分解低通滤波器：振幅响应')
grid on
subplot(212)
plot(f,unwrap(angle(h))*180/pi)
ylabel('相位/°')
title('相位响应')
xlabel('频率/Hz')
grid on
figure(24)
[h,w]=freqz(Hi_D,1,n);
subplot(211)
plot(f,abs(h))
title('db5离散小波分解高通滤波器：振幅响应')
grid on
subplot(212)
plot(f,unwrap(angle(h))*180/pi)
title('相位响应')
xlabel('频率/Hz')
grid on
ylabel('相位/°')