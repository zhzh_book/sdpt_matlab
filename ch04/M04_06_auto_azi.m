function back_azi=M04_06_auto_azi(p_start,p_length,y_n,y_e,y_u)
%自动搜索方位角
max=0;
for n=1:1:360
azi=pi/180*n;
y2=y_n*cos(azi)+y_e*sin(azi);
y1=-y_n*sin(azi)+y_e*cos(azi);
z2=abs(y2);
z1=abs(y1);
sum2(n)=sum(z2(p_start:p_start+p_length));
sum1(n)=sum(z1(p_start:p_start+p_length));
diff(n)=sum2(n)-sum1(n);
if diff(n)>max
    max=diff(n);
    max_n=n;
end
end
y_ru=0;
azi0=max_n*pi/180;
for i=p_start:p_start+p_length
y_ru=y_ru+(y_n(i)*cos(azi0)+y_e(i)*sin(azi0))*y_u(i);
end
y_ru
if y_ru<0
back_azi=max_n;
else
    back_azi=max+180;
end
if back_azi>360
    back_azi=back_az-360;
end
figure(1)
plot(diff)
xlabel('方位角/度')
title({'两水平分量短段绝对值之和的差值,反方位角=',back_azi,'度'})
end

