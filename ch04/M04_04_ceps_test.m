% 倒谱变换cepstest.m
% cepstest.m
clear
N=1000
Fs=50
n=1:N;dt=1/Fs
t=dt:dt:N/Fs;
df=Fs/N
f=df:df:Fs;
x=sin(2*pi*n/Fs+pi/4);
figure(1) %原始信号
subplot(2,1,1)
plot(t,x)
title('正弦信号x=sin(2*pi*n/Fs+pi/4)，Fs=50sps')

y=x;
for i=301:700
    %y(i)=x(i)*exp(-0.005*i);
    y(i)=1.5*x(i);
end
subplot(2,1,2)
plot(t,y)
xlabel('时间/s')
title('在第301-700点振幅放大为1.5倍')
yfft=fft(y);
figure(2) %原始信号的FFT的幅值和相角
subplot(2,1,1)
plot(f,abs(yfft)*2/N)
title('FFT振幅')
subplot(2,1,2)
plot(f,unwrap(angle(yfft))*180/pi)
title('FFT相位')
ylogfft=log(yfft);
xlabel('频率/Hz')
ylabel('度')
figure(3) % 原始信号FFT后取自然对数后的实部和虚部
subplot(2,1,1)
plot(real(ylogfft))
title('FFT的自然对数实部')
subplot(2,1,2)
plot(imag(ylogfft))
title('FFT的自然对数虚部')
xlabel('数据点序号')
yceps=ifft(ylogfft);
figure(4) % 原始信号FFT后再IFFT的实部和虚部
subplot(2,1,1)
plot(real(ifft(yfft)))
title('FFT的IFFT的实部')
subplot(2,1,2)
plot(imag(ifft(yfft)))
title('FFT的IFFT的虚部')
xlabel('时间/s')
figure(5) % 复倒谱的实部和虚部
%subplot(2,1,1)
plot(t,real(yceps))
title('FFT的自然对数的IFFT（倒谱）的实部')
%subplot(2,1,2)
xlabel('倒频率（quefrency）/s')
figure(6)
plot(t,imag(yceps))
title('FFT的自然对数的IFFT（倒谱）的虚部')
xlabel('倒频率（quefrency）/s')
figure(7)
subplot(2,1,1)
plot(t,cceps(y))
title('用cceps函数得到的复倒谱的实部')
subplot(2,1,2)
plot(t,rceps(y))
title('用rceps函数得到的实倒谱')
xlabel('倒频率（quefrency）/s')
%使用ceps函数
xhat=cceps(y); % 复倒谱
[xhat1,nd] = cceps(y); %复倒谱，nd为计算复倒谱时引入的延迟点数
nd
figure(8) 
subplot(2,1,1)
plot(xhat)
title('用cceps函数得到的复倒谱')
subplot(2,1,2)
plot(xhat1)
title({'用cceps函数得到的复倒谱，nd=',nd})
xlabel('倒频率（quefrency）/s')
figure(9) % 复倒谱逆变换
subplot(2,1,1)
plot(t,icceps(xhat,nd))
title({'用icceps恢复原始信号,使用求倒谱时得到的nd=',nd})
subplot(2,1,2)
plot(t,icceps(xhat,200))
title('用icceps恢复原始信号,指定nd=200')
xlabel('时间/s')
figure(10)
rhat=rceps(y);
subplot(2,1,1)
plot(icceps(rhat,nd))
title({'使用实倒谱，用icceps恢复原始信号,使用求复倒谱时得到的nd=',nd})
subplot(2,1,2)
plot(icceps(rhat))
title('使用实倒谱,用icceps恢复原始信号')
xlabel('时间/s')
