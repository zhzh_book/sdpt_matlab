function eme_d=M04_06_auto_eme(p_start,p_length,y_u,y_r,m)
%用垂直分量和径向分量自动搜索P波视入射角
%   Detailed explanation goes here
max=0;
for n=1:1:90
theta_test=pi/180*n;
for i=1:m
z_l1(i)=y_u(i)*cos(theta_test)-y_r(i)*sin(theta_test);
z_q1(i)=y_u(i)*sin(theta_test)+y_r(i)*cos(theta_test);
end
z2=abs(z_l1);
z1=abs(z_q1);
sum2(n)=sum(z2(p_start:p_start+p_length));
sum1(n)=sum(z1(p_start:p_start+p_length));
diff(n)=sum2(n)-sum1(n);
if diff(n)>max
    max=diff(n);
    max_n=n;
end
end
figure(5)
plot(diff(1:90))
xlabel('视入射角/°')
title({'Z和R分量短段绝对值之和的差值,差值最大值对应视入射角=',max_n,'度'})
eme_d=max_n

end

