function energy=M04_06_ene_para(y_u,y_n,y_e,y_r,y_t,z_q,z_l,m,Fs,n_moving)
%计算能量参数
%以噪声部分计算均值
t=[1:m]/Fs;
y_umean=mean(y_u(1:500)) 
y_nmean=mean(y_n(1:500))
y_emean=mean(y_e(1:500))
for i=1:n_moving
    rj2(i)=y_u(i)^2+y_n(i)^2+y_e(i)^2;
    hjh2(i)=z_q(i)^2+y_t(i)^2;
    hja2(i)=z_l(i)^2+hjh2(i);
    rnj2(i)=0;
    hnjh2(i)=0;
    hnja2(i)=0;
end
for i=n_moving+1:m
   % rj2(i)=y_u(i)^2+y_n(i)^2+y_e(i)^2;     %三分量单采样点平方和
    hjh2(i)=z_q(i)^2+y_t(i)^2;   % Q分量和T分量平方和
    hja2(i)=z_l(i)^2+hjh2(i);    % Z、Q、T三分量平方和
    hbjh2(i)=y_n(i)^2+y_e(i)^2; %北南、东西两分量平方和
    rj2(i)=hbjh2(i)+y_u(i)^2;   %三分量单采样点平方和
    rnj2(i)=0;
     hnjh2(i)=0;
    hnja2(i)=0;
    hbnjh2(i)=0;
    hbnja2(i)=0;
    hbnj2(i)=0;
    for j=1:n_moving
        rnj2(i)=rnj2(i)+rj2(i-n_moving+j);  %三分量滑动平方和
        hnjh2(i)=hnjh2(i)+hjh2(i-n_moving+j);% Q分量和T分量滑动平方和
        hnja2(i)=hnja2(i)+hja2(i-n_moving+j);% Z、Q、T三分量滑动平方和
        hbnjh2(i)=hbnjh2(i)+hbjh2(i-n_moving+j);%北南、东西两分量滑动平方和
        hbnja2(i)=hbnja2(i)+rj2(i-n_moving+j);  %三分量滑动平方和
    end
    rnj2(i)=rnj2(i)/n_moving;   %  式（3.33） ，三分量滑动平方和按窗长归一化
     hnjh2(i)= hnjh2(i)/n_moving;  
       hnja2(i)= hnja2(i)/n_moving;  
         hbnjh2(i)= hbnjh2(i)/n_moving;  
          hbnja2(i)= hbnja2(i)/n_moving;  
    if hnja2(i)==0
        hj2(i)=0;
    else
        hj2(i)=hnjh2(i)/hnja2(i);  %式（3.34），横向能量与总能量之比
    end
     if hnjh2(i)==0
        hj2(i)=0;
    else
        hj21(i)=hnja2(i)/hnjh2(i); 
     end

    if hbnja2(i) ==0
        hbj2(i)=0;
    else
        hbj2(i)=hbnjh2(i)/hbnja2(i);   %式（3.35），水平向能量与总能量之比
    end
     if hbnjh2(i)==0
        hbj2(i)=0;
    else
        hbj21(i)=hbnja2(i)/hbnjh2(i); 
     end
end

%显示地震波能量参数
figure(7)
subplot(4,1,1)
plot(t,hnja2)
title({'(a)  Z、Q、T三分量滑动平方和（已按窗长归一化)'})
subplot(4,1,2)
plot(t,hbnja2);
title({'(b)  三分量滑动平方和（已按窗长归一化)'})
subplot(4,1,3)
plot(t, hnjh2)
%xlim([850 1500]);ylim([-20000 20000]);
title({'(c)  Q分量和T分量滑动平方和（已按窗长归一化)'})
subplot(4,1,4)

plot(t,hbnjh2)
%xlim([850 1500]);ylim([-20000 20000]);
title('(d)  北南、东西两分量滑动平方和（已按窗长归一化)')
xlabel('时间/s')

figure(8)
subplot(4,1,1)
plot(t,rj2);
title({'(a)  三分量单采样平方和'})
ylabel('/(um/s)^2')
subplot(4,1,2)
plot(t,rnj2);
title({'(b)  三分量滑动平方和（已按窗长归一化)'})
ylabel('/(um/s)^2')
subplot(4,1,3)
plot(t,hj2);
title({'(c) 横向能量与总能量之比'})
subplot(4,1,4)
plot(t,hbj2);
%xlim([850 1500]);ylim([-20000 20000]);
title('(d)  水平向能量与总能量之比')
xlabel('时间/s')


energy=1
end

