function a=M04_06_cova_n(w1,w2,w3,m,c_start,c_num,n_cova,n_step)
%计算协方差矩阵
%w1,w2,w3    信号北南、东西、垂直三分量（2018-03-20修）
%m   信号长度（采样点数）
%c_start：计算协方差矩阵的起点
%c_num：计算协方差矩阵的个数，p_du=1则只计算p_start一点的协方差矩阵
%n_cova：计算协方差矩阵的时间窗长度（采样点数）
%a：返回的协方差矩阵  a(ii,jj,i),ii=1,2,3;jj=1,2,3;i为协方差矩阵序号，i=1,2,...,c_num
b=0;
mmm=m;
w_mean(1)=mean(w1);
w_mean(2)=mean(w2);
w_mean(3)=mean(w3);
for i=1:m
    w(1,i)=w1(i);
    w(2,i)=w2(i);
    w(3,i)=w3(i);
end
for ii=1:3
    for jj=1:3
for i=1:c_num
    iii=i;
    a(ii,jj,i)=0;
    n_cov=n_cova+n_step*(i-1);
for j=c_start:c_start+n_cov-1+i-1
    jjj=j;
    wjjj=w(jj,j);
    wiij=w(ii,j);
  a(ii,jj,i)= a(ii,jj,i)+(w(ii,j)-w_mean(ii))*(w(jj,j)-w_mean(jj));
end
 a(ii,jj,i)= a(ii,jj,i)/n_cova;
end
    end
end
end
