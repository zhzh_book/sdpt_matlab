%短时傅里叶变换（STFT）试验
% 构成5段波形
clear all
%构造原始信号
N=200 %5段信号，每段长度为N
n=0:N-1;
Fs=80
% 5段信号，频率为1,2,5,10,20Hz
x([1:N])=cos(2*pi*1.0*n/Fs);
x([N+1:2*N])=cos(2*pi*2.0*n/Fs);
x([2*N+1:3*N])=cos(2*pi*5.0*n/Fs);
x([3*N+1:4*N])=cos(2*pi*10.0*n/Fs);
x([4*N+1:5*N])=cos(2*pi*20.0*n/Fs);
x=x+0.5;
figure(1)
xfft=fft(x);
xfft_abs=abs(xfft)*2/(5*N);     % 真实振幅
xfft_abs(1)=xfft_abs(1)/2;        % 真实直流分量
subplot(2,1,1);
plot(x);
title(['原始时间序列，5段，频率依次为1，2，5，10，20Hz,直流分量=0.5，采样率=',num2str(Fs)])
xlabel('采样点')
ylabel('振幅')
axis([0 5*N -1 2])
subplot(2,1,2)
m=1:5*N;
f([1:5*N])=Fs/(5*N)*m;
plot(f,xfft_abs);
title('傅里叶变换振幅谱')
xlabel('频率/Hz')
ylabel('振幅')
axis([-1 40 0 0.8])
figure(2)
nw=N;%给定窗宽度
over=100; %给定重叠点数
specgram(x,5*N,Fs,gausswin(nw),over)
title(['短时傅里叶变换谱图，窗宽=',num2str(nw),', 重叠点数=',num2str(over)])
xlabel('时间/s')
ylabel('频率/Hz') 
figure(3)
nw=240;%给定窗宽度
over=120; %给定重叠点数
specgram(x,5*N,Fs,gausswin(nw),over)
title(['短时傅里叶变换谱图，窗宽=',num2str(nw),', 重叠点数=',num2str(over)])
xlabel('时间/s')
ylabel('频率/Hz')
figure(4)
nw=320;%给定窗宽度
over=160; %给定重叠点数
specgram(x,5*N,Fs,gausswin(nw),over)
title(['短时傅里叶变换谱图，窗宽=',num2str(nw),', 重叠点数=',num2str(over)])
xlabel('时间/s')
ylabel('频率/Hz')
figure(5)
nw=300;%给定窗宽度
over=280; %给定重叠点数
specgram(x,5*N,Fs,gausswin(nw),over)
title(['短时傅里叶变换谱图，窗宽=',num2str(nw),', 重叠点数=',num2str(over)])
xlabel('时间/s')
ylabel('频率/Hz')
figure(6)
nw=100;%给定窗宽度
over=50; %给定重叠点数
specgram(x,5*N,Fs,gausswin(nw),over)
title(['短时傅里叶变换谱图，窗宽=',num2str(nw),', 重叠点数=',num2str(over)])
xlabel('时间/s')
ylabel('频率/Hz')
figure(7)
nw=80;%给定窗宽度
over=40; %给定重叠点数
specgram(x,5*N,Fs,gausswin(nw),over)
title(['短时傅里叶变换谱图，窗宽=',num2str(nw),', 重叠点数=',num2str(over)])
xlabel('时间/s')
ylabel('频率/Hz')

