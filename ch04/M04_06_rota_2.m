function [y_r,y_t]=M04_06_rota_2(y_n,y_e,m,azi_d)
%水平分量X、Y旋转成R、T分量
azi=azi_d*pi/180
for i=1:m
y_r(i)=y_n(i)*cos(azi)+y_e(i)*sin(azi);
y_t(i)=-y_n(i)*sin(azi)+y_e(i)*cos(azi);
end




end

