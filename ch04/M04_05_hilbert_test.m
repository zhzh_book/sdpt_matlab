clear all
clear figure
%-----------------------------------------------
load ./DKU1206.asc
x=DKU1206(1:3000);
%-----------------------------------------------

%用傅立叶变换计算希尔伯特变换
Fs=50
n=3000;
xfft=fft(x);
yfft(1)=xfft(1);
yfft(2:n/2)=2*xfft(2:n/2);
yfft(n/2+1:n)=0;
y=ifft(yfft);
figure(1)
subplot(2,1,1)
m=1:1:3000;
plot(m(500:1000),real(y(500:1000)),'b',m(500:1000),imag(y(500:1000)),'r:')
title('用FFT和IFFT计算的希尔伯特变换')
legend('原始信号','信号的希尔伯特变换')
subplot(2,1,2)
for i=1:1:3000
    amp(i)=sqrt(real(y(i))^2+imag(y(i))^2);
end
plot(m(500:1000),real(y(500:1000)),'b',m(500:1000),amp(500:1000),'r:')
title('信号的包络')
legend('原始信号','信号的包络')
xlabel('点序号')
z=hilbert(x); % 直接用Matlab函数
figure(2)
subplot(2,1,1)

plot(m(500:1000),real(z(500:1000)),'b',m(500:1000),imag(z(500:1000)),'r:')
legend('原始信号','信号的希尔伯特变换')
title('用y=hilbert(x)计算的希尔伯特变换')

subplot(2,1,2)
plot(m(500:1000),real(z(500:1000)),'b',m(500:1000),abs(z(500:1000)),'r:')

legend('原始信号','信号的包络')
xlabel('点序号')
figure(3)

u=unwrap(angle(z(1:3000)));
%计算瞬时频率
for i=1:2999
    tf(i)=(u(i+1)-u(i))*Fs/(2*pi);
end
tf(201:300)
subplot(211)

plot(m(500:1000),tf(500:1000),'b')
title('信号的瞬时频率和瞬时相位')
legend('瞬时频率')

ylabel('频率/Hz')
subplot(212)
plot(m(500:1000),(u(500:1000)-u(500)))
ylabel('弧度')
legend('瞬时相位')
xlabel('点序号')
