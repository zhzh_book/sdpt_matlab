function c=zslipV_fun(str,dip,rake,str2,iflag,siz,lon,lat)   
%画0点、A点和N点
%str,dip,rake单位为弧度
 A=[cos(rake)*cos(str)+sin(rake)*cos(dip)*sin(str), cos(rake)*sin(str)-sin(rake)*cos(dip)*cos(str), -sin(rake)*sin(dip)];   %A（滑动矢量）的单位方向矢量,根据（10-2-8）式计算
 N=[-sin(str)*sin(dip), cos(str)*sin(dip), -cos(dip)];   %N（节面法线）的单位方向矢量，根据（10-2-9）式计算
 [Ntrpl]=wan_v2trpl(N); %求出N走向和俯角
 [Atrpl]=wan_v2trpl(A); %求出A走向和俯角
%绘出中心点
plot(0,0,'g+');hold on
%绘制A轴，在第一节线上
 rp = siz*sqrt(2)*sind((90-Atrpl(2))/2);    %根据（10-5-13）计算Schmidt投影的距中心点距离
xpA=lon+rp*sind(Atrpl(1)); ypA=lat+rp*cosd(Atrpl(1));
p=plot(xpA,ypA,'.');hold on
set(p,'Color','black')
text(xpA+0.1,ypA,'A');  
xs1=siz*sin(str);    
ys1=siz*cos(str);   %Schmidt投影的坐标
if iflag==1 || iflag==2 || iflag==3 
if rake<0
     line([0  xs1],[0  ys1 ],'Color','black','LineWidth',1);hold on  %第1节线走向黄色
     line([-xs1  0],[-ys1  0],'Color','black','LineWidth',1,'LineStyle',':');hold on  %第1节线走向黄色
    xx=[0 0.7*xpA]
yy=[0 0.7*ypA]
else
     line([-xs1  0],[-ys1  0],'Color','black','LineWidth',1);hold on  %第1节线走向黄色
     line([0  xs1],[0  ys1 ],'Color','black','LineWidth',1,'LineStyle',':');hold on  %第1节线走向黄色
xx=[0.7*xpA  0]
yy=[0.7*ypA 0]
end
[xxf,yyf]=ds2nfu(xx,yy)
th=annotation('arrow',xxf,yyf,'Color','black');hold on
 line([0 xpA],[0 ypA],'Color','black','LineWidth',1);hold on  %A轴黄色
end
%}
 %-----------------------------------------------------
%绘制N轴
rp = siz*sqrt(2)*sind((90-Ntrpl(2))/2);    %根据（10-5-13）计算Schmidt投影的距中心点距离
xpN=lon+rp*sind(Ntrpl(1)); ypN=lat+rp*cosd(Ntrpl(1));
p=plot(xpN,ypN,'.');hold on
set(p,'Color','black')
text(xpN+0.1,ypN,'N');  
 %绘出滑动矢量
 xs2=siz*sin(str2);    
ys2=siz*cos(str2);   %Schmidt投影的坐标
if iflag==1 || iflag==2 || iflag==4 
% 画第2节线走向及滑动矢量
 if (rake<0)
      line([0  xs2],[0  ys2],'Color','black','LineWidth',1);hold on  %第2节线走向绿色
      line([-xs2  0],[-ys2  0],'Color','black','LineWidth',1,'LineStyle',':');hold on  %第2节线走向绿色
xx=[0 0.7*xpN ]
yy=[0 0.7*ypN ]
 else
      line([-xs2  0],[-ys2  0],'Color','black','LineWidth',1);hold on  %第2节线走向绿色
      line([0  xs2],[0  ys2],'Color','black','LineWidth',1,'LineStyle',':');hold on  %第2节线走向绿色
xx=[0.7*xpN  0]
yy=[0.7*ypN  0]
 end
[xxf,yyf]=ds2nfu(xx,yy)
th=annotation('arrow',xxf,yyf,'Color','black');hold on   %数据空间坐标到归一化图形单位（nfu)转换
line([0 xpN],[ 0 ypN],'Color','black','LineWidth',1);hold on     %N轴绿色
 %line([0 -0.3*xpN],[ 0 -0.3*ypN],'Color','g','LineWidth',2);hold on     %N轴绿色
 %画中心点出发的走向方向
end
c=1
end

