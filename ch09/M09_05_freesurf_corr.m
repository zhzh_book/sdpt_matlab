% 移植FOCMEC中的自由表面校正程序

	rd = 45.0/atan(1.0)
	ash = 2.0
	%open(2,file='freesurf.lst',status='unknown')
	vpvs =sqrt(3)  % vp/vs
    %vpvs=1/0.6       %FOCMEC手册图C2
    aa=0;   %给定视出射角
    aa=1;   %给出真出射角
    factor=10
    for i=1:90*factor
        if aa==0;
	    angin = i/factor;
	    Pang(i) = rd*asin(vpvs*sin(0.5*angin/rd));    %P真出射角(°）
	    Sang(i) = Pang(i);  % S真出射角
	  else
	    Pang(i) = i/factor;
	    Sang(i) = i/factor;
	   end 
	  [apr,apv,phrp]=fun_gdmot(rd,1,1/vpvs,Pang(i)) ;  
      % P波入射振幅为1，则地面P垂直向（向上为正）振幅为apv，地面水平向（径向）为apr
      %所以实际上这里的apv、apr是地面记录波形振幅与入射波振幅之比
       [asr,asv,phrs]=fun_gdmot(rd,2,1/vpvs,Sang(i));  
       %S波入射振幅为1，则地面S垂直向（向上为正）振幅为asv，地面水平向（径向）为asr
	%所以实际上这里的asv、asr是地面记录波形振幅与入射波振幅之比
	  asr = abs(asr);
	  asv = abs(asv);
      svr_pz(i)=apv/asr;   %SV(R)/P(Z) 校正值
      pz_svz(i)=asv/apv;%SV(Z)/P(Z) 校正值
      sht_pz(i)=apv/ash;
      svr_sht(i)=ash/asr;
      svz_sht(i)=ash/asv;
      svr_svz(i)=asr/asv;
	  pz(i)=apv;
      pr(i)=apr;
      sz(i)=asv;
      sr(i)=asr;
      phrpd(i)=phrp;
      phrsd(i)=phrs;
       phrsvv(i)=0;
      if phrs>0
      phrsvv(i)=phrs+90;
      end
      %SV入射的临界角
      sv_cri=asin(1/vpvs)*rd;
      ph_90=asin(vpvs*sin(0.5*45/rd));   %S入射角45°时，
          %{
 if Pang(i)<ph_cri
          phrsvr(i)=phrs;
      else
          phrsvr(i)=phrs-180;
      end
     %}
	end 
figure(1)
plot(Pang,pz,Pang,pr)
legend('pz','pr')
xlabel('入射角/°')
%ylabel('Cz')
figure(2)
plot(Pang,sz,Pang,sr)
legend('sz','sr')
xlabel('入射角/°')
%ylabel('Cz')
figure(3)
plot(Pang,phrpd,Pang,phrsd)
legend('phrpd','phrsd')
xlabel('自由表面入射角/°')
%ylabel('Cz')
figure(4)
plot(Pang,phrsd)
xlabel('入射角/°')
%ylabel('Cz')
legend('phrsd')
figure(5)
plot(Pang,phrsvv,Pang,phrsd)
legend('phrv','phrr')
xlabel('入射角/°')
%ylabel('Cz')
%ph_cri*rd
sv_cri
figure(6)
plot(Pang,pz_svz)
legend('Sigma=0.25')
xlabel('自由表面入射角/°')
ylabel('Cz')
