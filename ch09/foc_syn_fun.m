function [ value] = foc_syn_fun(A,N,TREND,PLUNGE,pflag)
%给定震源机制和下半震源球上射线出头点的方位角和离源角，计算出头点的理论初动符号或辐射振幅
%给定震源机制的走向strike、倾角dip、离源角rake;单位°
%下半震源球上射线出头点的方位角azi和离源角toa;单位°
%pflag：=1，P初动；2，SV初动（沿射线方向为正）；3，SH初动（沿射线方向，向右为正）
%4，P振幅；5，SV振幅；6，SH振幅。正负号与初动符号一致。
%   返回值：初动用+/-1表示，振幅用其值表示
%增（2017-04-11）flag=7 、8画偏振角
	
	    if (pflag==1 || pflag==4)   % P
          
      COST = cos(TREND);
	  SINT = sin(TREND);
	  COSP =cos(PLUNGE);
	  SINP = sin(PLUNGE);
	  XYZ(1) = COST*COSP;
	  XYZ(2) = SINT*COSP;
	  XYZ(3) = SINP;
        RA = 0.0;
	RN = 0.0;
	for J=1:3
	  RA = RA + XYZ(J)*A(J);
	  RN = RN + XYZ(J)*N(J);
    end
	  value = 2*RA*RN;      %P
      if  (pflag==1)
        value=sign(value);
    end
    end
  if (pflag==2 || pflag==5  || pflag==7 || pflag==8)   % SH
          
     COST = cos(TREND);
	  SINT = sin(TREND);
	  COSP =cos(PLUNGE);
	  SINP = sin(PLUNGE);
	  XYZ(1) = COST*COSP;
	  XYZ(2) = SINT*COSP;
	  XYZ(3) = SINP;
	
	
%注意Snoke的注：以下两矢量的符号与通常约定相反，因为这里的约定是
%面向台站，SV向下为正，SH为向左为正
%	Next two vectors reversed in sign from normal convention because
%	  of my convention for SV and SH (down and left, facing the station)
%遵循Herrmann(1975),面向台站，SV向下为正，SH向右为正，故在此对得到的SH要变号，SV不变号。

	  XYZ(4) = -COST*SINP;
	  XYZ(5) = -SINT*SINP;
	  XYZ(6) = +COSP;
	  XYZ(7) = SINT;
	  XYZ(8) = -COST;
	  XYZ(9) = 0.0;
	  
     RA = 0.0;
	RN = 0.0;
	TA = 0.0;
	TN = 0.0;
	PA = 0.0;
	PN = 0.0;
	for J=1:3
	  RA = RA + XYZ(J)*A(J);
	  RN = RN + XYZ(J)*N(J);
	  TA = TA + XYZ(J+3)*A(J);
	  TN = TN + XYZ(J+3)*N(J);
	  PA = PA + XYZ(J+6)*A(J);
	  PN = PN + XYZ(J+6)*N(J);
    end
	  value =  RA*PN + RN*PA;
      value=-value;     %SH变号 
	if (pflag==2)
        value=sign(value);
    end
    if (pflag==7 || pflag==8)
           sh_value=value;
    end
  end
     if (pflag==3 || pflag==6 || pflag==7 || pflag==8 )   % SV
          
     COST = cos(TREND);
	  SINT = sin(TREND);
	  COSP =cos(PLUNGE);
	  SINP = sin(PLUNGE);
	  XYZ(1) = COST*COSP;
	  XYZ(2) = SINT*COSP;
	  XYZ(3) = SINP;
	
	
%注意Snoke的注：以下两矢量的符号与通常约定相反，因为这里的约定是
%面向台站，SV向下为正，SH为向左为正
%	Next two vectors reversed in sign from normal convention because
%	  of my convention for SV and SH (down and left, facing the station)
%遵循Herrmann(1975),面向台站，SV向下为正，SH向右为正，故在此对得到的SH要变号，SV不变号。
	  XYZ(4) = -COST*SINP;
	  XYZ(5) = -SINT*SINP;
	  XYZ(6) = +COSP;
	  XYZ(7) = SINT;
	  XYZ(8) = -COST;
	  XYZ(9) = 0.0;
	  
     RA = 0.0;
	RN = 0.0;
	TA = 0.0;
	TN = 0.0;
	PA = 0.0;
	PN = 0.0;
	for J=1:3
	  RA = RA + XYZ(J)*A(J);
	  RN = RN + XYZ(J)*N(J);
	  TA = TA + XYZ(J+3)*A(J);
	  TN = TN + XYZ(J+3)*N(J);
	  PA = PA + XYZ(J+6)*A(J);
	  PN = PN + XYZ(J+6)*N(J);
    end
	  value =   RA*TN + RN*TA;
	if (pflag==3)
        value=sign(value);
    end
     if (pflag==7 || pflag==8)
       % 偏振角（°）
        sv_value=value ;
    end
     end
    if (pflag==7 || pflag==8)
       % 偏振角（°）
        value=atan2(sh_value,sv_value)*180/pi ;
    end 
end

