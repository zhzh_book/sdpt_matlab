function foc = foc_GA_fun1(x)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
 %给出的观测P波初动的走向、偏垂角及符号
 
Indxp=find(x(:,3)==1);    %找到初动为正的数据序号
Indxn=find(x(:,3)==-1);   %找到初动为负的数据序号
azi=x(:,1);tak=x(:,2);psign=x(:,3);    %将数据的走向、俯角和初动符号分开
nlength=length(azi);      %得到数据的总个数
ddelt=5;daz=5;dr=5;   %搜索间隔
vect=[cosd(azi).*sind(tak),sind(azi).*sind(tak),cosd(tak)]';    %给出数据在北东下坐标系中的投影向量
chisqmin=1.0e20;      %记录最大平方和
posqmax=0;          %初动为正的总权重的最大值
MDBmin=[];         %记录得到结果的矛盾比
sol=[];
start=[];

%------------------------------------------------------
%定义遗传算法参数
NIND=50;               %个体数目(Numbe of individuals)
MAXGEN=400;            %最大遗传代数(Maximum number of generations)
NVAR=3;               %变量的维数(delt,az,r)
PRECI=20;              %变量的二进制位数(Precision of variables)
GGAP=0.9;              %代沟(Generation gap)
trace=zeros(MAXGEN, 2);
%建立区域描述器(Build field descriptor)

%rep.m：重复一个矩阵，同repmat.m


FieldD=[repmat(PRECI,[1,NVAR]);0.0,0.0,0.0;90.0,360.0,90.0;repmat([1;0;1;1],[1,NVAR])];
  
%参见雷英杰等，2004，第65页
Chrom=ga_crtbp(NIND, NVAR*PRECI);                   %创建初始种群
gen=0;                                               %代计数器
Xini=ga_bs2rv(Chrom, FieldD); % X 为NINDx4的矩阵，函数bs2rv见雷英杰等，2004，第64页
[ObjV]=foc_GA_MDB_fun(Xini,x);
ob=ObjV;
sizeob=size(ob);
% 计算到时残差均方根,为NINDx1的列矢量
while gen<MAXGEN                                     %迭代
    FitnV=ga_ranking(ObjV) ;%分配适应度值(Assign fitness values)
    Chrom;
    FitnV;
    SelCh=ga_select('ga_sus', Chrom, FitnV, GGAP);         %选择
    SelCh=ga_recombin('ga_xovsp', SelCh, 0.7);             %重组
    SelCh=ga_mut(SelCh);                                %变异
    %ObjVSel=objfun1(bs2rv(SelCh, FieldD)); %计算子代目标函数值
    X_GA=ga_bs2rv(SelCh, FieldD);
    
    [ObjVSel]=foc_GA_MDB_fun(X_GA,x); % 计算到时残差均方根
    [Chrom ObjV]=ga_reins(Chrom, SelCh, 1, 1, ObjV, ObjVSel);     %重插入
    gen=gen+1;                                                 %代计数器增加
    trace(gen, 1)=min(ObjV);                                   %遗传算法性能跟踪
    trace(gen, 2)=sum(ObjV)/length(ObjV);
end
%输出最优解及其对应的20个自变量的十进制值,Y为最优解,I为种群的序号
[Y, I]=min(ObjV);
X_GA=ga_bs2rv(Chrom, FieldD);
XI=X_GA(I,:)

[MDBmin,PTchange,sol]=foc_GA_MDB_fun1(X_GA(I,:),x) ;  % 计算最小矛盾符号比


%绘图

siz=50;    %绘制等面积投影的半径
alpha=0:0.1:2*pi+0.1;
plot(siz*cos(alpha),siz*sin(alpha));
rp = siz*sqrt(2)*sin(deg2rad(x(Indxp,2))/2);    %根据（10-5-13）计算Schmidt投影的距中心点距离
xp=rp.*sin(deg2rad(x(Indxp,1))); yp=rp.*cos(deg2rad(x(Indxp,1)));  %得到初动为正的数据在大圆上的Schmidt投影点坐标
rn = siz*sqrt(2)*sin(deg2rad(x(Indxn,2))/2);    %根据（10-5-13）计算Schmidt投影的距中心点距离
xn=rn.*sin(deg2rad(x(Indxn,1))); yn=rn.*cos(deg2rad(x(Indxn,1)));   %得到初动为负的数据在大圆上的Schmidt投影点坐标
hold on
for ii=1:length(PTchange)
  Ttrpl=wan_v2trpl(sol((ii-1)*3+1,:))
  Ptrpl=wan_v2trpl(sol((ii-1)*3+2,:))
  if(PTchange(ii))
      temp=Ttrpl;Ttrpl=Ptrpl; Ptrpl=temp;
  end
  Btrpl=wan_v2trpl(sol((ii-1)*3+3,:));
  
 [str1,dip1,rake1,str2,dip2,rake2,Btrpl,PTangle]=wan_pt2ds(Ptrpl,Ttrpl);



for iflag=1:4
  figure(iflag)
beachball_ea_z(str1,dip1,rake1,siz,48,'r',0,0,1,1,1,iflag)
axis off %不显示坐标轴
hold on
if iflag==1
%画初动符号点
plot(xp,yp,'.');hold on    %绘制初动为正的数据点
plot(xn,yn,'o');hold on  %绘制初动为负的数据点
end
%}
axis equal    %使坐标轴相等，看上去为正圆
end


disp(sprintf('所求震源机制节面1走向%5.1f，倾角%4.1f，滑动角%6.1f\n',str1,dip1,rake1))
disp(sprintf('所求震源机制节面2走向%5.1f，倾角%4.1f，滑动角%6.1f\n',str2,dip2,rake2))
disp(sprintf('所求震源机制P轴走向%5.1f，倾伏角%4.1f\n',Ptrpl))
disp(sprintf('所求震源机制T轴走向%5.1f，倾伏角%4.1f\n',Ttrpl))
disp(sprintf('所求震源机制B轴走向%5.1f，倾伏角%4.1f\n',Btrpl))
disp(sprintf('所求震源机制的矛盾比为%f\n',MDBmin))

 foc(1)=str1;
 foc(2)=dip1;
 foc(3)=rake1;
 foc(4)=str2;
 foc(5)=dip2;
 foc(6)=rake2 ;
 foc(7)=MDBmin;
end

