
%套用相应函数，画震源机制投影球、震源机制解节面投影节线（已知Az、H、R）、初动符号在投影图上的分布
%dnodaz=30
dnodaz=50
%dnodr=40
dnodr=20
%dnodh=20
dnodh=60
cx0=0;   %震源机制投影图圆心x坐标
cy0=0;   %震源机制投影图圆心y坐标
radi=100;  %震源机制投影图圆半径
com='震源机制投影图，下半球等面积投影';
A=pltcircle(cx0,cy0,radi,com);  %画投影图的底图圆
firstmo=getFirstmo('firstm2021');   %调用函数，以读出文件firstmo1中的初动符号数据
fmoNum=length(firstmo);  %符号数
% firstmo = 1xfmoNum 结构数组，其字段为:
%    sign：初动符号（+，c,:向上，c 清晰，+不大清晰；-,d：向下，d清晰，-不大清晰)
%    stn：台站代码
%    phase：震相代码 
%    azi：方位角（度）（自北顺时针量） 
%    toff：离源角（度）（沿垂线自下向上量）
%    dist：震中距
for i=1:1:fmoNum
    [x(i),y(i)]=ab2xy(firstmo(i).azi,firstmo(i).toff,cx0,cy0,radi);
    if (firstmo(i).sign=='+' || firstmo(i).sign=='c') 
        plot(x(i),y(i),'r+');hold on
    elseif (firstmo(i).sign=='-' || firstmo(i).sign=='d') 
        plot(x(i),y(i),'b*');hold on
    end
end
xlabel('+:初动向上，*：初动向下')
[aza,dipa,azb,dipb,azc,dipc,azp,dipp,azt,dipt]=nodac(dnodaz,dnodr,dnodh) %调用函数nodac

[x1,y1]=pltnodline(cx0,cy0,radi,aza,dipa); 
plot(x1,y1,'r-');%调用函数;pltnodeline;
hold on
[x1,y1]=pltnodline(cx0,cy0,radi,azc,dipc);  
plot(x1,y1,'b-')
hold on
[x1,y1]=pltnodline(cx0,cy0,radi,azb,dipb);  
plot(x1,y1,'g-')
[ax,ay]=ab2xy(aza+90-180,dipa,cx0,cy0,radi); %aza为A节面法线的走向
[bx,by]=ab2xy(azb+90-180,dipb,cx0,cy0,radi); %aza为B节面法线的走向
[cx,cy]=ab2xy(azc+90-180,dipc,cx0,cy0,radi); %dipc为节面的倾角
[px,py]=ab2xy(azp+90-180,dipp,cx0,cy0,radi); %aza为A节面法线的走向
[tx,ty]=ab2xy(azt+90-180,dipt,cx0,cy0,radi); %aza为B节面法线的走向
plot(cx,cy,'bd')
text(cx+3,cy,'C')
plot(ax,ay,'rd')
text(ax+3,ay,'A')
plot(bx,by,'gd')
text(bx+3,by,'B')
plot(px,py,'blacko')
text(px+3,py,'P')
plot(tx,ty,'blacko')
text(tx+3,ty,'T')
