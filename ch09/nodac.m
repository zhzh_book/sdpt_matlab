% 根据节面法线的Azi、H、R角得到节面的走向和倾角
function [aza,dipa,azb,dipb,azc,dipc,azp,dipp,azt,dipt]=nodac(dnodaz,dnodr,dnodh)
    rad=pi/180;
    nodh=dnodh*rad;
    nodaz=dnodaz*rad;
    nodr=dnodr*rad;
	ax=-sin(nodh)*cos(nodaz)*cos(nodr)+sin(nodaz)*sin(nodr);
	ay=-sin(nodh)*sin(nodaz)*cos(nodr)-cos(nodaz)*sin(nodr);
	azz=cos(nodh)*cos(nodr);
    bx=cos(nodh)*cos(nodaz);
    by=cos(nodh)*sin(nodaz);
    bzz=sin(nodh);
	cx=-sin(nodh)*cos(nodaz)*sin(nodr)-sin(nodaz)*cos(nodr);
	cy=-sin(nodh)*sin(nodaz)*sin(nodr)+cos(nodaz)*cos(nodr);
	czz=sin(nodr)*cos(nodh);
    px=ax+cx;
    py=ay+cy;
    pzz=azz+czz;
    tx=ax-cx;
    ty=ay-cy;
    tzz=azz-czz;
 % call dipdd ax=%f ay=%f azz=%f\n",ax9,ay9,azz9);
	[phi,delta]=phidelta(ax,ay,azz);
	aza=phi+90;
	if (aza>360) 
        aza=aza-360;
    end
	dipa=90-delta;
 %call dipdd ax=%f ay=%f azz=%f\n",ax9,ay9,azz9);
	[phi,delta]=phidelta(bx,by,bzz);
	azb=phi+90;
	if (azb>360) 
        azb=azb-360;
    end
	dipb=90-delta;
 % call dipdd cx=%f cy=%f czz=%f \n",cx9,cy9,czz9);
	[phi,delta]=phidelta(cx,cy,czz);
	azc=phi+90;
	if (azc>360) 
        azc=azc-360;
    end
	dipc=90-delta;
   %call dipdd ax=%f ay=%f azz=%f\n",ax9,ay9,azz9);
	[phi,delta]=phidelta(px,py,pzz);
	azp=phi+90;
	if (azp>360) 
        azp=azp-360;
    end
	dipp=90-delta;
     %call dipdd ax=%f ay=%f azz=%f\n",ax9,ay9,azz9);
	[phi,delta]=phidelta(tx,ty,tzz);
	azt=phi+90;
	if (azt>360) 
        azt=azt-360;
    end
	dipt=90-delta;