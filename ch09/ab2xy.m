%为画初动符号分布，计算震源机制解投影图上初动符号的X、Y坐标
%给定台站的初动符号、方位角、离源角
%下半球，等面积投影
% 2009-12-06 
%等面积投影的基本公式：如果震源球上的一点到球原点的直线（长度为震源球的半径radius)
%的方位角为alpha，离源角为beta(倾角的余角），则
% 该点在等面积投影图上的投影点到投影圆心的距离 dist=radius*sqrt(2)*sin(beta/2)
% 于是该点的x和y坐标分别为：x=dist*sin(alpha)+centerx
% y=centery+dist*cos(alpha)
%式中radius为等面积投影圆的半径，beta为“离源角”，alpha为方位角。
%centerx和centery分别为圆心的坐标
function [x,y]=ab2xy(alpha,beta,centerx,centery,radius)
sqrt2=sqrt(2);
if beta<0
    beta=beta+180
end
if alpha>360
    alpha=alpha-360
end
rad=alpha*pi/180;  %北为正y方向(alpha从正北向东量）
off=beta*pi/180;
dist=radius*sqrt2*sin(off/2);
x=centerx+dist*sin(rad);
y=centery+dist*cos(rad);
