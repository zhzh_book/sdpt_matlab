clear all;
figure(1)
pt = 0:pi/100:2*pi;
polar(pt,abs(sin(2*pt)),'b');
hold on

figure(2)
st = 0:pi/100:2*pi;
polar(st,abs(cos(2*st)),'b');