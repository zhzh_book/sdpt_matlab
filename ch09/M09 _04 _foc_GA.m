%根据P波走时差求视速度a(sec/degree)，再根据视慢度求射线参数p=R*a,
%再对给定震源深度，计算不同震中距（度）的离源角beta=asin(p*ch/(R-h))
% c15为震源处（深度h=15km）的P波速度（km/sec）
%q取震源深度为15km，iasp91模型c15=5.8km/s,平均地球半径R=6371km（《测震学原理与方法》，p42）
clear all
model='iasp91'
d2km=111.19;
R=6371;
taupTime('iasp91',5,'ScS','deg',20)
h=15
c15=5.8;
vel_num=900
for i=1:vel_num
    tt=taupTime(model,15,'P','deg',i*0.1+1);
    timeP(i)=tt.time;      
    dis(i)=tt.distance;
    dep(i)=tt.srcDepth;
    ray(i)=tt.rayParam;
end
%figure(1)
%plot(dis,timeP,'r.');
%xlabel('震中距/度')
%ylabel('走时/s')
%figure(2)
tt=taupTime(model,15,'P','deg',0.9);
tt10=taupTime(model,15,'P','deg',1);
timeP9=tt.time
timeP10=tt10.time
apar_vp(1)=0.2/(timeP(1)-timeP9);    %10.1-9.9:vp(1)=10.0°的vp
apar_vp(2)=0.2/(timeP(2)-timeP10);    %10.2-10:vp(1)=10.1°的vp
for i=3:vel_num
   apar_vp(i)=0.2/(timeP(i)-timeP(i-2));   
end
%plot(dis(1:vel_num)-0.1,apar_vp(1:vel_num))
%xlabel('震中距/度')
%ylabel('视速度/(°/s)')
fil='apa_vel'
fid=fopen(fil,'w')
fprintf(fid,'%d\n',vel_num)
for i=1:vel_num
    fprintf(fid,'%i   %5.1f   %9.4f \n',i,0.9+i*0.1,apar_vp(i));
end
fclose(fid)
fil='apa_vel1'
fid=fopen(fil,'w')
fprintf(fid,'%5.1f %7.2f \n',10,apar_vp(1));
for i=1:78
    fprintf(fid,'%5.1f %7.2f \n',dis(i*10),apar_vp(i*10));
end
fclose(fid)
for i=1:798
    dis(i)
    if dis(i)==16.1
        av=apar_vp(i);
        break
    end
    
end
ava=av
%figure(3)
for i=1:vel_num
appr(i)=1/(apar_vp(i)*d2km);
p(i)=appr(i)*R;
beta(i)=(180/pi)*asin(p(i)*c15/(R-h));
end
%plot(dis(1:vel_num)-0.1,appr(1:vel_num))
%xlabel('震中距/度')
%ylabel('视慢度/(s/°)')
figure(4)
plot(dis(11:vel_num-10)-0.1,beta(11:vel_num-10))
xlabel('震中距/°')
ylabel('离源角/°')
