function [ value,amp] = foc_syn_fun_z(A,N,TREND,TAKEOF,pflag)
%给定震源机制和下半震源球上射线出头点的方位角和离源角，计算出头点的理论初动符号或辐射振幅
%给定震源机制的走向strike、倾角dip、离源角rake;单位°
%下半震源球上射线出头点的方位角azi和离源角toa;单位°
%pflag：=1，P初动；2，SV初动（沿射线方向为正）；3，SH初动（沿射线方向，向右为正）
%4，P振幅；5，SV振幅；6，SH振幅。正负号与初动符号一致。
%   返回值：初动用+/-1表示，振幅用其值表示
%增（2017-04-11）flag=7 画S波振幅、8画偏振角
	amp=0;
	    if (pflag==1 || pflag==4)   % P
          
      COST = cos(TREND);
	  SINT = sin(TREND);
	 % COSP =cos(PLUNGE);  %= SINA=sin(TAKEOF);
     SINA=sin(TAKEOF);
	 % SINP = sin(PLUNGE); %=COSA=cos(TAKEOF);
     COSA=cos(TAKEOF);
     %以下为台站射线在震源球上出头点的R矢量方向余弦
	  XYZ(1) = COST*SINA;
	  XYZ(2) = SINT*SINA;
	  XYZ(3) = COSA;
        RA = 0.0;
	RN = 0.0;
	for J=1:3
	  RA = RA + XYZ(J)*A(J);
	  RN = RN + XYZ(J)*N(J);
    end
	  value = 2*RA*RN;      %P波辐射花样，第9章式（9.9）
       if  (pflag==1)
        value=sign(value);
    end
 
    end

  if (pflag==2 || pflag==5  || pflag==7 || pflag==8)   % SH
          
     COST = cos(TREND);
	  SINT = sin(TREND);
		 % COSP =cos(PLUNGE);
     SINA=sin(TAKEOF);
	 % SINP = sin(PLUNGE);
     COSA=cos(TAKEOF);
	 XYZ(1) = COST*SINA;
	  XYZ(2) = SINT*SINA;
	  XYZ(3) = COSA;
	
	
%注意Snoke的注（见FOCMEC源程序focmecL.f中的子程序FOCINP）：以下两矢量的符号与通常约定相反，因为这里的约定是
%面向台站，SV向下为正，SH为向左为正
%	Next two vectors reversed in sign from normal convention because
%	  of my convention for SV and SH (down and left, facing the station)
%遵循Herrmann(1975),面向台站，SV向下为正，SH向右为正，故在此对得到的SH要变号，SV不变号。
%{
%Snoke，SH向左为正：
	  XYZ(4) = -COST*SINP;
	  XYZ(5) = -SINT*SINP;
	  XYZ(6) = +COSP;
% Snoke, SV向下为正：
	  XYZ(7) = SINT;     
	  XYZ(8) = -COST;
	  XYZ(9) = 0.0;
	 %}
   %按安艺敬一和理查兹（1986）第一卷124页式（4.83）
  % 第9章式（9.2）
  %SH的正方向是方位角增加的方向（按安艺和理查兹（1986））
	  XYZ(7) = -SINT;
	  XYZ(8) = COST;
	  XYZ(9) = 0.0;
   
     RA = 0.0;
	RN = 0.0;
	
	PA = 0.0;
	PN = 0.0;
	for J=1:3
	  RA = RA + XYZ(J)*A(J);
	  RN = RN + XYZ(J)*N(J);
	  
	  PA = PA + XYZ(J+6)*A(J);
	  PN = PN + XYZ(J+6)*N(J);
    end
	  value =  RA*PN + RN*PA;
     % value=-value;     %SH变号 ，按安艺和理查兹（1986），不须变号
	if (pflag==2)
        value=sign(value);
    end
    if (pflag==7 || pflag==8)
           sh_value=value;
    end
  end
     if (pflag==3 || pflag==6 || pflag==7 || pflag==8 )   % SV
          
     COST = cos(TREND);
	  SINT = sin(TREND);
		 % COSP =cos(PLUNGE);
     SINA=sin(TAKEOF);
	 % SINP = sin(PLUNGE);
     COSA=cos(TAKEOF);
	 XYZ(1) = COST*SINA;
	  XYZ(2) = SINT*SINA;
	  XYZ(3) = COSA;
	
%注意Snoke的注：以下两矢量的符号与通常约定相反，因为这里的约定是
%面向台站，SV向下为正，SH为向左为正
%	Next two vectors reversed in sign from normal convention because
%	  of my convention for SV and SH (down and left, facing the station)
%遵循Herrmann(1975),面向台站，SV向下为正，SH向右为正，故在此对得到的SH要变号，SV不变号。
	 %{
 XYZ(4) = -COST*SINP;
	  XYZ(5) = -SINT*SINP;
	  XYZ(6) = +COSP;
	  XYZ(7) = SINT;
	  XYZ(8) = -COST;
	  XYZ(9) = 0.0;
%}
% SV的正方向是离源角增加的方向（按安艺和理查兹（1986））
	 XYZ(4) = COST*COSA;
	  XYZ(5) = SINT*COSA;
	  XYZ(6) = -SINA;
	  
     RA = 0.0;
	RN = 0.0;
	TA = 0.0;
	TN = 0.0;
	
	for J=1:3
	  RA = RA + XYZ(J)*A(J);
	  RN = RN + XYZ(J)*N(J);
	  TA = TA + XYZ(J+3)*A(J);
	  TN = TN + XYZ(J+3)*N(J);
	 
    end
	  value =   RA*TN + RN*TA;
	if (pflag==3)
        value=sign(value);
    end
     if (pflag==7 || pflag==8)
       % 偏振角（°）
        sv_value=value ;
    end
     end
    if (pflag==7 || pflag==8)
    % if (pflag==7)                                         %S波振幅
          amp=sqrt(sh_value^2+sv_value^2);
      %end
     %    if (pflag==8)
       % 偏振角（°）
        value=atan2(sh_value,sv_value)*180/pi ;
    %end 
    end
end

