function c = zcross_fun( iflag,xs0,ys0,nn,col )
%在节线上画十字
if iflag==1 || iflag==2 || iflag==3
 plot(xs0(nn/8+1),ys0(nn/8+1),col);hold on
plot(xs0(nn/4+1),ys0(nn/4+1),col);hold on
plot(xs0(nn*3/8+1),ys0(nn*3/8+1),col);hold on
end
c=1
end

