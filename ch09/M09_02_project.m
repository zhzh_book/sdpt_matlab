%比较3种投影
clear oll
R=1;

for i=1:90
    beta_d(i)=i;
    beta=i*pi/180;
    r_s(i)=R*tan(beta/2);    %乌尔夫网
    r_l(i)=sqrt(2)*R*sin(beta/2);    %兰勃特-斯密特网（等面积投影）
    r_o(i)=R*sin(beta);   %正交投影
end
figure(1)
plot(beta_d,r_s,beta_d,r_l,beta_d,r_o);hold on
legend('乌尔夫网投影','斯密特网投影','正交投影')
xlabel('离源角/°')
ylabel('r/R')
title('不同投影方法给出的投影点到圆心距离r与圆半径R的比值')
