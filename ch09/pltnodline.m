%画震源机制解的节面图
%给定节面的走向strike和倾向dip
%下半球，等面积投影
%  赵仲和 2009-12-06 
%等面积投影的基本公式：如果震源球上的一点到球队原点的直线（长度为震源球的半径radius)
%的方位角为alpha，离源角为beta(倾角的余角），则
% 该点在等面积投影图上的投影点到投影圆心的距离 dist=radius*sqrt(2)*sin(beta/2)
% 于是该点的x和y坐标分别为：x=dist*sin(alpha)+centerx
% y=centery+dist*cos(alpha)
%式中radius为等面积投影圆的半径，beta为“离源角”，alpha为方位角。
%centerx和centery分别为圆心的坐标
function [x1,y1]=pltnodline(centerx,centery,radi,strike,dip)
sqrt2=sqrt(2);
for i=1:1:181
    rad=(i-1+strike)*pi/180;  %北为正y方向(strike从正北向东量）
    off=atan(tan((90-dip)*pi/180)/cos((i-1-90)*pi/180)); %计算
    offd=off*180/pi; 
    dist=radi*sqrt2*sin(off/2);
    x1(i)=centerx+dist*sin(rad);
    y1(i)=centery+dist*cos(rad);
end
%plot(x1,y1,'r-')
%hold on
a=1;
