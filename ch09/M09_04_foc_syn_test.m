clear all
%越西地震（魏娅玲，2016）
striked=355;
dipd=81;
raked=29;
%画断层面解投影图（等面积投影）
a=20;%绘制的Schmidt网的大圆半径
siz=a;
n=500;%用500个点绘制断层面在等面积投影网上的投影
x0=0;y0=0;  %坐标原点
RD=pi/180;
STRIKE=striked*RD;
DIP=dipd*RD;
RAKE=raked*RD;
vpvs=sqrt(3);
VPVS3=vpvs^3;
%求滑动矢量A和断层面法向矢量N的方向余弦
%安艺敬一和理查兹《定量地震学》（1986）第一卷第125页式（4.83）
%中国地震局监测预报司，2003，《地震参数-数字地震学在地震预测中的应用》，第31页式（3.10）（3.11）（郑斯华）
%FOCMEC(Snoke，2009),源程序focmekL.f中包含的子程序LRATIO中计算，该子程序计算P、S振幅比的对数
A(1) = cos(RAKE)* cos(STRIKE) + sin(RAKE)* cos(DIP)*sin(STRIKE);
A(2) =  cos(RAKE)*sin(STRIKE) - sin(RAKE)* cos(DIP)* cos(STRIKE);
A(3) = -sin(RAKE)*sin(DIP);
N(1) = -sin(STRIKE)*sin(DIP);
N(2) =  cos(STRIKE)*sin(DIP);
N(3) = - cos(DIP);
i=0;
azi_step0=10;
toa_step=10;
ntoa=floor(90/toa_step);
vmax=0;
for pflag=4:6
    for k=1:ntoa+1
        toad=(k-1)*toa_step;
        if toad~=0
            azi_step=60*azi_step0/toad;
        else
            azi_step=360;
        end
        naz=floor(360/azi_step);   
        for j=1:naz
            azid=j*azi_step;
            TREND = azid*RD;       %方位角
            PLUNGE = (90.0 - toad)*RD;     %俯角
            TAKEOF=toad*RD;       %离源角
            [value,amp] = foc_syn_fun_z(A,N,TREND,TAKEOF,pflag);
            
            if vmax<abs(value)
                vmax=abs(value);
            end
        end
    end
end
% -------------------------------------------------------------------------
i=0;
for pflag=1:8
    figure(pflag)  
    %以上得到value绝对最大值，彩色显示振幅用
    for k=1:ntoa+1 
        toad=(k-1)*toa_step;
        if toad~=0
            azi_step=60*azi_step0/toad;
        else
            azi_step=359;
        end
        naz=floor(360/azi_step);   
        for j=1:naz
            azid=j*azi_step;
            TREND = azid*RD;
            PLUNGE = (90.0 - toad)*RD;
            TAKEOF=toad*RD;
            [value,amp] = foc_syn_fun_z(A,N,TREND,TAKEOF,pflag);
            rp = siz*sqrt(2)*sin(TAKEOF /2);    %根据（10-5-16）计算Schmidt投影的距中心点距离
            xp=rp*sin(TREND); yp=rp*cos(TREND);  %得到初动为正的数据在大圆上的Schmidt投影点坐标
            if (pflag<4)
            %画射线出头点的P\SV\SH波初动符号
                if  value==1
                    plot(xp,yp,'.');hold on    %绘制初动为正的数据点
                else
                    plot(xp,yp,'o') ;hold on %绘制初动为负的数据点
                end
            end
            if (pflag>3 & pflag<7)
            %画射线出头点的P\SV\SH波振幅 
                if value>0
                    pcolor=[value/vmax,1-value/vmax,0];
                    p=plot(xp,yp,'.');hold on    %绘制初动为正的数据点    
                    set(p,'Color',pcolor)
                else
                    pcolor=[-value/vmax,1+value/vmax,0]   
                    p=plot(xp,yp,'o');hold on    %绘制初动为负的数据点    
                    set(p,'Color',pcolor)      
                end
            end
            if pflag==7
                value1=amp;    %画S波振幅
                pcolor=[value1,1-value1,0];
                p=plot(xp,yp,'.');hold on    %绘制偏振角为正的数据点    

                set(p,'Color',pcolor)
            end
            if pflag==8
            %画偏振角
            %右边点
            value11=value
                if  value>0
                    epsilon=value;   %偏振角    
                else
                    epsilon=-value;   
                end
                aa=TAKEOF;
                bb=0.01*siz*amp;
                CC=(180-epsilon)*pi/180;
                AplusB=2*atan2(cot(CC/2)*cos(0.5*(aa-bb)),cos(0.5*(aa+bb)));
                AminusB=2*atan2(cot(CC/2)*sin(0.5*(aa-bb)),sin(0.5*(aa+bb)));
                AA=0.5*(AplusB+AminusB);
                BB=AplusB-AA;
                cc=2*atan2(tan(0.5*(aa+bb))*cos(0.5*AplusB),cos(0.5*AminusB));
                TAKEOF11=cc;
                if value>0
                    TREND11=TREND+BB;
                else
                    TREND11=TREND-BB; 
                end
                rp11 = siz*sqrt(2)*sin(TAKEOF11 /2);    %根据（10-5-16）计算Schmidt投影的距中心点距离
                xp11=rp11*sin(TREND11); yp11=rp11*cos(TREND11);  %得到初动为正的数据在大圆上的Schmidt投影点坐标 

                rp00 = siz*sqrt(2)*sin(TAKEOF /2);    %根据（10-5-16）计算Schmidt投影的距中心点距离
                xp00=rp00*sin(TREND); yp00=rp00*cos(TREND);  
                x11=[xp00 xp11];
                x12=[yp00 yp11];
                plot(xp00,yp00,'g.')
                line(x11,x12);hold on
            end
        end
    end
    
    beachball_ea(striked,dipd,raked,a,n,'r',0,0,1,1,0);   %调用绘图程序,最末参数fillcode=0，不填充
    hold on
    if pflag==1
        title('P波初动符号')
    end
    if pflag==2
       title('SH波初动符号')
    end
    if pflag==3
      title('SV波初动符号')
    end
    if pflag==4
        title('P波位移振幅')
    end
    if pflag==5
      title('SH波位移振幅')
    end
    if pflag==6
       title('SV波位移振幅')
    end
    if pflag==7
        title('S波振幅')
    end
    if pflag==8
        title('S波偏振矢量')
    end
    axis equal  
end
%色标显示
figure(10)
xp=[-10:10]*0.1;
value0=[-10:10];
yp=[1:21]*0;
vmax=10;
for i=1:21
    value=value0(i);
 if value>0
 pcolor=[value/vmax,1-value/vmax,0];
p=plot(xp(i),yp(i),'.');hold on    %绘制初动为正的数据点    
set(p,'Color',pcolor)
      else
   pcolor=[-value/vmax,1+value/vmax,0]
   
p=plot(xp(i),yp(i),'o');hold on    %绘制初动为负的数据点    
set(p,'Color',pcolor)      
 end
end