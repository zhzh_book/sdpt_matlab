function [ MDBmin,PTchange,sol] = foc_GA_MDB_fun1( X_GA,x )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
%for delt=0:ddelt:90
%给出的观测P波初动的走向、偏垂角及符号
 
Indxp=find(x(:,3)==1);    %找到初动为正的数据序号
Indxn=find(x(:,3)==-1);   %找到初动为负的数据序号
azi=x(:,1);tak=x(:,2);psign=x(:,3);    %将数据的走向、俯角和初动符号分开
nlength=length(azi);      %得到数据的总个数
ddelt=5;daz=5;dr=5;   %搜索间隔
%ddelt=2;daz=2;dr=2;   %搜索间隔
vect=[cosd(azi).*sind(tak),sind(azi).*sind(tak),cosd(tak)]';    %给出数据在北东下坐标系中的投影向量
chisqmin=1.0e20;      %记录最大平方和
posqmax=0;          %初动为正的总权重的最大值
MDBmin=[];         %记录得到结果的矛盾比
sol=[];
start=[];
Xsize=length(X_GA)

    delt=X_GA(1) ;   %显示搜索的进程
    az=X_GA(2);
    r=X_GA(3);
    csdelt=cosd(delt);sndelt=sind(delt);
   % for az=0:daz:360
        csaz=cosd(az);  snaz=sind(az);
       % for r=0:dr:90
            csR=cosd(r);  snR=sind(r);
             T=[csaz*csR-snaz*csdelt*snR,snaz*csR+csaz*csdelt*snR,sndelt*snR];   %张轴在北东下坐标系中的表示
             P=[-csaz*snR-snaz*csdelt*csR,-snaz*snR+csaz*csdelt*csR,sndelt*csR];  %压轴在北东下坐标系中的表示
            B=cross(T,P);                                  %中间轴在北东下坐标系中的表示
            RTPB=[T;P;B];                 %转换矩阵
            PTPB=RTPB*vect;                              %给出地震射线在TPB轴下的表示
            trpl=[];
            for ii=1:nlength                                  %求出与T轴夹角fai和与B轴的夹角theta
               trpl=[trpl;wan_v2trpl([PTPB(:,ii)]')];    %trpl的第一个数为该向量和T轴的夹角，第二个数为该向量与TP面的夹角                   
            end
            weit=cosd(2*trpl(:,1)).*sind(90-trpl(:,2));            
            mul=weit.*psign;
            Indxfit=find(mul>0);    %符合初动符号的序号
            Indxunfit=find(mul<0);    %符合初动符号的序号
            posq=sum(weit(Indxfit));    %符合初动符号的权重
           chisq=sum(4*weit(Indxunfit).^2);   %不符合初动符号的权重的平方和
           MDB=length((Indxunfit))/(length(Indxunfit)+length(Indxfit));   %矛盾比
           N=sum(abs(weit));
           if(chisq==chisqmin)
               if(posq==posqmax)
                   PTchange=[PTchange;0];
                  start=[start;az,delt,r];
                  sol=[sol;T;P;B];
                  MDBmin=[MDBmin,MDB];
               else
                   posqmax=posq;
               end
           elseif(chisq<chisqmin)
                  PTchange=[0]; 
                  start=[az,delt,r];
                  sol=[T;P;B];
                  chisqmin=chisq;
                  posqmax=posq;
                  MDBmin=[MDB];
            end
           %交换P,T轴的位置就覆盖了另外的一半空间，将计算的权重符号完全相反，求这种情况下的拟合情况
            weit=-weit;   %将权重符号反号，相当于原来的正号区变为负号区，原来的负号区变为正号区,虽然符号改变，但权重的绝对值不改变
            mul=weit.*psign;
            Indxfit=find(mul>0);
            Indxunfit=find(mul<0);
            MDB=length(Indxunfit)/(length(Indxunfit)+length(Indxfit));    
           chisq=sum(4*weit(Indxunfit).^2);
           N=sum(abs(weit));
           if(chisq==chisqmin)
               if(posq==posqmax)
                   PTchange=[PTchange;1];
                  start=[start;az,delt,r];
                  sol=[sol;T;P;B];
                  MDBmin=[MDBmin,MDB];
               else
                   posqmax=posq;
               end
           elseif(chisq<chisqmin)
                  PTchange=[1]; 
                  start=[az,delt,r];
                  sol=[T;P;B];
                  chisqmin=chisq;
                  posqmax=posq;
                  MDBmin=[MDB];
            end
          
end
           %         end
  %  end
%end

