function [str1,dip1,rake1,str2,dip2,rake2,Btrpl,PTangle]=wan_pt2ds(Ptrpl,Ttrpl)
% 采用震源机制的PT轴的走向和倾伏角求解震源机制的其他表示形式
%输入：Ptrpl为P轴的走向和倾伏角，Ttrpl为T轴的走向和倾伏角，单位均为度
%输出：str1,dip1,rake1为震源机制第一个节面的走向、倾角和滑动角；
%str2,dip2,rake2为震源机制第一个节面的走向、倾角和滑动角；
%Btrpl为B轴的走向和倾伏角；
%PTangle为P,T轴之间的夹角，应该接近90度,如果不接近90度,程序会给出警告
%所有输出变量的单位均为度
SR2=0.707107;    %2开平方
Ptrpl=deg2rad(Ptrpl);Ttrpl=deg2rad(Ttrpl);  %将角度转化成弧度值
P=[cos(Ptrpl(1))*cos(Ptrpl(2)) sin(Ptrpl(1))*cos(Ptrpl(2)) sin(Ptrpl(2))];  %P轴单位方向矢量，根据（10-3-1）式给出
T=[cos(Ttrpl(1))*cos(Ttrpl(2)) sin(Ttrpl(1))*cos(Ttrpl(2)) sin(Ttrpl(2))];  %T轴单位方向矢量，根据（10-3-1）式给出
PTangle=rad2deg(acos(dot(P,T))); %p与T进行点乘，因为模数为1，得到P轴与T轴夹角余弦值，再进行反余弦
if(abs(PTangle-90)>10)  
   warning('两个节面不垂直')         %判断两个节面是否垂直
end
B=cross(T,P);  %T轴与P轴进行叉乘，得到B轴单位方向矢量
A=SR2*(T+P)    %根据（10-3-11）第一式得到滑动方向
N=SR2*(T-P) %根据（10-3-11）第一式得到断层面法向
Btrpl=wan_v2trpl(B);  %求出B轴走向和倾伏角
[str1,dip1,rake1]=wan_an2dsr_wan(A,N);   %得到第一个节面的走向、倾角和滑动角
[str2,dip2,rake2]=wan_an2dsr_wan(N,A);   %得到第一个节面断层的走向、倾角和滑动角
return
