%画震源机制解的投影底图
%下半球，等面积投影
function a=pltcircle(centerx,centery,radius,comment)
for i=1:1:360
    rad=i*pi/180;
    x(i)=centerx+radius*sin(rad);
    y(i)=centery+radius*cos(rad);
end
plot(x,y,'b-')
hold on
plot(centerx,centery,'y+')
title({comment})
hold on
a=1;