%遗传算法估计震源机制解
%为求震源机制解准备数据
%读入台站坐标（hyposat-sta）
%ZJG   314733.0N1044022.4E  612.0SC
clear all

%计时
c=clock;
year=num2str(c(1),4);
manth=num2str(c(2),2);
day=num2str(c(3),2);
hour=num2str(c(4),2);
minute=num2str(c(5),2);
second=num2str(c(6),3);
str=['MTcc矩张量反演，起始时间  ',year,'-',manth,'-',day,' ',hour,':',minute,':',second];
sos_dep=10   %震源深度10km
sta=struct('name',{},'lat',{},'lon',{},'ele',{},'net',{});
fid=fopen('SC_hyposat_sta','r');   %台站坐标文件
for i=1:79
    sta(i).name=fscanf(fid,'%s/');
    sta(i).lat=fscanf(fid,'%i/')+fscanf(fid,'%i/')/60+fscanf(fid,'%f/')/3600;  %变成°
    tem1=fscanf(fid,'%s/');
    sta(i).lon=fscanf(fid,'%i/')+fscanf(fid,'%i/')/60+fscanf(fid,'%f/')/3600 ;  %变成°
    tem2=fscanf(fid,'%s/');
    sta(i).ele=fscanf(fid,'%f/')/1000;   %变成km
    sta(i).net=fscanf(fid,'%s/');
end
   fclose(fid);
   sta_num=79
   for i=1:sta_num
staname=sta(i).name
stalat=sta(i).lat
stalon=sta(i).lon
staele=sta(i).ele
stanet=sta(i).net
   end
% CA.XC16.00.BHZ   0.15 321   IPg R 2014-10-01 09:23:33.050  0.23 1.0
phase=struct('net',{},'stname',{},'comp',{},'distD',{},'aziD',{},'pha',{},'fm',{},'year',{},...
    'mon',{},'day',{},'hour',{},'minute',{},'sec',{},'res',{},'wt',{},'toaD',{})
fmo=struct('stname',{},'pha',{},'sign',{},'wt',{},'aziD',{},'toaD',{},'aziD1',{},'plunge',{})
fi=fopen('SC_yx_phase.txt','r');   %震相数据
phase_num=101
for i=1:phase_num
    tem1=fscanf(fi,'%s/');
    %将原来的.换成空格
    tem2=strrep(tem1,'.',' ');
    %利用regexp实现字符串分离，这里以空格为分隔符
    S = regexp(tem2,'\s+', 'split')   ;
    phase(i).net=S(1);
    phase(i).stname=S(2);
    phase(i).comp=S(4);
    phase(i).distD=fscanf(fi,'%f/');
    phase(i).aziD=fscanf(fi,'%f/');
    phase(i).pha=fscanf(fi,'%s/');
    phase(i).fm=fscanf(fi,'%s/');
    tem1=fscanf(fi,'%s/');
    tem2=strrep(tem1,'-',' ');
    S = regexp(tem2,'\s+', 'split');
    phase(i).year=str2double(S(1));
    phase(i).mon=str2double(S(2));
    phase(i).day=str2double(S(3));
    tem1=fscanf(fi,'%s/');
    tem2=strrep(tem1,':',' ');
    S = regexp(tem2,'\s+', 'split');
    phase(i).hour=str2double(S(1));
    phase(i).minute=str2double(S(2));
    phase(i).sec=str2double(S(3));
    phase(i).res=fscanf(fi,'%f/');
    phase(i).wt=fscanf(fi,'%f/');
end    
fclose(fi);
for i=1:phase_num
    fmot=phase(i).fm
end
%读入速度模型SC_CRH_m（msdp6.2中提供的四川分层速度模型SC_CRH），双层地壳模型
crust=struct('top',{},'Pvel',{},'Svel',{})
layer_num=4
fic=fopen('SC_CRH_m')
layer_num=4
for i=1:layer_num
    crust(i).top=fscanf(fic,'%f/');
    crust(i).Pvel=fscanf(fic,'%f/');
    crust(i).Svel=fscanf(fic,'%f/');
end
fclose(fic);
for i=1:layer_num
    topdep= crust(i).top
    p=crust(i).Pvel
    s=crust(i).Svel
end
depth=sos_dep

%计算离源角toaD
%已知震中距phase(i).distD，震源深度sos.dep，速度模型SC_CRH
%震源在第1层
j=0
for i=1:phase_num
    c=strcmp(phase(i).pha,'IPg'  )
      c1=strcmp(phase(i).pha,'IPn'  )
        if (c==1 || c1==1)
          if (phase(i).fm=='U')   
            j=j+1;
            fmo(j).sign='+';
            fmo(j).wt=+1;
            % fmo(j).wt=-1;
             fmo(j).stname=phase(i).stname;
               fmo(j).stname=phase(i).stname;
                 fmo(j).pha=phase(i).pha;
        end
        if(phase(i).fm=='R')
           j=j+1;
            fmo(j).sign='-';
          fmo(j).wt=-1;
             %fmo(j).wt=+1;
             fmo(j).stname=phase(i).stname;
              fmo(j).pha=phase(i).pha;
        end  
        if c==1
            fmo(j).toaD=180-atan2(phase(i).distD*111.19,sos_dep)*180/pi
            fmo(j).plungeD=fmo(j).toaD-90
            fmo(j).aziD1=phase(i).aziD-180
            if  fmo(j).aziD1<0
                fmo(j).aziD1=fmo(j).aziD1+360
            end
        end
        if c1==1
            sini1=(crust(2).Pvel/crust(3).Pvel);
            fmo(j).toaD=asin(sini1*crust(1).Pvel/crust(2).Pvel)*180/pi
              fmo(j).aziD1=phase(i).aziD
               fmo(j).plungeD=90-fmo(j).toaD
        end
end     
     c=strcmp(phase(i).pha,'Pg'  )
      c1=strcmp(phase(i).pha,'Pn'  )
        if (c==1 || c1==1)
        if (phase(i).fm=='U')
            j=j+1;
            fmo(j).sign='+';
           % fmo(j).wt=+0.5;
            fmo(j).wt=1.0
            %fmo(j).wt=-1;
            fmo(j).stname=phase(i).stname;
             fmo(j).pha=phase(i).pha;
             
        end
        if(phase(i).fm=='R')
           j=j+1;
            fmo(j).sign='-';
            %fmo(j).wt=-0.5;
            fmo(j).wt=-1.0;
            %fmo(j).wt=+1.0;
             fmo(j).stname=phase(i).stname;
              fmo(j).pha=phase(i).pha;
        end   
        if c==1
            fmo(j).toaD=180-atan2(phase(i).distD*111.19,sos_dep)*180/pi
            fmo(j).plungeD=fmo(j).toaD-90
           fmo(j).aziD1=phase(i).aziD-180
            if  fmo(j).aziD1<0
                fmo(j).aziD1=fmo(j).aziD1+360
            end
        end
        if c1==1
            sini1=(crust(2).Pvel/crust(3).Pvel);
            fmo(j).toaD=asin(sini1*crust(1).Pvel/crust(2).Pvel)*180/pi
               fmo(j).aziD1=phase(i).aziD
               fmo(j).plungeD=90-fmo(j).toaD
        end
     end     
end
fmo_num=j
for i=1:fmo_num
    ii=i
    fmost=fmo(i).stname
    fmosign=fmo(i).sign
    fmowt=fmo(i).wt
     fmopha=fmo(i).pha
     fmotoaD= fmo(i).toaD
     fmoaziD1= fmo(j).aziD1
     fmoplungeD= fmo(j).plungeD
end
fmo_num
for i=1:fmo_num
    x(i,1)=fmo(i).aziD1;
    x(i,2)=90-fmo(i).plungeD;
    x(i,3)=fmo(i).wt;
end
xx=x
GA_num=1
boot_num=1
foc= foc_GA_fun1(x) ;

 
 
c1=clock;
year1=num2str(c1(1),4);
manth1=num2str(c1(2),2);
day1=num2str(c1(3),2);
hour1=num2str(c1(4),2);
minute1=num2str(c1(5),2);
second1=num2str(c1(6),3);
str=['估计震源机制解，起始时间  ',year,'-',manth,'-',day,' ',hour,':',minute,':',second];
disp(str)
str1=['估计震源机制解，结束时间  ',year1,'-',manth1,'-',day1,' ',hour1,':',minute1,':',second1];
disp(str1)