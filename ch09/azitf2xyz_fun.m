function [ XYZ ] = azitf2xyz_fun( AZIN,PTOANG,STOANG )
%转自Snoke的FOCMEC子程序FOCINP
%   
RD=pi/180;
TREND = AZIN/RD;
	  PLUNGE = (90.0 - PTOANG)/RD;
	  COST = cos(TREND);
	  SINT = sin(TREND);
	  COSP =cos(PLUNGE);
	  SINP = sin(PLUNGE);
	  XYZ(1) = COST*COSP;
	  XYZ(2) = SINT*COSP;
	  XYZ(3) = SINP;
	  SPLUNG = (90.0 - STOANG)/RD;
	  SINP = sin(SPLUNG);
	  COSP = cos(SPLUNG);
%注意Snoke的注：以下两矢量的符号与通常约定相反，因为这里的约定是
%	Next two vectors reversed in sign from normal convention because
%	  of my convention for SV and SH (down and left, facing the station)

	  XYZ(4) = -COST*SINP;
	  XYZ(5) = -SINT*SINP;
	  XYZ(6) = +COSP;
	  XYZ(7) = SINT;
	  XYZ(8) = -COST;
	  XYZ(9) = 0.0;


end

