% 
%--calcutate azimuth and dip angle from direction cosines
function [phi,delta]=phidelta(xx,yy,zz)
	if(zz>0.)
	xx=-xx;
	yy=-yy;
	zz=-zz;
    end
	if(abs(xx)<0.0001 && abs(yy)<0.0001) 
     delta=90.;
	 phi=0.;  
    else 
	phi=atan2(yy,-xx)*180/pi;
	delta=atan2(abs(zz),sqrt(xx*xx+yy*yy))*180/pi;
    end
    if(phi<0.) 
        phi=phi+360.;
    end
