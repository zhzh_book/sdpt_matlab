%读出初动符号数据
%数据文件格式:初动符号(1:1) 台站名(3:5) 震相名(6:7) 方位角(度) 离源角(度)  震中距
% 例: + aa  p 40 20  100.0 
function firstmotion=getFirstmo(fn)
    fmo = struct('sign',{},'stn',{},'phase',{},'azi',{},'toff',{},'dist',{});
    firstmotion = struct('sign',{},'stn',{},'phase',{},'azi',{},'toff',{},'dist',{});
    fmoInfo = importdata(fn);
    fmoNum = length(fmoInfo.rowheaders);
    for i=1:fmoNum
        fmoText = char(fmoInfo.textdata(i));
        sign = fmoText(1:1);
        stn = fmoText(3:5);
        phase=fmoText(6:7);
        azi = fmoInfo.data(i,1);
        toff = fmoInfo.data(i,2);
        dist = fmoInfo.data(i,3) ;
        fmo(i) = struct('sign',sign, ...
          'stn',stn, ...
          'phase',phase, ...
          'azi',azi, ...
          'toff',toff, ...   
          'dist',dist);
    end
    firstmotion = fmo;