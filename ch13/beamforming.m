%聚束
disp('聚束')
figure(7)
t1start=1.0 %sec
twindow=1.0 %sec
sx1=-0.4 %s/km
sx2=0.4  %s/km
sxpoint=101
sy1=-0.4
sy2=0.4
sypoint=101
flag=0  %能量判据
%flag=1  %振幅判据
[beamwave,bm,bmdb,sx,sy] = beam_func( array,testwave,num_cha,t1start,twindow,sample_rate,sx1,sx2,sxpoint,sy1,sy2,sypoint,flag);
dsx=(sx2-sx1)/(sxpoint-1) 
dsy=(sy2-sy1)/(sypoint-1)
for i=1:sxpoint
      sxx(i)=sx1+dsx*i;
end
for i=1:sypoint
      syy(i)=sy1+dsy*i;
end
contourf(sxx,syy,bmdb');
slowness=sqrt(sx^2+sy^2)
vapparent=1/slowness
azi=atan2(sx,sy)*180/pi
%contourf(kxx/fmid,kyy/fmid,fkdb)
%beamforming
clear all
title({'福州台阵2007110122，聚束（能量判据），t1=0.8s,tl=2.56s','方位角',azi,'度，视慢度 ',slowness,'s/km'})
%title('福州台阵2007110122合成波形，宽带F-K，4-6Hz，t1=0.0s,tl=6s')
xlabel('slowness/(s/km)')
ylabel('slowness/(s/km)')
colorbar
figure(8)
plot(testwave(1,:)); hold on
plot(testwave(2,:)+10000); hold on
plot(testwave(3,:)+10000*2); hold on
plot(testwave(4,:)+10000*3); hold on
plot(testwave(5,:)+10000*4); hold on
plot(testwave(6,:)+10000*5); hold on
plot(testwave(7,:)+10000*6); hold on
plot(testwave(8,:)+10000*7); hold on
plot(testwave(9,:)+10000*8); hold on
plot(testwave(10,:)+10000*9); hold on
plot(testwave(11,:)+10000*10); hold on
plot(testwave(12,:)+10000*11); hold on
plot(testwave(13,:)+10000*12); hold on
plot(testwave(14,:)+10000*13); hold on
plot(testwave(15,:)+10000*14); hold on
plot(testwave(16,:)+10000*15);hold on
plot(beamwave+10000*16); hold on
xlabel('采样点序号（50s/s)')
title('福州台阵2007110122合成波形聚束'); hold off
figure(9)


