% RESPONSE.m
% 计算台阵响应
% 赵仲和据张岭编写的.cpp程序修改  2007-11-17
% 台站文件：
%   array_fuqing.lat：福清台阵子台纬度（度.度），第一个数字是子台总数
%   array_fuqing.lon：福清台阵子台经度（度.度），第一个数字是子台总数
%   array_fuqing.x：福清台阵子台x坐标（东，km），第一个数字是子台总数
%   array_fuqing.y：福清台阵子台y坐标（北.km），第一个数字是子台总数
%   array_fuqing.z：福清台阵子台高程（上，km），第一个数字是子台总数
%   array_fuqing.lam：福清台阵子台纬度（度 分.分），第一个数字是子台总数
%   array_fuqing.lom：福清台阵子台经度（度 分.分），第一个数字是子台总数  
clear all
%//float ab[50][50],a0;
double ab[201][201];
double kxx[201];
double kyy[201];
%int stnum; /* number of stations in the network */
%int stlatd[102];   /* degree part of station latitude  */
%int stlond[102];   /* degree part of station longitude */

%char stcode[102][4]; /* station code                     */
%char stname[102][9]; /* station name                     */

%float stlatm[102]; /* minute part of station latitude  */
%float stlonm[102]; /* minute part of station longitude */
%float stelekm[102];/* station elevation in km          */
%float stlat0,stlon0; /* lat. & log of a selected point in the net */
%float stx[102],sty[102]; /* sta. x & y relative to stlat0 & stlon0 */
%float stx0,sty0;

%  //stin(); /* read in station table from "station.dat"    */
%  stinxy(); /* read in station table from "staxy.dat"    */
%  //ll2xy(); /* change station lat. & lon . to X & Y       */
%  Response(stx, sty, stnum);


%  int i, j, k;
%  float kx, ky, Coeff;
%  float dk;		/* Step Length */
%  float a1, a2;

%auto4S001
%2007.11.17 赵仲和
%加载台阵坐标,绘制台阵的子台分布图
%图形文件名：array_xx.fig
%台阵坐标文件 array_fuqing.x(东,km）; array_fuqing.y（北,km）; array_fuqing.z（下，km);
%array_fuqing.lat(纬度，度.度）；array_fuqing.lon（经度，度.度）
%array_fuqing.lam(纬度，度 分.分）；array_fuqing.lom (经度，度 分.分）
%
%load array_fuqing.x
%x=array_fuqing.x
%load array_fuqing.y
%y=array_fuqing.y
%load array_fuqing.z
%load array_fuqing.lat
%z=array_fuqing.lat
%filen='array_yka.asc'   %黄刀台阵YKA
filen='array_arces.asc'   %挪威台阵ARCES
%filen='array_grf.asc'   %德国台阵GRF
array_cord=importdata(filen,' ',2);  %读入台阵坐标数据文件
x=array_cord.data(:,2)/1000;  %m->km
y=array_cord.data(:,3)/1000;
%x=array_cord.data(:,3);  %经度(GRF)
%y=array_cord.data(:,2);  %纬度(GRF)
%x=(x-x(1))*111.19*cos(y(1)*pi/180);
%y=(y-y(1))*111.19;
%load array_arc.x;
%x=array_arc(1:25);
%load array_arc.y;
%y=array_arc(1:25);
stnum=length(x)
%stnum=length(x)-4  %YKA去掉4个宽带台 
figure(1)
plot(x(1:stnum),y(1:stnum),'*')
%title('YKA台阵短周期子台分布图')
title('ARCES台阵子台分布图')
%title('GRF台阵子台分布图')
xlabel('km') 
ylabel('km')

%stnum=25;   
k0=-1.0;
%k0=-0.1;
%k0=-0.04;
%kn=41;
   
%dk=0.05;
dk=0.01;
%dk=0.001;
kn=-2*k0/dk+1
for i=1:1:kn
    ky=k0+dk*(i-1);
    for j=1:1:kn
        kx=k0+dk*(j-1);
        a1=0.0;
        a2=0.0;
        for k=1:1:stnum
            Coeff=2.0*3.1415926535897932*(kx*x(k)+ky*y(k));
            a1=a1+cos(Coeff);
            a2=a2+sin(Coeff);
        end
        ab(i,j)=sqrt(a1*a1+a2*a2)/stnum;
    end
end
%  //a0=ab[20][20];
itemp=(kn-1)/2;
a0=ab(itemp,itemp);
for i=1:1:kn
    ky=k0+dk*(i-1);
    for j=1:1:kn
      kx=k0+dk*(j-1);
      ab(i,j)=log10(ab(i,j)/a0);
      ab(i,j)=20.0*ab(i,j);
      if ab(i,j)<-60.0
          ab(i,j)=-60.1;
      end
    end
end
for i=1:1:kn
      kxx(i)=k0+dk*i;
      kyy(i)=k0+dk*i;
end
figure(2)
contourf(kxx,kyy,ab)
colorbar
xlabel('kx/(1/km)')
ylabel('ky/(1/km)')
title('YKA台阵响应图')
%title('ARCES台阵响应图')
%title('GRF台阵响应图')
%surf(kxx,kyy,ab)
%view(0,90)

%L形16台---------------------
xlabel('km') 
ylabel('km')
load array_L16.x
x=array_L16(1:16);
load array_L16.y
y=array_L16(1:16);
stnum=length(x); 

figure(3)
plot(x(1:stnum),y(1:stnum),'*')
%title('YKA台阵短周期子台分布图')
%title('ARCES台阵子台分布图')
%title('GRF台阵子台分布图')
title('L形台阵子台分布图')

%dk=0.05;
dk=0.01;
%dk=0.001;
kn=-2*k0/dk+1
for i=1:1:kn
    ky=k0+dk*(i-1);
    for j=1:1:kn
      kx=k0+dk*(j-1);
      a1=0.0;
      a2=0.0;
      for k=1:1:stnum
         Coeff=2.0*3.1415926535897932*(kx*x(k)+ky*y(k));
	     a1=a1+cos(Coeff);
	     a2=a2+sin(Coeff);
      end
      ab(i,j)=sqrt(a1*a1+a2*a2)/stnum;
    end
end
 %  a0=ab[20][20];
 %  itemp=(kn-1)/2;
 %  a0=ab(itemp,itemp);
 %  修正对最大值a0的确定。
  
  maxab=-60
  minab=10
for j=1:kn
    for i=1:kn
        if ab(i,j)>maxab
              maxab=ab(i,j);
              imax=i;
              jmax=j;
        end
        if ab(i,j)<minab
              minab=ab(i,j);
        end
    end
end
a0=maxab   % 归一化响应的最大值应该为1
imax
jmax
b0=20*log10(minab)    %最小dB值
if b0<-12 b0=-12   %L形台阵
    %if b0<-60 b0=-60  
end
for i=1:1:kn
    ky=k0+dk*(i-1);
    for j=1:1:kn
        kx=k0+dk*(j-1);
        ab(i,j)=log10(ab(i,j)/a0);
        ab(i,j)=20.0*ab(i,j);
        if ab(i,j)<b0
            ab(i,j)=b0;
        end
    end
end
for i=1:1:kn
    kxx(i)=k0+dk*i;
    kyy(i)=k0+dk*i;
end
dbgap=2
%dbgap=3
dbgap=3
%根据指定的dB间隔确定每条等值线对应的响应dB值（矢量v)
for i=1:40
    v1(i)=(i-1)*dbgap;
    if v1(i)>-b0  break
    end
end
ii=i
for i=1:ii
      v(i)=-v1(ii+1-i);
end
v1
v   %要求v为递增矢量
imax=i     
figure(4)
% 2021-08-13补
%相邻等值线间隔分贝数

ncont=length(v)    %等值线极数
%------------------------
contourf(kxx,kyy,ab,v);  %替换原contourf(kxx,kyy,ab）
%矢量v给出每个等值线的dB值
%contourf(kxx,kyy)   %不给定等值线分级
colorbar
xlabel('kx/(1/km)')
ylabel('ky/(1/km)')
title('L形台阵响应图')
%title('YKA台阵响应图')
%title('ARCES台阵响应图')
%title('GRF台阵响应图')
%surf(kxx,kyy,ab)
%view(0,90)

%----------------------------
%dk=0.05;
dk=0.01;
%dk=0.001;
kn=-2*k0/dk+1
for i=1:1:kn
    ky=k0+dk*(i-1);
    for j=1:1:kn
        kx=k0+dk*(j-1);
        a1=0.0;
        a2=0.0;
        for k=1:1:stnum
            Coeff=2.0*3.1415926535897932*(kx*x(k)+ky*y(k));
            a1=a1+cos(Coeff);
            a2=a2+sin(Coeff);
        end
        ab(i,j)=sqrt(a1*a1+a2*a2)/stnum;
    end
end
itemp=(kn-1)/2;
%a0=ab[20][20];
a0=ab(itemp,itemp);
for i=1:1:kn
    ky=k0+dk*(i-1);
    for j=1:1:kn
        kx=k0+dk*(j-1);
        ab(i,j)=log10(ab(i,j)/a0);
        ab(i,j)=20.0*ab(i,j);
        if ab(i,j)<-60.0
            ab(i,j)=-60.1;
        end
    end
end
for i=1:1:kn
    kxx(i)=k0+dk*i;
    kyy(i)=k0+dk*i;
end
figure(5)
contourf(kxx,kyy,ab,'LevelStep',3)
colorbar
xlabel('kx/(1/km)')
ylabel('ky/(1/km)')
% title('YKA台阵响应图')
% title('ARCES台阵响应图')
title('GRF台阵响应图')
%surf(kxx,kyy,ab)
%view(0,90)

