%速度谱分析，福州台阵记录2007110122
%台站文件：
%   array_fuqing.lat：福清台阵子台纬度（度.度），第一个数字是子台总数
%   array_fuqing.lon：福清台阵子台经度（度.度），第一个数字是子台总数
%   array_fuqing.x：福清台阵子台x坐标（东，km），第一个数字是子台总数
%   array_fuqing.y：福清台阵子台y坐标（北.km），第一个数字是子台总数
%   array_fuqing.z：福清台阵子台高程（上，km），第一个数字是子台总数
%   array_fuqing.lam：福清台阵子台纬度（度 分.分），第一个数字是子台总数
%   array_fuqing.lom：福清台阵子台经度（度 分.分），第一个数字是子台总数  

%//float ab[50][50],a0;
%int stnum; /* number of stations in the network */
%2007.11.17 赵仲和
%加载台阵坐标,绘制台阵的子台分布图
%图形文件名：array_xx.fig
%台阵坐标文件 array_fuqing.x(东,km）; array_fuqing.y（北,km）; array_fuqing.z（下，km);
%array_fuqing.lat(纬度，度.度）；array_fuqing.lon（经度，度.度）
%array_fuqing.lam(纬度，度 分.分）；array_fuqing.lom (经度，度 分.分）
%
clear;close all
load array_fuqing.lat;
lat=array_fuqing(1:17);
lat0=lat(1);
cos_lat0=cos(lat(1));
for n=1:1:17
    lat1(n)=lat(n)-lat0;
    y(n)=lat1(n)*111.119;
end
load array_fuqing.lon;
lon=array_fuqing(1:17);
lon0=lon(1);
for n=1:1:17
    lon1(n)=lon(n)-lon0;
    x(n)=lon1(n)*111.119*cos(lat(n));
end
for i=1:4
    array(i,1)=x(i);
    array(i,2)=y(i);
end
for i=5:16
    array(i,1)=x(i+1);
    array(i,2)=y(i+1);
end

figure(1)
%计算3个圆圈的半径：相应子台距中心台距离的平均值
r1=0;
for n=2:1:4
  r1=r1+sqrt((x(n)-x(1))*(x(n)-x(1))+(y(n)-y(1))*(y(n)-y(1)));
end
r2=0;
for n=5:1:9
  r2=r2+sqrt((x(n)-x(1))*(x(n)-x(1))+(y(n)-y(1))*(y(n)-y(1)));
end
r3=0;
for n=10:1:16 %现在在外圈上有8个候选子台
  r3=r3+sqrt((x(n)-x(1))*(x(n)-x(1))+(y(n)-y(1))*(y(n)-y(1)));
end
r1=r1/3;
r2=r2/5;
r3=r3/7;
for n=1:1:360
    x1(n)=r1*sin(n*3.1415926535897932/180);
    y1(n)=r1*cos(n*3.1415926535897932/180);
end
%subplot(1,2,1)
plot(x1,y1,'r-')
hold on
for n=1:1:360
    x1(n)=r2*sin(n*3.1415926535897932/180);
    y1(n)=r2*cos(n*3.1415926535897932/180);
end
plot(x1,y1,'r-')
hold on
for n=1:1:360
    x1(n)=r3*sin(n*3.1415926535897932/180);
    y1(n)=r3*cos(n*3.1415926535897932/180);
end
plot(x1,y1,'r-')
hold on
plot(x,y,'*') %绘子台分布图
hold off
a=[1,0];

f=[0,0.04,0.04,1];m=[0,0,1,1];
%高通滤波器:拐角频率为0.04*采样率/2=0.04*50/2=1Hz；m给出相应的幅度
%利用Matlab中的帮助 help fir2; help filter; help filtfilt
%参考书：《Matlab及在电子信息课程中的应用》 第7章    
%2007-12-03   赵仲和
b=fir2(30,f,m);n=0:30;
%利用Matlab中的信号处理工具箱中的FIR滤波器设计工具fir2生成递归滤波器系数b,
% 30  为滤波器的阶
%subplot(3,2,1);stem(n,b,'.')
%xlabel('n');ylabel('h(n)');
%axis([0,30,-0.4,0.5]),line([0,30],[0,0])
[h,w]=freqz(b,1,256);
%subplot(3,2,2);plot(w/pi,20*log10(abs(h)));grid
%axis([0,1,-80,0]);xlabel('w/pi');ylabel('幅度(dB)');
%读入福州台阵波形
load st02007110122.ud
waveud(1,:)=st02007110122(1:60*50);
load st12007110122.ud
waveud(2,:)=st12007110122(1:60*50);
load st22007110122.ud
waveud(3,:)=st22007110122(1:60*50);
load st32007110122.ud
waveud(4,:)=st32007110122(1:60*50);
%没有st42007110122
load st52007110122.ud
waveud(5,:)=st52007110122(1:60*50);
load st62007110122.ud
waveud(6,:)=st62007110122(1:60*50);
load st72007110122.ud
waveud(7,:)=st72007110122(1:60*50);
load st82007110122.ud
waveud(8,:)=st82007110122(1:60*50);
load st92007110122.ud
waveud(9,:)=st92007110122(1:60*50);
load s102007110122.ud
waveud(10,:)=s102007110122(1:60*50);
load s112007110122.ud
waveud(11,:)=s112007110122(1:60*50);
load s122007110122.ud
waveud(12,:)=s122007110122(1:60*50);
load s132007110122.ud
waveud(13,:)=s132007110122(1:60*50);
load s142007110122.ud
waveud(14,:)=s142007110122(1:60*50);
load s152007110122.ud
waveud(15,:)=s152007110122(1:60*50);
load s172007110122.ud
waveud(16,:)=s172007110122(1:60*50);

for j=1:60*50
     waveud_sum(j)=0;
for i=1:16
    waveud_sum(j)=waveud_sum(j)+waveud(i,j);
end
waveud_sum(j)=waveud_sum(j)/16;
end
figure(2)
plot(waveud(1,:)); hold on
plot(waveud(2,:)+10000); hold on
plot(waveud(3,:)+10000*2); hold on
plot(waveud(4,:)+10000*3); hold on
plot(waveud(5,:)+10000*4); hold on
plot(waveud(6,:)+10000*5); hold on
plot(waveud(7,:)+10000*6); hold on
plot(waveud(8,:)+10000*7); hold on
plot(waveud(9,:)+10000*8); hold on
plot(waveud(10,:)+10000*9); hold on
plot(waveud(11,:)+10000*10); hold on
plot(waveud(12,:)+10000*11); hold on
plot(waveud(13,:)+10000*12); hold on
plot(waveud(14,:)+10000*13); hold on
plot(waveud(15,:)+10000*14); hold on
plot(waveud(16,:)+10000*15);hold on
plot(waveud_sum+10000*16); hold on
xlabel('采样点序号（50s/s)')
title('福州台阵2007110122地震波形'); hold off
sample_rate=50   %采样率
num_cha=16   %信号道数（子台个数）
% F-K分析
f1=4 %Hz
f2=6 %Hz
tstart=0.8 %sec   
twindow=2.56 %sec

kx1=-1.0 %1/km
kx2=1.0 %1/km
kxpoint=101
ky1=-1.0
ky2=1.0
kypoint=101

nstart=tstart*sample_rate
nwindow=twindow*sample_rate
nend=nstart+nwindow
for j=1:num_cha
for i=1:nwindow
    fkwave(j,i)=waveud(j,i+nstart);
end
end
size(fkwave) 

figure(3)
plot(array(:,1),array(:,2),'*');

%速度谱分析-vespagram
% 给定射线方位角，改变视慢度
%flag2=0  %给定射线方位角
flag2=1  %给定慢度值
if flag2==0
vespa_azid=201.7
vespa_azi=vespa_azid*pi/180
vespa_vappmin=4.0
vespa_vappmax=26.0   %视速度（km/s）
vespa_slowmin=1/vespa_vappmax   % 视慢度（s/km）
vespa_slowmax=1/vespa_vappmin
vespa_num_slow=400
vespa_slow_step=(vespa_slowmax-vespa_slowmin)/vespa_num_slow
end
if flag2==1
vespa_vapp=1/0.171 %设视速度为给定方位角得到的视速度(flag2==0)
vespa_slow=1/vespa_vapp
vespa_azimin=0.0*pi/180
vespa_azimax=360*pi/180   %方位角
vespa_num_azi=360
vespa_azi_step=(vespa_azimax-vespa_azimin)/vespa_num_azi
end
flag1=0  %能量判据
%flag1=1  %振幅判据
%滑动窗参数
disp('速度谱分析')
%twindow=2.56 %滑动窗长sec
twindow=1.0 
%已知信号长度3000点，采样率50s/s,信号总长60s。
nwindow=twindow*sample_rate
len=30 
num_len=len*sample_rate
%t0=10.0   %起始初值
t0=5.0 
t_step=0.1 %滑动步长0.1秒
num_step=t_step*sample_rate
num_end=50
num_slid=floor((num_len-nwindow-num_end)/num_step)
vespa_max=0;
for i_slid=1:num_slid
    disp({'滑动序号',i_slid})
    tstart=(i_slid-1)*t_step+t0
    vespa_t(i_slid)=tstart;
if flag2==0
for i_vespa=1:vespa_num_slow
    vespa_slow=vespa_slowmin+(i_vespa-1)*vespa_slow_step;
    [vespa_bm] = vespa_func(array,waveud,num_cha,tstart,twindow,sample_rate,vespa_slow,vespa_azi,flag1);
    vespa(i_vespa,i_slid)=vespa_bm;
    vespa_s(i_vespa)=vespa_slow;
    if  vespa(i_vespa,i_slid)>=vespa_max
        vespa_max=vespa(i_vespa,i_slid);
        vespa_max_slow=vespa_slow;
        vespa_max_t=tstart;
    end
end  
end
if flag2==1
  for i_vespa=1:vespa_num_azi
    vespa_azi=vespa_azimin+(i_vespa-1)*vespa_azi_step;
    [vespa_bm] = vespa_func(array,waveud,num_cha,tstart,twindow,sample_rate,vespa_slow,vespa_azi,flag1);
    vespa(i_vespa,i_slid)=vespa_bm;
    vespa_a(i_vespa)=vespa_azi;
    if  vespa(i_vespa,i_slid)>vespa_max
        vespa_max=vespa(i_vespa,i_slid);
        vespa_max_azi=vespa_azi;
        vespa_max_t=tstart;
    end
  end  
end
end
figure(5)
if flag2==0
if flag1==0
    contourf(vespa_t,vespa_s,10*log10(vespa/vespa_max)); %能量
end
if flag1==1
    contourf(vespa_t,vespa_s,20*log10(vespa/vespa_max));  %振幅
end

title(strcat('福州台阵2007110122地震波形速度谱分析图（能量判据），方位角',num2str(vespa_azid),'度,视慢度峰值',num2str(vespa_max_slow),'s/km,起始时刻',num2str(vespa_max_t)))
%title('福州台阵2007110122合成波形，宽带F-K，4-6Hz，t1=0.0s,tl=6s')
ylabel('视慢度/(s/km)')
end
if flag2==1
if flag1==0
    contourf(vespa_t,vespa_a*180/pi,10*log10(vespa/vespa_max)); %能量
end
if flag1==1
    contourf(vespa_t,vespa_a*180/pi,20*log10(vespa/vespa_max));  %振幅
end

title(strcat('福州台阵2007110122地震波形速度谱分析图（能量判据），视慢度',num2str(vespa_slow),'s/km,方位角峰值',num2str(vespa_max_azi*180/pi),'度,起始时刻',num2str(vespa_max_t)))
%title('福州台阵2007110122合成波形，宽带F-K，4-6Hz，t1=0.0s,tl=6s')
ylabel('方位角/度')
end
xlabel('时间/s')

colorbar
