%三分量台阵处理
% 台站文件：
%   array_fuqing.lat：福清台阵子台纬度（度.度），第一个数字是子台总数
%   array_fuqing.lon：福清台阵子台经度（度.度），第一个数字是子台总数
%   array_fuqing.x：福清台阵子台x坐标（东，km），第一个数字是子台总数
%   array_fuqing.y：福清台阵子台y坐标（北.km），第一个数字是子台总数
%   array_fuqing.z：福清台阵子台高程（上，km），第一个数字是子台总数
%   array_fuqing.lam：福清台阵子台纬度（度 分.分），第一个数字是子台总数
%   array_fuqing.lom：福清台阵子台经度（度 分.分），第一个数字是子台总数  

%//float ab[50][50],a0;
%int stnum; /* number of stations in the network */
%2007.11.17 赵仲和
%加载台阵坐标,绘制台阵的子台分布图
%图形文件名：array_xx.fig
%台阵坐标文件 array_fuqing.x(东,km）; array_fuqing.y（北,km）; array_fuqing.z（下，km);
%array_fuqing.lat(纬度，度.度）；array_fuqing.lon（经度，度.度）
%array_fuqing.lam(纬度，度 分.分）；array_fuqing.lom (经度，度 分.分）
%
clear;close all
load array_fuqing.lat;
lat=array_fuqing(1:17);
lat0=lat(1);
cos_lat0=cos(lat(1));
for n=1:1:17
    lat1(n)=lat(n)-lat0;
    y(n)=lat1(n)*111.119;
end
load array_fuqing.lon;
lon=array_fuqing(1:17);
lon0=lon(1);
for n=1:1:17
    lon1(n)=lon(n)-lon0;
    x(n)=lon1(n)*111.119*cos(lat(n));
end
for i=1:4
    array(i,1)=x(i);
    array(i,2)=y(i);
end
for i=5:16
    array(i,1)=x(i+1);
    array(i,2)=y(i+1);
end
%计算理论到时差
num_cha=16  %道数
sample_rate=50 %s/s
azi_d=40
azi_c=azi_d*pi/180;
vapp=8
slow=1/vapp
slowx=slow*sin(azi_c);
slowy=slow*cos(azi_c);

for i=1:num_cha
    arit(i)=(slowx*array(i,1)+slowy*array(i,2));
    del(i)=floor(arit(i)*sample_rate);
end
%计算理论水平分量
%视出射角b,视慢度slow，真出射角b0，入射角a0，sin(a0)=vp/vapp,vp是P波真速度。
vp=6.0  %设真速度为6.0km/s
vpvs=1.73  %P与S的波速比
a0=asin(vp/vapp) 
a0d=a0*180/pi
b0=0.5*pi-a0
b0d=b0*180/pi
b=acos(cos(b0)/vpvs)*2-pi/2 %视出射角公式（   ）
bd=b*180/pi
%径向分量与垂直分量的比值v2r
v2r=tan(b)
figure(1)
%计算3个圆圈的半径：相应子台距中心台距离的平均值
r1=0;
for n=2:1:4
  r1=r1+sqrt((x(n)-x(1))*(x(n)-x(1))+(y(n)-y(1))*(y(n)-y(1)));
end
r2=0;
for n=5:1:9
  r2=r2+sqrt((x(n)-x(1))*(x(n)-x(1))+(y(n)-y(1))*(y(n)-y(1)));
end
r3=0;
for n=10:1:16 %现在在外圈上有8个候选子台
  r3=r3+sqrt((x(n)-x(1))*(x(n)-x(1))+(y(n)-y(1))*(y(n)-y(1)));
end
r1=r1/3;
r2=r2/5;
r3=r3/7;
for n=1:1:360
    x1(n)=r1*sin(n*3.1415926535897932/180);
    y1(n)=r1*cos(n*3.1415926535897932/180);
end
%subplot(1,2,1)
plot(x1,y1,'r-')
hold on
for n=1:1:360
    x1(n)=r2*sin(n*3.1415926535897932/180);
    y1(n)=r2*cos(n*3.1415926535897932/180);
end
plot(x1,y1,'r-')
hold on
for n=1:1:360
    x1(n)=r3*sin(n*3.1415926535897932/180);
    y1(n)=r3*cos(n*3.1415926535897932/180);
end
plot(x1,y1,'r-')
hold on
plot(x,y,'*') %绘子台分布图
hold off
a=[1,0];

f=[0,0.04,0.04,1];m=[0,0,1,1];
%高通滤波器:拐角频率为0.04*采样率/2=0.04*50/2=1Hz；m给出相应的幅度
%利用Matlab中的帮助 help fir2; help filter; help filtfilt
%参考书：《Matlab及在电子信息课程中的应用》 第7章    
%2007-12-03   赵仲和
b=fir2(30,f,m);n=0:30;
%利用Matlab中的信号处理工具箱中的FIR滤波器设计工具fir2生成递归滤波器系数b,
% 30  为滤波器的阶
%subplot(3,2,1);stem(n,b,'.')
%xlabel('n');ylabel('h(n)');
%axis([0,30,-0.4,0.5]),line([0,30],[0,0])
[h,w]=freqz(b,1,256);
%subplot(3,2,2);plot(w/pi,20*log10(abs(h)));grid
%axis([0,1,-80,0]);xlabel('w/pi');ylabel('幅度(dB)');
%读入福州台阵波形
load st02007110122.ud
waveud(1,:)=st02007110122(1:60*50);
load st12007110122.ud
waveud(2,:)=st12007110122(1:60*50);
load st22007110122.ud
waveud(3,:)=st22007110122(1:60*50);
load st32007110122.ud
waveud(4,:)=st32007110122(1:60*50);
%没有st42007110122
load st52007110122.ud
waveud(5,:)=st52007110122(1:60*50);
load st62007110122.ud
waveud(6,:)=st62007110122(1:60*50);
load st72007110122.ud
waveud(7,:)=st72007110122(1:60*50);
load st82007110122.ud
waveud(8,:)=st82007110122(1:60*50);
load st92007110122.ud
waveud(9,:)=st92007110122(1:60*50);
load s102007110122.ud
waveud(10,:)=s102007110122(1:60*50);
load s112007110122.ud
waveud(11,:)=s112007110122(1:60*50);
load s122007110122.ud
waveud(12,:)=s122007110122(1:60*50);
load s132007110122.ud
waveud(13,:)=s132007110122(1:60*50);
load s142007110122.ud
waveud(14,:)=s142007110122(1:60*50);
load s152007110122.ud
waveud(15,:)=s152007110122(1:60*50);
load s172007110122.ud
waveud(16,:)=s172007110122(1:60*50);

for j=1:60*50
     waveud_sum(j)=0;
for i=1:16
    waveud_sum(j)=waveud_sum(j)+waveud(i,j);
end
waveud_sum(j)=waveud_sum(j)/16;
end
figure(2)
plot(waveud(1,:)); hold on
plot(waveud(2,:)+10000); hold on
plot(waveud(3,:)+10000*2); hold on
plot(waveud(4,:)+10000*3); hold on
plot(waveud(5,:)+10000*4); hold on
plot(waveud(6,:)+10000*5); hold on
plot(waveud(7,:)+10000*6); hold on
plot(waveud(8,:)+10000*7); hold on
plot(waveud(9,:)+10000*8); hold on
plot(waveud(10,:)+10000*9); hold on
plot(waveud(11,:)+10000*10); hold on
plot(waveud(12,:)+10000*11); hold on
plot(waveud(13,:)+10000*12); hold on
plot(waveud(14,:)+10000*13); hold on
plot(waveud(15,:)+10000*14); hold on
plot(waveud(16,:)+10000*15);hold on
plot(waveud_sum+10000*16); hold on
xlabel('采样点序号（50s/s)')
title('福州台阵2007110122地震波形'); hold off
% F-K分析
f1=4 %Hz
f2=6 %Hz
tstart=0.8 %sec
twindow=2.56 %sec

kx1=-1.0 %1/km
kx2=1.0 %1/km
kxpoint=101
ky1=-1.0
ky2=1.0
kypoint=101

nstart=tstart*sample_rate
nwindow=twindow*sample_rate
nend=nstart+nwindow
for j=1:num_cha
for i=1:nwindow
    fkwave(j,i)=waveud(j,i+nstart);
end
end
size(fkwave) 

figure(3)
plot(array(:,1),array(:,2),'*');
%假想信号
for kk=1:1
    disp('kkkkkkk')
for i=1:300
    testwave(1,i)=0;
    testwave(2,i)=testwave(1,i);
    testwave(3,i)=testwave(1,i);
    testwave(4,i)=testwave(1,i);
    testwave(5,i)=testwave(1,i);
    testwave(6,i)=testwave(1,i);
    testwave(7,i)=testwave(1,i);
    testwave(8,i)=testwave(1,i);
    testwave(9,i)=testwave(1,i);
    testwave(10,i)=testwave(1,i);
    testwave(11,i)=testwave(1,i);
    testwave(12,i)=testwave(1,i);
    testwave(13,i)=testwave(1,i);
    testwave(14,i)=testwave(1,i);
    testwave(15,i)=testwave(1,i);
    testwave(16,i)=testwave(1,i);
end
c=10000
snr=10
d=c/snr
for i=1:300
    for j=1:num_cha
    testwave(j,i)=d*randn(1);
    testwave_ns(j,i)=d*randn(1);
    testwave_ew(j,i)=d*randn(1);
    end
end
for j=1:num_cha
    meanj(j)=mean(testwave(j,1:50));
    rmsj(j)=0;
    for i=1:50
        rmsj(j)=rmsj(j)+(testwave(j,i)-meanj(j))^2;
       
    end
    rmsj(j)=sqrt(rmsj(j)/50);
   
end
 meanj
 rmsj
 starti=81
for i=starti:280
    testwave(1,i)=c*sin(2*pi*5*i/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
end
for i=81:280
    testwave(2,i+del(2))=c*sin(2*pi*5*i/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
    testwave(3,i+del(3))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
    testwave(4,i+del(4))=c*sin(2*pi*5*i/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
    testwave(5,i+del(5))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
    testwave(6,i+del(6))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
    testwave(7,i+del(7))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
    testwave(8,i+del(8))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
    testwave(9,i+del(9))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
    testwave(10,i+del(10))=c*sin(2*pi*5*i/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
    testwave(11,i+del(11))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
    testwave(12,i+del(12))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
    testwave(13,i+del(13))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
    testwave(14,i+del(14))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
    testwave(15,i+del(15))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
    testwave(16,i+del(16))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
end
starti=81
for i=starti:280
    testwave_ns(1,i)=(c*sin(2*pi*5*i/sample_rate)*exp(-0.01*(i-starti)))*v2r*cos(azi_c)+d*randn(1);
end
for i=81:280
    testwave_ns(2,i+del(2))=c*sin(2*pi*5*i/sample_rate)*exp(-0.01*(i-starti))*v2r*cos(azi_c)+d*randn(1);
    testwave_ns(3,i+del(3))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))*v2r*cos(azi_c)+d*randn(1);
    testwave_ns(4,i+del(4))=c*sin(2*pi*5*i/sample_rate)*exp(-0.01*(i-starti))*v2r*cos(azi_c)+d*randn(1);
    testwave_ns(5,i+del(5))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))*v2r*cos(azi_c)+d*randn(1);
    testwave_ns(6,i+del(6))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))*v2r*cos(azi_c)+d*randn(1);
    testwave_ns(7,i+del(7))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))*v2r*cos(azi_c)+d*randn(1);
    testwave_ns(8,i+del(8))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))*v2r*cos(azi_c)+d*randn(1);
    testwave_ns(9,i+del(9))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))*v2r*cos(azi_c)+d*randn(1);
    testwave_ns(10,i+del(10))=c*sin(2*pi*5*i/sample_rate)*exp(-0.01*(i-starti))*v2r*cos(azi_c)+d*randn(1);
    testwave_ns(11,i+del(11))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))*v2r*cos(azi_c)+d*randn(1);
    testwave_ns(12,i+del(12))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))*v2r*cos(azi_c)+d*randn(1);
    testwave_ns(13,i+del(13))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))*v2r*cos(azi_c)+d*randn(1);
    testwave_ns(14,i+del(14))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))*v2r*cos(azi_c)+d*randn(1);
    testwave_ns(15,i+del(15))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))*v2r*cos(azi_c)+d*randn(1);
    testwave_ns(16,i+del(16))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))*v2r*cos(azi_c)+d*randn(1);
end
starti=81
for i=starti:280
    testwave_ew(1,i)=c*sin(2*pi*5*i/sample_rate)*exp(-0.01*(i-starti))*v2r*sin(azi_c)+d*randn(1);
end
for i=81:280
    testwave_ew(2,i+del(2))=c*sin(2*pi*5*i/sample_rate)*exp(-0.01*(i-starti))*v2r*sin(azi_c)+d*randn(1);
    testwave_ew(3,i+del(3))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))*v2r*sin(azi_c)+d*randn(1);
    testwave_ew(4,i+del(4))=c*sin(2*pi*5*i/sample_rate)*exp(-0.01*(i-starti))*v2r*sin(azi_c)+d*randn(1);
    testwave_ew(5,i+del(5))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))*v2r*sin(azi_c)+d*randn(1);
    testwave_ew(6,i+del(6))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))*v2r*sin(azi_c)+d*randn(1);
    testwave_ew(7,i+del(7))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))*v2r*sin(azi_c)+d*randn(1);
    testwave_ew(8,i+del(8))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))*v2r*sin(azi_c)+d*randn(1);
    testwave_ew(9,i+del(9))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))*v2r*sin(azi_c)+d*randn(1);
    testwave_ew(10,i+del(10))=c*sin(2*pi*5*i/sample_rate)*exp(-0.01*(i-starti))*v2r*sin(azi_c)+d*randn(1);
    testwave_ew(11,i+del(11))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))*v2r*sin(azi_c)+d*randn(1);
    testwave_ew(12,i+del(12))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))*v2r*sin(azi_c)+d*randn(1);
    testwave_ew(13,i+del(13))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))*v2r*sin(azi_c)+d*randn(1);
    testwave_ew(14,i+del(14))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))*v2r*sin(azi_c)+d*randn(1);
    testwave_ew(15,i+del(15))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))*v2r*sin(azi_c)+d*randn(1);
    testwave_ew(16,i+del(16))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))*v2r*sin(azi_c)+d*randn(1);
end
figure(4)
plot(testwave(1,:)); hold on
plot(testwave(2,:)+10000); hold on
plot(testwave(3,:)+10000*2); hold on
plot(testwave(4,:)+10000*3); hold on
plot(testwave(5,:)+10000*4); hold on
plot(testwave(6,:)+10000*5); hold on
plot(testwave(7,:)+10000*6); hold on
plot(testwave(8,:)+10000*7); hold on
plot(testwave(9,:)+10000*8); hold on
plot(testwave(10,:)+10000*9); hold on
plot(testwave(11,:)+10000*10); hold on
plot(testwave(12,:)+10000*11); hold on
plot(testwave(13,:)+10000*12); hold on
plot(testwave(14,:)+10000*13); hold on
plot(testwave(15,:)+10000*14); hold on
plot(testwave(16,:)+10000*15);hold on
for i=1:num_cha
    maxi(i)=0;
    meani(i)=mean(testwave(i,1:300));
    for j=1:300
        if abs(testwave(i,j)-meani(i))>maxi(i)
            maxi(i)=abs(testwave(i,j)-meani(i));
        end
    end
end
test_maxi=maxi
for i=1:num_cha
test_snr(i)=test_maxi(i)/rmsj(i);
end
test_snr
snr_mean=mean(test_snr)
for j=1:300
    testwave_sum(j)=0;
    simple_sum(j)=0;
end
for j=1:250
for i=1:16
    testwave_sum(j+20)=testwave_sum(j+20)+testwave(i,j+20+del(i));
    simple_sum(j+20)=simple_sum(j+20)+testwave(i,j+20);
end
end
testwave_sum_max=0;
simple_sum_max=0;
test_sum_mean=mean(testwave_sum)/num_cha
simple_sum_mean=mean(simple_sum)/num_cha
for j=1:300
testwave_sum(j)=testwave_sum(j)/num_cha;
simple_sum(j)=simple_sum(j)/num_cha;
if abs(testwave_sum(j)-test_sum_mean)>testwave_sum_max
    testwave_sum_max=abs(testwave_sum(j)-test_sum_mean);
    testwave_sum_maxj=j;
end
if abs(simple_sum(j)-simple_sum_mean)>simple_sum_max
    simple_sum_max=abs(simple_sum(j)-simple_sum_mean);
    simple_sum_maxj=j;
end
end
dmax=testwave_sum_max
dmaxj=testwave_sum_maxj
smax=simple_sum_max
smaxj=simple_sum_maxj
d_rms=0;
s_rms=0;
for i=1:50
        d_rms=d_rms+testwave_sum(i)^2;
        s_rms=s_rms+simple_sum(i)^2;
    end
    d_rms=sqrt(d_rms/50)
    s_rms=sqrt(s_rms/50)
%用信号最大值与信号前噪声rms之比作为信噪比
%
simple_snr=smax/s_rms
sum_imp=simple_snr/snr_mean
beam_snr=dmax/d_rms
dsum_imp=beam_snr/snr_mean
dsum_imp_kk(kk)=dsum_imp;
end
dsum_kk=dsum_imp_kk
dsum_imp_mean=mean(dsum_imp_kk)
imp_max=0;
imp_min=100;
xlabel('采样点序号（50s/s)')
title('福州台阵合成波形'); hold off

figure(5)
subplot(311)
plot(testwave(1,:))
subplot(312)
plot(testwave_ns(1,:))
subplot(313)
plot(testwave_ew(1,:))
y1=testwave_ns(1,:);   %北南
y2=testwave_ew(1,:);   %东西
y3=testwave(1,:);      %上下
%三分量偏振分析
disp('三分量偏振分析')
start=81    %第1道计算协方差矩阵起点
nstart=start; %针对本例中的数据，P初动到时在第0点开始，以此起算协方差矩阵
tmax=2   %时间长度s
nmax=tmax*sample_rate %信号窗长

    for jj=1:nmax
        a(1,jj)=-y1(jj+nstart);
%北南向，加符号，成为x(指南）为正方向，形成x（南）y（东）z（上）右旋坐标系
        a(2,jj)=y2(jj+nstart); %东西向
        a(3,jj)=y3(jj+nstart); %垂直向
    end
 figure(6)
subplot(311)
plot(a(1,:))
subplot(312)
plot(a(2,:))
subplot(313)
plot(a(3,:))
  
   nlen=nmax;
    %for nlen=1:nmax
        for ii=1:3  %计算各分量的均值rmean(ii)  
            rmean(ii)=0;
            for n=1:nlen
                rmean(ii)=rmean(ii)+a(ii,n);
            end
            rmean1(ii,nlen)=rmean(ii)/nlen;
        end
       for ii=1:3
          for jj=1:3
          cov(ii,jj)=0;
             for n=1:nlen
               cov(ii,jj)=cov(ii,jj)+(a(ii,n)-rmean1(ii,nlen))*(a(jj,n)-rmean1(jj,nlen));
             end
          cov1(ii,jj,nlen)=cov(ii,jj)/nlen;
          end
       end
       dev1=sqrt(cov1(1,1,nlen))  %模拟地震信号初始段(长度=nlen)北南分量的标准偏差
       dev2=sqrt(cov1(2,2,nlen))  %模拟地震信号初始段(长度=nlen)东西分量的标准偏差
       dev3=sqrt(cov1(3,3,nlen))  %模拟地震信号初始段(长度=nlen)上下分量的标准偏差
       cov
    %end
     [T,E] = eig(cov)  %计算协方差矩阵的特征向量和特征值
     Td = double(T)  %矩阵中的列向量为特征向量,已经归一化，因此是该向量的方向余弦
     Ed = double(E)  %矩阵中的对角线元素为相应的特征值，由小到大排列
     % 因此，公式中的主偏振方向对应于第3列中的向量，即Td(1,3),Td(2,3),Td(3,3)
    %计算方位角
    Td(1,3)
    Td(2,3)
   
    bazi=atan2(Td(2,3),Td(1,3)) %atan2(y,x)，y为分子，x为分母
  
    bazid=bazi*180/pi
    if (Td(3,3)>0)
        azid=bazid+180  %相对于x（南）的方位角
   else
    azid=bazid
   end
  azidn=180-azid  %震中方位角
  if azidn<0
      azidn=azidn+360
  end
  disp('射线方位角')
  azi_ray=azidn-180
  if azi_ray<0
      azi_ray=azi_ray+360
  end
  % 计算协方差矩阵所用波形段的起始位置和长度对结果有影响，待研究
%画质点运动图
figure(7)

disp('nstart=')
nstart
nmax
plot(y2(nstart:nstart+nmax),y1(nstart:nstart+nmax))
title('被分析波形段的水平质点运动')
xlabel('西---东/counts')
ylabel('南---北/counts')
%plot(y2(nstart:nstart+nmax))
%去掉受噪声影响的部分
j=0;
for i=1:nlen
    angle(i)=atan2(y2(i+nstart),y1(i+nstart))*180/pi;
   if angle(i)<0 
       angle(i)=angle(i)+180;
   end
    diff=abs(angle(i)-azidn);
    diff1=abs(angle(i)-azidn+360);
    %if (diff<100|| diff1<10) &(abs(y1(i+1000))>40||abs(y2(i+1000))>40)
        j=j+1;
        y11(j)=y1(i+nstart);
        y22(j)=y2(i+nstart);
        y33(j)=y3(i+nstart);
    %end
    
end
num=j
for i=1:num
    angle1(i)=atan2(y22(i),y11(i))*180/pi;
    if angle1(i)<0 angle1(i)=angle1(i)+180;
    end
end
figure(8)
plot(angle1(1:num))  %瞬时方位角
title('瞬时射线方位角随时间的变化')
xlabel('被分析波形段的采样序号')
ylabel('射线方位角/度')


