function [tk,tkdb,sx,sy,tkmax]=beampacking_func(array,wave,num_cha,f1,f2,tstart,twindow,sample_rate,sx1,sx2,sxpoint,sy1,sy2,sypoint)
%fk_func ����F-Kͼ
%   Detailed explanation goes here
array

nstart=round(tstart*sample_rate)
nwindow=round(twindow*sample_rate)
if1=floor(f1*twindow)
if2=ceil(f2*twindow)

dsx=(sx2-sx1)/(sxpoint-1) 
dsy=(sy2-sy1)/(sypoint-1)
for i=1:sxpoint
      sxx(i)=sx1+dsx*i;
end
for i=1:sypoint
      syy(i)=sy1+dsy*i;
end
  tkmax=0; 
%��������ɨ��
for i=1:sxpoint
    for j=1:sypoint
      
           
         
         for m=1:nwindow
             beam(m)=0;
             for k=1:num_cha
                 delayij=round((sxx(i)*array(k,1)+syy(j)*array(k,2))*sample_rate);
                wavek(k,m)=wave(k,m+nstart);
                 beam(m)=beam(m)+wave(k,m+nstart+delayij);
             end
         end
         fftmean=0;
      
     
      fftij=fft(beam);
      tk(i,j)=0;
      for iff=if1:if2
         tk(i,j)=tk(i,j)+(abs(fftij(iff)))^2;
      end
        
           if tk(i,j)>tkmax
                tkmax=tk(i,j);
                sxmax=sxx(i);
                symax=syy(j);
            end
        
    end
end
tkmax
sxmax
symax
azi=atan2(sxmax,symax)*180/pi
fmid=(f1+f2)/2
sx=sxmax
sy=symax
slowness=sqrt(sx^2+sy^2)
vapparent=1/slowness
 for i=1:sxpoint
     for j=1:sypoint
         tkdb(i,j)=10*log10(tk(i,j)/tkmax);
     end
    
 end

end

