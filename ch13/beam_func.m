function [beamwave,bm,bmdb,sx,sy,bmmax] = beam_func( array,wave,num_cha,t1start,twindow,sample_rate,sx1,sx2,sxpoint,sy1,sy2,sypoint,flag)
%beam_func:台阵波形聚束
% 输入参数：
% array: 以台阵中心为原点的台阵子台直角坐标（km）， array(i,1);X坐标，array(i,2):Y坐标，i为子台序号
% wave：波形数据，wave(i,j),i为子台序号，j为数据点序号
% num_cha：信号道数，单分量处理时等于子台个数
% t1start：进行聚束时的第1道起始时间（秒）（在进入本函数之前，调用程序应人工指定或通过检测算法给出该值）
% twindow： 进行聚束的进行聚束时间窗长度（秒）
% sample_rate: 波形数据的采样率，s/s
% sx1,sx2: 进行聚束扫描时视慢度X分量的最小和最大值
% sxpoint：进行聚束扫描时视慢度X分量的网格点数
% sy1,sy2: 进行聚束扫描时视慢度Y分量的最小和最大值
% sypoint: 进行聚束扫描时视慢度Y分量的网格点数 
% 返回参数
% beamwave：聚束波形
% flag：聚束判据选择：flag=0 能量判据（振幅平方积分），flag=1 振幅判据（振幅绝对值积分）
% bm：聚束判据值
% bmdb：用分贝表示的聚束判据值，以聚束判据值的最大值对应0dB
% sx,sy: 聚束判据值的最大值对应的视慢度X分量和Y分量（s/km）
array;
nwindow=round(twindow*sample_rate)
n1start=round(t1start*sample_rate)
dsx=(sx2-sx1)/(sxpoint-1); 
dsy=(sy2-sy1)/(sypoint-1);
for i=1:sxpoint
      sxx(i)=sx1+dsx*i;
end
for i=1:sypoint
      syy(i)=sy1+dsy*i;
end
  bmmax=0; 
%对视慢度扫描
for i=1:sxpoint
    for j=1:sypoint
            for m=1:nwindow 
                 beam(i,j,m)=0;
            end
            for k=1:num_cha
              tao(k)=(sxx(i)*array(k,1)+syy(j)*array(k,2));
              taom(k)=round(tao(k)*sample_rate);
              
              for m=1:nwindow 
                  beam(i,j,m)= beam(i,j,m)+wave(k,m+n1start+taom(k));
              end
           end
        bm(i,j)=0;
           for m=1:nwindow
             if flag==0
                bm(i,j)=bm(i,j)+beam(i,j,m)^2;
             end
             if flag==1
                bm(i,j)=bm(i,j)+abs(beam(i,j,m));
             end
           end
           if bm(i,j)>bmmax
                bmmax=bm(i,j);
                sxmax=sxx(i);
                symax=syy(j);
                bmi=i;
                bmj=j;
            end
        
    end
end
bmmax;



for m=1:nwindow
    beamwave(m+n1start)=beam(bmi,bmj,m)/num_cha;
end
azi=atan2(sxmax,symax)*180/pi
sx=sxmax;
sy=symax;
slowness=sqrt(sx^2+sy^2)

vapparent=1/slowness
 for i=1:sxpoint
     for j=1:sypoint
         bmdb(i,j)=10*log10(bm(i,j)/bmmax);
     end
    
 end

end





