function [wave_R,wave_T,wave_L,wave_Q] = ZNE2RTLQ(wave_ud,wave_ns,wave_ew,azi,inci_a)
%计算径向分量和横向分量
%   输入：垂直向波形wave_ud,北南向波形wave_ns和东西向波形wave_ew
%         射线方位角azi(单位弧度)，地震波视入射角inci_a，
%   输出：径向波形wave_r，横向波形wave_t
%计算公式：《地震记录处理》第2章 （2.41）
bazi=azi+pi;   % 反方位角
if bazi>2*pi
    bazi=bazi-pi;
end
wave_R=wave_ns*cos(bazi)+wave_ew*sin(bazi);  %径向分量的正向指向震中
wave_T=-wave_ns*sin(bazi)+wave_ew*cos(bazi); %横向分量正向为径向分量顺时钟转90度方向。
wave_L=wave_ud*cos(inci_a)-wave_R*sin(inci_a);%L分量正向为射线方向
wave_Q=wave_ud*sin(inci_a)+wave_R*cos(inci_a);%Q分量正向指向震中

end

