%坐标文件 array_fuqing.x(东,km）; array_fuqing.y（北,km）; array_fuqing.z（下，km);
%array_fuqing.lat(纬度，度.度）；array_fuqing.lon（经度，度.度）

clear;close all
% 读入坐标
lat=[25.38;25.38;25.39;25.38;25.3876;25.38;25.38;25.39;25.3913;25.40;25.39;25.38;25.39;25.38;25.37;25.37;25.40;]
lon=[119.61;119.62;119.62;119.61;119.62;119.62;119.61;119.61;119.62;119.61;119.60;119.60;119.63;119.63;119.62;119.62;119.62;]
lat0=lat(1);
cos_lat0=cos(lat(1));
for n=1:1:17
    lat1(n)=lat(n)-lat0;
    y(n)=lat1(n)*111.119;
end
lon0=lon(1);
for n=1:1:17
    lon1(n)=lon(n)-lon0;
    x(n)=lon1(n)*111.119*cos(lat(n));
end
for i=1:4
    array(i,1)=x(i);
    array(i,2)=y(i);
end
for i=5:16
    array(i,1)=x(i+1);
    array(i,2)=y(i+1);
end
%--------------------------------------------------------------------------------
%计算理论到时差
num_cha=3  %道数
sample_rate=50 % s/s
azi_d=40   %设定方位角40°
azi_c=azi_d*pi/180;
vapp=8  %设定视速度 8km/s
slow=1/vapp
slowx=slow*sin(azi_c);
slowy=slow*cos(azi_c);

for i=1:num_cha
    arit(i)=(slowx*array(i,1)+slowy*array(i,2)); %计算到时差
    del(i)=floor(arit(i)*sample_rate);  %换算成采样点数
end
%--------------------------------------------------------------------------------
%计算理论水平分量
%视出射角b,视慢度slow，真出射角b0，入射角a0，sin(a0)=vp/vapp,vp是P波真速度。
vp=6.0  %设真速度为6.0km/s
vpvs=1.73  %P与S的波速比
a0=asin(vp/vapp) 
a0d=a0*180/pi
b0=0.5*pi-a0
b0d=b0*180/pi
b=acos(cos(b0)/vpvs)*2-pi/2 %视出射角公式
bd=b*180/pi
%径向分量与垂直分量的比值v2r
v2r=tan(b)
%---------------------------------------------------------------------------
%假想信号
for i=1:300
    for j=1:num_cha
        testwave(j,i)=0;
    end
end
c=10000    %设定信号最大振幅
snr=10     %设定信噪比
d=c/snr    %设定噪声方差
for i=1:300
    for j=1:num_cha      %加入随机噪声
        testwave(j,i)=d*randn(1);
        testwave_ns(j,i)=d*randn(1);
        testwave_ew(j,i)=d*randn(1);
    end
end
for j=1:num_cha
    meanj(j)=mean(testwave(j,1:50));
    rmsj(j)=0;
    for i=1:50
        rmsj(j)=rmsj(j)+(testwave(j,i)-meanj(j))^2;
    end
    rmsj(j)=sqrt(rmsj(j)/50);   %计算由随机噪声得到的噪声均方根
end
%---------------------------------------------------------------------
% 生成带随机噪声的合成地震波形：带指数衰减的正弦波
starti=81
for i=starti:280
    testwave(1,i)=c*sin(2*pi*5*i/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
end
for i=81:280
    for j=1:num_cha
        testwave(j,i+del(j))=c*sin(2*pi*5*i/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
    end
end
starti=81
for i=starti:280
    testwave_ns(1,i)=(c*sin(2*pi*5*i/sample_rate)*exp(-0.01*(i-starti)))*v2r*cos(azi_c)+d*randn(1);
end
for i=81:280
    for j=2:num_cha
        testwave_ns(j,i+del(j))=c*sin(2*pi*5*i/sample_rate)*exp(-0.01*(i-starti))*v2r*cos(azi_c)+d*randn(1);
    end
end
starti=81
for i=starti:280
    testwave_ew(1,i)=c*sin(2*pi*5*i/sample_rate)*exp(-0.01*(i-starti))*v2r*sin(azi_c)+d*randn(1);
end
for i=81:280
    for j=2:num_cha
        testwave_ew(j,i+del(j))=c*sin(2*pi*5*i/sample_rate)*exp(-0.01*(i-starti))*v2r*sin(azi_c)+d*randn(1);
    end
end
figure(4)
%绘合成波形图（垂直向）
for j=1:num_cha
    plot(testwave(j,:)+c*(j-1)); hold on
end
xlabel('采样点序号（50s/s)')
title('合成波形'); hold off
for i=1:num_cha
    maxi(i)=0;
    meani(i)=mean(testwave(i,1:300));
    for j=1:300
        if abs(testwave(i,j)-meani(i))>maxi(i)
            maxi(i)=abs(testwave(i,j)-meani(i));
        end
    end
end
test_maxi=maxi
for i=1:num_cha
    test_snr(i)=test_maxi(i)/rmsj(i);
end
test_snr    %各道合成信号的信噪比
snr_mean=mean(test_snr)   %各道信号信噪比的平均值
for j=1:300
    testwave_sum(j)=0;
    simple_sum(j)=0;
end

%=========================================================================
%三分量偏振分析
tmax=2   %时间窗长度2s
nmax=tmax*sample_rate %信号窗长

for ich=1: num_cha
    y1=testwave_ns(ich,:);   %北南
    y2=testwave_ew(ich,:);   %东西
    y3=testwave(ich,:);      %上下
    %三分量偏振分析
    disp('三分量偏振分析')
    nstart=81+del(ich)    %第ich道计算协方差矩阵起点
    %针对本例中的数据，P初动到时在第0点开始，以此起算协方差矩阵

    for jj=1:nmax
        a(1,jj)=-y1(jj+nstart);
        %北南向，加负号，成为x(指南）为正方向，形成x（南）y（东）z（上）右旋坐标系
        a(2,jj)=y2(jj+nstart); %东西向
        a(3,jj)=y3(jj+nstart); %垂直向
    end
    nlen=nmax;

    for ii=1:3  %计算各分量的均值rmean(ii)  
        rmean(ii)=0;
        for n=1:nlen
            rmean(ii)=rmean(ii)+a(ii,n);
        end
        rmean1(ii,nlen)=rmean(ii)/nlen;
    end
    for ii=1:3
        for jj=1:3
            cov(ii,jj)=0;
            for n=1:nlen
                cov(ii,jj)=cov(ii,jj)+(a(ii,n)-rmean1(ii,nlen))*(a(jj,n)-rmean1(jj,nlen)); %计算协方差
            end
            cov1(ii,jj,nlen)=cov(ii,jj)/nlen;
        end
   end

    [T,E] = eig(cov)  %计算协方差矩阵的特征向量和特征值
    Td = double(T)  %矩阵中的列向量为特征向量,已经归一化，因此是该向量的方向余弦
    Ed = double(E)  %矩阵中的对角线元素为相应的特征值，由小到大排列
    % 因此，公式中的主偏振方向对应于第3列中的向量，即Td(1,3),Td(2,3),Td(3,3)
    %计算方位角
    Td(1,3)
    Td(2,3)
   
    bazi=atan2(Td(2,3),Td(1,3)) %atan2(y,x)，y为分子，x为分母
  
    bazid=bazi*180/pi
    if (Td(3,3)>0)
        azid=bazid+180  %相对于x（南）的方位角
    else
        azid=bazid
    end
    azidn=180-azid  %震中方位角
    if azidn<0
        azidn=azidn+360
    end
    disp('射线方位角')
    azi_ray=azidn-180
    if azi_ray<0
        azi_ray=azi_ray+360
    end
    azi_rayi(ich)=azi_ray;
    %计算协方差矩阵所用波形段的起始位置和长度对结果有影响
    %画质点运动图
    figure(10+ ich)
    plot(y2(nstart:nstart+nmax),y1(nstart:nstart+nmax))
    hold on
    plot(y2(nstart),y1(nstart),'r*')
    title('被分析波形段的水平质点运动')
    xlabel('西---东/counts')
    ylabel('南---北/counts')
    %去掉受噪声影响的部分
    j=0;
    for i=1:nlen
        angle(i)=atan2(y2(i+nstart),y1(i+nstart))*180/pi;
        if angle(i)<0 
            angle(i)=angle(i)+180;
        end
        diff=abs(angle(i)-azidn);
        diff1=abs(angle(i)-azidn+360);

        j=j+1;
        y11(j)=y1(i+nstart);
        y22(j)=y2(i+nstart);
        %y33(j)=y3(i+nstart);
    end
    num=j
    for i=1:num
        angle1(i)=atan2(y22(i),y11(i))*180/pi;
        if angle1(i)<0 
            angle1(i)=angle1(i)+180;
        end
    end

    figure(30+ich)
    plot(angle1(1:num))  %瞬时方位角
    title('瞬时射线方位角随时间的变化')
    xlabel('被分析波形段的采样序号')
    ylabel('射线方位角/度')

end


