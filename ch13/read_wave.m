function [waveud,wavens,waveew] = read_wave( num_cha,sample_rate,twindow,fileflag,filename)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
if fileflag==1
%读入福州台阵ud波形
num_data=sample_rate*twindow
load st02007110122.ud
waveud(1,:)=st02007110122(1:num_data);
load st12007110122.ud
waveud(2,:)=st12007110122(1:num_data);
load st22007110122.ud
waveud(3,:)=st22007110122(1:num_data);
load st32007110122.ud
waveud(4,:)=st32007110122(1:num_data);
%没有st42007110122
load st52007110122.ud
waveud(5,:)=st52007110122(1:num_data);
load st62007110122.ud
waveud(6,:)=st62007110122(1:num_data);
load st72007110122.ud
waveud(7,:)=st72007110122(1:num_data);
load st82007110122.ud
waveud(8,:)=st82007110122(1:num_data);
load st92007110122.ud
waveud(9,:)=st92007110122(1:num_data);
load s102007110122.ud
waveud(10,:)=s102007110122(1:num_data);
load s112007110122.ud
waveud(11,:)=s112007110122(1:num_data);
load s122007110122.ud
waveud(12,:)=s122007110122(1:num_data);
load s132007110122.ud
waveud(13,:)=s132007110122(1:num_data);
load s142007110122.ud
waveud(14,:)=s142007110122(1:num_data);
load s152007110122.ud
waveud(15,:)=s152007110122(1:num_data);
load s172007110122.ud
waveud(16,:)=s172007110122(1:num_data);

for j=1:num_data
     waveud_sum(j)=0;
for i=1:num_cha
    waveud_sum(j)=waveud_sum(j)+waveud(i,j);
end
waveud_sum(j)=waveud_sum(j)/num_cha;
end
figure(91)
plot(waveud(1,:)); hold on
plot(waveud(2,:)+10000); hold on
plot(waveud(3,:)+10000*2); hold on
plot(waveud(4,:)+10000*3); hold on
plot(waveud(5,:)+10000*4); hold on
plot(waveud(6,:)+10000*5); hold on
plot(waveud(7,:)+10000*6); hold on
plot(waveud(8,:)+10000*7); hold on
plot(waveud(9,:)+10000*8); hold on
plot(waveud(10,:)+10000*9); hold on
plot(waveud(11,:)+10000*10); hold on
plot(waveud(12,:)+10000*11); hold on
plot(waveud(13,:)+10000*12); hold on
plot(waveud(14,:)+10000*13); hold on
plot(waveud(15,:)+10000*14); hold on
plot(waveud(16,:)+10000*15);hold on
plot(waveud_sum+10000*16); hold on
xlabel('采样点序号（50s/s)')
title('福州台阵2007110122地震垂直向分量波形'); hold off
%读入福州台阵ns波形
load st02007110122.ns
wavens(1,:)=st02007110122(1:num_data);
load st12007110122.ns
wavens(2,:)=st12007110122(1:num_data);
load st22007110122.ns
wavens(3,:)=st22007110122(1:num_data);
load st32007110122.ns
wavens(4,:)=st32007110122(1:num_data);
%没有st42007110122
load st52007110122.ns
wavens(5,:)=st52007110122(1:num_data);
load st62007110122.ns
wavens(6,:)=st62007110122(1:num_data);
load st72007110122.ns
wavens(7,:)=st72007110122(1:num_data);
load st82007110122.ns
wavens(8,:)=st82007110122(1:num_data);
load st92007110122.ns
wavens(9,:)=st92007110122(1:num_data);
load s102007110122.ns
wavens(10,:)=s102007110122(1:num_data);
load s112007110122.ns
wavens(11,:)=s112007110122(1:num_data);
load s122007110122.ns
wavens(12,:)=s122007110122(1:num_data);
load s132007110122.ns
wavens(13,:)=s132007110122(1:num_data);
load s142007110122.ns
wavens(14,:)=s142007110122(1:num_data);
load s152007110122.ns
wavens(15,:)=s152007110122(1:num_data);
load s172007110122.ns
wavens(16,:)=s172007110122(1:num_data);

for j=1:num_data
     wavens_sum(j)=0;
for i=1:num_cha
    wavens_sum(j)=wavens_sum(j)+wavens(i,j);
end
wavens_sum(j)=wavens_sum(j)/num_cha;
end
figure(92)
plot(wavens(1,:)); hold on
plot(wavens(2,:)+10000); hold on
plot(wavens(3,:)+10000*2); hold on
plot(wavens(4,:)+10000*3); hold on
plot(wavens(5,:)+10000*4); hold on
plot(wavens(6,:)+10000*5); hold on
plot(wavens(7,:)+10000*6); hold on
plot(wavens(8,:)+10000*7); hold on
plot(wavens(9,:)+10000*8); hold on
plot(wavens(10,:)+10000*9); hold on
plot(wavens(11,:)+10000*10); hold on
plot(wavens(12,:)+10000*11); hold on
plot(wavens(13,:)+10000*12); hold on
plot(wavens(14,:)+10000*13); hold on
plot(wavens(15,:)+10000*14); hold on
plot(wavens(16,:)+10000*15);hold on
plot(wavens_sum+10000*16); hold on
xlabel('采样点序号（50s/s)')
title('福州台阵2007110122地震北南分量波形'); hold off
%读入福州台阵ew波形
load st02007110122.ew
waveew(1,:)=st02007110122(1:num_data);
load st12007110122.ew
waveew(2,:)=st12007110122(1:num_data);
load st22007110122.ew
waveew(3,:)=st22007110122(1:num_data);
load st32007110122.ew
waveew(4,:)=st32007110122(1:num_data);
%没有st42007110122
load st52007110122.ew
waveew(5,:)=st52007110122(1:num_data);
load st62007110122.ew
waveew(6,:)=st62007110122(1:num_data);
load st72007110122.ew
waveew(7,:)=st72007110122(1:num_data);
load st82007110122.ew
waveew(8,:)=st82007110122(1:num_data);
load st92007110122.ew
waveew(9,:)=st92007110122(1:num_data);
load s102007110122.ew
waveew(10,:)=s102007110122(1:num_data);
load s112007110122.ew
waveew(11,:)=s112007110122(1:num_data);
load s122007110122.ew
waveew(12,:)=s122007110122(1:num_data);
load s132007110122.ew
waveew(13,:)=s132007110122(1:num_data);
load s142007110122.ew
waveew(14,:)=s142007110122(1:num_data);
load s152007110122.ew
waveew(15,:)=s152007110122(1:num_data);
load s172007110122.ew
waveew(16,:)=s172007110122(1:num_data);

for j=1:num_data
     waveew_sum(j)=0;
for i=1:num_cha
    waveew_sum(j)=waveew_sum(j)+waveew(i,j);
end
waveew_sum(j)=waveew_sum(j)/num_cha;
end
figure(93)
plot(waveew(1,:)); hold on
plot(waveew(2,:)+10000); hold on
plot(waveew(3,:)+10000*2); hold on
plot(waveew(4,:)+10000*3); hold on
plot(waveew(5,:)+10000*4); hold on
plot(waveew(6,:)+10000*5); hold on
plot(waveew(7,:)+10000*6); hold on
plot(waveew(8,:)+10000*7); hold on
plot(waveew(9,:)+10000*8); hold on
plot(waveew(10,:)+10000*9); hold on
plot(waveew(11,:)+10000*10); hold on
plot(waveew(12,:)+10000*11); hold on
plot(waveew(13,:)+10000*12); hold on
plot(waveew(14,:)+10000*13); hold on
plot(waveew(15,:)+10000*14); hold on
plot(waveew(16,:)+10000*15);hold on
plot(waveew_sum+10000*16); hold on
xlabel('采样点序号（50s/s)')
title('福州台阵2007110122地震东西分量波形'); hold off
end
if fileflag==2   %读入合成波形 
   disp(filename)
end
end

