function [ waveud_f,wavens_f,waveew_f] = bandpass(a,f,m,n,waveud,wavens,waveew,num_cha,alpha)
%带通滤波

%设计带通滤波器
% a=[1,0]; %FIR滤波器的分母系数

% f=[0,0.04,0.04,1];m=[0,0,1,1];
%高通滤波器:拐角频率为0.04*采样率/2=0.04*50/2=1Hz；m给出相应的幅度
%利用Matlab中的帮助 help fir2; help filter; help filtfilt
%参考书：《Matlab及在电子信息课程中的应用》 第7章    
% n=0:30; 30  为滤波器的阶
%利用Matlab中的信号处理工具箱中的FIR滤波器设计工具fir2生成递归滤波器系数b,
b=fir2(30,f,m);
figure(81)

subplot(1,2,1);stem(n,b,'.')
xlabel('n');ylabel('h(n)');
axis([0,30,-0.4,0.5]),line([0,30],[0,0])
[h,w]=freqz(b,1,256);
subplot(1,2,2);plot(w*25/pi,20*log10(abs(h)));grid
%axis([0,1,-80,0]);xlabel('w/pi');ylabel('幅度(dB)');
axis([0,20,-80,0]);xlabel('频率/Hz');ylabel('幅度(dB)');
for i=1:num_cha
  waveud_f(i,:)=filtfilt(b,a,waveud(i,:))*alpha;
 
wavens_f(i,:)=filtfilt(b,a,wavens(i,:))*alpha;
 
  waveew_f(i,:)=filtfilt(b,a,waveew(i,:))*alpha;
 
end

end

