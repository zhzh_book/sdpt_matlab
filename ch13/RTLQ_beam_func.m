function [b_ud,b_ns,b_ew,b_R,b_T,b_L,b_Q,bm_ud,bm_ns,bm_ew,bm_R,bm_T,bm_L,bm_Q,....
    sx_max,sy_max,bm_i,bm_j,bm_max,bw] = ....
    RTLQ_beam_func( array,w_ud,w_ns,w_ew,num_cha,t1start,tw,....
waveud_f_sum,wavens_f_sum,waveew_f_sum,s_rate,sx1,sx2,sxp,sy1,sy2,syp,flag,flag_ps,vp,vs)
%beam_func:台阵波形聚束
% 输入参数：
% array: 以台阵中心为原点的台阵子台直角坐标（km）， array(i,1);X坐标，array(i,2):Y坐标，i为子台序号
% wave：波形数据，wave(i,j),i为子台序号，j为数据点序号
% num_cha：信号道数，单分量处理时等于子台个数
% t1start：进行聚束时的第1道起始时间（秒）（在进入本函数之前，调用程序应人工指定或通过检测算法给出该值）
% twindow： 进行聚束的进行聚束时间窗长度（秒）
% sample_rate: 波形数据的采样率，s/s
% sx1,sx2: 进行聚束扫描时视慢度X分量的最小和最大值
% sxpoint：进行聚束扫描时视慢度X分量的网格点数
% sy1,sy2: 进行聚束扫描时视慢度Y分量的最小和最大值
% sypoint: 进行聚束扫描时视慢度Y分量的网格点数 
% 返回参数
% beamwave：聚束波形
% flag：聚束判据选择：flag=0 能量判据（振幅平方积分），flag=1 振幅判据（振幅绝对值积分）
% bm：聚束判据值
% bmdb：用分贝表示的聚束判据值，以聚束判据值的最大值对应0dB
% sx,sy: 聚束判据值的最大值对应的视慢度X分量和Y分量（s/km）
array;
nwindow=round(tw*s_rate);
n1start=round(t1start*s_rate);
dsx=(sx2-sx1)/(sxp-1); 
dsy=(sy2-sy1)/(syp-1);
for i=1:sxp
      sxx(i)=sx1+dsx*i;
end
for i=1:syp
      syy(i)=sy1+dsy*i;
end
for k=1:7
  bm_max(k)=0; 
end

%对视慢度扫描
for i=1:sxp
    for j=1:syp
        azi=atan2(sxx(i),syy(j));  %震中方位角
if azi<0
    azi=azi+2*pi;
end
azi_d=azi*180/pi;
%针对P波
sin_inci_p=vp*(sqrt(sxx(i)^2+syy(j)^2));
if sin_inci_p>1
    sin_inci_p=1;
end
inci_a_p=asin(sin_inci_p);  %假设视入射角=真出射角
%针对S波
sin_inci_s=vs*(sqrt(sxx(i)^2+syy(j)^2));
if sin_inci_s>1
    sin_inci_s=1;
end
inci_a_s=asin(sin_inci_s); 
if flag_ps==0
    inci_a=inci_a_p;
else
    inci_a=inci_a_s;
end
inci_a_d=inci_a*180/pi;
%偏振滤波
for ii=1:num_cha
wave_ud=w_ud(ii,:);
wave_ns=w_ns(ii,:);
wave_ew=w_ew(ii,:);
[wave_R,wave_T,wave_L,wave_Q] = ZNE2RTLQ(wave_ud,wave_ns,wave_ew,azi,inci_a);
w_R(ii,:)=wave_R;
w_T(ii,:)=wave_T;
w_L(ii,:)=wave_L;
w_Q(ii,:)=wave_Q;
end
            for m=1:nwindow 
                 b_ud(i,j,m)=0;
                  b_ns(i,j,m)=0;
                   b_ew(i,j,m)=0;
                    b_R(i,j,m)=0;
                     b_T(i,j,m)=0;
                      b_L(i,j,m)=0;
                       b_Q(i,j,m)=0;
                      
                      
            end
            for k=1:num_cha
              tao(k)=(sxx(i)*array(k,1)+syy(j)*array(k,2));
              taom(k)=round(tao(k)*s_rate);
              
              for m=1:nwindow 
                  b_ud(i,j,m)= b_ud(i,j,m)+w_ud(k,m+n1start+taom(k));
                  b_ns(i,j,m)= b_ns(i,j,m)+w_ns(k,m+n1start+taom(k));
                  b_ew(i,j,m)= b_ew(i,j,m)+w_ew(k,m+n1start+taom(k));
                  b_R(i,j,m)= b_R(i,j,m)+w_R(k,m+n1start+taom(k));
                  b_T(i,j,m)= b_T(i,j,m)+w_T(k,m+n1start+taom(k));
                  b_L(i,j,m)= b_L(i,j,m)+w_L(k,m+n1start+taom(k));
                  b_Q(i,j,m)= b_Q(i,j,m)+w_Q(k,m+n1start+taom(k));
                 
                       
              end
            end
            ud_sum=0;
            ns_sum=0;
            ew_sum=0;
           for m=1:nwindow 
         ud_sum=ud_sum+abs(waveud_f_sum(m+n1start));
                       ns_sum=ns_sum+abs(wavens_f_sum(m+n1start));
                       ew_sum=ew_sum+abs(waveew_f_sum(m+n1start));
           end
        bm_ud(i,j)=0;
        bm_ns(i,j)=0;
        bm_ew(i,j)=0;
        bm_R(i,j)=0;
        bm_T(i,j)=0;
        bm_L(i,j)=0;
        bm_Q(i,j)=0;
           for m=1:nwindow
             if flag==0
        bm_ud(i,j)=bm_ud(i,j)+b_ud(i,j,m)^2;
        bm_ns(i,j)=bm_ns(i,j)+b_ns(i,j,m)^2;
        bm_ew(i,j)=bm_ew(i,j)+b_ew(i,j,m)^2;
        bm_R(i,j)=bm_R(i,j)+b_R(i,j,m)^2;
        bm_T(i,j)=bm_T(i,j)+b_T(i,j,m)^2;
        bm_L(i,j)=bm_L(i,j)+b_L(i,j,m)^2;
        bm_Q(i,j)=bm_Q(i,j)+b_Q(i,j,m)^2;
               
             end
             if flag==1
        bm_ud(i,j)=bm_ud(i,j)+abs(b_ud(i,j,m));
        bm_ns(i,j)=bm_ns(i,j)+abs(b_ns(i,j,m));
        bm_ew(i,j)=bm_ew(i,j)+abs(b_ew(i,j,m));
        bm_R(i,j)=bm_R(i,j)+abs(b_R(i,j,m));
        bm_T(i,j)=bm_T(i,j)+abs(b_T(i,j,m));
        bm_L(i,j)=bm_L(i,j)+abs(b_L(i,j,m));
        bm_Q(i,j)=bm_Q(i,j)+abs(b_Q(i,j,m));
        
             end
    %尝试归一化
   %       bm_ud(i,j)=bm_ud(i,j)/ud_sum;
    %    bm_ns(i,j)=bm_ns(i,j)/ns_sum;
     %   bm_ew(i,j)=bm_ew(i,j)/ew_sum;   
           end
           if bm_ud(i,j)>bm_max(1)
               
                bm_max(1)=bm_ud(i,j);
                sx_max(1)=sxx(i);
                sy_max(1)=syy(j);
                bm_i(1)=i;
                bm_j(1)=j;
           end
            if bm_ns(i,j)>bm_max(2)
               
               bm_max(2)=bm_ns(i,j);
                sx_max(2)=sxx(i);
                sy_max(2)=syy(j);
                bm_i(2)=i;
                bm_j(2)=j;
            end
            if bm_ew(i,j)>bm_max(3)
              
                bm_max(3)=bm_ew(i,j);
                sx_max(3)=sxx(i);
                sy_max(3)=syy(j);
                bm_i(3)=i;
                bm_j(3)=j;
            end
            if bm_R(i,j)>bm_max(4)
                bm_max(4)=bm_R(i,j);
                sx_max(4)=sxx(i);
                sy_max(4)=syy(j);
                bm_i(4)=i;
                bm_j(4)=j;
            end
            if bm_T(i,j)>bm_max(5)
                bm_max(5)=bm_T(i,j);
                sx_max(5)=sxx(i);
                sy_max(5)=syy(j);
                bm_i(5)=i;
                bm_j(5)=j;
            end
            if bm_L(i,j)>bm_max(6)
                bm_max(6)=bm_L(i,j);
                sx_max(6)=sxx(i);
                sy_max(6)=syy(j);
                bm_i(6)=i;
                bm_j(6)=j;
            end
            if bm_Q(i,j)>bm_max(7)
                bm_max(7)=bm_Q(i,j);
                sx_max(7)=sxx(i);
                sy_max(7)=syy(j);
                bm_i(7)=i;
                bm_j(7)=j;
            end
        
    end
end




for m=1:nwindow
    bw(1,m+n1start)=b_ud( bm_i(1), bm_j(1),m)/num_cha;
    bw(2,m+n1start)=b_ns( bm_i(2), bm_j(2),m)/num_cha;
    bw(3,m+n1start)=b_ew( bm_i(3), bm_j(3),m)/num_cha;
    bw(4,m+n1start)=b_R( bm_i(4), bm_j(4),m)/num_cha;
    bw(5,m+n1start)=b_T( bm_i(5), bm_j(5),m)/num_cha;
    bw(6,m+n1start)=b_L( bm_i(6), bm_j(6),m)/num_cha;
    bw(7,m+n1start)=b_Q( bm_i(7), bm_j(7),m)/num_cha;
end
        



end





