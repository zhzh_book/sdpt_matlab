function [fk,fkdb,sx,sy,fkmax,fftmax,fk_norm,fk_norm_max]=fk_func_norm(array,fkwave,num_cha,f1,f2,tstart,twindow,sample_rate,sx1,sx2,sxpoint,sy1,sy2,sypoint)

%fk_func 计算F-K图
%   Detailed explanation goes here
array;
%figure(11)
nstart=round(tstart*sample_rate);
nwindow=round(twindow*sample_rate);
if1=floor(f1*twindow);
if2=ceil(f2*twindow);
for i=1:nwindow
    f(i)=i*sample_rate/nwindow;
end
for i=1:num_cha
    for j=1:nwindow
    wavetemp(j)=fkwave(i,j+nstart);
    end
    ffttemp=fft(wavetemp);
    %plot(f,abs(ffttemp)); hold on
    for j=1:nwindow
        fftch(i,j)=ffttemp(j);
    end
    le=length(ffttemp); 
end
%xlabel('频率/Hz')
%hold off

dsx=(sx2-sx1)/(sxpoint-1) 
dsy=(sy2-sy1)/(sypoint-1)
for i=1:sxpoint
      sxx(i)=sx1+dsx*i;
end
for i=1:sypoint
      syy(i)=sy1+dsy*i;
end
fftmax=0;
  fkmax=0; 
  fk_norm_max=0;
%对视慢度扫描
for i=1:sxpoint
    for j=1:sypoint
       for m=1:nwindow 
           f=m*sample_rate/nwindow;
        for k=1:num_cha
        phaseij=2*pi*(sxx(i)*array(k,1)+syy(j)*array(k,2));
          
        psdtemp(k,m)=fftch(k,m)*(cos(phaseij*f)+sin(phaseij*f)*sqrt(-1));
          end
        end
        for m=1:nwindow
        psd(m)=0;
        fftsum(m)=0;
          for k=1:num_cha
            psd(m)=psd(m)+psdtemp(k,m);
            fftsum(m)=fftsum(m)+abs(fftch(k,m))^2;
          end
        end
        fk(i,j)=0;
       ffts(i,j)=0;
        for m=if1:if2
            fk(i,j)=fk(i,j)+(abs(psd(m)))^2;
            ffts(i,j)=ffts(i,j)+fftsum(m);
        end
        fk_norm(i,j)=fk(i,j)/ffts(i,j);
           if fk(i,j)>fkmax
                fkmax=fk(i,j);
               
                sxmax=sxx(i);
                symax=syy(j);
           end
           if fk_norm(i,j)>fk_norm_max
               fk_norm_max=fk_norm(i,j);
           end
           if ffts(i,j)>fftmax
             fftmax=ffts(i,j);
           end
        
    end
end
fkmax;
sxmax;
symax;
fftmax;
azi=atan2(sxmax,symax)*180/pi;
fmid=(f1+f2)/2;
sx=sxmax;
sy=symax;
slowness=sqrt(sx^2+sy^2)
vapparent=1/slowness
 for i=1:sxpoint
     for j=1:sypoint
         fkdb(i,j)=10*log10(fk(i,j)/fkmax);
     end
    
 end

end

