function [vespa_bm] = vespa_pws_func(array,wave,num_cha,tstart,twindow,sample_rate,vespa_slow,vespa_azi,flag,phase,nu)
%vespa_pws_func:相位加权叠加速度谱分析pws-vespagram
% 输入参数：
% array: 以台阵中心为原点的台阵子台直角坐标（km）， array(i,1);X坐标，array(i,2):Y坐标，i为子台序号
% wave：波形数据，wave(i,j),i为子台序号，j为数据点序号
% num_cha：信号道数，单分量处理时等于子台个数
% t1start：进行聚束时的第1道起始时间（秒）（在进入本函数之前，调用程序应人工指定或通过检测算法给出该值）
% twindow： 进行聚束的进行聚束时间窗长度（秒）
% sample_rate: 波形数据的采样率，s/s
% sx1,sx2: 进行聚束扫描时视慢度X分量的最小和最大值
% sxpoint：进行聚束扫描时视慢度X分量的网格点数
% sy1,sy2: 进行聚束扫描时视慢度Y分量的最小和最大值
% sypoint: 进行聚束扫描时视慢度Y分量的网格点数 
% 返回参数
% beamwave：聚束波形
% flag：聚束判据选择：flag=0 能量判据（振幅平方积分），flag=1 振幅判据（振幅绝对值积分）
% bm：聚束判据值
% bmdb：用分贝表示的聚束判据值，以聚束判据值的最大值对应0dB
% sx,sy: 聚束判据值的最大值对应的视慢度X分量和Y分量（s/km）
array;
nwindow=round(twindow*sample_rate);
nstart=round(tstart*sample_rate);
sx=vespa_slow*sin(vespa_azi);
sy=vespa_slow*cos(vespa_azi);
root_1=sqrt(-1);
for m=1:nwindow 
    
    pws(m)=0;
    for k=1:num_cha
        tao(k)=(sx*array(k,1)+sy*array(k,2));
              taom(k)=round(tao(k)*sample_rate);
              mm=m+nstart+taom(k);
    pws(m)=pws(m)+(cos(phase(k,mm))+sin(phase(k,mm))*root_1);
    end
    
    abspws(m)=(abs(pws(m))/num_cha)^nu;
   
end   
            for m=1:nwindow 
                 beam(m)=0;
            end
            for k=1:num_cha
              tao(k)=(sx*array(k,1)+sy*array(k,2));
              taom(k)=round(tao(k)*sample_rate);
              
              for m=1:nwindow 
                  mm=m+nstart+taom(k);
                  
                  beam(m)= beam(m)+wave(k,mm)*abspws(m);
              end
            end
          % figure(6)
          % plot(beam)
        vespa_bm=0;
           for m=1:nwindow
             if flag==0
                vespa_bm=vespa_bm+beam(m)^2;
             end
             if flag==1
                vespa_bm=vespa_bm+abs(beam(m));
             end
           end
          
        
vespa_bm;


end





