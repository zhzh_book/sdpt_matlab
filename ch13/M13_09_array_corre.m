% 台站文件：
%   array_fuqing.lat：福清台阵子台纬度（度.度），第一个数字是子台总数
%   array_fuqing.lon：福清台阵子台经度（度.度），第一个数字是子台总数
%   array_fuqing.x：福清台阵子台x坐标（东，km），第一个数字是子台总数
%   array_fuqing.y：福清台阵子台y坐标（北.km），第一个数字是子台总数
%   array_fuqing.z：福清台阵子台高程（上，km），第一个数字是子台总数
%   array_fuqing.lam：福清台阵子台纬度（度 分.分），第一个数字是子台总数
%   array_fuqing.lom：福清台阵子台经度（度 分.分），第一个数字是子台总数  

%//float ab[50][50],a0;
%int stnum; /* number of stations in the network */
%2007.11.17 赵仲和
%加载台阵坐标,绘制台阵的子台分布图
%图形文件名：array_xx.fig
%台阵坐标文件 array_fuqing.x(东,km）; array_fuqing.y（北,km）; array_fuqing.z（下，km);
%array_fuqing.lat(纬度，度.度）；array_fuqing.lon（经度，度.度）
%array_fuqing.lam(纬度，度 分.分）；array_fuqing.lom (经度，度 分.分）
%
clear;close all
load array_fuqing.lat;
lat=array_fuqing(1:17);
lat0=lat(1);
cos_lat0=cos(lat(1));
for n=1:1:16
    lat1(n)=lat(n)-lat0;
    y(n)=lat1(n)*111.119;
end
load array_fuqing.lon;
lon=array_fuqing(1:17);
lon0=lon(1);
for n=1:1:16
    lon1(n)=lon(n)-lon0;
    x(n)=lon1(n)*111.119*cos(lat(n));
end


%计算3个圆圈的半径：相应子台距中心台距离的平均值
r1=0;
for n=2:1:4
  r1=r1+sqrt((x(n)-x(1))*(x(n)-x(1))+(y(n)-y(1))*(y(n)-y(1)));
end
r2=0;
for n=5:1:9
  r2=r2+sqrt((x(n)-x(1))*(x(n)-x(1))+(y(n)-y(1))*(y(n)-y(1)));
end
r3=0;
for n=10:1:16 %现在在外圈上有8个候选子台
  r3=r3+sqrt((x(n)-x(1))*(x(n)-x(1))+(y(n)-y(1))*(y(n)-y(1)));
end
r1=r1/3;
r2=r2/5;
r3=r3/7;
for n=1:1:360
    x1(n)=r1*sin(n*3.1415926535897932/180);
    y1(n)=r1*cos(n*3.1415926535897932/180);
end
%subplot(1,2,1)
plot(x1,y1,'r-')
hold on
for n=1:1:360
    x1(n)=r2*sin(n*3.1415926535897932/180);
    y1(n)=r2*cos(n*3.1415926535897932/180);
end
plot(x1,y1,'r-')
hold on
for n=1:1:360
    x1(n)=r3*sin(n*3.1415926535897932/180);
    y1(n)=r3*cos(n*3.1415926535897932/180);
end
plot(x1,y1,'r-')
hold on
plot(x,y,'*') %绘子台分布图
hold off
a=[1,0];

f=[0,0.04,0.04,1];m=[0,0,1,1];
%高通滤波器:拐角频率为0.04*采样率/2=0.04*50/2=1Hz；m给出相应的幅度
%利用Matlab中的帮助 help fir2; help filter; help filtfilt
%参考书：《Matlab及在电子信息课程中的应用》 第7章    
%2007-12-03   赵仲和
b=fir2(30,f,m);n=0:30;
%利用Matlab中的信号处理工具箱中的FIR滤波器设计工具fir2生成递归滤波器系数b,
% 30  为滤波器的阶
%subplot(3,2,1);stem(n,b,'.')
%xlabel('n');ylabel('h(n)');
%axis([0,30,-0.4,0.5]),line([0,30],[0,0])
[h,w]=freqz(b,1,256);
%subplot(3,2,2);plot(w/pi,20*log10(abs(h)));grid
%axis([0,1,-80,0]);xlabel('w/pi');ylabel('幅度(dB)');
%计算波形互相关函数
load st02007110122.ud
wave1=st02007110122(1:60*50);
%subplot(2,1,1)
%plot(wave1)
%title('st02007110122.ud原始记录波形')
%xlabel('采样点，起始时间：2007-11-01 22：38，采样率=50s/s')
%ylabel('counts')
filen='s172007110122.ud'
load  s172007110122.ud
wave2=s172007110122(1:60*50);
%subplot(2,1,2)
%plot(wave2)
%title('st02007110122.ud原始记录波形')
%xlabel('采样点，起始时间：2007-11-01 22：38，采样率=50s/s')
%ylabel('counts')

wave1_filtered=filtfilt(b,a,wave1);
%wave1_filtered=wave1;
%for i=1:1:300
 %   wave1_filtered(i+580)=0;
%end
%filtfilt为零相位滤波器，即正向和反向各进行一次滤波
subplot(3,1,1)
plot(wave1_filtered)
title('st02007110122.ud高通滤波后的波形')
xlabel('采样点，起始时间：2007-11-01 22：38，采样率=50s/s')
ylabel('counts')
wave2_filtered=filtfilt(b,a,wave2);
%wave2_filtered=wave2;
%for i=1:1:300
 %   wave2_filtered(i+580)=0;
%end

%filtfilt为零相位滤波器，即正向和反向各进行一次滤波
subplot(3,1,2)
plot(wave2_filtered)
title({filen,'高通滤波后的波形'})
xlabel('采样点，起始时间：2007-11-01 22：38，采样率=50s/s')
ylabel('counts')
num_corre=40 %互相关长度,%自相关长度
for i=1:1:200
    cross_corre(i)=0;
    for j=1:1:num_corre 
   %s10与st0互相关时使用1:1:40
    cross_corre(i)=cross_corre(i)+wave1_filtered(j+535)*wave2_filtered(j+535+i-101);
    end
end
auto_corre1=0;
for j=1:1:num_corre
    %s10与st0互相关时使用1:1:40
    auto_corre1=auto_corre1+wave1_filtered(j+535)*wave1_filtered(j+535);
end
for i=1:1:200
    auto_corre2(i)=0;
    for j=1:1:num_corre  
%s10与st0互相关时使用1:1:40
    auto_corre2(i)=auto_corre2(i)+wave2_filtered(j+535+i-101)*wave2_filtered(j+535+i-101);
    end
end
for i=1:1:200
    cross_corre(i)=cross_corre(i)/(sqrt(auto_corre1)*sqrt(auto_corre2(i)));
    %cross_corre(i)=cross_corre(i)/(sqrt(auto_corre1)*sqrt(auto_corre1)); 
    %s11与st0互相关时使用第2个计算公式
end
corre_max=-100;
for i=1:1:120  %考虑台阵孔径
   % s11与st0互相关时使用1:1:110限定取最大值的区间
    if corre_max<cross_corre(i)
        corre_max=cross_corre(i);
        corre_maxi=i-101;
    end
end
subplot(3,1,3)
plot(cross_corre);
hold on
stem(corre_maxi+101,corre_max,'.')

title(strcat('s17与st0互相关函数 corre-max=',num2str(corre_max),'s17-st0到时差（sec)=',num2str(corre_maxi/50)))
