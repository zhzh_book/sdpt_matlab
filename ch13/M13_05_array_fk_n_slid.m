% 台站文件：
%   array_fuqing.lat：福清台阵子台纬度（度.度），第一个数字是子台总数
%   array_fuqing.lon：福清台阵子台经度（度.度），第一个数字是子台总数
%   array_fuqing.x：福清台阵子台x坐标（东，km），第一个数字是子台总数
%   array_fuqing.y：福清台阵子台y坐标（北.km），第一个数字是子台总数
%   array_fuqing.z：福清台阵子台高程（上，km），第一个数字是子台总数
%   array_fuqing.lam：福清台阵子台纬度（度 分.分），第一个数字是子台总数
%   array_fuqing.lom：福清台阵子台经度（度 分.分），第一个数字是子台总数  

%//float ab[50][50],a0;
%int stnum; /* number of stations in the network */
%2007.11.17 赵仲和
%加载台阵坐标,绘制台阵的子台分布图
%图形文件名：array_xx.fig
%台阵坐标文件 array_fuqing.x(东,km）; array_fuqing.y（北,km）; array_fuqing.z（下，km);
%array_fuqing.lat(纬度，度.度）；array_fuqing.lon（经度，度.度）
%array_fuqing.lam(纬度，度 分.分）；array_fuqing.lom (经度，度 分.分）
%
clear;close all
load array_fuqing.lat;
lat=array_fuqing(1:17);
lat0=lat(1);
cos_lat0=cos(lat(1));
for n=1:1:17
    lat1(n)=lat(n)-lat0;
    y(n)=lat1(n)*111.119;
end
load array_fuqing.lon;
lon=array_fuqing(1:17);
lon0=lon(1);
for n=1:1:17
    lon1(n)=lon(n)-lon0;
    x(n)=lon1(n)*111.119*cos(lat(n));
end
for i=1:4
    array(i,1)=x(i);
    array(i,2)=y(i);
end
for i=5:16
    array(i,1)=x(i+1);
    array(i,2)=y(i+1);
end
%计算理论到时差
num_cha=16  %道数
sample_rate=50 %s/s
azi_d=20
azi_c=azi_d*pi/180;
vapp=8
slow=1/vapp
slowx=slow*sin(azi_c);
slowy=slow*cos(azi_c);

for i=1:num_cha
    arit(i)=(slowx*array(i,1)+slowy*array(i,2));
    del(i)=floor(arit(i)*sample_rate);
end

figure(1)
%计算3个圆圈的半径：相应子台距中心台距离的平均值
r1=0;
for n=2:1:4
  r1=r1+sqrt((x(n)-x(1))*(x(n)-x(1))+(y(n)-y(1))*(y(n)-y(1)));
end
r2=0;
for n=5:1:9
  r2=r2+sqrt((x(n)-x(1))*(x(n)-x(1))+(y(n)-y(1))*(y(n)-y(1)));
end
r3=0;
for n=10:1:16 %现在在外圈上有8个候选子台
  r3=r3+sqrt((x(n)-x(1))*(x(n)-x(1))+(y(n)-y(1))*(y(n)-y(1)));
end
r1=r1/3;
r2=r2/5;
r3=r3/7;
for n=1:1:360
    x1(n)=r1*sin(n*3.1415926535897932/180);
    y1(n)=r1*cos(n*3.1415926535897932/180);
end
%subplot(1,2,1)
plot(x1,y1,'r-')
hold on
for n=1:1:360
    x1(n)=r2*sin(n*3.1415926535897932/180);
    y1(n)=r2*cos(n*3.1415926535897932/180);
end
plot(x1,y1,'r-')
hold on
for n=1:1:360
    x1(n)=r3*sin(n*3.1415926535897932/180);
    y1(n)=r3*cos(n*3.1415926535897932/180);
end
plot(x1,y1,'r-')
hold on
plot(x,y,'*') %绘子台分布图
hold off
a=[1,0];

f=[0,0.04,0.04,1];m=[0,0,1,1];
%高通滤波器:拐角频率为0.04*采样率/2=0.04*50/2=1Hz；m给出相应的幅度
%利用Matlab中的帮助 help fir2; help filter; help filtfilt
%参考书：《Matlab及在电子信息课程中的应用》 第7章    
%2007-12-03   赵仲和
b=fir2(30,f,m);n=0:30;
%利用Matlab中的信号处理工具箱中的FIR滤波器设计工具fir2生成递归滤波器系数b,
% 30  为滤波器的阶
%subplot(3,2,1);stem(n,b,'.')
%xlabel('n');ylabel('h(n)');
%axis([0,30,-0.4,0.5]),line([0,30],[0,0])
[h,w]=freqz(b,1,256);
%subplot(3,2,2);plot(w/pi,20*log10(abs(h)));grid
%axis([0,1,-80,0]);xlabel('w/pi');ylabel('幅度(dB)');
%读入福州台阵波形
load st02007110122.ud
waveud(1,:)=st02007110122(1:60*50);
load st12007110122.ud
waveud(2,:)=st12007110122(1:60*50);
load st22007110122.ud
waveud(3,:)=st22007110122(1:60*50);
load st32007110122.ud
waveud(4,:)=st32007110122(1:60*50);
%没有st42007110122
load st52007110122.ud
waveud(5,:)=st52007110122(1:60*50);
load st62007110122.ud
waveud(6,:)=st62007110122(1:60*50);
load st72007110122.ud
waveud(7,:)=st72007110122(1:60*50);
load st82007110122.ud
waveud(8,:)=st82007110122(1:60*50);
load st92007110122.ud
waveud(9,:)=st92007110122(1:60*50);
load s102007110122.ud
waveud(10,:)=s102007110122(1:60*50);
load s112007110122.ud
waveud(11,:)=s112007110122(1:60*50);
load s122007110122.ud
waveud(12,:)=s122007110122(1:60*50);
load s132007110122.ud
waveud(13,:)=s132007110122(1:60*50);
load s142007110122.ud
waveud(14,:)=s142007110122(1:60*50);
load s152007110122.ud
waveud(15,:)=s152007110122(1:60*50);
load s172007110122.ud
waveud(16,:)=s172007110122(1:60*50);

for j=1:60*50
     waveud_sum(j)=0;
for i=1:16
    waveud_sum(j)=waveud_sum(j)+waveud(i,j);
end
waveud_sum(j)=waveud_sum(j)/16;
end
figure(2)
plot(waveud(1,:)); hold on
plot(waveud(2,:)+10000); hold on
plot(waveud(3,:)+10000*2); hold on
plot(waveud(4,:)+10000*3); hold on
plot(waveud(5,:)+10000*4); hold on
plot(waveud(6,:)+10000*5); hold on
plot(waveud(7,:)+10000*6); hold on
plot(waveud(8,:)+10000*7); hold on
plot(waveud(9,:)+10000*8); hold on
plot(waveud(10,:)+10000*9); hold on
plot(waveud(11,:)+10000*10); hold on
plot(waveud(12,:)+10000*11); hold on
plot(waveud(13,:)+10000*12); hold on
plot(waveud(14,:)+10000*13); hold on
plot(waveud(15,:)+10000*14); hold on
plot(waveud(16,:)+10000*15);hold on
plot(waveud_sum+10000*16); hold on
xlabel('采样点序号（50s/s)')
title('福州台阵2007110122地震波形'); hold off
% F-K分析
f1=4 %Hz
f2=6 %Hz
tstart=1.4 %sec
twindow=2.56 %sec

kx1=-1.0 %1/km
kx2=1.0 %1/km
kxpoint=101
ky1=-1.0
ky2=1.0
kypoint=101

nstart=tstart*sample_rate
nwindow=twindow*sample_rate
nend=nstart+nwindow
for j=1:num_cha
for i=1:nwindow
    fkwave(j,i)=waveud(j,i+nstart);
end
end
size(fkwave) 

figure(3)
plot(array(:,1),array(:,2),'*');

%假想信号
for kk=1:1
    disp('kkkkkkk')
for i=1:300
    testwave(1,i)=0;
    testwave(2,i)=testwave(1,i);
    testwave(3,i)=testwave(1,i);
    testwave(4,i)=testwave(1,i);
    testwave(5,i)=testwave(1,i);
    testwave(6,i)=testwave(1,i);
    testwave(7,i)=testwave(1,i);
    testwave(8,i)=testwave(1,i);
    testwave(9,i)=testwave(1,i);
    testwave(10,i)=testwave(1,i);
    testwave(11,i)=testwave(1,i);
    testwave(12,i)=testwave(1,i);
    testwave(13,i)=testwave(1,i);
    testwave(14,i)=testwave(1,i);
    testwave(15,i)=testwave(1,i);
    testwave(16,i)=testwave(1,i);
end
c=10000
snr=10
d=c/snr
for i=1:300
    for j=1:num_cha
    testwave(j,i)=d*randn(1);
    end
end
for j=1:num_cha
    meanj(j)=mean(testwave(j,1:50));
    rmsj(j)=0;
    for i=1:50
        rmsj(j)=rmsj(j)+(testwave(j,i)-meanj(j))^2;
       
    end
    rmsj(j)=sqrt(rmsj(j)/50);
   
end
 meanj
 rmsj
 starti=81
for i=starti:280
    testwave(1,i)=c*sin(2*pi*5*i/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
end
for i=81:280
    testwave(2,i+del(2))=c*sin(2*pi*5*i/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
    testwave(3,i+del(3))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
    testwave(4,i+del(4))=c*sin(2*pi*5*i/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
    testwave(5,i+del(5))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
    testwave(6,i+del(6))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
    testwave(7,i+del(7))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
    testwave(8,i+del(8))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
    testwave(9,i+del(9))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
    testwave(10,i+del(10))=c*sin(2*pi*5*i/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
    testwave(11,i+del(11))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
    testwave(12,i+del(12))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
    testwave(13,i+del(13))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
    testwave(14,i+del(14))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
    testwave(15,i+del(15))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
    testwave(16,i+del(16))=c*sin(2*pi*5*(i)/sample_rate)*exp(-0.01*(i-starti))+d*randn(1);
end
figure(4)
plot(testwave(1,:)); hold on
plot(testwave(2,:)+10000); hold on
plot(testwave(3,:)+10000*2); hold on
plot(testwave(4,:)+10000*3); hold on
plot(testwave(5,:)+10000*4); hold on
plot(testwave(6,:)+10000*5); hold on
plot(testwave(7,:)+10000*6); hold on
plot(testwave(8,:)+10000*7); hold on
plot(testwave(9,:)+10000*8); hold on
plot(testwave(10,:)+10000*9); hold on
plot(testwave(11,:)+10000*10); hold on
plot(testwave(12,:)+10000*11); hold on
plot(testwave(13,:)+10000*12); hold on
plot(testwave(14,:)+10000*13); hold on
plot(testwave(15,:)+10000*14); hold on
plot(testwave(16,:)+10000*15);hold on
for i=1:num_cha
    maxi(i)=0;
    meani(i)=mean(testwave(i,1:300));
    for j=1:300
        if abs(testwave(i,j)-meani(i))>maxi(i)
            maxi(i)=abs(testwave(i,j)-meani(i));
        end
    end
end
test_maxi=maxi
for i=1:num_cha
test_snr(i)=test_maxi(i)/rmsj(i);
end
test_snr
snr_mean=mean(test_snr)
for j=1:300
    testwave_sum(j)=0;
    simple_sum(j)=0;
end
for j=1:250
for i=1:16
    testwave_sum(j+20)=testwave_sum(j+20)+testwave(i,j+20+del(i));
    simple_sum(j+20)=simple_sum(j+20)+testwave(i,j+20);
end
end
testwave_sum_max=0;
simple_sum_max=0;
test_sum_mean=mean(testwave_sum)/num_cha
simple_sum_mean=mean(simple_sum)/num_cha
for j=1:300
testwave_sum(j)=testwave_sum(j)/num_cha;
simple_sum(j)=simple_sum(j)/num_cha;
if abs(testwave_sum(j)-test_sum_mean)>testwave_sum_max
    testwave_sum_max=abs(testwave_sum(j)-test_sum_mean);
    testwave_sum_maxj=j;
end
if abs(simple_sum(j)-simple_sum_mean)>simple_sum_max
    simple_sum_max=abs(simple_sum(j)-simple_sum_mean);
    simple_sum_maxj=j;
end
end
dmax=testwave_sum_max
dmaxj=testwave_sum_maxj
smax=simple_sum_max
smaxj=simple_sum_maxj
d_rms=0;
s_rms=0;
for i=1:50
        d_rms=d_rms+testwave_sum(i)^2;
        s_rms=s_rms+simple_sum(i)^2;
    end
    d_rms=sqrt(d_rms/50)
    s_rms=sqrt(s_rms/50)
%用信号最大值与信号前噪声rms之比作为信噪比
%
simple_snr=smax/s_rms
sum_imp=simple_snr/snr_mean
beam_snr=dmax/d_rms
dsum_imp=beam_snr/snr_mean
dsum_imp_kk(kk)=dsum_imp;
end
dsum_kk=dsum_imp_kk
dsum_imp_mean=mean(dsum_imp_kk)
imp_max=0;
imp_min=100;
xlabel('采样点序号（50s/s)')
title('福州台阵合成波形'); hold off
%滑动窗F-K分析
disp('滑动F-K分析')
sx1=-0.4 %s/km
sx2=0.4  %s/km
sxpoint=101
sy1=-0.4
sy2=0.4
sypoint=101
f1=4 %Hz
f2=6 %Hz
%tstart=0.8 %sec
twindow=2.56 %sec
%已知信号长度300点，采样率50s/s,信号总长6s。
nwindow=twindow*sample_rate
len=6 
num_len=len*sample_rate
step=0.1 %滑动步长0.1秒
num_step=step*sample_rate
num_slid=floor((num_len-nwindow)/num_step)
%num_slid=3
dkx=(kx2-kx1)/(kxpoint-1) 
dky=(ky2-ky1)/(kypoint-1)

for i=1:kxpoint
      kxx(i)=kx1+dkx*i;
end
for i=1:kypoint
      kyy(i)=ky1+dky*i;
end
dsx=(sx2-sx1)/(sxpoint-1) 
dsy=(sy2-sy1)/(sypoint-1)
for i=1:sxpoint
      sxx(i)=sx1+dsx*i;
end
for i=1:sypoint
      syy(i)=sy1+dsy*i;
end
fk_slid_max=0
for i_slid=1:num_slid
    disp({'滑动序号',i_slid})
    tstart=(i_slid-1)*step
[fk,fkdb,sx,sy,fkmax,fftmax,fk_norm,fk_norm_max]=fk_func_norm(array,testwave,num_cha,f1,f2,tstart,twindow,sample_rate,sx1,sx2,sxpoint,sy1,sy2,sypoint);

for isx=1:sxpoint
    for isy=1:sypoint
        fk_slid(i_slid,isx,isy)=fk(isx,isy);
        fk_slid_norm(i_slid,isx,isy)=fk_norm(isx,isy);
        fkdb_slid(i_slid,isx,isy)=fkdb(isx,isy);
        sx_slid(i_slid)=sx;
        sy_slid(i_slid)=sy;
        fkmax_slid(i_slid)=fkmax;
        fk_norm_max_slid(i_slid)=fk_norm_max;
        fftmax_slid(i_slid)=fftmax;
    end
    if  fkmax_slid(i_slid)>fk_slid_max
        fk_slid_max= fkmax_slid(i_slid);
        i_slid_max=i_slid;
    end
end
end
figure(5)
for i=1:num_slid   
 for isx=1:sxpoint
    for isy=1:sypoint
      
        fkdb(isx,isy)=fkdb_slid(i_slid,isx,isy);
        
    end
    
end   
contourf(sxx,syy, fkdb');
title({'滑动序号',i_slid})
end
for isx=1:sxpoint
    for isy=1:sypoint
        fkdb(isx,isy)=fkdb_slid(i_slid_max,isx,isy);
    end
end
  figure(6)
  size(fkdb)
  fmid=(f2+f1)/2
  %contourf(kxx,kyy,fkdb)
  
contourf(sxx,syy,fkdb'); 
%fkdb(i,j)的i为行号，代表纵坐标位置，j为列号，代表横坐标位置，sxx是横坐标，syy是纵坐标，
%所以在画等值线时应将fkdb转置，即用'表示。当行和列数不等时，便可发现这一问题。
sx=sx_slid(i_slid_max);
sy=sy_slid(i_slid_max);
tstart0=(i_slid_max-1)*step  %第1道（台阵中心）有最大功率的波形段起始时刻
slowness=sqrt(sx^2+sy^2)
vapparent=1/slowness
azi=atan2(sx,sy)*180/pi
%contourf(kxx/fmid,kyy/fmid,fkdb)
title(strcat('福州台阵合成波形，滑动宽带F-K，4-6Hz，tstart=',num2str(tstart0),'twindow=2.56s,方位角',num2str(azi),'度，视慢度 ',num2str(slowness),'s/km'))
%title('福州台阵2007110122合成波形，宽带F-K，4-6Hz，t1=0.0s,tl=6s')
xlabel('Sx/(s/km)')
ylabel('Sy/(s/km)')
colorbar

%细扫描
disp('细扫描')
tstart=tstart0
sx1=sx-0.1 %s/km
sx2=sx+0.1  %s/km
sxpoint=101
sy1=sy-0.1
sy2=sy+0.1
sypoint=101
dsx=(sx2-sx1)/(sxpoint-1) 
dsy=(sy2-sy1)/(sypoint-1)
for i=1:sxpoint
      sxx(i)=sx1+dsx*i;
end
for i=1:sypoint
      syy(i)=sy1+dsy*i;
end
[fk,fkdb,sx,sy,fkmax,fftmax,fk_norm,fk_norm_max]=fk_func_norm(array,testwave,num_cha,f1,f2,tstart,twindow,sample_rate,sx1,sx2,sxpoint,sy1,sy2,sypoint);
  figure(7)
  
  %contourf(kxx,kyy,fkdb)
contourf(sxx,syy,fkdb');
slowness=sqrt(sx^2+sy^2)
vapparent=1/slowness
azi=atan2(sx,sy)*180/pi
%contourf(kxx/fmid,kyy/fmid,fkdb)
title(strcat('福州台阵合成波形，滑动宽带F-K细扫描，4-6Hz，tstart=',num2str(tstart0),'twindow=2.56s,方位角',num2str(azi),'度，视慢度 ',num2str(slowness),'s/km'))
%title('福州台阵2007110122合成波形，宽带F-K，4-6Hz，t1=0.0s,tl=6s')
xlabel('Sx/(s/km)')
ylabel('Sy/(s/km)')
colorbar
%聚束
disp('聚束')
figure(8)
t1start=1.0 %sec
twindow=1.0 %sec
sx1=-0.4 %s/km
sx2=0.4  %s/km
sxpoint=101
sy1=-0.4
sy2=0.4
sypoint=101
flag=0  %能量判据
%flag=1  %振幅判据
[beamwave,bm,bmdb,sx,sy] = beam_func( array,testwave,num_cha,t1start,twindow,sample_rate,sx1,sx2,sxpoint,sy1,sy2,sypoint,flag);
dsx=(sx2-sx1)/(sxpoint-1) 
dsy=(sy2-sy1)/(sypoint-1)
for i=1:sxpoint
      sxx(i)=sx1+dsx*i;
end
for i=1:sypoint
      syy(i)=sy1+dsy*i;
end
contourf(sxx,syy,bmdb');
slowness=sqrt(sx^2+sy^2)
vapparent=1/slowness
azi=atan2(sx,sy)*180/pi
%contourf(kxx/fmid,kyy/fmid,fkdb)

title(strcat('福州台阵合成波形，聚束（能量判据），t1=0.8s,tl=2.56s','方位角',num2str(azi),'度，视慢度 ',num2str(slowness),'s/km'))
%title('福州台阵2007110122合成波形，宽带F-K，4-6Hz，t1=0.0s,tl=6s')
xlabel('Sx/(s/km)')
ylabel('Sy/(s/km)')
colorbar
figure(9)
plot(testwave(1,:)); hold on
plot(testwave(2,:)+10000); hold on
plot(testwave(3,:)+10000*2); hold on
plot(testwave(4,:)+10000*3); hold on
plot(testwave(5,:)+10000*4); hold on
plot(testwave(6,:)+10000*5); hold on
plot(testwave(7,:)+10000*6); hold on
plot(testwave(8,:)+10000*7); hold on
plot(testwave(9,:)+10000*8); hold on
plot(testwave(10,:)+10000*9); hold on
plot(testwave(11,:)+10000*10); hold on
plot(testwave(12,:)+10000*11); hold on
plot(testwave(13,:)+10000*12); hold on
plot(testwave(14,:)+10000*13); hold on
plot(testwave(15,:)+10000*14); hold on
plot(testwave(16,:)+10000*15);hold on
plot(beamwave+10000*16); hold on
xlabel('采样点序号（50s/s)')
title('福州台阵合成波形聚束'); hold off
figure(10)


%计算波形互相关函数
load st02007110122.ud
%load st02007110122.ns
wave1=st02007110122(1:60*50);
%wave1=st02007110122(1:60*50);
subplot(2,1,1)
plot(wave1)
title('st02007110122.ud原始记录波形')
xlabel('采样点，起始时间：2007-11-01 22：38，采样率=50s/s')
ylabel('counts')
load s172007110122.ud
wave2=s172007110122(1:60*50);
subplot(2,1,2)
plot(wave2)
title('st02007110122.ud原始记录波形')
xlabel('采样点，起始时间：2007-11-01 22：38，采样率=50s/s')
ylabel('counts')

wave1_filtered=filtfilt(b,a,wave1);
%wave1_filtered=wave1;
%for i=1:1:300
 %   wave1_filtered(i+580)=0;
%end
%filtfilt为零相位滤波器，即正向和反向各进行一次滤波
figure(11)
subplot(3,1,1)
plot(wave1_filtered)
title('st02007110122.ud高通滤波后的波形')
xlabel('采样点，起始时间：2007-11-01 22：38，采样率=50s/s')
ylabel('counts')
wave2_filtered=filtfilt(b,a,wave2);
%wave2_filtered=wave2;
%for i=1:1:300
 %   wave2_filtered(i+580)=0;
%end

%filtfilt为零相位滤波器，即正向和反向各进行一次滤波
subplot(3,1,2)
plot(wave2_filtered)
title('s172007110122.ud高通滤波后的波形')
xlabel('采样点，起始时间：2007-11-01 22：38，采样率=50s/s')
ylabel('counts')

for i=1:1:200
    cross_corre(i)=0;
   %for j=1:1:30  %互相关长度
   for j=1:1:40  %互相关长度    
   %s10与st0互相关时使用1:1:40
    cross_corre(i)=cross_corre(i)+wave1_filtered(j+535)*wave2_filtered(j+535+i-101);
    end
end
auto_corre1=0;
%for j=1:1:30
    for j=1:1:40
    %s10与st0互相关时使用1:1:40
    auto_corre1=auto_corre1+wave1_filtered(j+535)*wave1_filtered(j+535);
end
for i=1:1:200
    auto_corre2(i)=0;
    %for j=1:1:30  %自相关长度
         for j=1:1:40  %自相关长度
%s10与st0互相关时使用1:1:40
    auto_corre2(i)=auto_corre2(i)+wave2_filtered(j+535+i-101)*wave2_filtered(j+535+i-101);
    end
end
for i=1:1:200
    cross_corre(i)=cross_corre(i)/(sqrt(auto_corre1)*sqrt(auto_corre2(i)));
    %cross_corre(i)=cross_corre(i)/(sqrt(auto_corre1)*sqrt(auto_corre1)); 
    %s11与st0互相关时使用第2个计算公式
end
corre_max=-100;
for i=1:1:120  %考虑台阵孔径
   % s11与st0互相关时使用1:1:110限定取最大值的区间
    if corre_max<cross_corre(i)
        corre_max=cross_corre(i);
        corre_maxi=i-101;
    end
end
subplot(3,1,3)
plot(cross_corre);
hold on
stem(corre_maxi+101,corre_max,'.')

%title({'s17与st0互相关函数 corre-max=',corre_max,'s17-st0到时差（sec)=',corre_maxi/50,'(st0取30点（536-565))'})
title(strcat('s17与st0互相关函数 corre-max=',num2str(corre_max),'s17-st0到时差（sec)=',num2str(corre_maxi/50),'(st0取40点（536-575))'))
disp('滑动极大值')
fkmax_slid
fftmax_slid
fkmax_slid_max=0;
 fftmax_slid_max=0;

 fk_slid_norm_max=0;
for i_slid=1:num_slid
    ti(i_slid)=(i_slid-1)*step;
    if fkmax_slid(i_slid)>fkmax_slid_max
        fkmax_slid_max=fkmax_slid(i_slid);
        fkmax_slid_maxi=i_slid;
    end
    if  fk_norm_max_slid(i_slid)>fk_slid_norm_max
        fk_slid_norm_max= fk_norm_max_slid(i_slid);
        fk_slid_norm_maxi=i_slid;
    end
    if fftmax_slid(i_slid)>fftmax_slid_max
        fftmax_slid_max=fftmax_slid(i_slid);
        fftmax_slid_maxi=i_slid;
    end
    
end
fkmax_slid_norm_db=10*log10(fk_norm_max_slid/fk_slid_norm_max)
fkmax_slid_db=10*log10(fkmax_slid/fkmax_slid_max)
fftmax_slid_db=10*log10(fftmax_slid/fftmax_slid_max)
figure(12)
plot(ti,fkmax_slid_db,'.',ti,fftmax_slid_db,'*',ti,fkmax_slid_norm_db,'+') 
legend('fkmax','fftmax','fknorm')
fkmax_max=fkmax_slid_max
fkmax_maxi=fkmax_slid_maxi
tstart_max=ti(fkmax_maxi) 
fftmax_max=fftmax_slid_max
fftmax_maxi=fftmax_slid_maxi
title('滑动F-K极大值随滑动窗起始时刻的变化')
xlabel('时间/s')
ylabel('滑动F-K极大值/dB')
sx_slid
sy_slid

