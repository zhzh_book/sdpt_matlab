%三分量台阵处理,使用福州勘选台阵记录2007110122晋江水域地震波形
% 读入福清台阵坐标
clear all
flag00=0
[ array,x,y ] = array_cord( flag00 )
% -------------------------------------------
%计算理论到时差
num_cha=16  %道数
sample_rate=50 %s/s
azi_d=20  %给定方位角20度
azi_c=azi_d*pi/180;
vapp=8  %给定视速度8 km/s
slow=1/vapp
slowx=slow*sin(azi_c);
slowy=slow*cos(azi_c);

for i=1:num_cha
    arit(i)=(slowx*array(i,1)+slowy*array(i,2));
    del(i)=floor(arit(i)*sample_rate);
end
%计算理论水平分量
%视出射角b,视慢度slow，真出射角b0，入射角a0，sin(a0)=vp/vapp,vp是P波真速度。
vp=6.0  %设真速度为6.0km/s
vpvs=1.73  %P与S的波速比
a0=asin(vp/vapp) 
a0d=a0*180/pi
b0=0.5*pi-a0
b0d=b0*180/pi
b=acos(cos(b0)/vpvs)*2-pi/2 %视出射角公式（   ）
bd=b*180/pi
%径向分量与垂直分量的比值v2r
v2r=tan(b)
figure(1)
%计算3个圆圈的半径：相应子台距中心台距离的平均值
r1=0;
for n=2:1:4
  r1=r1+sqrt((x(n)-x(1))*(x(n)-x(1))+(y(n)-y(1))*(y(n)-y(1)));
end
r2=0;
for n=5:1:9
  r2=r2+sqrt((x(n)-x(1))*(x(n)-x(1))+(y(n)-y(1))*(y(n)-y(1)));
end
r3=0;
for n=10:1:16 %现在在外圈上有8个候选子台
  r3=r3+sqrt((x(n)-x(1))*(x(n)-x(1))+(y(n)-y(1))*(y(n)-y(1)));
end
r1=r1/3;
r2=r2/5;
r3=r3/7;
for n=1:1:360
    x1(n)=r1*sin(n*3.1415926535897932/180);
    y1(n)=r1*cos(n*3.1415926535897932/180);
end
%subplot(1,2,1)
plot(x1,y1,'r-')
hold on
for n=1:1:360
    x1(n)=r2*sin(n*3.1415926535897932/180);
    y1(n)=r2*cos(n*3.1415926535897932/180);
end
plot(x1,y1,'r-')
hold on
for n=1:1:360
    x1(n)=r3*sin(n*3.1415926535897932/180);
    y1(n)=r3*cos(n*3.1415926535897932/180);
end
plot(x1,y1,'r-')
hold on
plot(x,y,'*') %绘子台分布图
hold off
% ---------------------------------------------------
%读入福州台阵波形
sample_rate=50
num_cha=16
twindow=60   %秒
num_data=sample_rate*twindow
fileflag=1   %读入福州台阵波形
filename='test'
[waveud,wavens,waveew] = read_wave( num_cha,sample_rate,twindow,fileflag,filename);
%设计高通滤波器
a=[1,0]; %FIR滤波器的分母系数
f=[0,0.04,0.04,1];
m=[0,0,1,1];
n=0:30;% 30  为滤波器的阶
%高通滤波器:拐角频率为0.04*采样率/2=0.04*50/2=1Hz；m给出相应的幅度
%利用Matlab中的帮助 help fir2; help filter; help filtfilt
%参考书：《Matlab及在电子信息课程中的应用》 第7章    
%利用Matlab中的信号处理工具箱中的FIR滤波器设计工具fir2生成递归滤波器系数b,
alpha=5000
[ waveud_f,wavens_f,waveew_f] = bandpass(a,f,m,n,waveud,wavens,waveew,num_cha,alpha);
N=1
roo=1/N
%变换成N次方根
for i=1:num_cha
for j=1:num_data
    waveud_f(i,j)=abs(waveud_f(i,j))^roo*sign(waveud_f(i,j));
    wavens_f(i,j)=abs(wavens_f(i,j))^roo*sign(wavens_f(i,j));
    waveew_f(i,j)=abs(waveew_f(i,j))^roo*sign(waveew_f(i,j));
end
end
for j=1:num_data
     waveud_f_sum(j)=0;
for i=1:num_cha
    waveud_f_sum(j)=waveud_f_sum(j)+waveud_f(i,j);
end
waveud_f_sum(j)=waveud_f_sum(j)/num_cha;
end
figure(22)
apart=10000
for i=1:num_cha
plot(waveud_f(i,:)+apart*(i-1)); hold on
end

plot(waveud_f_sum+apart*num_cha); hold on
xlabel('采样点序号（50s/s)')
title('福州台阵2007110122地震垂直向分量滤波后波形'); hold off


for j=1:num_data
     wavens_f_sum(j)=0;
for i=1:num_cha
    wavens_f_sum(j)=wavens_f_sum(j)+wavens_f(i,j);
end
wavens_f_sum(j)=wavens_f_sum(j)/num_cha;
end
figure(23)
for i=1:num_cha
plot(wavens_f(i,:)+apart*(i-1)); hold on
end
plot(wavens_f_sum+apart*num_cha); hold on
xlabel('采样点序号（50s/s)')
title('福州台阵2007110122地震北南向分量滤波后波形'); hold off

for j=1:num_data
     waveew_f_sum(j)=0;
for i=1:num_cha
    waveew_f_sum(j)=waveew_f_sum(j)+waveew_f(i,j);
end
waveew_f_sum(j)=waveew_f_sum(j)/num_cha;
end
figure(24)
for i=1:num_cha
plot(waveew_f(i,:)+apart*(i-1)); hold on
end
plot(waveew_f_sum+apart*num_cha); hold on
xlabel('采样点序号（50s/s)')
title('福州台阵2007110122地震东西向分量滤波后波形'); hold off
% ---------------------------------------------
%聚束扫描
vp=5.0  %近地面P波速
vs=5.0/1.73   %S波速
sx1=-0.4 %s/km
sx2=0.4  %s/km
sxpoint=101
sy1=-0.4
sy2=0.4
sypoint=101

dsx=(sx2-sx1)/(sxpoint-1); 
dsy=(sy2-sy1)/(sypoint-1);
for i=1:sxpoint
    sxx(i)=sx1+(i-1)*dsx;
end
for i=1:sypoint
    syy(i)=sy1+(i-1)*dsy;
end
flag_ps=0   %针对P震相
%滑动聚束
%twindow=2.56 %sec
twindow=0.8
%已知信号长度300点，采样率50s/s,信号总长6s。
nwindow=twindow*sample_rate
t0=4.0  %秒
len=2.5   %秒
num_len=len*sample_rate
step=0.02 %滑动步长0.1秒
num_step=step*sample_rate
num_slid=floor((num_len-nwindow)/num_step)   %滑动点数
%num_slid=1

flag=0  %能量
for k=1:7
bm_slid_max(k)=0;   % 找出7个极大值
end
for i_slid=1:num_slid
    disp({'滑动序号',i_slid})
    tstart=(i_slid-1)*step+t0;
   flag_ps=0;
   %if i_slid>220
   %    flag_ps=1
   %end
   flag_ps=1
   [b_ud,b_ns,b_ew,b_R,b_T,b_L,b_Q,bm_ud,bm_ns,bm_ew,bm_R,bm_T,bm_L,bm_Q,....
    sx_max,sy_max,bm_i,bm_j,bm_max,bw] = ....
    RTLQ_beam_func( array,waveud_f,wavens_f,waveew_f,num_cha,tstart,twindow,....
waveud_f_sum,wavens_f_sum,waveew_f_sum,sample_rate,sx1,sx2,sxpoint,sy1,sy2,sypoint,flag,flag_ps,vp,vs);
sx=sx_max
sy=sy_max

for isx=1:sxpoint
    for isy=1:sypoint
        bm_ud_slid(i_slid,isx,isy)=bm_ud(isx,isy);
        bm_ns_slid(i_slid,isx,isy)=bm_ns(isx,isy);
        bm_ew_slid(i_slid,isx,isy)=bm_ew(isx,isy);
        bm_R_slid(i_slid,isx,isy)=bm_R(isx,isy);
        bm_T_slid(i_slid,isx,isy)=bm_T(isx,isy);
        bm_L_slid(i_slid,isx,isy)=bm_L(isx,isy);
        bm_Q_slid(i_slid,isx,isy)=bm_Q(isx,isy);
    end  
end


for k=1:7
        
       bm_max_slid(i_slid,k)=bm_max(k);
        
   
    if  bm_max_slid(i_slid,k)>bm_slid_max(k)
        bm_slid_max(k)= bm_max_slid(i_slid,k);
        sx_slid(k)=sx_max(k);
        sy_slid(k)=sy_max(k);
        i_slid_max(k)=i_slid;
    end
end
end
for i=1:num_slid
    ti(i)=(i-1)*step+t0;   %第i点的时刻，第1点时刻是初始时刻
end
for k=1:7
    figure(k+50)
   

if flag==0
bmmax_db=10*log10(bm_max_slid(:,k)/bm_slid_max(k));
else
   bmmax_db=20*log10(bm_max_slid(:,k)/bm_slid_max(k)); 
end
plot(ti,bmmax_db)
%plot(ti,bm_max_slid(:,k))
end
for k=1:7
figure(k+60)

     i=i_slid_max(k)
     sx=sx_slid(k)
     sy=sy_slid(k)
     tstart=i_slid_max(k)*step+t0;
     slow=sqrt(sx_slid(k)^2+sy_slid(k)^2);
     azi=atan2(sx_slid(k),sy_slid(k))*180/pi;
     if k==1
         
         temp(:,:)=bm_ud_slid(i_slid_max(k),:,:);
     contourf(sxx,syy,temp');title({'UD分量，tstart=',tstart,'视慢度=',slow,'s/km','方位角=',azi,'度'})
     end
     if k==2
         
         temp(:,:)=bm_ns_slid(i_slid_max(k),:,:);
     contourf(sxx,syy,temp');title({'NS分量，tstart=',tstart,'视慢度=',slow,'s/km','方位角=',azi,'度'})
     end
     if k==3
         
         temp(:,:)=bm_ew_slid(i_slid_max(k),:,:);
     contourf(sxx,syy,temp');title({'EW分量，tstart=',tstart,'视慢度=',slow,'s/km','方位角=',azi,'度'})
     end
     if k==4
         
         temp(:,:)=bm_R_slid(i_slid_max(k),:,:);
     contourf(sxx,syy,temp');title({'R分量，tstart=',tstart,'视慢度=',slow,'s/km','方位角=',azi,'度'})
     end
     if k==5
         
         temp(:,:)=bm_T_slid(i_slid_max(k),:,:);
     contourf(sxx,syy,temp');title({'T分量，tstart=',tstart,'视慢度=',slow,'s/km','方位角=',azi,'度'})
     end
     if k==6
        
         temp(:,:)=bm_L_slid(i_slid_max(k),:,:);
     contourf(sxx,syy,temp'); title({'L分量，tstart=',tstart,'视慢度=',slow,'s/km','方位角=',azi,'度'})
     end
     if k==7
         
         temp(:,:)=bm_Q_slid(i_slid_max(k),:,:);
     contourf(sxx,syy,temp');title({'Q分量，tstart=',tstart,'视慢度=',slow,'s/km','方位角=',azi,'度'})
     end
end
i_max=i_slid_max     
% 画出聚束波形

for kk=1:7
    
    i=i_slid_max(kk);
 
    
 
   
     sx=sx_slid(kk);
     sy=sy_slid(kk);
      slow=sqrt(sx^2+sy^2);
 % 计算入射角
 if i>220   % 晋江地震的S波段
      %针对P波
sin_inci_p=vp*slow;
if sin_inci_p>1
    sin_inci_p=1;
end
inci_a=asin(sin_inci_p); 
 else

%针对S波
sin_inci_s=vs*slow;
if sin_inci_s>1
    sin_inci_s=1;
end
inci_a=asin(sin_inci_s); 
 end

inci_a_d=inci_a*180/pi
      
      
     azi=atan2(sx_slid(kk),sy_slid(kk));
     azi_d=azi*180/pi
    
     for m=101:num_data-100
                 bw_ud(m)=0;
                  bw_ns(m)=0;
                   bw_ew(m)=0;
                    bw_R(m)=0;
                     bw_T(m)=0;
                      bw_L(m)=0;
                       bw_Q(m)=0;
            end
            for k=1:num_cha
              tao(k)=(sx*array(k,1)+sy*array(k,2));
              taom(k)=round(tao(k)*sample_rate);
              
              for m=1:num_data-200 
                  bw_ud(m)= bw_ud(m)+waveud_f(k,m+100+taom(k));
                  bw_ns(m)= bw_ns(m)+wavens_f(k,m+100+taom(k));
                  bw_ew(m)= bw_ew(m)+waveew_f(k,m+100+taom(k));
wave_ud=waveud_f(k,:);
wave_ns=wavens_f(k,:);
wave_ew=waveew_f(k,:);
[wave_R,wave_T,wave_L,wave_Q] = ZNE2RTLQ(wave_ud,wave_ns,wave_ew,azi,inci_a);
w_R(k,:)=wave_R;
w_T(k,:)=wave_T;
w_L(k,:)=wave_L;
w_Q(k,:)=wave_Q;
                  bw_R(m)= bw_R(m)+w_R(k,m+100+taom(k));
                  bw_T(m)= bw_T(m)+w_T(k,m+100+taom(k));
                  bw_L(m)= bw_L(m)+w_L(k,m+100+taom(k));
                  bw_Q(m)= bw_Q(m)+w_Q(k,m+100+taom(k));
              end
            end

figure(71+kk)
disp('分量聚束波形')
plot(bw_ud/num_cha);hold on
plot(bw_ns/num_cha+10000);hold on
plot(bw_ew/num_cha+10000*2);hold on
plot(bw_R/num_cha+10000*3);hold on
plot(bw_T/num_cha+10000*4);hold on
plot(bw_L/num_cha+10000*5);hold on
plot(bw_Q/num_cha+10000*6);hold off
title({'分量聚束，自下而上：UD，NS，EW,R,T,L，Q，101点起,kk=',kk})
% 针对给定的视慢度矢量,计算各分量的聚束
%
end
figure(27)
for i=1:num_cha
    plot(w_R(i,:)+apart*(i-1)); hold on
end  
title('wave-f-R');hold off
figure(28)
for i=1:num_cha
    plot(w_T(i,:)+apart*(i-1)); hold on
end  
title('wave-f-T');hold off
figure(29)
for i=1:num_cha
    plot(w_L(i,:)+apart*(i-1)); hold on
end  
title('wave-f-L');hold off
figure(30)
for i=1:num_cha
    plot(w_Q(i,:)+apart*(i-1)); hold on
end  
title('wave-f-Q');hold off


