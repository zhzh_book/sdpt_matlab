%M10_02_MT_decomp(osition)
%矩张量分解（第10章式（10.8）-（10.30））
% 举例：2008-05-12 汶川地震矩张量解，刘超等（2008）

%Mrr=1.71  Mtt=0.24  Mpp=-1.22  Mrt=-0.03  Mrp=1.09  Mtp=-0.76   USE坐标系
%MDC=1.97  MEXP=0.24  MCLV=0.10  D=0.10   x1021N.m  MW=8.1
clear all
mt=[0.24 0.76 -0.03; 0.76 -1.22 -1.09;-0.03 -1.09 1.71]    %NED坐标系 x e21
[V,E] = eig(mt)   % 式（10.6）
for i=1:3
m(i)=E(i,i)
end
for i=1:3
    a1(i)=V(i,1);
    a2(i)=V(i,2);
    a3(i)=V(i,3);
end
a1   % 行矢量
a2
a3
a1'   %列矢量
M=[a1' a2' a3']*E*[a1;a2; a3]    %式（10.6）,等于给出的mt
M_EV=V*E*V'
 %下面按式（10.7） 给出矩张量各分量
Mxx=m(1)*a1(1)^2+m(2)*a2(1)^2+m(3)*a3(1)^2
Myy=m(1)*a1(2)^2+m(2)*a2(2)^2+m(3)*a3(2)^2
Mzz=m(1)*a1(3)^2+m(2)*a2(3)^2+m(3)*a3(3)^2
Mxy=m(1)*a1(1)*a1(2)+m(2)*a2(1)*a2(2)+m(3)*a3(1)*a3(2)
Mxz=m(1)*a1(1)*a1(3)+m(2)*a2(1)*a2(3)+m(3)*a3(1)*a3(3)
Myz=m(1)*a1(3)*a1(2)+m(2)*a2(3)*a2(2)+m(3)*a3(3)*a3(2)
tr=m(1)+m(2)+m(3)   %矩张量的迹
for i=1:3
    m_star(i)=m(i)-tr/3     %式（10.9）
end
%分解成一个各向同性部分和3个矢量偶极,式（10.18）和式（10.19）
couple1=m_star(1)*a1'*a1
[b,couple1_eig]=eig(couple1)
couple2=m_star(2)*a2'*a2
couple3=m_star(3)*a3'*a3
M_3couple=(1/3)*tr*[1 0 0;0 1 0;0 0 1]+couple1+couple2+couple3
%分解成一个各向同性部分和3个双力偶 （式（10.21）
DC1=(1/3)*(m_star(1)-m_star(2))*(a1'*a1-a2'*a2)
DC2=(1/3)*(m_star(2)-m_star(3))*(a2'*a2-a3'*a3)
DC3=(1/3)*(m_star(3)-m_star(1))*(a3'*a3-a1'*a1)
[b,DC3_eig]=eig(DC3)   %符合DC定义
M_3DC=(1/3)*tr*[1 0 0;0 1 0;0 0 1]+DC1+DC2+DC3
%分解成一个各向同性部分和3个补偿线性矢量偶极 式（10.23）
CLVD1=(1/3)*m(1)*(2*a1'*a1-a2'*a2-a3'*a3)
CLVD2=(1/3)*m(2)*(2*a2'*a2-a1'*a1-a3'*a3)
CLVD3=(1/3)*m(3)*(2*a3'*a3-a2'*a2-a1'*a1)
[b,CLVD3_eig]=eig(CLVD3)    %符号CLVD定义
I=[1 0 0;0 1 0;0 0 1]
M_3CLVD=(1/3)*tr*I+CLVD1+CLVD2+CLVD3
%此前的计算中，可不按（本征值-tr/3）的绝对值大小换序，也可先换序，下面的计算必须先换序，
%以满足（本征值-tr/3）的绝对值递增
%分解成一个各向同性部分和一个大双力偶和一个小双力偶 式（10.25） （10.26） （10.27）
% 先按如下条件换序
for i=1:3
    m_temp(i)=m(i)-(1/3)*tr
end
eii=[abs(m_temp(1)) abs(m_temp(2)) abs( m_temp(3))]  
[bii,ii]=sort(eii)
for j=1:3
    for i=1:3
    a(i,j)=V(i,ii(j));   %三个列矢量相应换序
    end
end
for j=1:3
    m_star(j,j)=m_temp(ii(j));
end
%生成了满足绝对值递增的m_star
for i=1:3
   for  j=1:3
    if i<j || i>j
        m_star(i,j)=0;    %张量
    end
   end
end
m_star
for i=1:3
    m1_star(i)=m_star(i,i)   %矢量，式（10.8）-（10.30）中换序后的m_star
end
M_123=a*m_star*a'+(1/3)*tr*I
M_maj=a*[0 0 0;0 -m1_star(3) 0;0 0 m1_star(3)]*a'   %式（10.25）
M_min=a*[m1_star(1) 0 0;0 -m1_star(1) 0;0 0 0]*a'   %式（10.26）
M_maj_min=(1/3)*tr*I+M_maj+M_min
[b,M_maj_eig]=eig(M_maj)  %符合DC定义的大双力偶
[b,M_min_eig]=eig(M_min) %符合DC定义的小双力偶
%分解成一个各向同性部分和一个双力偶和一个补偿线性矢量偶极 (式（10.29），（10.30）
F=-m1_star(1)/m1_star(3)
F_1=m1_star(2)/m1_star(3)
disp('偏量部分的绝对值递增排序本征值')
m1_star
sum_m1_star=m1_star(1)+m1_star(2)+m1_star(3)
M_dc=m1_star(3)*(1-2*F)   %=-1.6661
DC_f=M_dc*[0 0 0;0 -1 0;0 0 1]
M_clvd=m1_star(3)*F   %=-0.2028
CLVD_f=M_clvd*[-1 0 0;0 -1 0;0 0 2]
iso=(1/3)*tr  
disp('iso+DC+CLVD主轴分解')
M_dc_clvd=iso*I+DC_f+CLVD_f
disp('相应的本征矢量')
%采用最佳双力偶定义
disp('最佳双力偶')
md_star=0.5*(abs(m1_star(2))+abs(m1_star(3))) *sign(m1_star(3))  %保持原双力偶的取向正负
M_dc_b=md_star 
DC_f_b=M_dc_b*[0 0 0;0 -1 0;0 0 1]
disp('剩余部分构成新的CLVD')
m2_star(1)=m1_star(1);
m2_star(2)=m1_star(2)+md_star;
m2_star(3)=m1_star(3)-md_star;
m2_star  
%自定义最佳双力偶时的F值，纯双力偶的Fd=0
Fd=(abs(m1_star(3))-abs(m1_star(2)))/abs(md_star)

       