%估计功率谱密度的误差范围
%给定自由度DOF，给定置信水平=clevel%
clear
DOF=120;
dBl=-1.5;
dBu=1.5;
Prl = gammainc(10^(dBl/10)*DOF/2,DOF/2,'lower')*100
Pru = gammainc(10^(dBu/10)*DOF/2,DOF/2,'upper')*100
clevel=99
updown=(100-clevel)/2
for i=1:20000
    dBl=i*(-0.001);
Prl = gammainc(10^(dBl/10)*DOF/2,DOF/2,'lower')*100;
if Prl<updown+0.01
    Prll=Prl
    dBll=dBl
    break;
end
end
for i=1:20000
    dBu=i*(0.001);
Pru = gammainc(10^(dBu/10)*DOF/2,DOF/2,'upper')*100;
if Pru<updown+0.01
    Pruu=Pru
    dBuu=dBu
    break;
end
end

