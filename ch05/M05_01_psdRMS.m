%M05_01_psdRMS.m实现由功率谱密度（PSD）计算地动噪声均方根振幅。
%参考《地震记录处理》第5章5.1节
%计算地动噪声功率谱和地动位移RMS，Welch方法
%使用Welch方法2014-07-21
clear all;
load lay200912.17U  % 涞源台（lay）FBS-3记录，
% 采样率100s/s，长度3600s,360000个数据点。
Xs=lay200912;   %记录波形（counts）
Xs=Xs-mean(Xs);
Fs=100;   %采样率
N=length(Xs)
nn=floor(log2(N))
cbegin=clock
Nfft=2^nn  
for i=1:2^nn
    xn(i)=Xs(i)/(5*10^8);   %简单地去掉仪器响应：仪器灵敏度500counts/um/s
end
figure(1)
disp('ok00')
subplot(2,1,1);
plot(xn)
title('涞源台FBS-3记录地动噪声200912.17U垂直向转换成地动速度-m/s')
legend('未加窗')
%n=0:2^nn-1;
t=N/Fs; %数据长度、时间序列
for i=1:floor(Nfft/10) %加左右10%汉宁窗 
    xn(i)=xn(i)*(0.5*(1+cos(2*pi*5*(i/Nfft-0.2/2))));
end
for i=ceil(0.9*Nfft)+1:Nfft
    xn(i)=xn(i)*(0.5*(1+cos(2*pi*5*(i/Nfft-1+0.2/2))));
end

subplot(2,1,2)
plot(xn)
legend('左右10%汉宁窗')
Xfft=abs(fft(xn));
for i=1:Nfft
Pxxu(i)=1.142857*2*(Xfft(i)^2)/(Nfft*Fs);% Peterson归一化因子+窗函数补偿
end
lp=length(Pxxu)
f1=(1:lp)*Fs/Nfft;%求得对应的频率向量(单边）
for i=1:lp
    Pxxuadb(i)=10*log10(Pxxu(i)*((2*pi*f1(i))^2));
end
disp('ok1')
figure(2)
semilogx(f1,Pxxuadb,'r')
load lay200912.17N  % 涞源台（lay）FBS-3记录，
% 采样率100s/s，长度3600s,15000个数据点。
Xs=lay200912;
Xs=Xs-mean(Xs);
Fs=100;   %采样率
N=length(Xs)
nn=floor(log2(N))
Nfft=2^nn
for i=1:2^nn
    xn(i)=Xs(i)/(5*10^8);
end
figure(3)
subplot(2,1,1);
plot(xn)
title('涞源台FBS-3记录地动噪声200912.17N北南向转换成地动速度-m/s')
legend('未加窗')
for i=1:floor(Nfft/10) %加左右10%汉宁窗 
    xn(i)=xn(i)*(0.5*(1+cos(2*pi*5*(i/Nfft-0.2/2))));
end
for i=ceil(0.9*Nfft)+1:Nfft
    xn(i)=xn(i)*(0.5*(1+cos(2*pi*5*(i/Nfft-1+0.2/2))));
end

subplot(2,1,2)
plot(xn)
Xfft=abs(fft(xn));
for i=1:Nfft
Pxxn(i)=1.142857*2*(Xfft(i)^2)/(Nfft*Fs);
end
lp=length(Pxxn)
f1=(1:lp)*Fs/Nfft;%求得对应的频率向量(单边）
for i=1:lp
    Pxxnadb(i)=10*log10(Pxxn(i)*((2*pi*f1(i))^2));
end
disp('ok1')
figure(4)
semilogx(f1,Pxxnadb,'r')
load lay200912.17E  % 涞源台（lay）FBS-3记录，
% 采样率100s/s，长度3600s,15000个数据点。
Xs=lay200912;
Xs=Xs-mean(Xs);
Fs=100;   %采样率
N=length(Xs)
nn=floor(log2(N))
Nfft=(2^nn)
for i=1:2^nn
    xn(i)=Xs(i)/(5*10^8);
end
figure(5)
subplot(2,1,1)
plot(xn)
title('涞源台FBS-3记录地动噪声200912.17E东西向转换成地动速度-m/s')
legend('未加窗')
for i=1:floor(Nfft/10) %加左右10%汉宁窗 
    xn(i)=xn(i)*(0.5*(1+cos(2*pi*5*(i/Nfft-0.2/2))));
end
for i=ceil(0.9*Nfft)+1:Nfft
    xn(i)=xn(i)*(0.5*(1+cos(2*pi*5*(i/Nfft-1+0.2/2))));
end
subplot(2,1,2)
plot(xn)
legend('左右10%汉宁窗')
Xfft=abs(fft(xn));
for i=1:Nfft
Pxxe(i)=1.142857*2*(Xfft(i)^2)/(Nfft*Fs);
end
lp=length(Pxxe)
for i=1:lp
    Pxxeadb(i)=10*log10(Pxxe(i)*((2*pi*f1(i))^2));
end
disp('ok3')
figure(6)
n2=Nfft/2
semilogx(f1(1:n2),Pxxuadb(1:n2),'r',f1(1:n2),Pxxnadb(1:n2),'b',f1(1:n2),Pxxeadb(1:n2),'black');
legend('U-D','N-S','E-W')
title('涞源地震台地动噪声加速度功率谱(FFT方法) ，数据：lay200912.17')
xlabel('频率/Hz');ylabel('加速度功率谱/dB');
cend=clock
title('PSD—Welch方法，单边'); grid on
%由功率谱计算地动噪声RMS
%1 计算1-20Hz三分之一倍频程的速度功率谱
%2 取每个三分之一倍频程的频率中心点的功率谱作为该频段的平均功率谱
%3 以此功率谱乘以相应三分之一倍频程的带宽，再取其平方根，得到相应频带对应的地动速度RMS
%4 取各频段RMS的平均值作为整个1-20Hz频段的RMS
%5 与直接测定的RMS比较：将地动噪声记录去掉仪器响应得到地动噪声速度波形；用1-20Hz带通滤波器滤波，
%6 得到1-20Hz频段地动噪声
%7 根据RMS定义，计算地动噪声RMS，看与用PSD计算的RMS相差多少。