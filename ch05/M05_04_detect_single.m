%计算单台检测能力
%给定噪声水平（RMS）（单位m/s）
noise=1e-7;
%给定S振幅与噪声rms之比rs
rs=6;
Vs=rs*noise   %S波最大振幅
%给定震中距dis
dis0=5;
dis_step=5;
dis1=400;
R1=importdata('R1.txt')
dist=100;
R1(dist/5+1)
for dis=dis0:dis_step:dis1
    dis
    Rdis=R1(dis/5+1);   
    %给定S最大振幅的卓越周期Ts
    if dis<100 || dis==100
        Ts=0.2 %广东，震中距<100km
    end
    if dis>100 &&  (dis<200 || dis==200)
        Ts=0.3
    end
    if dis>200 && dis<400
        Ts=0.4
    end
    ML=log10(0.5*Vs*Ts/pi)+Rdis+6
end
