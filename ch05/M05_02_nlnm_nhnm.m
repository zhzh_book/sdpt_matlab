%根据Peterson（1993）提供的加速度功率谱数据，换算成速度和位移功率谱密度
%生成表为P.鲍曼（2006）的表4.1和4.2，见《地震记录处理》第5章表5.1(a)和(b)
clear all
nlnma=importdata('nlnm.asc', ' ', 1);
la=length(nlnma.data)
for i=1:la
    peril(i)=nlnma.data(i,1);
    nlnmap(i)=nlnma.data(i,2);
    nlnmadb(i)=10*log10(nlnmap(i));
    nlnmvp(i)=nlnmap(i)*((0.5*peril(i)/pi)^2);
    nlnmvdb(i)=10*log10(nlnmvp(i));
    nlnmdp(i)=nlnmvp(i)*((0.5*peril(i)/pi)^2);
    nlnmddb(i)=10*log10(nlnmdp(i));
end
nhnma=importdata('nhnm.asc', ' ', 1);
lha=length(nhnma.data)
for i=1:lha
    perih(i)=nhnma.data(i,1);
    nhnmap(i)=nhnma.data(i,2);
    nhnmadb(i)=10*log10(nhnmap(i));
    nhnmvp(i)=nhnmap(i)*((0.5*perih(i)/pi)^2);
    nhnmvdb(i)=10*log10(nhnmvp(i));
    nhnmdp(i)=nhnmvp(i)*((0.5*perih(i)/pi)^2);
    nhnmddb(i)=10*log10(nhnmdp(i));
end
figure(1)
loglog(peril,nlnmap);hold on
xlabel('周期/s')
ylabel('噪声加速度功率谱密度/(m**2/s**4/Hz)')
title('NLNM和NHNM（Peterson,1993）')
%semilogx(perih,nhnmap);hold on
loglog(perih,nhnmap)
grid on
figure(2)
semilogx(peril,nlnmadb);hold on
xlabel('周期/s')
ylabel('噪声加速度功率谱密度/10*log(m**2/s**4/Hz)')
title('NLNM和NHNM（Peterson,1993）')
semilogx(perih,nhnmadb);hold off
grid on
nlnmvp
nlnmvdb
nlnmdp
nlnmddb
nhnmvp
nhnmvdb
nhnmdp
nhnmddb
%待生成数据表,
fil='nlnmdva.asc'
fid=fopen(fil,'w')
fprintf(fid,'NLNM，据Peterson(1993)给出的加速度功率谱密度数据换算\n')
fprintf(fid,'     T/s       Pa*      Pa/dB      Pv**      Pv/dB      Pd***     Pd/dB\n')
for i=1:la
    fprintf(fid,'%8.2f   %4.2e   %6.1f   %4.2e   %6.1f   %4.2e   %6.1f\n',...
    peril(i),nlnmap(i),nlnmadb(i),nlnmvp(i),nlnmvdb(i),nlnmdp(i),nlnmddb(i));
end
fprintf(fid,'* Pa单位为(m^2*s^-4/Hz)\n')
fprintf(fid,'** Pv单位为(m^2*s^-2/Hz)\n')
fprintf(fid,'*** Pd单位为(m^2/Hz)\n')
fclose(fid);
fil='nhnmdva.asc'
fid=fopen(fil,'w')
fprintf(fid,'NHNM，据Peterson(1993)给出的加速度功率谱密度数据换算\n')
fprintf(fid,'     T/s       Pa*      Pa/dB      Pv**      Pv/dB      Pd***     Pd/dB\n')
for i=1:lha
    fprintf(fid,'%8.2f  %4.2e  %6.1f  %4.2e  %6.1f  %4.2e  %6.1f\n',...
    perih(i),nhnmap(i),nhnmadb(i),nhnmvp(i),nhnmvdb(i),nhnmdp(i),nhnmddb(i));
end
fprintf(fid,'* Pa单位为(m^2*s^-4/Hz)\n')
fprintf(fid,'** Pv单位为(m^2*s^-2/Hz)\n')
fprintf(fid,'*** Pd单位为(m^2/Hz)\n')
fclose(fid);
figure(3)
loglog(peril,nlnmvp);hold on
xlabel('周期/s')
ylabel('噪声速度功率谱密度/(m**2/s**2/Hz)')
title('NLNM和NHNM（Peterson,1993）')
loglog(perih,nhnmvp);hold off
grid on
figure(4)
semilogx(peril,nlnmvdb);hold on
xlabel('周期/s')
ylabel('噪声速度功率谱密度/10*log(m**2/s**2/Hz)')
title('NLNM和NHNM（Peterson,1993）')
semilogx(perih,nhnmvdb);hold off
grid on
figure(5)
loglog(peril,nlnmdp);hold on
xlabel('周期/s')
ylabel('噪声位移功率谱密度/(m**2/Hz)')
title('NLNM和NHNM（Peterson,1993）')
loglog(perih,nhnmdp);hold off
grid on
figure(6)
semilogx(peril,nlnmddb);hold on
xlabel('周期/s')
ylabel('噪声位移功率谱密度/10*log(m**2/Hz)')
title('NLNM和NHNM（Peterson,1993）')
semilogx(perih,nhnmddb);hold off
grid on
%备注：与P.Bormann的NMSOP相比，结果一致，但：表4.1 nlnm中的1000秒的速度PSD值3.5x10-14
% 应为1.65e-9;100000秒的位移2.6x10+6应为3.14e+6。佐证：换算成的dB值两者相同。
