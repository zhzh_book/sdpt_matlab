%建立分类数据
%Ms=1.25mb-2.5
clear all
a=1.25;
b=-2.5;
bstep=0.5;
mb0=3.0;
mbstep=0.5;
j=0;
for k=1:10
    b=b-bstep;     %ms不发育-核爆 
    for i=1:10
        mbtemp=mb0+mbstep*(i-1);
        mstemp=a*mbtemp+b;
        if mstemp>0 
            j=j+1;
            mb1(j)=mbtemp;
            ms1(j)=mstemp;
        end
    end
end
N1=j;
b=-2.5;
j=0;
for k=1:10
    b=b+bstep;   %ms发育-地震
    for i=1:10
        mbtemp=mb0+mbstep*(i-1);
        mstemp=a*mbtemp+b;
        if mstemp>0 
            j=j+1;
            mb2(j)=mbtemp;
            ms2(j)=mstemp;
        end
    end
end
N2=j;
N=N1+N2;
%plot(mb1,ms1,'ro');hold on
%plot(mb2,ms2,'b*')
for i=1:N1
    mb(i)=mb1(i);
    ms(i)=ms1(i);
    species(i)='1';    % 核爆-1
end
for i=1:N2
    mb(i+N1)=mb2(i);
    ms(i+N1)=ms2(i);
    species(i+N1)='2';  %地震-2
end 

figure(1)
%plot(mb,ms,'r+')
data=[mb',ms'];  %构建数据，两列
groups = ismember(species,'1');
[train, test] = crossvalind('holdOut',groups);  %随机混合，分成训练组train和测试组test.

cp = classperf(groups);

svmStruct = svmtrain(data(train,:),groups(train),'Kernel_Function','linear','showplot',true)
title(sprintf('核函数: %s',...
              func2str(svmStruct.KernelFunction)),...
              'interpreter','none');
classes = svmclassify(svmStruct,data(test,:),'showplot',true);
classperf(cp,classes,test);
cp.CorrectRate
xlabel('mb');
ylabel('Ms')

figure
svmStruct = svmtrain(data(train,:),groups(train),'Kernel_Function','mlp','Mlp_Params',[0.1,-0.1],'showplot',true)
title(sprintf('核函数: %s',...
              func2str(svmStruct.KernelFunction)),...
              'interpreter','none');

classes = svmclassify(svmStruct,data(test,:),'showplot',true);
classperf(cp,classes,test);
cp.CorrectRate
xlabel('mb');ylabel('Ms')

figure
svmStruct = svmtrain(data(train,:),groups(train),'Kernel_Function','polynomial','Polyorder',19,'showplot',true)
title(sprintf('核函数: %s',...
              func2str(svmStruct.KernelFunction)),...
              'interpreter','none');
classes = svmclassify(svmStruct,data(test,:),'showplot',true);
classperf(cp,classes,test);
cp.CorrectRate
xlabel('mb');ylabel('Ms')

figure
svmStruct = svmtrain(data(train,:),groups(train),'Kernel_Function','rbf','RBF_Sigma',1,'showplot',true)
title(sprintf('核函数: %s',...
              func2str(svmStruct.KernelFunction)),...
              'interpreter','none'); 
classes = svmclassify(svmStruct,data(test,:),'showplot',true);
classperf(cp,classes,test);
cp.CorrectRate
xlabel('mb');ylabel('Ms')

figure
svmStruct = svmtrain(data(train,:),groups(train),'Kernel_Function','mlp','Mlp_Params',[0.5,-1],'showplot',true)
title(sprintf('核函数: %s',...
              func2str(svmStruct.KernelFunction)),...
              'interpreter','none');
classes = svmclassify(svmStruct,data(test,:),'showplot',true);
classperf(cp,classes,test);
cp.CorrectRate
xlabel('mb');ylabel('Ms')  
