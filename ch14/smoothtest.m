%不同程度的平滑
rand('seed',6), x = (4*pi)*[0 1 rand(1,15)]; y = sin(x); %假设数据
noisy = y + .3*(rand(size(x))-.5); 
[scs,p] = csaps(x,noisy); fnplt(scs,2), hold on
scs
low=csaps(x,noisy,p/2);
low
fnplt(low,2,'k--'), set(gca,'Fontsize',16)
fnplt(csaps(x,noisy,(1+p)/2),2,'r:'), plot(x,noisy,'o')
legend('smoothing spline','more smoothed','less smoothed',...
'noisy data'), hold off

delow=scs-low;
figure
plot(delow)
