load fisheriris
%fisheriris.mat是Matlab自带的数据集（data set)之一
data = [meas(:,1), meas(:,2)];
groups = ismember(species,'setosa');
[train, test] = crossvalind('holdOut',groups);
cp = classperf(groups);
svmStruct = svmtrain(data(train,:),groups(train),'Kernel_Function','linear','showplot',true);
title(sprintf('Kernel Function: %s',...
              func2str(svmStruct.KernelFunction)),...
              'interpreter','none');
figure
svmStruct = svmtrain(data(train,:),groups(train),'Kernel_Function','mlp','Mlp_Params',[1,-1],'showplot',true);
title(sprintf('Kernel Function: %s',...
              func2str(svmStruct.KernelFunction)),...
              'interpreter','none');
figure
svmStruct = svmtrain(data(train,:),groups(train),'Kernel_Function','polynomial','Polyorder',3,'showplot',true);
title(sprintf('Kernel Function: %s',...
              func2str(svmStruct.KernelFunction)),...
              'interpreter','none');
figure
svmStruct = svmtrain(data(train,:),groups(train),'Kernel_Function','rbf','RBF_Sigma',1,'showplot',true);
title(sprintf('Kernel Function: %s',...
              func2str(svmStruct.KernelFunction)),...
              'interpreter','none');
          
%classes = svmclassify(svmStruct,data(test,:),'showplot',true);
%classperf(cp,classes,test);
%cp.CorrectRate
figure
%svmStruct = svmtrain(data(train,:),groups(train),...
%                     'showplot',true,'boxconstraint',1e6);
%classes = svmclassify(svmStruct,data(test,:),'showplot',true);
%classperf(cp,classes,test);
%cp.CorrectRate
