clear
% source type plot
% 震源类型图-2013-08-04
%纵横轴
u=[-1.2 1.2];v=[0,0];
plot(u,v); hold on
u=[0 0];v=[-1.2,1.2];
plot(u,v);hold on
%平行四边形
u=[0 4/3];v=[1 1/3];
plot(u,v);hold on
u=[0 -4/3];v=[1 -1/3];
plot(u,v); hold on
u=[-4/3 0];v=[-1/3,-1];
plot(u,v);hold on
u=[0 4/3];v=[-1,1/3];
plot(u,v); hold on
u=[-4/3 4/3];v=[-1/3 1/3];
plot(u,v);hold on
r=sqrt((64/9)+(4/9))
%等T线
for i=1:19
    x=8/3*i/20-4/3;
    u=[0 x];v=[1 x/4];
    plot(u,v,':');hold on
    u=[0 x];v=[-1 x/4];
    plot(u,v,':');hold on
end

%等k线
%kao-k平面中的第2象限（u=tao,v=k）
for i=1:10
    x=-1+1/10*i;
    u=[x 0];v=[x+1 1/10*i];
     plot(u,v,':');hold on
end
%kao-k平面中的第4象限（u=tao,v=k）
for i=1:10
    x=1/10*i;
    u=[0 x];v=[-1+1/10*i x-1];
     plot(u,v,':');hold on
end
%kao-k平面中的第1象限(tao>0,k>0)
%上区(tao<4k),u=tao/(1-tao/2),tao=T(1-k),v=k/(1-tao/2)
%设k均匀取值0至+1，步长1/10
%上右斜边上，u,v=(0,1),(4/3,1/3)
T=1;
for i=1:10
    k=1/10*i;tao=T*(1-k);x=tao/(1-tao/2);
    %y=k/(1-tao/2);
    y=1-x/2;
    if i==1
        x=tao/(1-2*k);
       y=x-1;
        u=[0 x];v=[1/10*i y];
   end
   u=[0 x];v=[1/10*i y];
    plot(u,v,':');hold on
    
end
T=-1;
for i=1:10
    k=-1/10*i;tao=T*(1+k);x=tao/(1+tao/2);y=k/(1+tao/2);
    if i==1
        x=tao/(1+2*k);
       y=x+1;
        u=[0 x];v=[1/10*i y];
   end
   u=[0 x];v=[-1/10*i y];
    plot(u,v,':');hold on
end   

%M=sym([0.38,-1.31,-0.85;-1.31,2.16,0.81;-0.85,0.81,-3.46])
M=[0.38,-1.31,-0.85;-1.31,2.16,0.81;-0.85,0.81,-3.46]
[V,E]=eig(M);
Vd = double(V)
Ed = double(E)
trE3=(Ed(1,1)+Ed(2,2)+Ed(3,3))/3
Ed1=Ed;
for i=1:3
Ed1(i,i)=Ed(i,i)-trE3;
end
Ed1
A(1)=abs(Ed1(1,1))
A(2)=abs(Ed1(2,2))
A(3)=abs(Ed1(3,3))
[C,I]=sort(A)
%[C,I]=max(abs(Ed1))
Ed2=Ed1;
v1=Vd;
for i=1:3
Ed2(4-i,4-i)=Ed1(I(i),I(i));
for j=1:3
    V1(j,4-i)=Vd(j,I(i));
end
end
Ed2
V1
%epsilon=-Ed2(1,1)/abs(Ed2(3,3))
%k=trE3/(abs(trE3)+abs(Ed2(3,3)))
epsilon=-A(I(1))/A(I(3))
k=trE3/(abs(trE3)+A(I(3)))
T=-2*epsilon
tao=T*(1-abs(k))
if (tao>0 & k<0)
    u=real(tao)
    v=real(k)
end
plot(u,v,'r*');hold on

%M=sym([0.90,-0.30,0.12;-0.30,1.03,0.01;0.12,0.01,1.57])
M=[0.90,-0.30,0.12;-0.30,1.03,0.01;0.12,0.01,1.57]
[V,E]=eig(M);
Vd = double(V)
Ed = double(E)
trE3=(Ed(1,1)+Ed(2,2)+Ed(3,3))/3
Ed1=Ed;
for i=1:3
Ed1(i,i)=Ed(i,i)-trE3;
end
Ed1
[C,I]=max(abs(Ed1))
Ed2=Ed1;
v1=Vd;
for i=1:3
Ed2(4-i,4-i)=Ed1(I(i),I(i));
for j=1:3
    V1(j,4-i)=Vd(j,I(i));
end
end
Ed2
V1
epsilon=-Ed2(1,1)/abs(Ed2(3,3))
k=trE3/(abs(trE3)+abs(Ed2(3,3)))
T=-2*epsilon
tao=T*(1-abs(k))
if (tao>0 & k<0)
    u=real(tao)
    v=real(k)
end
if (tao>0 & k>0 & tao<4*k)
    u=real(tao/(1-tao/2))
    v=real(k/(1-tao/2))
end
%plot(u,v,'ro')


