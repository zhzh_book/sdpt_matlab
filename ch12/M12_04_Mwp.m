%对速度波形积分，计算Mwp
clear all
hai=importdata('hai.asc',' ',3);  %用g_delta.m生成的g_delta数据文件
sample_rate=20;
hai1=hai.data(:,1);hai2=hai.data(:,2);hai3=hai.data(:,3);
hai4=hai.data(:,4);hai5=hai.data(:,5);

num_len1=length(hai1);
num_all=num_len1*5;   %波形段总长度（点数）
for i=1:num_all
haiwave(i)=0;
 t(i)=i/sample_rate;
end
hl=length(haiwave);
tl=length(t);
for i=1:num_len1
    haiwave((i-1)*5+1)=hai1(i);
    haiwave((i-1)*5+2)=hai2(i);
    haiwave((i-1)*5+3)=hai3(i);
    haiwave((i-1)*5+4)=hai4(i);
    haiwave((i-1)*5+5)=hai5(i);
end
%仪器响应
% 先假定在信号频带内仪器响应是平坦的,仪器灵敏度sens(counts/(m/s))=1
sens=5.5952E+9; % SEED文件中给出的系统灵敏度，先假定信号频带内仪器响应平坦
wmean=mean(haiwave)
for i=1:num_all
    haiwave(i)=haiwave(i)-wmean;
end
figure(1)
plot(t(1:num_all),haiwave(1:num_all))
% 截取P波段(人工从波形图上确定)
% 1221-6500, 5280点，264秒
num_use=3000
%num_len=5280   %全P波段
%num_len=920     %P波初始46秒（IRIS给出的破裂持续时间）
for i=1:num_use  
    
        vel(i)=haiwave(i+1000)/sens;
   
    tt(i)=i/sample_rate;
end
figure(2)
subplot(311)
   plot(tt,vel)
   velfft=fft(vel);
   for i=1:num_use
       f=i*sample_rate/num_use;
   dispfft(i)=velfft(i)/(2*pi*f);
   intdispfft(i)=dispfft(i)/(2*pi*f);
   end
   subplot(312)
   disp=ifft(dispfft);
   plot(tt,disp);
   
   subplot(313)
   intdisp=ifft(intdispfft);
   plot(tt,intdisp);
  
%积分
for i=1:num_use
    disp1(i)=0;
    
    for j=1:i
        disp1(i)=disp1(i)+vel(j);
    end
end
disp2=detrend(disp1);
for i=1:num_use
    intdisp1(i)=0;
    for j=1:i
        intdisp1(i)=intdisp1(i)+disp2(j);
    end
end
intdisp2=detrend(intdisp1);
figure(3)
subplot(311)
plot(tt,vel);
ylabel('速度/(m/s)')
xlabel('时间/s')
subplot(312);
plot(tt,disp2);
ylabel('位移/m')
xlabel('时间/s')
subplot(313);
plot(tt,intdisp2);
ylabel('位移积分/(m.s)')
xlabel('时间/s')
intmean=mean(intdisp2(1:20))
intmax=max(intdisp2)
intmax=intmax-intmean
alpha=7900 %P波速度m/s
r=25*111.19
rou=3400  %密度 kg/m**3

m0=intmax*4*pi*rou*(alpha^3)*r
mwp=(log10(m0)-9.1)/1.5+0.2