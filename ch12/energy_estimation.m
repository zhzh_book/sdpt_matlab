%估计远震地震波能量(Boatwright and Choy，1986)，2014-10-07
r=6371
h=15
%读入地震参数（发震时刻、经纬度、震源深度、震级）

%接收器所在处的介质参数：密度rho,P波速度alpha
rho=2700   %(kg/m^3)   2.7g/cm^3
alpha=6.0   % km/s


%读入记录该地震的台站目录（序号、台站代码、纬度、经度、高程（m）、）


%读入地震波形信息：道数、分向、波形长度（数据点数）、采样率）
num_cha=1  %波形道数
sample_rate=50
num_len=1000
c1=1
%读入几何扩散因子g_delta
gg=importdata('g_delta3190.asc',' ',1);  %用g_delta.m生成的g_delta数据文件
g_del=gg.data(:,1);
g_delt=gg.data(:,2);
for i=1:60
    g_delta(g_del(i))=g_delt(i);
end
figure(10)
plot(g_delta,'*')
figure(1)
for i=1:num_cha
    epi_dist(i)=40   %度，待计算
end
%读入地震波形数据wave(i,j),i为台站序号,j为数据点序号,i=1:num_cha
for i=1:num_cha
    for j=1:num_len
        wave4e(i,j)=c1*sin(2*pi*1*j/sample_rate)*exp(-0.01*j);
    end
    
end
figure(1)
    plot(wave4e(1,:))
%速度记录，不是位移记录
%显示地震波形
%人工确定P波段的起止位置，得到估计能量使用的信号段的长度
for i=1:num_cha
    wave_len(i)=1000  %采样点数,20秒
end
%生成用于估计能量的波形段wave4e(i,j)
for i=1:num_cha    %计算每道波形的能通量
    epsilon_t(i)=0;
    for j=1:wave_len(i)
        epsilon_t(i)=epsilon_t(i)+wave4e(i,j)^2;
        wave_temp(j)=wave4e(i,j);
    end
    d_t=1/sample_rate;
    epsilon_t(i)=epsilon_t(i)*rho*alpha*d_t  % 第13章式（13.35）,时间域计算能通量
    wave_fft=fft(wave_temp)*d_t;   %FFT乘以d_t得到定义的傅里叶变换
    figure(2)
    plot(abs(wave_fft))
    epsilon_star(i)=0;
    epsilon_star0(i)=0;
    d_f=sample_rate/wave_len(i)
    d_omega=2*pi*d_f;
    for j=1:floor(wave_len(i)/2)   %单边积分
       %  for j=1:10  
     omega=j*d_omega;
        f=d_f*j;
        if f<=0.1    %Hz   第13章式（13.37）
            t_star(j)=0.9-0.1*log10(f);
        end
        if f>=0.1 & f<=1   %Hz
            t_star(j)=0.5-0.5*log10(f);
        end
        if f>=1    %Hz
            t_star(j)=0.5-0.1*log10(f);
        end
        epsilon_star0(i)=epsilon_star0(i)+abs(wave_fft(j))^2;
        epsilon_star(i)=epsilon_star(i)+abs(wave_fft(j))^2*exp(omega*t_star(j));
    end
    epsilon_star0(i)=epsilon_star0(i)*rho*alpha*d_omega/pi
    epsilon_star(i)=epsilon_star(i)*rho*alpha*d_omega/pi
end

for i=1:num_cha
    g=g_delta(epi_dist(i))
    Rp(i)=r/g
    epsilon_FS_star(i)=Rp(i)^2*epsilon_star      %式（13.39）
end
%计算各台得到的epsilon_FS_star平均值
 ave_epsilon_FS_star=0
 for i=1:num_cha
      ave_epsilon_FS_star= ave_epsilon_FS_star+epsilon_FS_star(i);
 end
 ave_epsilon_FS_star= ave_epsilon_FS_star/num_cha
%计算结果
%epsilon_t =8.0490e+003
%d_f = 0.0500
%epsilon_star0 =8.0692e+003
%epsilon_star =2.5350e+022
%g =0.5480   对应震中距40度
%Rp =1.1626e+004
%epsilon_FS_star =3.4264e+030
% ---------------------------
%P波群地震辐射能量
% 按Boatwright and Choy,1986,的公式一步步计算
% p：射线参数dT/ds，T走时，s震中距（以弧度为单位）
c0=(1/vs^2-2*p^2);
c1=c0^2;
c2=4*p^2*cos(i0)*cos(j0)/(vp*vs);
c3=4*vs*p*cos(j0)*c0/(vp*vs);
PP=(c2-c1)/(c2+c1)
SP=c3/(c2+c1)
SPbc=va*cos(ih)*SP/(vp*cos(jh));

FgP_square=Fp_square+(PP*FpP)^2+2*vp*q*(SPbc*FsP)^2/(3*vs)
Ep=4*pi*ave_Fp_square*epsilon_FS_star/FgP_square

Ep=4*pi*ave_Fp_square*epsilon_FS_star/FgP_square



