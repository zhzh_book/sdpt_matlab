%估计远震地震波能量(采用Newman and Okal,1998的简化算法)，2014-10-07
clear all
r=6371   % 地球平均半径km
h=15     %震源深度km
vp=6000.0   %地表附近P波速度m/s (注意，单位制是米-千克-秒，才有能量单位为焦耳)
alpha=vp
vs=6000.0/1.73  %地表附近S波速度m/s，设波速比=1.73
rho=2700   %接收器所在处的介质密度rho(kg/m^3)   2.7g/cm^3
%读入地震参数（发震时刻、经纬度、震源深度、震级）; 云南鲁甸地震
date='2014-08-03'
ori='16:30:10.0'
lat=27.1
long=103.3
dep=12  % km
ms=6.5
%读入记录该地震的台站目录（序号、台站代码、纬度、经度、高程（m）、）
% 海拉尔地震台
lat0=49.2724  % 度
long0=119.7414
height0=620  % 米

%读入地震波形信息：道数、分向、波形长度（数据点数）、采样率）
num_cha=1  %波形道数
sample_rate=20  %采样率s/s
 
% 计算震中距和震中对台站方位角
[ azi,dist] =ll2ad_fnc(lat0,long0,lat,long)

%读入几何扩散因子g_delta
gg=importdata('g_delta3190.asc',' ',1);  %用g_delta.m生成的g_delta数据文件
gg=importdata('g_smooth1190.asc',' ',1);  %用g_delta.m生成的滑动平滑后的g_smooth数据文件，11度-90度
g_del=gg.data(:,1)
g_delt=gg.data(:,2)
%for i=1:60
    for i=1:80    %11度-90度
    g_delta(g_del(i))=g_delt(i);
end
figure(10)
plot(g_delta,'*')
g_delta
%计算各台站到震源的震中距
for i=1:num_cha
    epi_dist(i)=round(dist)   %度，海拉尔台
    %if epi_dist(i)<31
     %   epi_dist(i)=31    %当震中距小于31度时采用31度的g_delta
   % end
     if epi_dist(i)<11
        epi_dist(i)=11    %当震中距小于11度时采用31度的g_delta
    end
end
%读入地震波形数据wave(i,j),i为台站序号,j为数据点序号,i=1:num_cha
% 对于远震，可只用P波垂直向记录波形（因为入射角小，水平分量不发育），
% 以下是海拉尔台记录20140803鲁甸地震，垂直向（取自IRIS）
hai=importdata('hai.asc',' ',3); 
sample_rate=20
hai1=hai.data(:,1);hai2=hai.data(:,2);hai3=hai.data(:,3);
hai4=hai.data(:,4);hai5=hai.data(:,5);

num_len1=length(hai1)
num_all=num_len1*5   %波形段总长度（点数）
for i=1:num_all
haiwave(i)=0;
 t(i)=i/sample_rate;
end
hl=length(haiwave)
tl=length(t)
haiwave(1:5)
for i=1:num_len1
    haiwave((i-1)*5+1)=hai1(i);
    haiwave((i-1)*5+2)=hai2(i);
    haiwave((i-1)*5+3)=hai3(i);
    haiwave((i-1)*5+4)=hai4(i);
    haiwave((i-1)*5+5)=hai5(i);
end

figure(1)
plot(t(1:num_all),haiwave(1:num_all))
% 截取P波段(人工从波形图上确定)
% 1221-6500, 5280点，264秒
%num_len=5280   %全P波段
num_len=5280     %P波初始46秒（IRIS给出的破裂持续时间）
for i=1:num_cha
    for j=1:num_len
        wave4e(i,j)=haiwave(j+1220);
    end
    
end
%仪器响应
% 先假定在信号频带内仪器响应是平坦的,仪器灵敏度sens(counts/(m/s))=1
sens=5.5952E+9 % SEED文件中给出的系统灵敏度，先假定信号频带内仪器响应平坦
for j=1:num_len
    instru(j)=sens;
end
figure(2)
    plot(wave4e(1,:))
%速度记录，不是位移记录
%显示地震波形
%人工确定P波段的起止位置，得到估计能量使用的信号段的长度
for i=1:num_cha
    wave_len(i)=num_len  %采样点数,20秒
end
%生成用于估计能量的波形段wave4e(i,j)
for i=1:num_cha    %计算每道波形的能通量
    epsilon_t(i)=0;
    for j=1:wave_len(i)
        epsilon_t(i)=epsilon_t(i)+wave4e(i,j)^2;
        wave_temp(j)=wave4e(i,j);
    end
    d_t=1/sample_rate;
    epsilon_t(i)=epsilon_t(i)*rho*alpha*d_t  % 第13章式（13.35）,时间域计算能通量
    wave_fft=fft(wave_temp)*d_t;   %FFT乘以d_t得到定义的傅里叶变换
%在频率域去掉仪器响应（）
  for j=1:num_len
     wave_fft(j)= wave_fft(j)/instru(j);
     f(j)=j*sample_rate/num_len;
  end
  for j=1:2
       wave_fft(j)=0;
  end
  %积分从第3个频点开始：本例中对应于3/264=0.01136Hz，合88秒
    figure(3)
    fmax=floor(num_len/2)
    plot(f(1:fmax),abs(wave_fft(1:fmax)))
    epsilon_star(i)=0;
    epsilon_star0(i)=0;
    d_f=sample_rate/wave_len(i)
    d_omega=2*pi*d_f;
    for j=1:floor(wave_len(i)/2)   %单边积分，考虑几何衰减exp(omega*t_star(j)
       %  for j=1:10  
     omega=j*d_omega;
        f=d_f*j;
        if f<=0.1    %Hz   第13章式（13.37）
            t_star(j)=0.9-0.1*log10(f);
        end
        if f>=0.1 & f<=1   %Hz
            t_star(j)=0.5-0.5*log10(f);
        end
        if f>=1    %Hz
            t_star(j)=0.5-0.1*log10(f);
        end
        epsilon_star0(i)=epsilon_star0(i)+abs(wave_fft(j))^2;
        epsilon_star(i)=epsilon_star(i)+abs(wave_fft(j))^2*exp(omega*t_star(j));
    end
    epsilon_star0(i)=epsilon_star0(i)*rho*alpha*d_omega/pi
    epsilon_star(i)=epsilon_star(i)*rho*alpha*d_omega/pi
end
% 考虑几何扩散r/g(h,delta)，这里指定使用震源深度15km的g
g_delta
for i=1:num_cha
    epi_dist(i)
    g=g_delta(epi_dist(i))
    g=0.413   %未平滑的31度g值
    Rp(i)=r/g
    epsilon_FS_star(i)=Rp(i)^2*epsilon_star      %式（13.39）
end
%计算各台得到的epsilon_FS_star平均值
 ave_epsilon_FS_star=0
 for i=1:num_cha
      ave_epsilon_FS_star= ave_epsilon_FS_star+epsilon_FS_star(i);
 end
 ave_epsilon_FS_star= ave_epsilon_FS_star/num_cha
%计算结果
%epsilon_t =8.0490e+003
%d_f = 0.0500
%epsilon_star0 =8.0692e+0032
%epsilon_star =2.5350e+022
%g =0.5480   对应震中距40度
%Rp =1.1626e+004
%epsilon_FS_star =3.4264e+030
% ---------------------------
%P波群地震辐射能量
ave_Fp_square=4/15   %P波平方平均辐射花样，第10章震源辐射花样
% 引入广义辐射系数FgP_square的经验计算公式（Newman and Okal,1998,Table 1）
%source_flag=1  % all shalow events 全部浅源事件（0-70km)
%source_flag=2  % non strike-slip 非走滑
source_flag=3  % only strike-slip 只走滑(鲁甸地震为走滑型)
%source_flag=4  % shallowest(0-20km) non-strike-slip events 最浅的非走滑
delta=dist  %震中距（度）
if source_flag==1  %
    a=1.011
    b=-8.590E-3
    c=6.747E-5
end
if source_flag==2  %
    a=1.171
    b=-7.271E-3
    c=6.009E-5
end  
if source_flag==3  %
    a=0.407
    b=-4.011E-3
    c=8.783E-6
end
if source_flag==4  %
    a=0.983
    b=-1.605E-3
    c=3.457E-5
end
FgP_square=a+b*delta+c*delta^2
Ep=4*pi*ave_Fp_square*ave_epsilon_FS_star/FgP_square
% 加入S波导的贡献: q=15.6 (Boatwright and Choy,1986)
q=15.6
Et=(1+q)*Ep
me=2*log10(Et)/3-2.9



