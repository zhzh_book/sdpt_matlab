%计算几何扩散（第13章式（13.39））
% 给定震中距s(度)
% 给定地球速度模型，为与Kanamori and Stewart,1976的曲线比较，
% 采用Jeffreys-Bullen速度模型和α=6.8km/s的 P波射线参数
% 给定震源深度h=15km
clear all
r=6371  %地球平均半径km
rho0=2700
rho15=2700  %密度
v0=6.0 %地面附近P波速度
vh=6.8 %h=15km处
h=15
% 读入J-B表h=15km的P波走时
jbp=importdata('jb_p15.asc',' ',2);
jbp15=jbp.data(:,1);
figure(1)
d2km=1/111.19
d2r=pi/180;
r2d=180/pi;
% 计算射线参数ｐ＝dT/ds
for i=3:101
    p(i-1)=(jbp15(i)-jbp15(i-2))/2;    %秒/度
    q(i-1)=1/p(i-1); 
end
plot(p)
% p(i)对应i度，单位：秒/度

for i=1:100
    theta(i)=i*d2r;
    i0(i)=asin(p(i)*r2d*v0/r);   %使用速度6.0km/s
    d_theta(i)=h*tan(i0(i)/r);
    ih(i)=i0(i)-d_theta(i);
    g11(i)=(rho15*vh*sin(ih(i))/(rho0*v0*sin(theta(i))*cos(i0(i))));
    d(i)=i;
end
for i=1:100
    if i<100
        g12(i)=(ih(i)-ih(i+1))*r2d;
    else
        g12(i)=(ih(i-1)-ih(i))*r2d;    %度/度
    end
end
g12;
%dih=0;
%for i=31:80
%    dih=dih+(ih(i)-ih(i+1));
%end
%dih=abs(dih/50)
for i=1:100
   g_theta(i)=sqrt(g11(i)*abs(g12(i)));
end

figure(2)
plot(d,i0*r2d,'b',d,ih*r2d,'r');
title('P波射线的入射角和离源角随震中距的变化')
xlabel('震中距/度')
ylabel('/度')
legend('入射角','离源角')
%subplot(313)
figure(3)
plot(g12,'b');

% 得到的ih在30-90度与Kanamori and Stewart,1976,图8一致
%得到的ih在10-90度g
figure(4)
plot(g_theta,'b');
title('P波几何扩散因子中的无量纲参数g')
ylabel('g值')
xlabel('震中距/度')
fil='g_delta1190.asc'
fid=fopen(fil,'w')
fprintf(fid,'deg g(deg)\n')
for i=11:90
    fprintf(fid,'%d    %6.3f\n',i,g_theta(i));
end
fclose(fid);
g_theta
figure(5)
deg=[1:90]
plot(deg(11:90),g_theta(11:90))
%平滑
for i=11:90
    a=0;
   for j=1:11 
       a=a+g_theta(i-6+j);
   end
    g_smooth(i)=a/11;
end
%hold on;
figure(6)
plot(deg(11:90),g_smooth(11:90))
fil='g_smooth1190.asc'
fid=fopen(fil,'w')
fprintf(fid,'deg g(deg)\n')
for i=11:90
    fprintf(fid,'%d    %6.3f\n',i,g_smooth(i));
end
fclose(fid);

