%Ms的深度校正（Herak等，2001）
clear all
%ΔMs(h)=0                       当h< 20km
%ΔMs(h)=0.314log(h)-0.409       当20≤h< 60km
%ΔMs(h)=1.351log(h)-2.253       当60≤h< 100km
%ΔMs(h)=0.400log(h)-0.350       当100≤h< 600km
for i=1:599
    h(i)=i;
    if i<600
        dms(i)=0.400*log10(i)-0.350 ;
    end
    if i<100
        dms(i)=1.351*log10(i)-2.253;
    end
    if i<60
        dms(i)=0.314*log10(i)-0.409;
    end
    if i<20
        dms(i)=0;
    end
end
lh=length(h)
ldms=length(dms)
figure(1)

semilogx(h,dms)
xlabel('震源深度/km')
ylabel('震级Ms校正值')
title('震级Ms的深度校正值（据Herak等，2001）') 
figure(2)
plot(h,dms)
xlabel('震源深度/km')
ylabel('震级Ms校正值')
title('震级Ms的深度校正值（据Herak等，2001）')     
            
            
