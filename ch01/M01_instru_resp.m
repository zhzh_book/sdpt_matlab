clear
%----------------DD-1---------------------
%由王广福提供的参数计算DD-1的零极点
D1=0.45;
T1=1.0;
D2=0.707;
T2=0.05;
n1=2*pi/T1;
n2=2*pi/T2;
p1=-n1*D1+n1*sqrt(D1^2-1);
p2=-n1*D1-n1*sqrt(D1^2-1);
p3=-n2*D2+n2*sqrt(D2^2-1);
p4=-n2*D2-n2*sqrt(D2^2-1);
b0=-0.0291;	
b1=0.3279;	
b2=11.8101;	
b3=0.0243;	
a1=1.7287;	
a2=0.8314;	
a3=0.1106;
b=[b3 b2 b1 b0];
a=[a3 a2 a1 1];
p567=roots(a);
z456=roots(b);

%DD-1地震仪的零点和极点（承蒙王洪体提供）
%零点：（0，0，0），（-485.984574650881，-0.0654310375096144，0.037660009378371）
%极点：（-2.82743338823081+5.61106389550815i，-2.82743338823081-5.61106389550815i）,
%      （-4.47713098717191，-2.05943792860324，-0.98061010773298）,
%      (-0.188495559215388+0.37407092636721i, -0.188495559215388-0.37407092636721i)

%Num_zero=6;%位移
%Num_zero=5;%速度
%Num_zero=4;%加速度
nz=[6,5,4];
for Num_zero=nz
    Num_zero
    zero(1)=-485.984574650881+0i;
    zero(2)=-0.0654310375096144+0i;
    zero(3)=0.037660009378371+0i;
    zero(4)=0+0i;
    zero(5)=0+0i;
    zero(6)=0+0i;
    
    Num_pole=7;
    pole(1)=-2.82743338823081+5.61106389550815i;
    pole(2)=-2.82743338823081-5.61106389550815i;
    pole(3)=-4.47713098717191;
    pole(4)=-2.05943792860324;
    pole(5)=-0.98061010773298;
    pole(6)=-88.84+88.84i;
    pole(7)=-88.84-88.84i;

    f0=1;
    om=2*pi*f0;
    om*1i;
    nomi=1;
    nomi=n2^2/106.722
    for i=1:1:Num_zero
        nomi=(om*1i-zero(i))*nomi;
    end
    denomi=1;
    for i=1:1:Num_pole
        denomi=(om*1i-pole(i))*denomi
    end
    nomi;
    denomi;
    trans=nomi/denomi;
    trans_real=real(trans);
    trans_imag=imag(trans);
    trans_amp=sqrt(trans_real*trans_real+trans_imag*trans_imag);
    normal=1/trans_amp;
    %---------------------------
    for k=1:1:5000
        f0=0.02*k;
        om=2*pi*f0;
        om*1i;
        nomi=1;
        nomi=n2^2/106.733;
        for i=1:1:Num_zero
            nomi=(om*1i-zero(i))*nomi;
        end
        denomi=1;
        for i=1:1:Num_pole
            denomi=(om*1i-pole(i))*denomi;
        end
        nomi;
        denomi;
        trans=nomi/denomi;
        trans_real=real(trans);
        trans_imag=imag(trans);
        transf_amp(k)=sqrt(trans_real*trans_real+trans_imag*trans_imag);
        transf_phase(k)=atan(trans_imag/trans_real)*180/pi; %变成以度为单位
    end
    for k=1:1:2
        transf_phase1(k)=transf_phase(k);
    end
    a=0;
    for k=3:1:1500
        transf_phase1(k)=transf_phase(k);
        if transf_phase(k)>transf_phase(k-1)
          a=a+180;
        end
         transf_phase1(k)=transf_phase(k)-a;
    end
    for k=1501:1:5000
         transf_phase1(k)=transf_phase(k)-a;
    end
    for k=1:1:5000
        transf_amp(k)=transf_amp(k)*normal;
    end
    for k=1:1:5000
        x(k)=0.02*k;
    end
    for k=1:1:5000
        xlog(k)=log10(x(k));
    end
    figure
    loglog(x,transf_amp)
    xlabel('f(Hz)')
    ylabel('log10(Amplitude(m/s))')
    title(['DD-1 Seismograph with ',  num2str(Num_zero),  ' zeros and 7 poles, A0=', num2str(normal), ' at 5.0Hz'])
    xlim([.01 100])
    ylim([.1E-8 10000])
end

%----------------JCZ-1---------------------
zero(1)=0+0i;
zero(2)=0+0i;
zero(3)=0+0i;
pole(1)=-0.012342-0.012342i;
pole(2)=-0.012342+0.012342i;
pole(3)=-266.57-266.57i;
pole(4)=-266.57+266.57i;
pole(5)=-333.80-89.44i;
pole(6)=-333.80+89.44i;
pole(7)=-244.36-244.36i;
pole(8)=-244.36+244.36i;
pole(9)=-89.44-333.80i;
pole(10)=-89.44+333.80i;

%Num_zero=1; %加速度
%Num_zero=2; %速度
%Num_zero=3; %位移
nz=[1,2,3]
for Num_zero=nz
    Num_zero
    f00=0.2 %5s
    om=2*pi*f00
    om*1i;
    nomi=1;
    for i=1:1:Num_zero
        nomi=(om*1i-zero(i))*nomi;
    end
    denomi=1;
    for i=1:1:Num_pole
        denomi=(om*1i-pole(i))*denomi;
    end

    trans=nomi/denomi
    trans_real=real(trans);
    trans_imag=imag(trans);
    trans_amp=sqrt(trans_real*trans_real+trans_imag*trans_imag)
    normal=1/trans_amp
    %---------------------------
    for k=1:1:50000
        f0=0.001*k;
        om=2*pi*f0;
        om*1i;
        nomi=1;
        for i=1:1:Num_zero
            nomi=(om*1i-zero(i))*nomi;
        end
        denomi=1;
        for i=1:1:Num_pole
            denomi=(om*1i-pole(i))*denomi;
        end
        nomi;
        denomi;
        trans=nomi/denomi;
        trans_real=real(trans);
        trans_imag=imag(trans);
        transf_amp(k)=sqrt(trans_real*trans_real+trans_imag*trans_imag);
        transf_phase(k)=atan(trans_imag/trans_real)*180/pi; %变成以度为单位
    end
    for k=1:1:4999
        transf_phase1(k)=transf_phase(k);
    end
    a=0;
    for k=5000:1:50000
        transf_phase1(k)=transf_phase(k);
        if transf_phase(k)>transf_phase(k-1)
            a=a+180;
        end
         transf_phase1(k)=transf_phase(k)-a;
    end
    for k=1:1:50000
        transf_amp(k)=transf_amp(k)*normal;
    end
    for k=1:1:50000
        x(k)=0.001*k;
    end
    for k=1:1:50000
        xlog(k)=log10(x(k));
    end
    figure
    loglog(x,transf_amp)
    xlabel('f(Hz)')
    ylabel('log10(Amplitude(m/s))')
    title(['JCZ-1 seismometer with ', num2str(Num_zero), 'zero and 10 poles, A0=', normal, ' at ',0.2,'Hz'])
    xlim([.001 100])
    ylim([.1E-8 10000])
end